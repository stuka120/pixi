package at.binarycheese.pixi.service;

import at.binarycheese.pixi.domain.UsersEntity;
import at.binarycheese.pixi.service.interfaces.UserActionInterface;
import org.springframework.transaction.annotation.Transactional;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Dominik on 28.01.2015.
 */
public interface UsersService {

    UsersEntity findByUserId(int userId);

    public UsersEntity findByUserIdWithPermissions(int userId);

    UsersEntity findByUsername(String username);

    List<UsersEntity> findAll();

    UsersEntity findById(int userId);

    List<UsersEntity> findAllWithPermissions(boolean deactivated);

    String createEntity(UserActionInterface userAction);

    String update(UserActionInterface userAction);

    void toggleDeactivateUser(int id);

    void setPw(UserActionInterface userAction);

    List<UsersEntity> searchForUsers(String search);

    UsersEntity getTestEntity();

    JSONObject searchAllUsers(String query, int page, int size);

    boolean usernameExists(String username);

    int countByAdmins();

    String changePassword(String password);
}
