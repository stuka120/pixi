package at.binarycheese.pixi.service;

import at.binarycheese.pixi.domain.LanguagesEntity;

import java.util.List;

/**
 * Created by Dominik on 29.01.2015.
 */
public interface LanguagesService {
    List<LanguagesEntity> findAll();
}
