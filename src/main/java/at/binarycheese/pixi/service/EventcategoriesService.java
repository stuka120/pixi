package at.binarycheese.pixi.service;

import at.binarycheese.pixi.domain.EventcategoriesEntity;

import java.util.List;

/**
 * Created by Dominik on 29.01.2015.
 */
public interface EventcategoriesService {
    List<EventcategoriesEntity> findAll();

    EventcategoriesEntity findById(int id);

    void update(EventcategoriesEntity a);

}
