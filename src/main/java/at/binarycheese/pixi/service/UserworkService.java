package at.binarycheese.pixi.service;

import at.binarycheese.pixi.domain.ContactsEntity;
import at.binarycheese.pixi.domain.EventsEntity;
import at.binarycheese.pixi.domain.UsersEntity;
import at.binarycheese.pixi.domain.UserworksEntity;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.util.Collection;
import java.util.List;

/**
 * Created by Lukas on 05.02.2015.
 */
public interface UserworkService {
    List<UserworksEntity> findByEvent(int eventid);
    void delete(int id);
    String edit(UserworksEntity entity);
    String create(UserworksEntity entity);
    UserworksEntity find(int id);
    UserworksEntity getCopy(UserworksEntity userworksEntity);
    UserworksEntity getTestEntity(EventsEntity eventsEntity, UsersEntity usersEntity);
    void mergeEvent(UserworksEntity userwork);
    EventsEntity getConflict(UserworksEntity userworksEntity);

}
