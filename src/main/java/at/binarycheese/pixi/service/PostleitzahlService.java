package at.binarycheese.pixi.service;

import at.binarycheese.pixi.domain.PostleitzahlEntity;

import java.util.List;

/**
 * Created by Dominik on 17.03.2015.
 */
public interface PostleitzahlService {

    public List<PostleitzahlEntity> findByPlzLike(int plz);

    public List<PostleitzahlEntity> findByOrtLike(String ort);

}
