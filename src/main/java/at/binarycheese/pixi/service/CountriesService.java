package at.binarycheese.pixi.service;

import at.binarycheese.pixi.domain.CountriesEntity;

import java.util.List;

/**
 * Created by Dominik on 29.01.2015.
 */
public interface CountriesService{
    List<CountriesEntity> findAll();
}
