package at.binarycheese.pixi.service;

import at.binarycheese.pixi.domain.AcademicDegree;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Dominik on 15.03.2015.
 */
public interface AcademicDegreeService {

    List<AcademicDegree> findAcademicDegree(String query);

}
