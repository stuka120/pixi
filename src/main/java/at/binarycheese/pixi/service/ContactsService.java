package at.binarycheese.pixi.service;

import at.binarycheese.pixi.domain.ContactsEntity;
import at.binarycheese.pixi.service.interfaces.ContactActionInterface;
import org.json.JSONObject;
import org.springframework.data.domain.Sort;

import java.text.ParseException;
import java.util.List;

/**
 * Created by Dominik on 29.01.2015.
 */
public interface ContactsService {
    List<ContactsEntity> findAll();

    ContactsEntity findById(int selectedContact);

    void saveEntity(ContactActionInterface contactAction);

    void delete(Integer contactsId);

    void update(ContactActionInterface contactAction);

    List<ContactsEntity> searchByNameAndSurname(String searchString);

    List<ContactsEntity> searchByParams(String queryString, List<String> queryParams, List<String> queryParamsDatatypes, boolean showAll) throws NumberFormatException,ClassCastException,ParseException;

    //ContactsCustomPageing method --> Implementing the Paging with Datatables

    public List<ContactsEntity> findContactsWithPaging(int pagecount, int pagesize, String sortColumn, Sort.Direction direction);

    int getContactsCount();

    public List<ContactsEntity> findFilteredContacts(String search, int pageNumber, int rowCount,String sortColumn, Sort.Direction direction);

    int findFilteredContactsCount(String search);

    void saveContact(ContactsEntity contactsEntity);

    JSONObject searchByNameAndSurnameWithPageable(String query, int page, int size, boolean hasUser);

    ContactsEntity findByUserEntity_Username(String username);

    List<ContactsEntity> findUsersByIDs(List<Integer> ids);
}
