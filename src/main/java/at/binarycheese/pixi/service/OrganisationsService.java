package at.binarycheese.pixi.service;

import at.binarycheese.pixi.domain.ContactsEntity;
import at.binarycheese.pixi.domain.OrganisationsEntity;
import at.binarycheese.pixi.service.interfaces.OrganisationsActionInterface;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Dominik on 29.01.2015.
 */
public interface OrganisationsService {
    List<OrganisationsEntity> findAll();

    int getMemberCount(int id);

    List<ContactsEntity> findUnreferencedContacts();

    @Transactional
    List<ContactsEntity> findUnreferencedContactsBySurnameOrName(String name);

    void saveEntity(OrganisationsActionInterface organisationsAction);

    OrganisationsEntity findById(int selectedOrganisation);

    void update(OrganisationsActionInterface organisationsAction);

    void delete(int id);

    boolean checkOrgNameAvailable(String name);

    void addSelectedContactsToEntity(OrganisationsActionInterface organisationsAction);

    void findAllOrgsAndSetContactCount(OrganisationsActionInterface organisationAction);
}
