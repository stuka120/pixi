package at.binarycheese.pixi.service;

import at.binarycheese.pixi.domain.ProjectEntity;
import at.binarycheese.pixi.service.interfaces.ProjectActionInterface;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Dominik on 30.01.2015.
 */
public interface ProjectsService {
    List<ProjectEntity> findAll();

    boolean containsProjectNumber(String projectnumber);

    ProjectEntity findById(int id);

    String saveOrUpdateEntity(ProjectActionInterface projectAction);

    void delete(int id);

    void nextStage(ProjectActionInterface projectActionInterface);

    void deactivated(int id);

    void reactivate(int id, int projectStagesId);

    List<ProjectEntity> findAllActive();

    List<ProjectEntity> findAllDeactivated();

    JSONObject searchByName(String query, int page, int size);
}
