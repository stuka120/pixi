package at.binarycheese.pixi.service;

import at.binarycheese.pixi.domain.WorkingtimesEntity;
import at.binarycheese.pixi.service.interfaces.WorkingTimeActionInterface;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by Dominik on 28.01.2015.
 */
public interface WorkingTimesService {

    List<WorkingtimesEntity> findAllByUserId(int userId);

    List<Timestamp> findAllDatesByUserId(int userId);

    WorkingtimesEntity findById(int id);

    boolean checkEntryForUserOnDateIsPossible(int id, Timestamp date);

    void delete(int id);

    void save(WorkingtimesEntity entity);

    void update(WorkingtimesEntity workingtimesEntity);

    String createWorkingTimeEntity(WorkingTimeActionInterface workingtimeAction);

    String browse(WorkingTimeActionInterface workingtimeAction);

    int getUsedHolidayUntilMonth(int userId, Timestamp date);

    boolean CheckHolidayCount(WorkingTimeActionInterface workingTimeAction);
}
