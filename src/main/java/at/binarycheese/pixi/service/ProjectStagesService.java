package at.binarycheese.pixi.service;

import at.binarycheese.pixi.domain.ProjectstagesEntity;

import java.util.List;

/**
 * Created by Dominik on 30.01.2015.
 */
public interface ProjectStagesService {
    List<ProjectstagesEntity> findAllActiveStages();
}
