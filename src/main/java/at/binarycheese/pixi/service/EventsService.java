package at.binarycheese.pixi.service;

import at.binarycheese.pixi.domain.EventsEntity;
import at.binarycheese.pixi.domain.UserworksEntity;
import at.binarycheese.pixi.service.interfaces.EventActionInterface;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by Dominik on 29.01.2015.
 */
public interface EventsService {

    EventsEntity testEntity();

    String saveEntity(EventActionInterface eventAction);

    EventsEntity findById(int eventId);

    String update(EventActionInterface eventAction);

    void delete(int id);

    List<EventsEntity> findAll();

    List<EventsEntity> findByStartDateAndEndDateAndEventcategoriesByEventCategorieId_IdIn(Timestamp startDate, Timestamp endDate, List<Integer> id);

    String importXmlFromGoogle(EventActionInterface eventAction);

    EventsEntity findByIdWithUserworks(int eventId);

    List<EventsEntity> getResponsibleEvents(String username);

    void setChecklistShown(int eventId);

    List<EventsEntity> findAllByProjectId(int projectId);
}
