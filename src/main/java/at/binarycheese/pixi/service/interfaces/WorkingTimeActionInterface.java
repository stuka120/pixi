package at.binarycheese.pixi.service.interfaces;

import at.binarycheese.pixi.domain.UsersEntity;
import at.binarycheese.pixi.domain.WorkingtimesEntity;
import at.binarycheese.pixi.service.UsersService;
import at.binarycheese.pixi.service.WorkingTimesService;

import java.io.InputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Dominik on 29.01.2015.
 */
public interface WorkingTimeActionInterface {

    public String checkHoliday(int year, int monat, int tag) ;

    public InputStream getFileInputStream();

    public void setFileInputStream(InputStream fileInputStream);

    public List<WorkingtimesEntity> getWorkingtimesList();

    public void setWorkingtimesList(List<WorkingtimesEntity> workingtimesList);

    public WorkingtimesEntity getWorkingtimesEntity();

    public void setWorkingtimesEntity(WorkingtimesEntity workingtimesEntity);

    public Timestamp getDate();

    public void setDate(Timestamp date);

    public String getStarttime();

    public void setStarttime(String starttime);

    public String getEndtime();

    public void setEndtime(String endtime);

    public int getId();

    public void setId(int id);

    public String getInDate();

    public void setInDate(String inDate);

    public boolean isHoliday();

    public void setHoliday(boolean holiday);

    public Timestamp getEndHolidayDate();

    public void setEndHolidayDate(Timestamp endHolidayDate);

    public String getInEndHolidayDate();

    public void setInEndHolidayDate(String inEndHolidayDate);

    public Map<String, ArrayList<Timestamp>> getMenuDates();

    public void setMenuDates(Map<String, ArrayList<Timestamp>> menuDates);

    public List<Timestamp> getSelectedDates();

    public void setSelectedDates(List<Timestamp> selectedDates);

    public int getSelectedUserForExport();

    public void setSelectedUserForExport(int selectedUserForExport);

    public List<UsersEntity> getUsersEntityList();

    public void setUsersEntityList(List<UsersEntity> usersEntityList);

    public UsersService getUsersService();

    public void setUsersService(UsersService usersService);

    public WorkingTimesService getWorkingTimesService();

    public void setWorkingTimesService(WorkingTimesService workingTimesService);

}
