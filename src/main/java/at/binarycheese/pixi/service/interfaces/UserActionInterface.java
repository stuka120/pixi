package at.binarycheese.pixi.service.interfaces;

import at.binarycheese.pixi.domain.ContactsEntity;
import at.binarycheese.pixi.domain.RolesEntity;
import at.binarycheese.pixi.domain.UsersEntity;
import at.binarycheese.pixi.service.ContactsService;
import at.binarycheese.pixi.service.RolesService;
import at.binarycheese.pixi.service.UsersService;
import org.springframework.security.core.context.SecurityContext;

import java.util.List;

/**
 * Created by Dominik on 30.01.2015.
 */
public interface UserActionInterface {

    public List<UsersEntity> getUserList();

    public void setUserList(List<UsersEntity> userList);

    public String getUsername();

    public void setUsername(String username);

    public String getPassword();

    public void setPassword(String password);

    public String getConfirmPW();

    public void setConfirmPW(String confirmPW);

    public UsersEntity getUserEntity();

    public void setUserEntity(UsersEntity usersEntity);

    public int getSelectedUser();

    public void setSelectedUser(int selectedUser);

    public List<RolesEntity> getRolesEntityList();

    public void setRolesEntityList(List<RolesEntity> rolesEntityList);

    public List<String> getSelectedRoles();

    public void setSelectedRoles(List<String> selectedRoles);

    public List<Integer> getPreSelectedRoles();

    public int getPreSelectedContact();

    public void setPreSelectedContact(int preSelectedContact);

    public List<ContactsEntity> getContactsEntityList();

    public void setContactsEntityList(List<ContactsEntity> contactsEntityList);

    public int getSelectedContact();

    public void setSelectedContact(int selectedContact);

    public String getOldPW();

    public void setOldPW(String oldPW);

    public String getNewPW();

    public void setNewPW(String newPW);

    public String getNameBenutzer();

    public void setNameBenutzer(String nameBenutzer);

    public UsersService getUsersService();

    public void setUsersService(UsersService usersService);

    public ContactsService getContactsService();

    public void setContactsService(ContactsService contactsService);

    public RolesService getRolesService();

    public void setRolesService(RolesService rolesService);

    boolean hasRole(String role);

    boolean isNewPasswort();
}
