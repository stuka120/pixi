package at.binarycheese.pixi.service.interfaces;

import at.binarycheese.pixi.domain.*;
import at.binarycheese.pixi.service.ContactsService;
import at.binarycheese.pixi.service.CountriesService;
import at.binarycheese.pixi.service.LanguagesService;
import at.binarycheese.pixi.service.OrganisationsService;
import java.util.List;

/**
 * Created by Dominik on 30.01.2015.
 */
public interface ContactActionInterface {

    public ContactsEntity getContactsEntity();

    public void setContactsEntity(ContactsEntity contactsEntity);

    public List<ContactsEntity> getContactsEntityList();

    public void setContactsEntityList(List<ContactsEntity> contactsEntityList);

    public List<CountriesEntity> getCountriesEntitiesList();

    public void setCountriesEntitiesList(List<CountriesEntity> countriesEntitiesList);

    public String getSelectedCountry();

    public void setSelectedCountry(String selectedCountry);

    public List<OrganisationsEntity> getOrganisationsEntityList();

    public void setOrganisationsEntityList(List<OrganisationsEntity> organisationsEntityList);

    public int getSelectedOrganisation();

    public void setSelectedOrganisation(int selectedOrganisation);

    public int getSelectedContact();

    public void setSelectedContact(int selectedContact);

    public List<LanguagesEntity> getLanguagesEntityList();

    public void setLanguagesEntityList(List<LanguagesEntity> languagesEntityList);
    public List<String> getSelectedLanguages();

    public void setSelectedLanguages(List<String> selectedLanguages);

    public String getSelectedPhones();

    public void setSelectedPhones(String selectedPhones);

    public String getSelectedEmails();

    public void setSelectedEmails(String selectedEmails);

    public String getSelectedNewsletter();

    public void setSelectedNewsletter(String selectedNewsletter);

    public List<Integer> getPreSelectedLanguages();

    public void setPreSelectedLanguages(List<Integer> preSelectedLanguages);

    public String getPreSelectedPhones();

    public void setPreSelectedPhones(String preSelectedPhones);

    public String getPreSelectedEmails();

    public void setPreSelectedEmails(String preSelectedEmails);

    public List<String> getPreSelectedCountry();

    public void setPreSelectedCountry(List<String> preSelectedCountry);

    public List<Integer> getPreSelectedOrganisation();

    public void setPreSelectedOrganisation(List<Integer> preSelectedOrganisation);

    public String getAddName();

    public void setAddName(String addName);

    public String getAddValue();

    public void setAddValue(String addValue);

    public LanguagesService getLanguagesService();

    public void setLanguagesService(LanguagesService languagesService);

    public OrganisationsService getOrganisationsService();

    public void setOrganisationsService(OrganisationsService organisationsService);

    public CountriesService getCountriesService();

    public void setCountriesService(CountriesService countriesService);

    public ContactsService getContactsService();

    public void setContactsService(ContactsService contactsService);

    public List<AdditionalinformationEntity> getSelectedAdditionalinformationEntityList();

    public void setSelectedAdditionalinformationEntityList(List<AdditionalinformationEntity> selectedAdditionalinformationEntityList);

    public String getSelectedEmailsPrivate();

    public void setSelectedEmailsPrivate(String selectedEmailsPrivate);

    public String getPreSelectedEmailsPrivate();

    public void setPreSelectedEmailsPrivate(String preSelectedEmailsPrivate);

    public String getSelectedPhonesPrivate();

    public void setSelectedPhonesPrivate(String selectedPhonesPrivate);

    public String getPreSelectedPhonesPrivate();

    public void setPreSelectedPhonesPrivate(String preSelectedPhonesPrivate);
}
