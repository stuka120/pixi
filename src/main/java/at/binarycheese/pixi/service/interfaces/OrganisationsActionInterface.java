package at.binarycheese.pixi.service.interfaces;

import at.binarycheese.pixi.domain.ContactsEntity;
import at.binarycheese.pixi.domain.OrganisationsEntity;
import at.binarycheese.pixi.service.ContactsService;
import at.binarycheese.pixi.service.OrganisationsService;

import java.util.List;

/**
 * Created by Dominik on 30.01.2015.
 */
public interface OrganisationsActionInterface {

    public OrganisationsEntity getOrganisationsEntity();

    public void setOrganisationsEntity(OrganisationsEntity organisationsEntity);

    public List<OrganisationsEntity> getOrganisationsEntityList();

    public void setOrganisationsEntityList(List<OrganisationsEntity> organisationsEntityList);

    public int getSelectedOrganisation();

    public void setSelectedOrganisation(int selectedOrganisation);

    public List<ContactsEntity> getContactsEntitiesList();

    public void setContactsEntitiesList(List<ContactsEntity> contactsEntitiesList);

    public String getSelectedContacts();

    public void setSelectedContacts(String selectedContacts);

    public List<Integer> getPreSelectedContacts();

    public void setPreSelectedContacts(List<Integer> preSelectedContacts);

    public OrganisationsService getOrganisationsService();

    public void setOrganisationsService(OrganisationsService organisationsService);

    public ContactsService getContactsService();

    public void setContactsService(ContactsService contactsService);
}
