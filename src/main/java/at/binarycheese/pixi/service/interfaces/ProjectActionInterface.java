package at.binarycheese.pixi.service.interfaces;

import at.binarycheese.pixi.domain.OrganisationsEntity;
import at.binarycheese.pixi.domain.ProjectEntity;
import at.binarycheese.pixi.service.OrganisationsService;
import at.binarycheese.pixi.service.ProjectStagesService;
import at.binarycheese.pixi.service.ProjectsService;

import java.io.File;
import java.io.InputStream;
import java.util.List;

/**
 * Created by Dominik on 31.01.2015.
 */
public interface ProjectActionInterface {

    public List<ProjectEntity> getProjectEntityList();

    public void setProjectEntityList(List<ProjectEntity> projectEntityList);

    public ProjectEntity getProjectEntity();

    public void setProjectEntity(ProjectEntity projectEntity);

    public List<OrganisationsEntity> getOrganisationsEntityList();

    public void setOrganisationsEntityList(List<OrganisationsEntity> organisationsEntityList);

    public String getOrgId();

    public void setOrgId(String orgId);

    public int getId();

    public void setId(int id);

    public int getProjectStagesId();

    public void setProjectStagesId(int projectStagesId);

    public File getFile();

    public void setFile(File file);

    public String getFileContentType();

    public void setFileContentType(String fileContentType);

    public String getFileFileName();

    public void setFileFileName(String fileFileName);

    public List<File> getFileList();

    public void setFileList(List<File> fileList);

    public String getDocumentName();

    public void setDocumentName(String documentName);
    public InputStream getFileInputStream();

    public void setFileInputStream(InputStream fileInputStream);

    public List<File> getTemplateList();

    public void setTemplateList(List<File> templateList);

    public OrganisationsService getOrganisationsService();

    public void setOrganisationsService(OrganisationsService organisationsService);

    public ProjectStagesService getProjectStagesService();

    public void setProjectStagesService(ProjectStagesService projectStagesService);
    public ProjectsService getProjectsService();

    public void setProjectsService(ProjectsService projectsService);

    public String getDestPath();

    public void setDestPath(String destPath);
}
