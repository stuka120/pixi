package at.binarycheese.pixi.service.interfaces;

import at.binarycheese.pixi.domain.ContactsEntity;
import at.binarycheese.pixi.domain.EventcategoriesEntity;
import at.binarycheese.pixi.domain.EventsEntity;
import at.binarycheese.pixi.service.ContactsService;
import at.binarycheese.pixi.service.EventcategoriesService;
import at.binarycheese.pixi.service.EventsService;

import java.io.File;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

/**
 * Created by Dominik on 29.01.2015.
 */
public interface EventActionInterface {
    public File getUploadedFile();

    public void setUploadedFile(File uploadedFile);

    public int getSelectedCategorie();

    public void setSelectedCategorie(int selectedCategorie);

    public Object getModel();

    public void setEvent(EventsEntity event);

    public EventsEntity getEvent();

    public void setEventList(List<EventsEntity> eventList);

    public List<EventsEntity> getEventList();

    public List<EventcategoriesEntity> getCategoriesList();

    public void setCategoriesList(List<EventcategoriesEntity> categoriesList);

    public int getEventId();

    public void setEventId(int eventId);

    public String getStartDate();

    public List<ContactsEntity> getContactsList();

    public void setContactsList(List<ContactsEntity> contactsList);

    public int getEventCategory();

    public void setEventCategory(int eventCategory);

    public int getEventContactPerson();

    public void setEventContactPerson(int eventContactPerson);

    public String getMessage();

    public void setMessage(String message);

    public int getAmountWrong();

    public void setAmountWrong(int amountWrong);

    public EventcategoriesEntity getEventcategorie();

    public void setEventcategorie(EventcategoriesEntity eventcategorie);

    public InputStream getFileInputStream();

    public void setExportMonth(String monthyear) ;

    public Map<String, String> getExportmonths();

    public String getFileName();

    public List getEventMediaJson();

    public InputStream getDownloadEventMedia();

    public void setFilePath(String path);

    public String getFilename();

    public void setFilename(String filename);

    public void setFileContentType(String contentType);

    public void setFileFileName(String filename);

    public void setFile(File fileUpload);

    public EventcategoriesService getEventcategoriesService();

    public void setEventcategoriesService(EventcategoriesService eventcategoriesService);

    public ContactsService getContactsService();

    public void setContactsService(ContactsService contactsService);

    public EventsService getEventsService();

    public void setEventsService(EventsService eventsService);

    public List<EventsEntity> readFromIcsAndReturnEventEntities();

    public void addFieldError(String fieldName, String errorMessage);

    public String getText(String text);

}
