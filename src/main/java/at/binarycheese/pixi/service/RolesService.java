package at.binarycheese.pixi.service;

import at.binarycheese.pixi.domain.RolesEntity;

import java.util.List;

/**
 * Created by Dominik on 29.01.2015.
 */
public interface RolesService {
    List<RolesEntity> findAllWithoutNewUser();

    List<RolesEntity> findAll();

    RolesEntity findById(int i);
}
