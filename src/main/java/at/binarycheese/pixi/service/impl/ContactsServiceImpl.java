package at.binarycheese.pixi.service.impl;

import at.binarycheese.pixi.domain.*;
import at.binarycheese.pixi.repositories.*;
import at.binarycheese.pixi.service.ContactsService;
import at.binarycheese.pixi.service.interfaces.ContactActionInterface;
import com.google.common.collect.Lists;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.*;

/**
 * Created by Dominik on 29.01.2015.
 */

@Service
public class ContactsServiceImpl implements ContactsService {

    @Autowired
    ContactsRepository contactsRepository;
    @Autowired
    OrganisationsRepository organisationsRepository;
    @Autowired
    CountriesRepository countriesRepository;
    @Autowired
    LanguagesRepository languagesRepository;
    @Autowired
    ContactFilterRepository contactFilterRepository;
    @Autowired
    PostleitzahlRepository postleitzahlRepository;

    @Transactional(readOnly = true)
    @Override
    public List<ContactsEntity> findAll() {
        return Lists.newArrayList(contactsRepository.findAll());
    }

    @Transactional(readOnly = true)
    @Override
    public ContactsEntity findById(int selectedContact) {
        ContactsEntity contactsEntity = contactsRepository.findDistinctById(selectedContact);
        if (contactsEntity != null) {
            contactsEntity.getSpeaksesById().size();
        }

        return contactsEntity;
    }

    @Transactional
    @Override
    public void saveEntity(ContactActionInterface contactAction) {
        contactAction.getContactsEntity().setNewsletter(contactAction.getSelectedNewsletter() == null ? false : true);

        //Many To One Relationships
        //check if the default value ("") is selected --> no organisation got selected to the contact!
        //The IF is just necessary, if you have a nullable value!!!
        if (contactAction.getSelectedOrganisation() != 0) {
            contactAction.getContactsEntity().setOrganisationsByOrganisationsId(organisationsRepository.findOne(contactAction.getSelectedOrganisation()));
        }

        //OneToMany Relationships
        if (!contactAction.getSelectedPhones().isEmpty()) {
            List<String> phones = Arrays.asList(contactAction.getSelectedPhones().split(","));

            for (String phone : phones) {
                TelnumbersEntity contactaddressesEntity = new TelnumbersEntity();
                contactaddressesEntity.setImportant(true);
                contactaddressesEntity.setValue(phone);
                contactaddressesEntity.setContact(contactAction.getContactsEntity());
                contactAction.getContactsEntity().getTelnumbers().add(contactaddressesEntity);
            }
        }
        if (!contactAction.getSelectedPhonesPrivate().isEmpty()) {
            List<String> phones = Arrays.asList(contactAction.getSelectedPhonesPrivate().split(","));

            for (String phone : phones) {
                TelnumbersEntity contactaddressesEntity = new TelnumbersEntity();
                contactaddressesEntity.setImportant(false);
                contactaddressesEntity.setValue(phone);
                contactaddressesEntity.setContact(contactAction.getContactsEntity());
                contactAction.getContactsEntity().getTelnumbers().add(contactaddressesEntity);
            }
        }
        if (!contactAction.getSelectedEmails().isEmpty()) {
            List<String> emails = Arrays.asList(contactAction.getSelectedEmails().split(","));

            for (String email : emails) {
                EmailsEntity contactaddressesEntity = new EmailsEntity();
                contactaddressesEntity.setImportant(true);
                contactaddressesEntity.setValue(email);
                contactaddressesEntity.setContact(contactAction.getContactsEntity());
                contactAction.getContactsEntity().getEmails().add(contactaddressesEntity);
            }
        }
        if (!contactAction.getSelectedEmailsPrivate().isEmpty()) {
            List<String> emailsPrivate = Arrays.asList(contactAction.getSelectedEmailsPrivate().split(","));

            for (String email : emailsPrivate) {
                EmailsEntity contactaddressesEntity = new EmailsEntity();
                contactaddressesEntity.setImportant(false);
                contactaddressesEntity.setValue(email);
                contactaddressesEntity.setContact(contactAction.getContactsEntity());
                contactAction.getContactsEntity().getEmails().add(contactaddressesEntity);
            }
        }
        if (!contactAction.getSelectedCountry().isEmpty()) {
            contactAction.getContactsEntity().setCountriesByCountriesId(countriesRepository.findOne(contactAction.getSelectedCountry()));
        }

        if (contactAction.getSelectedAdditionalinformationEntityList().size() != 0) {
            for (AdditionalinformationEntity additionalinformationEntity : contactAction.getSelectedAdditionalinformationEntityList()) {
                additionalinformationEntity.setContactsByContactId(contactAction.getContactsEntity());
                additionalinformationEntity.setName(additionalinformationEntity.getName().trim());
                additionalinformationEntity.setValue(additionalinformationEntity.getValue().trim());
                contactAction.getContactsEntity().getAdditionalinformationsById().add(additionalinformationEntity);
            }
        }

        //Many To Many Relationships
        if (contactAction.getSelectedLanguages().size() != 0) {
            for (int i = 0; i < contactAction.getSelectedLanguages().size(); i++) {
                SpeaksEntity speaksEntity = new SpeaksEntity();
                speaksEntity.setContactsByContactsPersonId(contactAction.getContactsEntity());
                speaksEntity.setLanguagesByLanguagesId(languagesRepository.findOne(Integer.parseInt(contactAction.getSelectedLanguages().get(i))));
                contactAction.getContactsEntity().getSpeaksesById().add(speaksEntity);
            }
        }

        if (postleitzahlRepository.findByPlz(contactAction.getContactsEntity().getPostleitzahlByPlz().getPlz()) == null) {
            PostleitzahlEntity postleitzahlEntity = new PostleitzahlEntity();
            postleitzahlEntity.setPlz(contactAction.getContactsEntity().getPostleitzahlByPlz().getPlz());
            postleitzahlEntity.setName(contactAction.getContactsEntity().getPostleitzahlByPlz().getName());
            postleitzahlEntity.setBundesland(contactAction.getContactsEntity().getState());
            postleitzahlRepository.save(postleitzahlEntity);
        }
        contactAction.getContactsEntity().setPostleitzahlByPlz(postleitzahlRepository.findByPlz(contactAction.getContactsEntity().getPostleitzahlByPlz().getPlz()));
        //Finally set the createDate to the current Time

        Calendar calendar = Calendar.getInstance();
        java.util.Date now = calendar.getTime();
        contactAction.getContactsEntity().setCreateDate(new Timestamp(now.getTime()));

        try {
            contactsRepository.save(contactAction.getContactsEntity());
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
    }

    @Override
    @Transactional
    public void delete(Integer contactsId) {

        contactsRepository.deleteById(contactsId);
    }

    @Override
    @Transactional
    public void update(ContactActionInterface contactAction) {

        contactAction.getContactsEntity().setNewsletter(contactAction.getSelectedNewsletter() == null ? false : true);

        //Then I remove all the old Many To Many and One to Many Realtionships and push a commit to the Database.
        //Thats important, so that there a not longer some old constrains, that cause these strange Hibernate Errors
        contactAction.getContactsEntity().getSpeaksesById().clear();
        contactAction.getContactsEntity().getTelnumbers().clear();
        contactAction.getContactsEntity().getEmails().clear();
        contactAction.getContactsEntity().getAdditionalinformationsById().clear();

        //ManyToOne realtionships first
        if (!contactAction.getSelectedCountry().isEmpty()) {
            contactAction.getContactsEntity().setCountriesByCountriesId(countriesRepository.findOne(contactAction.getSelectedCountry()));
        }

        if (contactAction.getSelectedOrganisation() != 0) {
            contactAction.getContactsEntity().setOrganisationsByOrganisationsId(organisationsRepository.findOne(contactAction.getSelectedOrganisation()));
        }

        //After writing the empty Relationships to the Database, I need to load the new Relationships into the empty Arrays
        if (contactAction.getSelectedLanguages().size() != 0) {
            for (int i = 0; i < contactAction.getSelectedLanguages().size(); i++) {
                SpeaksEntity speaksEntity = new SpeaksEntity();
                speaksEntity.setContactsByContactsPersonId(contactAction.getContactsEntity());
                speaksEntity.setLanguagesByLanguagesId(languagesRepository.findOne(Integer.parseInt(contactAction.getSelectedLanguages().get(i))));
                contactAction.getContactsEntity().getSpeaksesById().add(speaksEntity);
            }
        }

        if (contactAction.getSelectedAdditionalinformationEntityList().size() != 0) {
            for (AdditionalinformationEntity additionalinformationEntity : contactAction.getSelectedAdditionalinformationEntityList()) {
                additionalinformationEntity.setContactsByContactId(contactAction.getContactsEntity());
                additionalinformationEntity.setName(additionalinformationEntity.getName().trim());
                additionalinformationEntity.setValue(additionalinformationEntity.getValue().trim());
                contactAction.getContactsEntity().getAdditionalinformationsById().add(additionalinformationEntity);
            }
        }

        if (!contactAction.getSelectedPhones().isEmpty()) {
            List<String> phones = Arrays.asList(contactAction.getSelectedPhones().split(","));

            for (String phone : phones) {
                TelnumbersEntity contactaddressesEntity = new TelnumbersEntity();
                contactaddressesEntity.setImportant(true);
                contactaddressesEntity.setValue(phone);
                contactaddressesEntity.setContact(contactAction.getContactsEntity());
                contactAction.getContactsEntity().getTelnumbers().add(contactaddressesEntity);
            }
        }

        if (!contactAction.getSelectedPhonesPrivate().isEmpty()) {
            List<String> phones = Arrays.asList(contactAction.getSelectedPhonesPrivate().split(","));

            for (String phone : phones) {
                TelnumbersEntity contactaddressesEntity = new TelnumbersEntity();
                contactaddressesEntity.setImportant(false);
                contactaddressesEntity.setValue(phone);
                contactaddressesEntity.setContact(contactAction.getContactsEntity());
                contactAction.getContactsEntity().getTelnumbers().add(contactaddressesEntity);
            }
        }

        if (!contactAction.getSelectedEmails().isEmpty()) {
            List<String> emails = Arrays.asList(contactAction.getSelectedEmails().split(","));

            for (String email : emails) {
                EmailsEntity contactaddressesEntity = new EmailsEntity();
                contactaddressesEntity.setImportant(true);
                contactaddressesEntity.setValue(email);
                contactaddressesEntity.setContact(contactAction.getContactsEntity());
                contactAction.getContactsEntity().getEmails().add(contactaddressesEntity);
            }
        }

        if (!contactAction.getSelectedEmailsPrivate().isEmpty()) {
            List<String> emailsPrivate = Arrays.asList(contactAction.getSelectedEmailsPrivate().split(","));

            for (String email : emailsPrivate) {
                EmailsEntity contactaddressesEntity = new EmailsEntity();
                contactaddressesEntity.setImportant(false);
                contactaddressesEntity.setValue(email);
                contactaddressesEntity.setContact(contactAction.getContactsEntity());
                contactAction.getContactsEntity().getEmails().add(contactaddressesEntity);
            }
        }

        if (postleitzahlRepository.findByPlz(contactAction.getContactsEntity().getPostleitzahlByPlz().getPlz()) == null) {
            PostleitzahlEntity postleitzahlEntity = new PostleitzahlEntity();
            postleitzahlEntity.setPlz(contactAction.getContactsEntity().getPostleitzahlByPlz().getPlz());
            postleitzahlEntity.setName(contactAction.getContactsEntity().getPostleitzahlByPlz().getName());
            postleitzahlEntity.setBundesland(contactAction.getContactsEntity().getState());
            postleitzahlRepository.save(postleitzahlEntity);
        }
        contactAction.getContactsEntity().setPostleitzahlByPlz(postleitzahlRepository.findByPlz(contactAction.getContactsEntity().getPostleitzahlByPlz().getPlz()));

        //After I have filled up the content, I can update the Relationsips with the update Method of the dao.

        Calendar calendar = Calendar.getInstance();
        java.util.Date now = calendar.getTime();
        contactAction.getContactsEntity().setUpdateDate(new Timestamp(now.getTime()));

        try {
            contactsRepository.save(contactAction.getContactsEntity());
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<ContactsEntity> searchByNameAndSurname(String searchString) {
        return contactsRepository.findByNameLikeOrSurnameLikeAllIgnoreCase(searchString, searchString);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ContactsEntity> searchByParams(String queryString, List<String> queryParams, List<String> queryParamsDatatypes, boolean showAll) throws NumberFormatException, ClassCastException, ParseException {
        try {
            return contactFilterRepository.searchByParams(queryString, queryParams, queryParamsDatatypes, showAll);
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<ContactsEntity> findContactsWithPaging(int pagecount, int pagesize, final String sortColumn, Sort.Direction direction) {
        if (sortColumn.equalsIgnoreCase("Emails_value"))
            return contactsRepository.findDistinctBy(new PageRequest(pagecount, pagesize, new Sort(new Sort.Order(Sort.Direction.DESC, "emails_important"), new Sort.Order(direction, sortColumn, Sort.NullHandling.NULLS_LAST)))).getContent();
        else if (sortColumn.equalsIgnoreCase("Telnumbers_value"))
            return contactsRepository.findDistinctBy(new PageRequest(pagecount, pagesize, new Sort(new Sort.Order(Sort.Direction.DESC, "telnumbers_important"), new Sort.Order(direction, sortColumn, Sort.NullHandling.NULLS_LAST)))).getContent();
        else
            return contactsRepository.findDistinctBy(new PageRequest(pagecount, pagesize, new Sort(new Sort.Order(direction, sortColumn, Sort.NullHandling.NULLS_LAST)))).getContent();
    }

    @Override
    @Transactional(readOnly = true)
    public int getContactsCount() {
        return (int) contactsRepository.count();
    }

    @Override
    @Transactional(readOnly = true)
    public List<ContactsEntity> findFilteredContacts(String search, int pageNumber, int rowCount, String sortColumn, Sort.Direction direction) {
        return contactsRepository.findDistinctByNameLikeOrSurnameLikeOrOrganisationsByOrganisationsId_NameLikeOrEmails_ValueLikeOrTelnumbers_ValueLikeAllIgnoreCase(search, search, search, search, search, new PageRequest(pageNumber, rowCount, new Sort(direction, sortColumn)));
    }

    @Override
    @Transactional(readOnly = true)
    public int findFilteredContactsCount(String search) {
        return contactsRepository.countDistinctByNameLikeOrSurnameLikeOrOrganisationsByOrganisationsId_NameLikeOrEmails_ValueLikeOrTelnumbers_ValueLikeAllIgnoreCase(search, search, search, search, search);
    }

    @Override
    public void saveContact(ContactsEntity contactsEntity) {
        if (contactsEntity != null) {
            contactsRepository.save(contactsEntity);
        }
    }

    @Override
    public JSONObject searchByNameAndSurnameWithPageable(String query, int page, int size, boolean hasUser) {
        String[] queryArray = query.trim().split(" ");
        String name = "%%";
        String surname = "%%";
        if (queryArray.length == 2) {
            name = "%" + queryArray[0] + "%";
            surname = "%" + queryArray[1] + "%";
        } else if (queryArray.length == 1) {
            name = "%" + queryArray[0] + "%";
        }
        query = "%" + query + "%";
        List<ContactsEntity> a = contactsRepository.findByNameLikeAndSurnameLikeOrNameLikeAndSurnameLikeAllIgnoreCase(name, surname, surname, name, new PageRequest(page, size)).getContent();
        Map<Integer, String> list = new HashMap<>();
        for (ContactsEntity item : a) {
            if (hasUser) {
                if (item.getUsersesById().size() == 0) {
                    list.put(item.getId(), item.getSurname() + " " + item.getName());
                }
            } else {
                list.put(item.getId(), item.getSurname() + " " + item.getName());
            }
        }
        JSONObject obj = new JSONObject();
        obj.put("items", list);
        obj.put("total_count", contactsRepository.countByNameLikeOrSurnameLikeAllIgnoreCase(query, query));
        return obj;
    }

    @Override
    public ContactsEntity findByUserEntity_Username(String username) {
        return contactsRepository.findByUsersesById_Username(username);
    }

    @Override
    @Transactional
    public List<ContactsEntity> findUsersByIDs(List<Integer> ids){
        if(ids == null){
            return new ArrayList<>();
        }
        List<ContactsEntity> contactsEntities = contactsRepository.findAllByIdIn(ids);
        for(ContactsEntity contactsEntity : contactsEntities){
            contactsEntity.getSpeaksesById().size();
        }
        return contactsEntities;
    }
}
