package at.binarycheese.pixi.service.impl;

import at.binarycheese.pixi.domain.RolesEntity;
import at.binarycheese.pixi.repositories.RolesRepository;
import at.binarycheese.pixi.service.RolesService;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Dominik on 29.01.2015.
 */

@Service
public class RolesServiceImpl implements RolesService {

    @Autowired
    RolesRepository rolesRepository;

    @Override
    @Transactional(readOnly = true)
    public List<RolesEntity> findAllWithoutNewUser() {
        return rolesRepository.findByNameNotIn(new String[]{RolesEntity.NewUser, RolesEntity.Employee});
    }

    @Override
    @Transactional(readOnly = true)
    public List<RolesEntity> findAll() {
        return Lists.newArrayList(rolesRepository.findAll());
    }

    @Override
    @Transactional(readOnly = true)
    public RolesEntity findById(int i) {
        return rolesRepository.findOne(i);
    }
}
