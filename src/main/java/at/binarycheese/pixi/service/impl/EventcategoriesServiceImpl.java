package at.binarycheese.pixi.service.impl;

import at.binarycheese.pixi.domain.EventcategoriesEntity;
import at.binarycheese.pixi.repositories.EventcategoriesRepository;
import at.binarycheese.pixi.service.EventcategoriesService;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Dominik on 29.01.2015.
 */

@Service
public class EventcategoriesServiceImpl implements EventcategoriesService {

    @Autowired
    EventcategoriesRepository eventcategoriesRepository;

    @Override
    @Transactional(readOnly = true)
    public List<EventcategoriesEntity> findAll() {
        return Lists.newArrayList(eventcategoriesRepository.findAll());
    }

    @Override
    @Transactional(readOnly = true)
    public EventcategoriesEntity findById(int id) {
        return eventcategoriesRepository.findOne(id);
    }

    @Override
    @Transactional
    public void update(EventcategoriesEntity a) {
        eventcategoriesRepository.save(a);
    }
}
