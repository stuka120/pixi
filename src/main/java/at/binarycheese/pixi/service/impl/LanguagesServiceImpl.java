package at.binarycheese.pixi.service.impl;

import at.binarycheese.pixi.domain.LanguagesEntity;
import at.binarycheese.pixi.repositories.LanguagesRepository;
import at.binarycheese.pixi.service.LanguagesService;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Dominik on 29.01.2015.
 */

@Service
public class LanguagesServiceImpl implements LanguagesService {

    @Autowired
    LanguagesRepository languagesRepository;

    @Override
    @Transactional(readOnly = true)
    public List<LanguagesEntity> findAll() {
        return Lists.newArrayList(languagesRepository.findAll());
    }
}
