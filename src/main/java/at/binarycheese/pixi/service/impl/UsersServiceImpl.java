package at.binarycheese.pixi.service.impl;

import at.binarycheese.pixi.config.spring.MyUser;
import at.binarycheese.pixi.config.spring.MyUserDetailsService;
import at.binarycheese.pixi.domain.ContactsEntity;
import at.binarycheese.pixi.domain.RolesEntity;
import at.binarycheese.pixi.domain.UsersEntity;
import at.binarycheese.pixi.repositories.ContactsRepository;
import at.binarycheese.pixi.repositories.RolesRepository;
import at.binarycheese.pixi.repositories.UsersRepository;
import at.binarycheese.pixi.service.UsersService;
import at.binarycheese.pixi.service.interfaces.UserActionInterface;
import com.google.common.collect.Lists;
import com.opensymphony.xwork2.Action;
import org.apache.struts2.ServletActionContext;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.*;

/**
 * Created by Dominik on 28.01.2015.
 */
@Service
public class UsersServiceImpl implements UsersService {

    @Autowired
    UsersRepository usersRepository;
    @Autowired
    ContactsRepository contactsRepository;
    @Autowired
    RolesRepository rolesRepository;

    @Override
    @Transactional(readOnly = true)
    public UsersEntity findByUserId(int userId) {
        return usersRepository.findOne(new Integer(userId));
    }

    @Override
    @Transactional(readOnly = true)
    public UsersEntity findByUserIdWithPermissions(int userId) {
        UsersEntity usersEntity = usersRepository.findOne(userId);
        if (usersEntity.getRoles() != null) {
            usersEntity.getRoles().size();
        }

        return usersEntity;
    }

    @Override
    @Transactional(readOnly = true)
    public int countByAdmins() {
        return rolesRepository.countRoles(RolesEntity.Admin);
    }

    @Override
    @Transactional(readOnly = true)
    public UsersEntity findByUsername(String username) {
        return usersRepository.findByUsername(username);
    }

    @Override
    @Transactional(readOnly = true)
    public List<UsersEntity> findAll() {
        return Lists.newArrayList(usersRepository.findAll());
    }

    @Override
    @Transactional(readOnly = true)
    public List<UsersEntity> findAllWithPermissions(boolean deactivated) {
        Iterable<UsersEntity> usersEntityIterable = usersRepository.findByDeactivated(deactivated);
        for (Iterator<UsersEntity> iterator = usersEntityIterable.iterator(); iterator.hasNext(); ) {
            UsersEntity usersEntity = iterator.next();
            usersEntity.getRoles().size();
        }
        return Lists.newArrayList(usersEntityIterable);
    }

    @Override
    @Transactional
    public String createEntity(UserActionInterface userAction) {
        //Many To One Relatioships
        //Add Contact Relationship
        userAction.getUserEntity().setContactsByContactsId(contactsRepository.findOne(userAction.getSelectedContact()));
        userAction.getUserEntity().setDeactivated(false);
        //Add Roles to the UsersEntity
        userAction.getSelectedRoles().add(String.valueOf(RolesEntity.EmployeeId));
        userAction.getSelectedRoles().add(String.valueOf(RolesEntity.NewUserId));
        for (String selectedRole : userAction.getSelectedRoles()) {
            RolesEntity rolesEntity = rolesRepository.findOne(Integer.parseInt(selectedRole));
            userAction.getUserEntity().getRoles().add(rolesEntity);
        }
        String pw = String.valueOf(1000 + (int) (Math.random() * 8999));
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        userAction.getUserEntity().setPassword(bCryptPasswordEncoder.encode(pw));
        userAction.setNewPW(pw);
        usersRepository.save(userAction.getUserEntity());
        return Action.SUCCESS;
    }

    @Override
    @Transactional
    public String update(UserActionInterface userAction) {
        UsersEntity user = usersRepository.findOne(userAction.getUserEntity().getId());
        boolean newUser = user.hasRole(RolesEntity.NewUserId);
        if (userAction.hasRole(RolesEntity.Admin)) {
            user.getRoles().clear();
            user.setContactsByContactsId(contactsRepository.findDistinctById(userAction.getSelectedContact()));
            userAction.getSelectedRoles().add(String.valueOf(RolesEntity.EmployeeId));
            if (newUser) {
                userAction.getSelectedRoles().add(String.valueOf(RolesEntity.NewUserId));
            }
            for (int i = 0; i < userAction.getSelectedRoles().size(); i++) {
                user.getRoles().add(rolesRepository.findOne(Integer.valueOf(userAction.getSelectedRoles().get(i))));
            }
        }
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        if (user.getUsername().equals(username) && userAction.isNewPasswort()) {
            user.setPassword(bCryptPasswordEncoder.encode(userAction.getUserEntity().getPassword()));
        }
        user.setPlannedHours(userAction.getUserEntity().getPlannedHours());
        usersRepository.save(user);
        if (username.equals(user.getUsername())) {
            MyUser userForSpring = MyUserDetailsService.buildUserForAuthentication(user, MyUserDetailsService.buildUserAuthority(user.getRoles()));
            Authentication authentication = new UsernamePasswordAuthenticationToken(userForSpring, user.getPassword(), MyUserDetailsService.buildUserAuthority(user.getRoles()));
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }
        return Action.SUCCESS;
    }

    @Override
    @Transactional
    public void toggleDeactivateUser(int id) {
        UsersEntity a = usersRepository.findOne(id);
        if (a.getDeactivated()) {
            a.setDeactivated(false);
        } else {
            a.setDeactivated(true);
        }
        usersRepository.save(a);
    }

    @Override
    @Transactional
    public String changePassword(String password) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        UsersEntity usersEntity = usersRepository.findByUsername(username);
        usersEntity.removeRole(RolesEntity.NewUserId);
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        usersEntity.setPassword(bCryptPasswordEncoder.encode(password));
        usersRepository.save(usersEntity);
        if (username.equals(usersEntity.getUsername())) {
            MyUser userForSpring = MyUserDetailsService.buildUserForAuthentication(usersEntity, MyUserDetailsService.buildUserAuthority(usersEntity.getRoles()));
            Authentication authentication = new UsernamePasswordAuthenticationToken(userForSpring, usersEntity.getPassword(), MyUserDetailsService.buildUserAuthority(usersEntity.getRoles()));
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }
        return Action.SUCCESS;
    }

    @Override
    @Transactional
    public void setPw(UserActionInterface userAction) {
        int pw = (int) (Math.random() * 10000);
        userAction.setUserEntity(usersRepository.findOne(userAction.getSelectedUser()));

        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        userAction.setNewPW(new DecimalFormat("0000").format(pw));
        userAction.getUserEntity().setPassword(bCryptPasswordEncoder.encode(userAction.getNewPW()));

        RolesEntity newUserRole = rolesRepository.findOne(6);

        if (!userAction.getUserEntity().getRoles().contains(newUserRole)) {
            userAction.getUserEntity().getRoles().add(newUserRole);
        }
        usersRepository.save(userAction.getUserEntity());
    }

    @Override
    @Transactional(readOnly = true)
    public JSONObject searchAllUsers(String query, int page, int size){
        query = "%" + query + "%";
        List<UsersEntity> a = usersRepository.findByContactsByContactsId_NameLikeOrContactsByContactsId_SurnameLikeAllIgnoreCase(query, query, new PageRequest(page, size)).getContent();
        Map<Integer, String> list = new HashMap<>();
        for (UsersEntity item : a) {
            list.put(item.getId(), item.getContactsByContactsId().getSurname() + " " + item.getContactsByContactsId().getName());
        }
        JSONObject obj = new JSONObject();
        obj.put("items", list);
        obj.put("total_count", usersRepository.countByContactsByContactsId_NameLikeOrContactsByContactsId_SurnameLikeAllIgnoreCase(query, query));
        return obj;
    }

    @Override
    public boolean usernameExists(String username) {
        return usersRepository.usernameExists(username);
    }

    @Override
    @Transactional(readOnly = true)
    public List<UsersEntity> searchForUsers(String search) {
        return usersRepository.findByUsernameLike("%" + search + "%");
    }

    @Override
    @Transactional
    public UsersEntity getTestEntity() {
        Random r = new Random();
        UsersEntity user = new UsersEntity();
        user.setPassword("test" + r.nextDouble() * r.nextDouble());
        user.setUsername("test" + r.nextDouble() * r.nextFloat());
        usersRepository.save(user);
        return user;
    }

    @Override
    @Transactional(readOnly = true)
    public UsersEntity findById(int userId) {
        return usersRepository.findOne(new Integer(userId));
    }
}
