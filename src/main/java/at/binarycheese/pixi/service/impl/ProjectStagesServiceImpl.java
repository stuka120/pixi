package at.binarycheese.pixi.service.impl;

import at.binarycheese.pixi.domain.ProjectstagesEntity;
import at.binarycheese.pixi.repositories.ProjectstagesRepository;
import at.binarycheese.pixi.service.ProjectStagesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Dominik on 30.01.2015.
 */

@Service
public class ProjectStagesServiceImpl implements ProjectStagesService {

    @Autowired
    ProjectstagesRepository projectstagesRepository;

    @Override
    public List<ProjectstagesEntity> findAllActiveStages() {
        List<ProjectstagesEntity> result = new ArrayList<>();
        Iterator<ProjectstagesEntity> iterator = projectstagesRepository.findAll().iterator();
        while(iterator.hasNext()){
            ProjectstagesEntity entity = iterator.next();
            if(!entity.getName().equals("Deaktiviert")){
                result.add(entity);
            }
        }
        return result;
    }
}
