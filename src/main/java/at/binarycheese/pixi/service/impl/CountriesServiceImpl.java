package at.binarycheese.pixi.service.impl;

import at.binarycheese.pixi.domain.CountriesEntity;
import at.binarycheese.pixi.repositories.CountriesRepository;
import at.binarycheese.pixi.service.CountriesService;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Dominik on 29.01.2015.
 */

@Service
public class CountriesServiceImpl implements CountriesService {

    @Autowired
    CountriesRepository countriesRepository;

    @Override
    @Transactional(readOnly = true)
    public List<CountriesEntity> findAll() {
        return Lists.newArrayList(countriesRepository.findAll());
    }
}
