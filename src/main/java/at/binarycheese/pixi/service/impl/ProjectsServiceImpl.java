package at.binarycheese.pixi.service.impl;

import at.binarycheese.pixi.domain.ContactsEntity;
import at.binarycheese.pixi.domain.ProjectEntity;
import at.binarycheese.pixi.repositories.OrganisationsRepository;
import at.binarycheese.pixi.repositories.ProjectsRepository;
import at.binarycheese.pixi.repositories.ProjectstagesRepository;
import at.binarycheese.pixi.service.ProjectsService;
import at.binarycheese.pixi.service.interfaces.ProjectActionInterface;
import com.google.common.collect.Lists;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Dominik on 30.01.2015.
 */

@Service
public class ProjectsServiceImpl implements ProjectsService {

    @Autowired
    ProjectsRepository projectsRepository;
    @Autowired
    OrganisationsRepository organisationsRepository;
    @Autowired
    ProjectstagesRepository projectstagesRepository;

    @Override
    @Transactional(readOnly = true)
    public List<ProjectEntity> findAll() {
        return Lists.newArrayList(projectsRepository.findAll());
    }

    @Override
    @Transactional(readOnly = true)
    public boolean containsProjectNumber(String projectnumber) {
        int count = projectsRepository.countByProjectnumber(projectnumber);
        return count > 0;
    }

    @Override
    @Transactional(readOnly = true)
    public ProjectEntity findById(int id) {
        ProjectEntity entity = projectsRepository.findOne(id);
        entity.getEventsesById().size();
        return entity;
    }

    @Override
    @Transactional
    public String saveOrUpdateEntity(ProjectActionInterface projectAction) {
        projectAction.getProjectEntity().setOrganisationsByOrganisationsId(organisationsRepository.findOne(Integer.parseInt(projectAction.getOrgId())));

        if (projectAction.getProjectEntity().getId() == 0) {
            projectAction.getProjectEntity().setProjectstagesByProjectStage(projectstagesRepository.findOne(1));
            projectsRepository.save(projectAction.getProjectEntity());
            return "created";
        } else {
            projectAction.getProjectEntity().setProjectstagesByProjectStage(projectstagesRepository.findOne(projectAction.getProjectStagesId()));
            projectsRepository.save(projectAction.getProjectEntity());
            return "edited";
        }
    }

    @Override
    @Transactional
    public void delete(int id) {
        projectsRepository.delete(id);
    }

    @Override
    @Transactional
    public void deactivated(int id) {
        ProjectEntity entity = projectsRepository.findOne(id);
        entity.setProjectstagesByProjectStage(projectstagesRepository.findOne(5));
        projectsRepository.save(entity);
    }

    @Override
    @Transactional
    public void reactivate(int id, int projectStagesId) {
        ProjectEntity entity = projectsRepository.findOne(id);
        entity.setProjectstagesByProjectStage(projectstagesRepository.findOne(projectStagesId));
        projectsRepository.save(entity);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ProjectEntity> findAllActive() {
        List<ProjectEntity> erg = projectsRepository.findAllByProjectstagesByProjectStage_nameNot("Deaktiviert");
        return erg;
    }

    @Override
    public List<ProjectEntity> findAllDeactivated() {
        List<ProjectEntity> erg = projectsRepository.findAllByProjectstagesByProjectStage_name("Deaktiviert");
        return erg;
    }

    public void nextStage(ProjectActionInterface projectActionInterface) {
        ProjectEntity entity = projectsRepository.findOne(projectActionInterface.getId());
        if(entity.getProjectstagesByProjectStage().getId() >= 4){
            return;
        }
        entity.setProjectstagesByProjectStage(projectstagesRepository.findOne(entity.getProjectstagesByProjectStage().getId() + 1));
        projectsRepository.save(entity);
    }

    @Override
    public JSONObject searchByName(String query, int page, int size) {
        String[] queryArray = query.trim().split(" ");
        String name = "%%";
        String number = "%%";
        if (queryArray.length == 2) {
            name = "%" + queryArray[0] + "%";
            number = "%" + queryArray[1] + "%";
        } else if (queryArray.length == 1) {
            name = "%" + queryArray[0] + "%";
        }
        List<ProjectEntity> a = projectsRepository.findByNameLikeAndProjectnumberLikeOrNameLikeAndProjectnumberLike(name, number, number, name, new PageRequest(page, size)).getContent();
        Map<Integer, String> list = new HashMap<>();
        for (ProjectEntity item : a) {
            list.put(item.getId(), item.getProjectnumber() + " - " + item.getName());
        }
        JSONObject obj = new JSONObject();
        obj.put("items", list);
        obj.put("total_count", projectsRepository.countByNameLikeAndProjectnumberLikeOrNameLikeAndProjectnumberLike(name, number, number, name));
        return obj;
    }
}
