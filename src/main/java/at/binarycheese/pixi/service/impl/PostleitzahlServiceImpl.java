package at.binarycheese.pixi.service.impl;

import at.binarycheese.pixi.domain.PostleitzahlEntity;
import at.binarycheese.pixi.repositories.PostleitzahlRepository;
import at.binarycheese.pixi.service.PostleitzahlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Dominik on 17.03.2015.
 */
@Service
public class PostleitzahlServiceImpl implements PostleitzahlService {

    @Autowired
    PostleitzahlRepository postleitzahlRepository;

    @Override
    public List<PostleitzahlEntity> findByPlzLike(int plz) {
        return postleitzahlRepository.findDistinctByPlzLike(plz);
    }

    @Override
    public List<PostleitzahlEntity> findByOrtLike(String ort) {
        return postleitzahlRepository.findDistinctByNameLike(ort);
    }
}
