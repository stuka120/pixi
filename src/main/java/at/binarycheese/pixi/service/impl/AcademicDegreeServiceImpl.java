package at.binarycheese.pixi.service.impl;

import at.binarycheese.pixi.domain.AcademicDegree;
import at.binarycheese.pixi.repositories.AcademicDegreeRepository;
import at.binarycheese.pixi.service.AcademicDegreeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Dominik on 15.03.2015.
 */
@Service
public class AcademicDegreeServiceImpl implements AcademicDegreeService {

    @Autowired
    AcademicDegreeRepository academicDegreeRepository;

    @Override
    public List<AcademicDegree> findAcademicDegree(String query) {
        return academicDegreeRepository.findByDegreeLikeOrDescriptionLikeAllIgnoreCase(query, query);
    }
}
