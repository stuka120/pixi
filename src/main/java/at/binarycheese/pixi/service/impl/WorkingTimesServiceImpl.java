package at.binarycheese.pixi.service.impl;

import at.binarycheese.pixi.domain.WorkingtimesEntity;
import at.binarycheese.pixi.repositories.UsersRepository;
import at.binarycheese.pixi.repositories.WorkingTimesRepository;
import at.binarycheese.pixi.service.WorkingTimesService;
import at.binarycheese.pixi.service.interfaces.WorkingTimeActionInterface;
import com.google.common.collect.Lists;
import com.opensymphony.xwork2.Action;
import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Dominik on 28.01.2015.
 */

@Service
public class WorkingTimesServiceImpl implements WorkingTimesService {

    @Autowired
    WorkingTimesRepository workingTimesRepository;
    @Autowired
    UsersRepository usersRepository;

    @Transactional(readOnly = true)
    @Override
    public List<WorkingtimesEntity> findAllByUserId(int userId) {
        return workingTimesRepository.findByUsersByUserId_Id(userId);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Timestamp> findAllDatesByUserId(int userId) {
        List<Timestamp> erg = new ArrayList<>();

        for(Iterator<WorkingtimesEntity> workingtimesEntityIterator = workingTimesRepository.findByUsersByUserId_Id(userId).iterator(); workingtimesEntityIterator.hasNext();) {
            erg.add(workingtimesEntityIterator.next().getStartTime());
        }

        return erg;
    }

    @Transactional(readOnly = true)
    @Override
    public WorkingtimesEntity findById(int id) {
        return workingTimesRepository.findOne(new Integer(id));
    }

    @Transactional(readOnly = true)
    @Override
    public boolean checkEntryForUserOnDateIsPossible(int id, Timestamp date) {
        List<WorkingtimesEntity> entries = findAllByUserId(id);
        for (WorkingtimesEntity entity : entries) {
            if (entity.getStartTime().getDate() == date.getDate()
                    && entity.getStartTime().getMonth() == date.getMonth()
                    && entity.getStartTime().getYear() == date.getYear()) {
                return false;
            }
        }
        return true;
    }

    @Transactional
    @Override
    public void delete(int id) {
        workingTimesRepository.delete(new Integer(id));
    }

    @Transactional
    @Override
    public void save(WorkingtimesEntity entity) {
        workingTimesRepository.save(entity);
    }

    @Transactional
    @Override
    public void update(WorkingtimesEntity workingtimesEntity) {
        workingTimesRepository.save(workingtimesEntity);
    }

    @Transactional
    @Override
    public String createWorkingTimeEntity(WorkingTimeActionInterface workingtimeAction) {
        workingtimeAction.getWorkingtimesEntity().setStartTime(workingtimeAction.getDate());
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (workingtimeAction.isHoliday()) {
            Timestamp funnyDate = workingtimeAction.getDate();
            WorkingtimesEntity entity = new WorkingtimesEntity();
            entity.setUsersByUserId(usersRepository.findByUsername(auth.getName()));
            if (funnyDate.getDay() != 0 && funnyDate.getDay() != 6
                    && checkEntryForUserOnDateIsPossible(entity.getUsersByUserId().getId(), funnyDate)) {
                entity.setDesc("Urlaub");
                entity.setStartTime(funnyDate);
                entity.setDuration(8.0);
                save(entity);
            }
            while (funnyDate.before(workingtimeAction.getEndHolidayDate())) {
                funnyDate = new Timestamp(funnyDate.getTime() + 1000 * 60 * 60 * 24);
                if (funnyDate.getDay() == 0 || funnyDate.getDay() == 6) {
                    continue;
                }
                if (checkEntryForUserOnDateIsPossible(entity.getUsersByUserId().getId(), funnyDate)) {
                    entity = new WorkingtimesEntity();
                    entity.setDesc("Urlaub");
                    entity.setDuration(8.0);
                    entity.setStartTime(funnyDate);
                    entity.setUsersByUserId(usersRepository.findByUsername(auth.getName()));
                    save(entity);
                }
            }
            PrintWriter out = null;
            try {
                out = ServletActionContext.getResponse().getWriter();
            } catch (IOException e) {
                e.printStackTrace();
            }
            out.write("success");
            return Action.NONE;
        }

        if (workingtimeAction.getStarttime() != null && !workingtimeAction.getStarttime().isEmpty()) {
            workingtimeAction.getWorkingtimesEntity().getStartTime().setHours(Integer.parseInt(workingtimeAction.getStarttime().split(":")[0]));
            workingtimeAction.getWorkingtimesEntity().getStartTime().setMinutes(Integer.parseInt(workingtimeAction.getStarttime().split(":")[1]));
        }

        if (workingtimeAction.getEndtime() != null && !workingtimeAction.getEndtime().isEmpty()) {
            workingtimeAction.getWorkingtimesEntity().setEndTime(new Timestamp(0, 0, 0,
                    Integer.parseInt(workingtimeAction.getEndtime().split(":")[0]),
                    Integer.parseInt(workingtimeAction.getEndtime().split(":")[1]), 0, 0));
        }


        workingtimeAction.getWorkingtimesEntity().setUsersByUserId(usersRepository.findByUsername(auth.getName()));

        if (workingtimeAction.getWorkingtimesEntity().getId() == 0) {
            save(workingtimeAction.getWorkingtimesEntity());
        } else {
            update(workingtimeAction.getWorkingtimesEntity());
        }


        PrintWriter out = null;
        try {
            out = ServletActionContext.getResponse().getWriter();
        } catch (IOException e) {
            e.printStackTrace();
        }
        out.write("success");
        return Action.NONE;
    }

    @Transactional(readOnly = true)
    @Override
    public String browse(WorkingTimeActionInterface workingtimeAction) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        int userId = usersRepository.findByUsername(auth.getName()).getId();
        workingtimeAction.setSelectedUserForExport(userId);
        workingtimeAction.setWorkingtimesList(findAllByUserId(userId));
        workingtimeAction.setUsersEntityList(Lists.newArrayList(usersRepository.findAll()));
        Date today = new Date();
        for (Iterator<WorkingtimesEntity> it = workingtimeAction.getWorkingtimesList().iterator(); it.hasNext(); ) {
            WorkingtimesEntity nextEntity = it.next();
            if (nextEntity.getStartTime().getYear() != today.getYear() || nextEntity.getStartTime().getMonth() != today.getMonth()) {
                it.remove();
            }
        }

        //generate list of all years and months for menu
        List<Timestamp> allDatesByUserId = findAllDatesByUserId(userId);
        Collections.sort(allDatesByUserId);
        workingtimeAction.setMenuDates(new LinkedHashMap<String, ArrayList<Timestamp>>());
        final SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");
        for (final Timestamp item : allDatesByUserId) {
            String year = yearFormat.format(item);
            Timestamp clone = (Timestamp) item.clone();
            clone.setMinutes(0);
            clone.setHours(0);
            clone.setSeconds(0);
            clone.setDate(1);
            if (!workingtimeAction.getMenuDates().containsKey(year)) {
                workingtimeAction.getMenuDates().put(year, new ArrayList<Timestamp>());
            }
            if (!workingtimeAction.getMenuDates().get(year).contains(clone)) {
                workingtimeAction.getMenuDates().get(year).add(clone);
            }
        }
        return Action.SUCCESS;
    }

    public int getUsedHolidayUntilMonth(int userId, Timestamp date){
        Date startDate = (Date) date.clone();
        startDate.setMonth(0);
        startDate.setDate(0);
        Date endDate = (Date) date.clone();
        Calendar mycal = new GregorianCalendar(date.getYear(), date.getMonth(), date.getDate());
        endDate.setDate(mycal.getActualMaximum(Calendar.DAY_OF_MONTH));
        List<WorkingtimesEntity> entries = workingTimesRepository.findByUsersByUserId_IdAndStartTimeBetweenAndDesc(userId, startDate, endDate, "Urlaub");
        return entries.size();
    }

    @Override
    public boolean CheckHolidayCount(WorkingTimeActionInterface workingTimeAction) {
        boolean entryPossible = false;
        Timestamp funnyDate = (Timestamp) workingTimeAction.getDate().clone();
        int userId = usersRepository.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).getId();
        do{
            if (funnyDate.getDay() != 0 && funnyDate.getDay() != 6
                    && checkEntryForUserOnDateIsPossible(userId, funnyDate)) {
                entryPossible = true;
                break;
            }
            funnyDate = new Timestamp(funnyDate.getTime() + 1000 * 60 * 60 * 24);
        }while (funnyDate.before(new Timestamp(workingTimeAction.getEndHolidayDate().getTime() + 1000 * 60 * 60 * 24)));
        return entryPossible;
    }
}
