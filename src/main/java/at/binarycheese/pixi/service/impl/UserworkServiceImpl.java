package at.binarycheese.pixi.service.impl;

import at.binarycheese.pixi.domain.EventsEntity;
import at.binarycheese.pixi.domain.UsersEntity;
import at.binarycheese.pixi.domain.UserworksEntity;
import at.binarycheese.pixi.repositories.UserworkRepository;
import at.binarycheese.pixi.service.UserworkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by Lukas on 05.02.2015.
 */
@Service
public class UserworkServiceImpl implements UserworkService {
    @Autowired
    UserworkRepository userworkRepository;

    @Override
    @Transactional(readOnly = true)
    public List<UserworksEntity> findByEvent(int eventid) {
        return userworkRepository.findByEvent_Id(eventid);
    }

    @Override
    @Transactional
    public void delete(int id) {
        userworkRepository.delete(id);
    }

    @Override
    @Transactional
    public String edit(UserworksEntity entity) {
        List<UserworksEntity> userworks = userworkRepository.findByStartTimeLessThanAndEndTimeGreaterThanAndUser_Id(entity.getEndTime(), entity.getStartTime(), entity.getUser().getId());
        if(userworks.size() == 0) {
            userworkRepository.save(entity);
            return "SUCCESS";
        }
        for(UserworksEntity userwork : userworks){
            if(userwork.getEvent().getId() != entity.getEvent().getId()){
                return "ERROR";
            }
        }
        return "EDIT";
    }

    @Override
    @Transactional
    public String create(UserworksEntity entity) {
        return edit(entity);
    }


    @Override
    @Transactional(readOnly = true)
    public UserworksEntity find(int id) {
        return userworkRepository.findOne(id);
    }

    @Override
    public UserworksEntity getCopy(UserworksEntity userworksEntity) {
        UserworksEntity userworksEntity1 = new UserworksEntity();
        userworksEntity1.setStartTime(userworksEntity.getStartTime());
        userworksEntity1.setEndTime(userworksEntity.getEndTime());
        userworksEntity1.setEvent(userworksEntity.getEvent());
        return userworksEntity1;
    }

    @Override
    @Transactional
    public UserworksEntity getTestEntity(EventsEntity eventsEntity, UsersEntity usersEntity) {
        UserworksEntity userworksEntity = new UserworksEntity();
        userworksEntity.setStartTime(new Timestamp(2015,2,18,22,3,0,0));
        userworksEntity.setEndTime(new Timestamp(2015,2,19,22,5,0,0));
        userworksEntity.setUser(usersEntity);
        userworksEntity.setEvent(eventsEntity);
        userworkRepository.save(userworksEntity);
        return userworksEntity;
    }

    @Override
    @Transactional
    public void mergeEvent(UserworksEntity userwork) {
        List<UserworksEntity> overlaping = userworkRepository.findByStartTimeLessThanAndEndTimeGreaterThanAndUser_Id(userwork.getEndTime(), userwork.getStartTime(), userwork.getUser().getId());
        Timestamp minStart = userwork.getStartTime();
        Timestamp maxEnd = userwork.getEndTime();
        UsersEntity u = userwork.getUser();
        EventsEntity e = userwork.getEvent();
        for(UserworksEntity overlap:overlaping){
            if(overlap.getEvent().getId() == userwork.getEvent().getId()){
                if(overlap.getStartTime().before(minStart)){
                    minStart = overlap.getStartTime();
                }
                if(overlap.getEndTime().after(maxEnd)){
                    maxEnd = overlap.getEndTime();
                }
                u = overlap.getUser();
                e = overlap.getEvent();
                userworkRepository.delete(overlap);
            }
        }
        UserworksEntity merged = new UserworksEntity();
        merged.setStartTime(minStart);
        merged.setEndTime(maxEnd);
        merged.setEvent(e);
        merged.setUser(u);
        userworkRepository.save(merged);
        userwork = merged;
    }

    @Override
    public EventsEntity getConflict(UserworksEntity userwork) {
        List<UserworksEntity> overlaping = userworkRepository.findByStartTimeLessThanAndEndTimeGreaterThanAndUser_Id(userwork.getEndTime(), userwork.getStartTime(), userwork.getUser().getId());
        for(UserworksEntity overlap : overlaping){
            if(overlap.getEvent().getId() != userwork.getEvent().getId()){
                return overlap.getEvent();
            }
        }
        return null;
    }


}
