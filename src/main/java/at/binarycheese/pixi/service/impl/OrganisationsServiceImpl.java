package at.binarycheese.pixi.service.impl;

import at.binarycheese.pixi.domain.ContactsEntity;
import at.binarycheese.pixi.domain.OrganisationsEntity;
import at.binarycheese.pixi.repositories.ContactsRepository;
import at.binarycheese.pixi.repositories.OrganisationsRepository;
import at.binarycheese.pixi.service.OrganisationsService;
import at.binarycheese.pixi.service.interfaces.OrganisationsActionInterface;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Dominik on 29.01.2015.
 */

@Service
public class OrganisationsServiceImpl implements OrganisationsService {

    @Autowired
    OrganisationsRepository organisationsRepository;
    @Autowired
    ContactsRepository contactsRepository;

    @Override
    public List<OrganisationsEntity> findAll() {
        return Lists.newArrayList(organisationsRepository.findAll());
    }

    @Override
    @Transactional(readOnly = true)
    public int getMemberCount(int id) {
        return contactsRepository.countByOrganisationsByOrganisationsId_Id(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ContactsEntity> findUnreferencedContacts() {
        return contactsRepository.findByOrganisationsByOrganisationsIdIsNull();
    }

    @Override
    @Transactional
    public List<ContactsEntity> findUnreferencedContactsBySurnameOrName(String name) {
        return contactsRepository.findByOrganisationsByOrganisationsIdIsNullAndNameLikeOrSurnameLikeAllIgnoreCase(name, name);
    }

    @Override
    @Transactional
    public void saveEntity(OrganisationsActionInterface organisationsAction) {
        //Add the ContactsEntities to the List
        if(organisationsAction.getSelectedContacts() != null && !organisationsAction.getSelectedContacts().isEmpty()) {
            for (String entity : Arrays.asList(organisationsAction.getSelectedContacts().split(","))) {
                if(entity.isEmpty()){
                    continue;
                }
                ContactsEntity contactsEntity = contactsRepository.findOne(Integer.parseInt(entity));
                contactsEntity.setOrganisationsByOrganisationsId(organisationsAction.getOrganisationsEntity());
                organisationsAction.getOrganisationsEntity().getContactsesById().add(contactsEntity);
            }
        }

        organisationsRepository.save(organisationsAction.getOrganisationsEntity());
    }

    @Override
    @Transactional(readOnly = true)
    public OrganisationsEntity findById(int selectedOrganisation) {
        OrganisationsEntity entity = organisationsRepository.findOne(selectedOrganisation);
        if(entity != null) {
            entity.getContactsesById().size();
        }
        return entity;
    }

    @Override
    @Transactional
    public void update(OrganisationsActionInterface organisationsAction) {

        //Remove the existing relationships. Because of the unidirectional relationship, i need to do this operation this way

        Iterable<ContactsEntity> iterableList = contactsRepository.findByOrganisationsByOrganisationsId_Id(organisationsAction.getOrganisationsEntity().getId());
        for (Iterator iterator = iterableList.iterator(); iterator.hasNext(); ) {
            ContactsEntity next = (ContactsEntity) iterator.next();
            next.setOrganisationsByOrganisationsId(null);
            contactsRepository.save(next);
        }
        //contactsRepository.save(iterableList);

        if (organisationsAction.getSelectedContacts() != null && !organisationsAction.getSelectedContacts().isEmpty() && Arrays.asList(organisationsAction.getSelectedContacts().split(",")).size() != 0) {
            //Add the ContactsEntities to the List
            for (String entity : Arrays.asList(organisationsAction.getSelectedContacts().split(","))) {
                ContactsEntity contactsEntity = contactsRepository.findOne(Integer.parseInt(entity));
                contactsEntity.setOrganisationsByOrganisationsId(organisationsAction.getOrganisationsEntity());
                organisationsAction.getOrganisationsEntity().getContactsesById().add(contactsEntity);
            }
        }

        organisationsRepository.save(organisationsAction.getOrganisationsEntity());
    }

    @Override
    @Transactional
    public void delete(int id) {
        List<ContactsEntity> contactsList = contactsRepository.findByOrganisationsByOrganisationsId_Id(id);

        for (Iterator<ContactsEntity> iterator = contactsList.iterator(); iterator.hasNext(); ) {
            ContactsEntity contactsEntity = iterator.next();
            contactsEntity.setOrganisationsByOrganisationsId(null);
            contactsRepository.save(contactsEntity);
        }

        organisationsRepository.delete(id);
    }

    @Override
    @Transactional
    public boolean checkOrgNameAvailable(String name){
        List<OrganisationsEntity> result = organisationsRepository.findByName(name);
        if(result.size() == 0){
            return true;
        }else {
            return false;
        }
    }

    @Override
    @Transactional
    public void addSelectedContactsToEntity(OrganisationsActionInterface organisationsAction) {
        String[] selectedContactsArray = organisationsAction.getSelectedContacts().split(",");
        for (int i = 0; i < selectedContactsArray.length; i++) {
            selectedContactsArray[i] = selectedContactsArray[i].trim();
            ContactsEntity contactsEntity = contactsRepository.findOne(Integer.parseInt(selectedContactsArray[i]));
            organisationsAction.getOrganisationsEntity().getContactsesById().add(contactsEntity);
        }
    }

    @Override
    @Transactional
    public void findAllOrgsAndSetContactCount(OrganisationsActionInterface organisationAction) {
        organisationAction.setOrganisationsEntityList(findAll());
        for (OrganisationsEntity entity : organisationAction.getOrganisationsEntityList()) {
            entity.setContactMembers(contactsRepository.countByOrganisationsByOrganisationsId_Id(entity.getId()));
        }
    }
}
