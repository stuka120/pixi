package at.binarycheese.pixi.service.impl;

import at.binarycheese.pixi.domain.EventsEntity;
import at.binarycheese.pixi.repositories.ContactsRepository;
import at.binarycheese.pixi.repositories.EventcategoriesRepository;
import at.binarycheese.pixi.repositories.EventsRepository;
import at.binarycheese.pixi.repositories.ProjectsRepository;
import at.binarycheese.pixi.service.EventsService;
import at.binarycheese.pixi.service.UsersService;
import at.binarycheese.pixi.service.interfaces.EventActionInterface;
import com.google.common.collect.Lists;
import com.opensymphony.xwork2.Action;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Dominik on 29.01.2015.
 */

@Service
public class EventsServiceImpl implements EventsService {

    @Autowired
    EventsRepository eventsRepository;
    @Autowired
    ContactsRepository contactsRepository;
    @Autowired
    EventcategoriesRepository eventcategoriesRepository;
    @Autowired
    ProjectsRepository projectsRepository;
    @Autowired
    UsersService usersService;


    @Override
    @Transactional
    public EventsEntity testEntity() {
        EventsEntity event = new EventsEntity();
        event.setStartDate(new Timestamp(2015, 2, 11, 22, 0, 0, 0));
        event.setEndDate(new Timestamp(2015, 2, 11, 23, 59, 0, 0));
        event.setDescription("Test");
        event.setTitle("Test");
        event.setEventcategoriesByEventCategorieId(eventcategoriesRepository.findAll().iterator().next());
        eventsRepository.save(event);
        return event;
    }

    @Override
    @Transactional
    public String saveEntity(EventActionInterface eventAction) {
        try {
            eventAction.getEvent().setContactsByContactPerson(contactsRepository.findDistinctById(eventAction.getEventContactPerson()));
            eventAction.getEvent().setEventcategoriesByEventCategorieId(eventcategoriesRepository.findOne(eventAction.getEventCategory()));
            eventAction.getEvent().setProjectByProjectId(projectsRepository.findOne(eventAction.getEvent().getProjectByProjectId().getId()));
            if(eventAction.getEvent().getUsersByResponsible() != null){
                eventAction.getEvent().setUsersByResponsible(usersService.findById(eventAction.getEvent().getUsersByResponsible().getId()));
            }
            eventsRepository.save(eventAction.getEvent());
        } catch (Exception e) {
            eventAction.setContactsList(Lists.newArrayList(contactsRepository.findAll()));
            eventAction.setCategoriesList(Lists.newArrayList(eventcategoriesRepository.findAll()));
            return Action.ERROR;
        }
        eventAction.setEventId(eventAction.getEvent().getId());
        return Action.SUCCESS;
    }

    @Transactional(readOnly = true)
    @Override
    public EventsEntity findById(int eventId) {
        return eventsRepository.findDistinctById(eventId);
    }

    @Transactional
    @Override
    public String update(EventActionInterface eventAction) {
        try {
            eventAction.getEvent().setContactsByContactPerson(contactsRepository.findDistinctById(eventAction.getEventContactPerson()));
            eventAction.getEvent().setEventcategoriesByEventCategorieId(eventcategoriesRepository.findOne(eventAction.getEventCategory()));
            eventAction.getEvent().setProjectByProjectId(projectsRepository.findOne(eventAction.getEvent().getProjectByProjectId().getId()));
            eventAction.getEvent().setUsersByResponsible(usersService.findById(eventAction.getEvent().getUsersByResponsible().getId()));

            eventsRepository.save(eventAction.getEvent());
        } catch (Exception e) {
            eventAction.setContactsList(Lists.newArrayList(contactsRepository.findAll()));
            eventAction.setCategoriesList(Lists.newArrayList(eventcategoriesRepository.findAll()));
            return Action.ERROR;
        }
        eventAction.setEventId(eventAction.getEvent().getId());
        return Action.SUCCESS;
    }

    @Override
    @Transactional
    public void delete(int id) {
        eventsRepository.delete(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<EventsEntity> findAll() {
        return Lists.newArrayList(eventsRepository.findAll());
    }

    @Override
    @Transactional(readOnly = true)
    public List<EventsEntity> findByStartDateAndEndDateAndEventcategoriesByEventCategorieId_IdIn(Timestamp startDate, Timestamp endDate, List<Integer> id) {
        return eventsRepository.findByStartDateBetweenOrEndDateBetweenAndEventcategoriesByEventCategorieId_IdIn(startDate, endDate, startDate, endDate, id);
    }

    @Override
    @Transactional
    public String importXmlFromGoogle(EventActionInterface eventAction) {
        if (eventAction.getUploadedFile() == null) {
            eventAction.setCategoriesList(Lists.newArrayList(eventcategoriesRepository.findAll()));
            return Action.SUCCESS;
        }
        List<EventsEntity> allEvents = eventAction.readFromIcsAndReturnEventEntities();
        for (EventsEntity item : allEvents) {
            eventsRepository.save(item);
        }
        if (eventAction.getAmountWrong() > allEvents.size()) {
            eventAction.setAmountWrong(eventAction.getAmountWrong() - allEvents.size());
            eventAction.addFieldError("errorMessage", eventAction.getText("import_wrongEvents"));
        }
        eventAction.setCategoriesList(Lists.newArrayList(eventcategoriesRepository.findAll()));
        return Action.SUCCESS;
    }

    @Override
    public List<EventsEntity> getResponsibleEvents(String username) {
        Date today = new Date();
        Date todayPlusWeek = new Date(today.getTime() + (1000 * 60 * 60 * 24) * 7);
        return eventsRepository.findByUsersByResponsible_UsernameAndStartDateBetweenAndChecklistShown(username, new Timestamp(today.getTime()), new Timestamp(todayPlusWeek.getTime()), (byte) 0);
    }

    @Override
    public void setChecklistShown(int eventId) {
        EventsEntity eventsEntity = eventsRepository.findOne(eventId);
        eventsEntity.setChecklistShown((byte) 1);
        eventsRepository.save(eventsEntity);
    }

    @Override
    @Transactional(readOnly = true)
    public EventsEntity findByIdWithUserworks(int eventId) {
        EventsEntity eventsEntity = eventsRepository.findDistinctById(eventId);
        eventsEntity.getUserworksesById().size();
        return eventsEntity;
    }

    @Override
    @Transactional(readOnly = true)
    public List<EventsEntity> findAllByProjectId(int projectId) {
        List<EventsEntity> events = new ArrayList<>();
        for(EventsEntity event : eventsRepository.findAll()) {
            if(event.getProjectByProjectId() != null && event.getProjectByProjectId().getId() == projectId) {
                events.add(event);
            }
        }
        return  events;
    }

}
