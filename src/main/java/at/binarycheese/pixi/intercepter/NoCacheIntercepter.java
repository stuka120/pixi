package at.binarycheese.pixi.intercepter;


import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;
import org.apache.struts2.ServletActionContext;

/**
 * Created by Joyce on 19.02.2015.
 */
public class NoCacheIntercepter implements Interceptor {

    @Override
    public void destroy() {

    }

    @Override
    public void init() {

    }

    @Override
    public String intercept(ActionInvocation invocation) throws Exception {
        ServletActionContext.getResponse().setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
        ServletActionContext.getResponse().setHeader("Pragma", "no-cache"); // HTTP 1.0.
        ServletActionContext.getResponse().setDateHeader("Expires", 0); // Proxies.
        return invocation.invoke();
    }
}
