package at.binarycheese.pixi.domain;

import javax.persistence.*;

/**
 * Created by Dominik on 17.01.2015.
 */
@MappedSuperclass
public abstract class BasePersistable {

    protected int id;

    @Id
    @GeneratedValue
    @Column(name = "ID")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
