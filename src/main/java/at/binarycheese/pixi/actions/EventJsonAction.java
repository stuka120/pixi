package at.binarycheese.pixi.actions;

import java.text.SimpleDateFormat;
import java.util.*;

import at.binarycheese.pixi.domain.EventsEntity;
import at.binarycheese.pixi.service.EventsService;
import com.opensymphony.xwork2.Action;
import org.json.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.concurrent.TimeUnit;

public class EventJsonAction {
    //FullCalendar Start and End
    java.sql.Timestamp start;
    java.sql.Timestamp end;

    @Autowired
    EventsService eventsService;

    Integer[] categories;

    public String events() {
        if (categories == null || categories.length == 0 || end == null || start == end) {
            return EventAction.ERROR;
        }
        return EventAction.SUCCESS;
    }

    public String execute() {
        return Action.SUCCESS;
    }

    public static Map toMap(JSONObject object) throws JSONException {
        Map<String, Object> map = new HashMap<String, Object>();

        Iterator<String> keysItr = object.keys();
        while (keysItr.hasNext()) {
            String key = keysItr.next();
            Object value = object.get(key);

            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            map.put(key, value);
        }
        return map;
    }

    public static List toList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<Object>();
        for (int i = 0; i < array.length(); i++) {
            Object value = array.get(i);
            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            list.add(value);
        }
        return list;
    }

    public void setCategories(Integer[] categories) {
        this.categories = categories;
    }

    public List getEvents() {
        if (categories == null || categories.length == 0) {
            return new ArrayList<EventsEntity>();
        }
        List<Integer> intlist = null;
        try {
            intlist = Arrays.asList(categories);
        } catch (Exception e) {
        }
        ;
        int[] categoriesArray = new int[categories.length];
        for (int i = 0; i < categories.length; i++) {
            categoriesArray[i] = categories[i];
        }
        List<EventsEntity> eventList = eventsService.findByStartDateAndEndDateAndEventcategoriesByEventCategorieId_IdIn(start, end, intlist);
        JSONArray events = new JSONArray();

        for (EventsEntity e : eventList) {
            if (categories != null && categories.length > 0 && !intlist.contains(e.getEventcategoriesByEventCategorieId().getId())) {
                continue;
            }
            JSONObject event = new JSONObject();
            event.put("id", e.getId());
            event.put("title", e.getTitle());
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'");
            SimpleDateFormat dateOnlyFormy = new SimpleDateFormat("yyyy-MM-dd");
            if (e.getEndDate() == null) {
                event.put("start", dateOnlyFormy.format(e.getStartDate()));
                java.sql.Timestamp ts = e.getStartDate();
                Calendar cal = Calendar.getInstance();
                cal.setTime(ts);
                cal.add(Calendar.DAY_OF_YEAR, 1);
                ts.setTime(cal.getTime().getTime()); // or
                ts = new java.sql.Timestamp(cal.getTime().getTime());
                event.put("end", dateOnlyFormy.format(ts));
            } else if (getDateDiff(e.getStartDate(), e.getEndDate(), TimeUnit.DAYS) > 0) {
                event.put("start", dateOnlyFormy.format(e.getStartDate()));
                java.sql.Timestamp ts = e.getEndDate();
                Calendar cal = Calendar.getInstance();
                cal.setTime(ts);
                cal.add(Calendar.DAY_OF_YEAR, 1);
                ts.setTime(cal.getTime().getTime());
                event.put("end", dateOnlyFormy.format(ts));
            } else if (e.getStartDate().equals(e.getEndDate())) {
                event.put("start", format.format(e.getStartDate()));
                java.sql.Timestamp ts = e.getEndDate();
                Calendar cal = Calendar.getInstance();
                cal.setTime(ts);
                cal.add(Calendar.HOUR_OF_DAY, 1);
                ts.setTime(cal.getTime().getTime()); // or
                ts = new java.sql.Timestamp(cal.getTime().getTime());
                event.put("end", format.format(ts));
            } else {
                event.put("start", format.format(e.getStartDate()));
                event.put("end", format.format(e.getEndDate()));
            }
            event.put("color", e.getEventcategoriesByEventCategorieId().getColor());
            events.put(event);

        }
        return toList(events);
    }

    /**
     * Get a diff between two dates
     *
     * @param date1    the oldest date
     * @param date2    the newest date
     * @param timeUnit the unit in which you want the diff
     * @return the diff value, in the provided unit
     */
    public static long getDateDiff(Timestamp date1, Timestamp date2, TimeUnit timeUnit) {
        long diffInMillies = date2.getTime() - date1.getTime();
        return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
    }

    static Timestamp toTimeStamp(String stamp) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date time = null;
        try {
            time = format.parse(stamp);
        } catch (ParseException e) {
            return null;
        }
        return new Timestamp(time.getTime());
    }

    //GETTERS AND SETTERS
    public void setEnd(String end) {
        this.end = toTimeStamp(end);
    }

    //START BY FULLCALENDAR
    public void setStart(String start) {
        this.start = toTimeStamp(start);
    }

    public EventsService getEventsService() {
        return eventsService;
    }

    public void setEventsService(EventsService eventsService) {
        this.eventsService = eventsService;
    }
}
