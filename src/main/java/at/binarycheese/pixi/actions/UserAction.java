package at.binarycheese.pixi.actions;

import at.binarycheese.pixi.domain.ContactsEntity;
import at.binarycheese.pixi.domain.RolesEntity;
import at.binarycheese.pixi.domain.UsersEntity;
import at.binarycheese.pixi.service.ContactsService;
import at.binarycheese.pixi.service.RolesService;
import at.binarycheese.pixi.service.UsersService;
import at.binarycheese.pixi.service.interfaces.UserActionInterface;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.ServletActionContext;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.xml.ws.handler.MessageContext;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Thomas on 16.11.2014.
 */
public class UserAction extends ActionSupport implements UserActionInterface {

    private List<UsersEntity> userList = new ArrayList<>();
    private String username;
    private String password;
    private String confirmPW;
    private UsersEntity userEntity;
    private String oldPW;

    //Selected Value of the User
    private int selectedUser;

    //Languages Select2List Values;
    private List<RolesEntity> rolesEntityList;
    private List<String> selectedRoles;
    private List<Integer> preSelectedRoles;

    //Contact List Entities
    private List<ContactsEntity> contactsEntityList;
    private int selectedContact;
    private int preSelectedContact;
    private boolean deactivated;

    private String userSearchString;
    private JSONArray userJson;

    @Autowired
    UsersService usersService;
    @Autowired
    ContactsService contactsService;
    @Autowired
    RolesService rolesService;

    //New Password
    private String newPW;
    private String nameBenutzer;
    private boolean newPasswort;

    //For searching through Users
    String query;
    int page;
    int size;

    public String execute() {
        return SUCCESS;
    }

    public String index() {
        userList = usersService.findAllWithPermissions(deactivated);
        return SUCCESS;
    }

    public String create() {
        rolesEntityList = rolesService.findAllWithoutNewUser();
        return SUCCESS;
    }

    public String listAPI() {
        JSONArray users = new JSONArray();
        for (ContactsEntity e : contactsService.searchByNameAndSurname(MessageFormat.format("%{0}%", userSearchString))) {
            JSONObject user = new JSONObject();
            Collection<UsersEntity> usersEntities = e.getUsersesById();
            if (usersEntities.size() == 0) {
                userJson = users;
                continue;
            }
            UsersEntity usersEntity = e.getUsersesById().iterator().next();
            user.put("id", usersEntity.getId());
            user.put("text", MessageFormat.format("{0} {1}", e.getName(), e.getSurname()));
            users.put(user);
        }
        userJson = users;
        return SUCCESS;
    }

    public String add() {
        return usersService.createEntity(this);
    }

    public String edit() {
        username = SecurityContextHolder.getContext().getAuthentication().getName();
        if (ServletActionContext.getRequest().getRequestURI().contains("editProfil")) {
            selectedUser = usersService.findByUsername(username).getId();
        }
        userEntity = usersService.findByUserIdWithPermissions(selectedUser);
        rolesEntityList = rolesService.findAllWithoutNewUser();
        return SUCCESS;
    }

    public String update() {
        usersService.update(this);
        return SUCCESS;
    }

    public String toggleDeactivateUser() {
        try {
            usersService.toggleDeactivateUser(selectedUser);
            return SUCCESS;
        } catch (Exception e) {
            return ERROR;
        }
    }

    public String resetPW() {
        try {
            usersService.setPw(this);
            ServletActionContext.getResponse().getWriter().write(getNewPW());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Action.NONE;
    }


    public String changePassword() {
        return usersService.changePassword(confirmPW);
    }


    public String getSearchAllUsers() throws Exception {
        JSONObject asdf = usersService.searchAllUsers(query, page, size);
        try {
            ServletActionContext.getResponse().setContentType("application/json");
            ServletActionContext.getResponse().getWriter().write(asdf.toString());
        } catch (IOException e) {
        }
        return Action.NONE;
    }

    @Override
    public void validate() {
        try {
            if (userEntity != null) {
                if (userEntity.getUsername().equals("")) {
                    addFieldError("userEntity.username", getText("usernameNotEmpty"));
                }
//                if (userEntity.getPlannedHours() == null) {
//                    addFieldError("userEntity.plannedHours", getText("plannedHoursNull"));
//                } else
                //PlannedHours are not necessary, but they if they are there they must be over 1
                if (userEntity.getPlannedHours() != null && userEntity.getPlannedHours() < 1) {
                    addFieldError("userEntity.plannedHours", getText("plannedHoursLessThanZero"));
                }

                if (hasRole(RolesEntity.Admin)) {
                    if (selectedContact <= 0) {
                        addFieldError("userEntity.selectedContact", getText("noSelectedContact"));
                    }
                    //Users do not have to have any roles --> Default is employee
//                    if (selectedRoles.size() == 0) {
//                        addFieldError("userEntity.selectedRoles", getText("noSelectedRole"));
//                    }
                    int admins = usersService.countByAdmins();
                    if (admins >= 3) {
                        if (selectedRoles.contains(String.valueOf(RolesEntity.AdminId))) {
                            UsersEntity users;
                            if (userEntity.getId() == 0) {
                                users = null;
                            } else {
                                users = usersService.findByUserIdWithPermissions(userEntity.getId());
                            }
                            if (users == null) {
                                addFieldError("selectedRoles", getText("TooMuchAdmins"));
                            } else if (!users.hasRole(RolesEntity.AdminId)) {
                                addFieldError("selectedRoles", getText("TooMuchAdmins"));
                            }
                        }
                    } else if (admins <= 1) {
                        if (!selectedRoles.contains(String.valueOf(RolesEntity.AdminId))) {
                            if (admins <= 1) {
                                UsersEntity users;
                                if (userEntity.getId() == 0) {
                                    users = null;
                                } else {
                                    users = usersService.findByUserIdWithPermissions(userEntity.getId());
                                }
                                if (users.hasRole(RolesEntity.AdminId)) {
                                    addFieldError("selectedRoles", getText("TooLessAdmins"));
                                }
                            }
                        }
                    }
                }
                if (ServletActionContext.getRequest().getRequestURI().contains("add")) {
                    if (usersService.usernameExists(userEntity.getUsername())) {
                        addFieldError("userEntity.username", getText("usernameAlreadyExists"));
                    }
                }
                if (ServletActionContext.getRequest().getRequestURI().contains("update")) {
                    username = SecurityContextHolder.getContext().getAuthentication().getName();
                    if (userEntity.getUsername().equals(username)) {
                        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
                        UsersEntity users = usersService.findById(userEntity.getId());
                        if (!bCryptPasswordEncoder.matches(oldPW, users.getPassword())) {
                            addFieldError("oldPW", getText("oldPasswortWrong"));
                        }
                        if (newPasswort) {
                            if (userEntity.getPassword() == null) {
                                addFieldError("userEntity.password", getText("passwordNotEmpty"));
                            } else if (userEntity.getPassword().equals("")) {
                                addFieldError("userEntity.password", getText("passwordNotEmpty"));
                            } else if (userEntity.getPassword().length() < 6) {
                                addFieldError("userEntity.password", getText("passwordTooShort"));
                            } else if (!userEntity.getPassword().equals(confirmPW)) {
                                addFieldError("userEntity.password", getText("confirmPassoword"));
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {

        }
    }

    /**
     * -----------------Getters/Setters----------------------
     */
    public List<UsersEntity> getUserList() {
        return userList;
    }

    public void setUserList(List<UsersEntity> userList) {
        this.userList = userList;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPW() {
        return confirmPW;
    }

    public void setConfirmPW(String confirmPW) {
        this.confirmPW = confirmPW;
    }

    public UsersEntity getUserEntity() {
        return userEntity;
    }

    public void setUserEntity(UsersEntity usersEntity) {
        this.userEntity = usersEntity;
    }

    public int getSelectedUser() {
        return selectedUser;
    }

    public void setSelectedUser(int selectedUser) {
        this.selectedUser = selectedUser;
    }

    public List<RolesEntity> getRolesEntityList() {
        return rolesEntityList;
    }

    public void setRolesEntityList(List<RolesEntity> rolesEntityList) {
        this.rolesEntityList = rolesEntityList;
    }

    public List<String> getSelectedRoles() {
        return selectedRoles;
    }

    public void setSelectedRoles(List<String> selectedRoles) {
        this.selectedRoles = selectedRoles;
    }

    public List<Integer> getPreSelectedRoles() {
        preSelectedRoles = new ArrayList<>();
        for (RolesEntity permissionEntity : userEntity.getRoles()) {
            preSelectedRoles.add(permissionEntity.getId());
        }
        return preSelectedRoles;
    }

    public int getPreSelectedContact() {
        if (userEntity.getContactsByContactsId() != null) {
            preSelectedContact = userEntity.getContactsByContactsId().getId();
            return preSelectedContact;
        } else {
            preSelectedContact = 0;
            return preSelectedContact;
        }
    }

    public void setPreSelectedContact(int preSelectedContact) {
        this.preSelectedContact = preSelectedContact;
    }

    public List<ContactsEntity> getContactsEntityList() {
        return contactsEntityList;
    }

    public void setContactsEntityList(List<ContactsEntity> contactsEntityList) {
        this.contactsEntityList = contactsEntityList;
    }

    public int getSelectedContact() {
        return selectedContact;
    }

    public void setSelectedContact(int selectedContact) {
        this.selectedContact = selectedContact;
    }

    public String getOldPW() {
        return oldPW;
    }

    public void setOldPW(String oldPW) {
        this.oldPW = oldPW;
    }

    public String getNewPW() {
        return newPW;
    }

    public void setNewPW(String newPW) {
        this.newPW = newPW;
    }

    public String getNameBenutzer() {
        return nameBenutzer;
    }

    public void setNameBenutzer(String nameBenutzer) {
        this.nameBenutzer = nameBenutzer;
    }

    public UsersService getUsersService() {
        return usersService;
    }

    public void setUsersService(UsersService usersService) {
        this.usersService = usersService;
    }

    public ContactsService getContactsService() {
        return contactsService;
    }

    public void setContactsService(ContactsService contactsService) {
        this.contactsService = contactsService;
    }

    public RolesService getRolesService() {
        return rolesService;
    }

    public void setRolesService(RolesService rolesService) {
        this.rolesService = rolesService;
    }

    public void setUserSearchString(String userSearchString) {
        this.userSearchString = userSearchString;
    }

    public List getUserJson() {
        return EventJsonAction.toList(userJson);
    }

    public boolean hasRole(String role) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null)
            return false;

        for (GrantedAuthority auth : authentication.getAuthorities()) {
            if (role.equals(auth.getAuthority()))
                return true;
        }
        return false;
    }

    public boolean isDeactivated() {
        return deactivated;
    }

    public void setDeactivated(boolean deactivated) {
        this.deactivated = deactivated;
    }

    @Override
    public boolean isNewPasswort() {
        return newPasswort;
    }

    public void setNewPasswort(boolean newPasswort) {
        this.newPasswort = newPasswort;
    }
    public void setQuery(String query) {
        this.query = query;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
