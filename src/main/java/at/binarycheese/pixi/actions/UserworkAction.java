package at.binarycheese.pixi.actions;

import at.binarycheese.pixi.domain.EventsEntity;
import at.binarycheese.pixi.domain.UsersEntity;
import at.binarycheese.pixi.domain.UserworksEntity;
import at.binarycheese.pixi.service.EventsService;
import at.binarycheese.pixi.service.UsersService;
import at.binarycheese.pixi.service.UserworkService;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Lukas on 09.02.2015.
 */
public class UserworkAction extends ActionSupport{

    //Services
    @Autowired
    UserworkService userworkService;


    @Autowired
    UsersService usersService;

    @Autowired
    EventsService eventsService;

    //Variables
    List<UserworksEntity> userworks;
    int userworkid;
    UserworksEntity userwork;

    public void setMessage(String message) {
        this.message = message;
    }

    String message;
    int eventid;
    int[] users;
    Map<String, Object> jsonResult;

    //Actions
    public String execute(){
        return SUCCESS;
    }

    //Return only SUCCESS If UserworksList was set
    public String list(){
        userworks = userworkService.findByEvent(eventid);
        return SUCCESS;
    }

    public String delete(){
        try {
            userworkService.delete(userwork.getId());
            return SUCCESS;
        }catch (Exception e){
            return ERROR;
        }
    }

    public String needsUserWork(){
        if(userwork != null){
            return SUCCESS;
        }
        return ERROR;
    }

    public String create(){
        try{
            List<UserworksEntity> entitiesToSave = new ArrayList<UserworksEntity>();
            if(userwork.getUser() != null) {
                entitiesToSave.add(userwork);

            }
            else {
                for (int userid : users) {
                    UsersEntity user = usersService.findById(userid);
                    UserworksEntity userworksEntity = userworkService.getCopy(userwork);
                    userworksEntity.setUser(user);
                    entitiesToSave.add(userworksEntity);
                }
            }
            List<UserworksEntity> succeeded = new ArrayList<>();
            List<UserworksEntity> error = new ArrayList<>();
            List<UserworksEntity> needConfirm = new ArrayList<>();
            List<String> conflicts = new ArrayList<>();
            for(UserworksEntity entity:entitiesToSave){
                    switch(userworkService.create(entity)){
                        case "SUCCESS":
                            succeeded.add(entity);
                            break;
                        case "ERROR":
                            EventsEntity conflict = userworkService.getConflict(entity);
                            conflicts.add(conflict.getTitle());
                            error.add(entity);
                            break;
                        case "EDIT":
                            needConfirm.add(entity);
                            break;
                    }

            }
            jsonResult = new HashMap<>();
            jsonResult.put("success", succeeded);
            jsonResult.put("errors", error);
            jsonResult.put("confirm", needConfirm);
            jsonResult.put("conflicts", conflicts);
            return SUCCESS;
        }catch (Exception e){
            return ERROR;
        }
    }

    public String merge(){
        if(userworks == null){
            return ERROR;
        }
        for(UserworksEntity tomerge:userworks){
            userworkService.mergeEvent(tomerge);
        }
        return SUCCESS;
    }

    //Utility Methods
    public static int[] convertIntegers(List<Integer> integers)
    {
        int[] ret = new int[integers.size()];
        Iterator<Integer> iterator = integers.iterator();
        for (int i = 0; i < ret.length; i++)
        {
            ret[i] = iterator.next().intValue();
        }
        return ret;
    }

    @Override
    public void validate() {
        super.validate();
        if(userwork != null){
            boolean datesexist = true;
            if(userwork.getStartTime() == null){
                datesexist = false;
                addFieldError("workstartdate", getText("userwork_startdate_required"));
            }
            if(userwork.getEndTime() == null){
                datesexist = false;
                addFieldError("workenddate", getText("userwork_enddate_required"));
            }
            if(userwork.getUser() == null && (users == null || users.length == 0)){
                addFieldError("users", getText("userwork_user_required"));
            }
            if(datesexist && !userwork.getStartTime().before(userwork.getEndTime())){
                addActionError(getText("createEdit_StartdateAfterEnddate"));
            }
        }
    }

    //Getters and Setters
    public void setUserworksbyEventId(int eventid){
        this.eventid = eventid;
        userworks = userworkService.findByEvent(eventid);
    }

    public int getEventid(){
        return eventid;
    }

    public int getEvent(){
        if(eventid == 0) {
            return userwork.getEvent().getId();
        }
        return eventid;
    }

    public void setEventId(int id){
        eventid = id;
    }

    public void setUserworkid(int userworkid){
        this.userworkid = userworkid;
    }

    public UserworksEntity getUserwork() {
        return userwork;
    }

    public List<UserworksEntity> getUserworks() {
        return userworks;
    }

    public void setUserwork(int id){
        this.userwork = userworkService.find(id);
    }

    public String getMessage(){
        return message;
    }

    public UserworkService getUserworkService() {
        return userworkService;
    }

    public void setUserworkService(UserworkService userworkService) {
        this.userworkService = userworkService;
    }

    public void setWorkstartdate(String string){
        String[] list = string.trim().split(",");
        try{
            LocalTime time = new LocalTime(0,0,0);
            if(list.length>=2) {
                try{
                    time = DateTimeFormat.forPattern("HH:mm").parseLocalTime(list[1].trim());
                }catch (Exception e){}
            }
            LocalDate date = DateTimeFormat.forPattern("dd.MM.yyyy").parseLocalDate(list[0].trim());
            LocalDateTime dateTime = date.toLocalDateTime(time);
            if(userwork == null){
                userwork = new UserworksEntity();
            }
            userwork.setStartTime(new Timestamp(dateTime.toDate().getTime()));
        }catch(Exception e){}
    }

    public void setWorkenddate(String string){
        String[] list = string.trim().split(",");
        try{
            LocalTime time = new LocalTime(23,59,59);
            if(list.length>=2) {
                try {
                    time = DateTimeFormat.forPattern("HH:mm").parseLocalTime(list[1].trim());
                }catch (Exception e){}
            }
            LocalDate date = DateTimeFormat.forPattern("dd.MM.yyyy").parseLocalDate(list[0].trim());
            LocalDateTime dateTime = date.toLocalDateTime(time);
            if(userwork == null){
                userwork = new UserworksEntity();
            }
            userwork.setEndTime(new Timestamp(dateTime.toDate().getTime()));
        }catch(Exception e){}
    }
    public UsersService getUsersService() {
        return usersService;
    }

    public void setUsersService(UsersService usersService) {
        this.usersService = usersService;
    }

    public void setUsers(String id){
        if(userwork == null){
            userwork = new UserworksEntity();
        }
        try{
            userwork.setUser(usersService.findById(Integer.parseInt(id)));
            return;
        }catch(Exception e){}
        String[] ids = id.split(",");
        List<Integer> idsints = new ArrayList<Integer>();
        for(String i : ids){
            try{
                idsints.add(Integer.parseInt(i.trim()));
            }catch (Exception e){}
        }
        try {
            users = convertIntegers(idsints);
        }catch(Exception e){}
    }

    public void setEvent(int id){
        if(userwork == null){
            userwork = new UserworksEntity();
        }
        userwork.setEvent(eventsService.findById(id));
    }

    public void setUser(int id){
        if(userwork == null){
            userwork = new UserworksEntity();
        }
        userwork.setUser(usersService.findById(id));
    }

    public String getJson(){
        class UserworkStrategy implements ExclusionStrategy {

            public boolean shouldSkipClass(Class<?> arg0) {
                return false;
            }

            public boolean shouldSkipField(FieldAttributes f) {
                List<String> attributesToMap = new ArrayList<String>(){{
                    add("startTime");
                    add("endTime");
                    add("user");
                    add("event");
                    add("id");
                }};
                return !attributesToMap.contains(f.getName());
            }

        }
        Gson gson = new GsonBuilder().setExclusionStrategies(new UserworkStrategy()).create();
        return gson.toJson(jsonResult);
    }

    public void setConfirm(String source){
        Gson gson = new Gson();
        userworks = gson.fromJson(source, new TypeToken<List<UserworksEntity>>(){}.getType());
    }
}
