package at.binarycheese.pixi.actions;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.I18nInterceptor;
import org.apache.struts2.interceptor.SessionAware;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Locale;
import java.util.Map;

/**
 * Created by Joyce on 07.12.2014.
 */
public class SharedAction extends ActionSupport implements SessionAware {
    String lang = "de";
    private Map<String, Object> userSession;
    private String redirectUrl = "";

    public String NavBar() {
        return SUCCESS;
    }

    public String changeLanguage() {
        HttpServletRequest request = ServletActionContext.getRequest();
        redirectUrl = Arrays.asList(request.getHeader("referer").split("^[?]$")).get(0);

        Locale locale = new Locale(lang);
        ActionContext.getContext().setLocale(locale);
        userSession.put(I18nInterceptor.DEFAULT_SESSION_ATTRIBUTE, locale);
        return SUCCESS;
    }


    //Getters and Setters
    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    @Override
    public void setSession(Map<String, Object> map) {
        userSession = map;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }
}
