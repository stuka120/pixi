package at.binarycheese.pixi.actions;

import at.binarycheese.pixi.domain.UsersEntity;
import at.binarycheese.pixi.domain.WorkingtimesEntity;
import at.binarycheese.pixi.service.UsersService;
import at.binarycheese.pixi.service.WorkingTimesService;
import at.binarycheese.pixi.service.interfaces.WorkingTimeActionInterface;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Tortuga on 23.11.2014.
 */
public class WorkingtimeAction extends ActionSupport implements WorkingTimeActionInterface {
    private List<WorkingtimesEntity> workingtimesList;
    private WorkingtimesEntity workingtimesEntity;
    private Map<String, ArrayList<Timestamp>> menuDates;
    private Timestamp date;
    private String starttime;
    private String endtime;
    private int id;
    private String timeRegex = "^([0-9]|0[0-9]|1[0-9]|2[0-4]):[0-5][0-9]";
    private String inDate;
    private String inEndHolidayDate;
    private boolean holiday;
    private Timestamp endHolidayDate;
    private List<Timestamp> selectedDates;
    private InputStream fileInputStream;
    private int selectedUserForExport;
    private List<UsersEntity> usersEntityList;
    private String documentName;

    @Autowired
    private UsersService usersService;
    @Autowired
    private WorkingTimesService workingTimesService;

    public String browse() {
        //There are more depending Transactions --> In Service Layer

        return workingTimesService.browse(this);
    }

    public String menu() {
        //There's just one transaction --> Leave it in the Action Class

        int userId = selectedUserForExport;
        if(userId == 0){
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            userId = usersService.findByUsername(auth.getName()).getId();
        }

        //generate list of all years and months for menu
        List<Timestamp> allDatesByUserId = workingTimesService.findAllDatesByUserId(userId);
        Collections.sort(allDatesByUserId);
        menuDates = new LinkedHashMap<>();
        final SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");
        for (final Timestamp item : allDatesByUserId) {
            String year = yearFormat.format(item);
            Timestamp colone = (Timestamp) item.clone();
            colone.setMinutes(0);
            colone.setHours(0);
            colone.setSeconds(0);
            colone.setDate(1);
            if (!menuDates.containsKey(year)) {
                menuDates.put(year, new ArrayList<Timestamp>());
            }
            if (!menuDates.get(year).contains(colone)) {
                menuDates.get(year).add(colone);
            }
        }
        return SUCCESS;
    }

    public String table() {
        // This method's queries are independent --> We leave it in the Action Method
        int userId = 0;
        if (selectedUserForExport == 0) {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            userId = usersService.findByUsername(auth.getName()).getId();
        } else {
            userId = selectedUserForExport;
        }

        if (selectedDates == null) {
            return SUCCESS;
        }
        workingtimesList = workingTimesService.findAllByUserId(userId);
        for (Iterator<WorkingtimesEntity> it = workingtimesList.iterator(); it.hasNext(); ) {
            WorkingtimesEntity nextEntity = it.next();
            Timestamp compareDate = (Timestamp) nextEntity.getStartTime().clone();
            compareDate.setMinutes(0);
            compareDate.setSeconds(0);
            compareDate.setHours(0);
            compareDate.setDate(1);
            compareDate.setNanos(0);
            if (!selectedDates.contains(compareDate)) {
                it.remove();
            }
        }
        return SUCCESS;
    }

    public String input() {
        // I leave this in the Action, because there's only one transaction
        if (id != 0) {
            workingtimesEntity = workingTimesService.findById(id);
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
            if (workingtimesEntity.getStartTime() != null) {
                starttime = sdf.format(workingtimesEntity.getStartTime());
                inDate = new SimpleDateFormat("dd.MM.yyyy").format(workingtimesEntity.getStartTime());
                date = workingtimesEntity.getStartTime();
            }
            if (workingtimesEntity.getEndTime() != null) {
                endtime = sdf.format(workingtimesEntity.getEndTime());
                if(endtime.equals("00:00")){
                    endtime = "24:00";
                }
            }

        }
        return INPUT;
    }

    @Override
    public void validate() {

        //This method uses just independent queries --> leave it in the Action
        if (date == null) {
            addFieldError("dateErr", getText("dateError"));
        }
        if (!holiday) {
            if (workingtimesEntity.getDuration() == null) {
                addFieldError("workingtimesEntity.durationErr", getText("durationError"));
            }
            if (workingtimesEntity.getDuration() != null && workingtimesEntity.getDuration() > 24) {
                addFieldError("workingtimesEntity.durationErr", getText("durationErrorTooLong"));
            }
            if (starttime != null && !starttime.isEmpty() && !starttime.matches(timeRegex)) {
                addFieldError("starttimeErr", getText("timeError"));
            }
            if (endtime != null && !endtime.isEmpty() && !endtime.matches(timeRegex)) {
                addFieldError("endtimeErr", getText("timeError"));
            }
            if (workingtimesEntity.getId() == 0 && date != null && !workingTimesService.checkEntryForUserOnDateIsPossible(usersService.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).getId(), date)) {
                addFieldError("dateErr", getText("doubleDateEntryError"));
            }
            if(starttime== null || starttime.equals("")){
                addFieldError("starttimeErr", getText("startTimeErr"));
            }
            if(endtime == null || endtime.equals("")){
                addFieldError("endtimeErr", getText("endTimeErr"));
            }
        } else {
            if (endHolidayDate == null) {
                addFieldError("endHolidayDateErr", getText("endHolidayDateErr"));
            } else if (endHolidayDate.before(date)) {
                addFieldError("dateErr", getText("timeMaschineError"));
            }
            if(!workingTimesService.CheckHolidayCount(this)){
                addFieldError("dateErr", getText("noHoliday"));
            }
        }
    }

    public String delete() {
        workingTimesService.delete(id);
        return SUCCESS;
    }

    public String create() throws Exception {
            return workingTimesService.createWorkingTimeEntity(this);

    }

    public String export() throws Exception {
        //Just independent Transactions --> We leave it in the ActionClass

        int userId = selectedUserForExport;
        if (selectedDates == null) {
            return ERROR;
        }
        workingtimesList = workingTimesService.findAllByUserId(userId);
        Map<Timestamp, WorkingtimesEntity> entries = new HashMap<>();
        for (Iterator<WorkingtimesEntity> it = workingtimesList.iterator(); it.hasNext(); ) {
            WorkingtimesEntity nextEntity = it.next();
            Timestamp compareDate = (Timestamp) nextEntity.getStartTime().clone();
            compareDate.setMinutes(0);
            compareDate.setSeconds(0);
            compareDate.setHours(0);
            compareDate.setDate(1);
            compareDate.setNanos(0);
            if (!selectedDates.contains(compareDate)) {
                it.remove();
            } else {
                entries.put(new Timestamp(nextEntity.getStartTime().getYear(),
                        nextEntity.getStartTime().getMonth(),
                        nextEntity.getStartTime().getDate(), 1, 1, 1, 1), nextEntity);
            }
        }

        HSSFWorkbook workbook = new HSSFWorkbook();
        SimpleDateFormat MMyyyy = new SimpleDateFormat("MMMMMM yyyy");
        SimpleDateFormat MMM = new SimpleDateFormat("MMM");
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");

        HSSFCellStyle borderStyle = workbook.createCellStyle();
        borderStyle.setBorderBottom(CellStyle.BORDER_THIN);
        borderStyle.setBorderLeft(CellStyle.BORDER_THIN);
        borderStyle.setBorderRight(CellStyle.BORDER_THIN);
        borderStyle.setBorderTop(CellStyle.BORDER_THIN);

        HSSFCellStyle styleRedBackground = workbook.createCellStyle();
        styleRedBackground.setFillForegroundColor(IndexedColors.RED.getIndex());
        styleRedBackground.setFillPattern(CellStyle.SOLID_FOREGROUND);

        HSSFCellStyle styleRedBackgroundAndBorder = workbook.createCellStyle();
        styleRedBackgroundAndBorder.setBorderBottom(CellStyle.BORDER_THIN);
        styleRedBackgroundAndBorder.setBorderLeft(CellStyle.BORDER_THIN);
        styleRedBackgroundAndBorder.setBorderRight(CellStyle.BORDER_THIN);
        styleRedBackgroundAndBorder.setBorderTop(CellStyle.BORDER_THIN);
        styleRedBackgroundAndBorder.setFillForegroundColor(IndexedColors.RED.getIndex());
        styleRedBackgroundAndBorder.setFillPattern(CellStyle.SOLID_FOREGROUND);

        HSSFCellStyle styleHeadline = workbook.createCellStyle();
        styleHeadline.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
        styleHeadline.setFillPattern(CellStyle.SOLID_FOREGROUND);

        for (Timestamp selectedDate : selectedDates) {
            if (selectedDate == null) {
                continue;
            }

            Object[] headline = new Object[]{MMM.format(selectedDate), "An", "Ab", "Zeit", "Beschreibung"};
            HSSFSheet sheet = workbook.createSheet(MMyyyy.format(selectedDate));
            Row row = sheet.createRow(0);
            int cellnum = 0;
            for (Object obj : headline) {
                Cell cell = row.createCell(cellnum++);
                if (obj instanceof String) {
                    cell.setCellValue((String) obj);
                } else if (obj instanceof Integer) {
                    cell.setCellValue((Integer) obj);
                }
                cell.setCellStyle(styleHeadline);
            }
            sheet.autoSizeColumn(4);

            int i = 0;
            int workDaysThisMonth = 0;
            Calendar mycal = new GregorianCalendar(selectedDate.getYear(), selectedDate.getMonth(), selectedDate.getDate());
            for (i = 1; i <= mycal.getActualMaximum(Calendar.DAY_OF_MONTH); i++) {
                row = sheet.createRow(i);
                Cell cell = row.createCell(0);
                cell.setCellValue(i);
                sheet.autoSizeColumn(0);
                Timestamp key = new Timestamp(selectedDate.getYear(), selectedDate.getMonth(), i, 1, 1, 1, 1);
                boolean so = false;
                boolean sa = false;
                if (key.getDay() == 6) {
                    sa = true;
                }
                if (key.getDay() == 0) {
                    so = true;
                }
                if(!sa && !so && checkHoliday(key.getYear(), key.getMonth() + 1, key.getDate()).isEmpty() ){
                    workDaysThisMonth++;
                }

                if (entries.containsKey(key)) {
                    WorkingtimesEntity entry = entries.get(key);
                    cell = row.createCell(1);
                    if (entry.getStartTime() != null && (entry.getStartTime().getHours() + entry.getStartTime().getMinutes()) != 0) {
                        cell.setCellValue(timeFormat.format(entry.getStartTime()));
                    } else {
                        cell.setCellValue("-");
                    }
                    cell = row.createCell(2);
                    cell.setCellValue(entry.getEndTime() != null ? timeFormat.format(entry.getEndTime()) : "-");
                    cell = row.createCell(3);
                    if (entry.getDuration() != null) {
                        cell.setCellValue(entry.getDuration());
                    }
                    cell = row.createCell(4);
                    if (entry.getDesc() == "") {
                        if (sa) {
                            cell.setCellValue("SA");
                        }
                        if (so) {
                            cell.setCellValue("SO");
                        }
                    } else {
                        cell.setCellValue(entry.getDesc());
                    }
                    if (sa || so || entry.getDesc().equals("Urlaub")) {
                        for (int x = 1; x < 4; x++) {
                            row.getCell(x).setCellStyle(styleRedBackground);
                        }
                    }
                } else if (sa || so) {
                    cell = row.createCell(4);
                    cell.setCellValue(sa ? "SA" : "SO");
                    for (int x = 1; x < 4; x++) {
                        row.createCell(x).setCellStyle(styleRedBackground);
                    }
                } else {
                    String holiday = checkHoliday(key.getYear(), key.getMonth() + 1, key.getDate());
                    if (!holiday.isEmpty()) {
                        cell = row.createCell(4);
                        cell.setCellValue(holiday);
                        for (int x = 1; x < 4; x++) {
                            row.createCell(x).setCellStyle(styleRedBackground);
                        }
                    }
                }
                for (int y = 0; y < 4; y++) {
                    cell = row.getCell(y);
                    if (cell == null) {
                        cell = row.createCell(y);
                    }
                    if (cell.getCellStyle().getFillForegroundColor() == styleRedBackground.getFillForegroundColor()) {
                        cell.setCellStyle(styleRedBackgroundAndBorder);
                    } else {
                        cell.setCellStyle(borderStyle);
                    }
                }
            }

            row = sheet.getRow(0);
            Cell cell = row.createCell(8);
            cell.setCellValue("Stunden Gesamt");
            cell.setCellStyle(borderStyle);
            row.createCell(9).setCellStyle(borderStyle);
            row.createCell(10).setCellStyle(borderStyle);
            cell = row.createCell(11);
            cell.setCellType(HSSFCell.CELL_TYPE_FORMULA);
            cell.setCellFormula("SUM(D2:D" + (i - 1) + ")");
            cell.setCellStyle(borderStyle);

            row = sheet.getRow(1);
            cell = row.createCell(8);
            cell.setCellValue("Stunden soll");
            cell.setCellStyle(borderStyle);
            row.createCell(9).setCellStyle(borderStyle);
            row.createCell(10).setCellStyle(borderStyle);
            cell = row.createCell(11);
            cell.setCellStyle(borderStyle);
            double sollstundenDaily = usersService.findById(userId).getPlannedHours();
            double sollstunden = 0;
            if (sollstundenDaily == 0) {
                sollstunden = 8 * workDaysThisMonth;
            } else {
                sollstunden = sollstundenDaily * workDaysThisMonth;
            }
            cell.setCellValue(sollstunden);

            row = sheet.getRow(2);
            cell = row.createCell(8);
            cell.setCellValue(getText("ueberstunden"));
            cell.setCellStyle(borderStyle);
            row.createCell(9).setCellStyle(borderStyle);
            row.createCell(10).setCellStyle(borderStyle);
            cell = row.createCell(11);
            cell.setCellType(HSSFCell.CELL_TYPE_FORMULA);
            cell.setCellFormula("L1-L2");
            cell.setCellStyle(borderStyle);

            Timestamp lastMonth = new Timestamp(selectedDate.getYear(), selectedDate.getMonth() - 1, selectedDate.getDate(), 0, 0, 0, 0);
            if (selectedDates.contains(lastMonth)) {
                row = sheet.getRow(3);
                cell = row.createCell(8);
                cell.setCellValue(getText("ueberstunden")+" Vormonat");
                cell.setCellStyle(borderStyle);
                row.createCell(9).setCellStyle(borderStyle);
                row.createCell(10).setCellStyle(borderStyle);
                cell = row.createCell(11);
                cell.setCellType(HSSFCell.CELL_TYPE_FORMULA);
                String sheetname = "'" + MMyyyy.format(lastMonth) + "'";
                String formula = sheetname + "!L5";
                cell.setCellFormula(formula);
                cell.setCellStyle(borderStyle);

            }
            row = sheet.getRow(4);
            cell = row.createCell(8);
            cell.setCellValue(getText("ueberstunden")+" Stand Akt.");
            cell.setCellStyle(borderStyle);
            row.createCell(9).setCellStyle(borderStyle);
            row.createCell(10).setCellStyle(borderStyle);
            cell = row.createCell(11);
            cell.setCellType(HSSFCell.CELL_TYPE_FORMULA);
            cell.setCellFormula("L3+L4");
            cell.setCellStyle(borderStyle);

            row = sheet.getRow(6);
            cell = row.createCell(8);
            cell.setCellValue("Urlaubstage:");
            cell.setCellStyle(borderStyle);
            row.createCell(9).setCellStyle(borderStyle);
            row.createCell(10).setCellStyle(borderStyle);
            cell = row.createCell(11);
            cell.setCellValue(workingTimesService.getUsedHolidayUntilMonth(userId, selectedDate));
            cell.setCellStyle(borderStyle);

            sheet.autoSizeColumn(1);
            sheet.autoSizeColumn(2);
            sheet.autoSizeColumn(3);
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        workbook.write(baos);
        fileInputStream = new ByteArrayInputStream(baos.toByteArray());
        SimpleDateFormat documentNameFormat = new SimpleDateFormat("yyyy_MM_dd");
        documentName = documentNameFormat.format(new Date()) + ".xls";

        return SUCCESS;
    }

    public String checkHoliday(int year, int monat, int tag) {
        year += 1900;
        //Check fixed Holidays
        if (tag == 1 && monat == 1) {
            return getText("newyear");
        }
        if (tag == 6 && monat == 1) {
            return getText("threeKings");
        }
        if (tag == 1 && monat == 5) {
            return getText("nationalHoliday");
        }
        if (tag == 15 && monat == 8) {
            return getText("assumptionDay");
        }
        if (tag == 26 && monat == 10) {
            return getText("AustrianNationalDay");
        }
        if (tag == 1 && monat == 11) {
            return getText("AllHallows");
        }
        if (tag == 15 && monat == 11) {
            return getText("StLeopoldsday");
        }
        if (tag == 8 && monat == 12) {
            return getText("ImmaculateConception");
        }
        if (tag == 25 && monat == 12) {
            return getText("christmasDay");
        }
        if (tag == 26 && monat == 12) {
            return getText("boxingDay");
        }

        //Check easterSunday and Dependent Holidays
        int day;
        int month;

        int a = year % 19;
        int b = year % 4;
        int c = year % 7;
        int k = (int) (year / 100);
        int p = (int) ((8 * k + 13) / 25);
        int q = (int) (k / 4);
        int M = (15 + k - p - q) % 30;
        int d = (19 * a + M) % 30;
        int N = (4 + k - q) % 7;
        int e = (2 * b + 4 * c + 6 * d + N) % 7;
        int ostern = (22 + d + e);
        if (ostern > 31) {
            month = 4;
            day = ostern - 31;
        } else {
            month = 3;
            day = ostern;
        }
        Date eastersunday = new Date(year, month - 1, day);
        Date isDate = new Date(year, monat - 1, tag);
        int timeDiff = (int) (isDate.getTime() / (1000 * 60 * 60 * 24) - eastersunday.getTime() / (1000 * 60 * 60 * 24));
        if (timeDiff == 0) {
            return getText("easterSunday");
        } else if (timeDiff == 1) {
            return getText("easterMonday");
        } else if (timeDiff == 39) {
            return getText("AscensionDay");
        } else if (timeDiff == 49) {
            return getText("whitsunday");
        } else if (timeDiff == 50) {
            return getText("whitmonday");
        } else if (timeDiff == 60) {
            return getText("corpusChristi");
        } else {
            return "";
        }
    }

    //getters and setters

    public InputStream getFileInputStream() {
        return fileInputStream;
    }

    public void setFileInputStream(InputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }

    public List<WorkingtimesEntity> getWorkingtimesList() {
        return workingtimesList;
    }

    public void setWorkingtimesList(List<WorkingtimesEntity> workingtimesList) {
        this.workingtimesList = workingtimesList;
    }

    public WorkingtimesEntity getWorkingtimesEntity() {
        return workingtimesEntity;
    }

    public void setWorkingtimesEntity(WorkingtimesEntity workingtimesEntity) {
        this.workingtimesEntity = workingtimesEntity;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getInDate() {
        return inDate;
    }

    public void setInDate(String inDate) {
        this.inDate = inDate;
    }

    public boolean isHoliday() {
        return holiday;
    }

    public void setHoliday(boolean holiday) {
        this.holiday = holiday;
    }

    public Timestamp getEndHolidayDate() {
        return endHolidayDate;
    }

    public void setEndHolidayDate(Timestamp endHolidayDate) {
        this.endHolidayDate = endHolidayDate;
    }

    public String getInEndHolidayDate() {
        return inEndHolidayDate;
    }

    public void setInEndHolidayDate(String inEndHolidayDate) {
        this.inEndHolidayDate = inEndHolidayDate;
    }

    public Map<String, ArrayList<Timestamp>> getMenuDates() {
        return menuDates;
    }

    public void setMenuDates(Map<String, ArrayList<Timestamp>> menuDates) {
        this.menuDates = menuDates;
    }

    public List<Timestamp> getSelectedDates() {
        return selectedDates;
    }

    public void setSelectedDates(List<Timestamp> selectedDates) {
        this.selectedDates = selectedDates;
    }

    public int getSelectedUserForExport() {
        return selectedUserForExport;
    }

    public void setSelectedUserForExport(int selectedUserForExport) {
        this.selectedUserForExport = selectedUserForExport;
    }

    public List<UsersEntity> getUsersEntityList() {
        return usersEntityList;
    }

    public void setUsersEntityList(List<UsersEntity> usersEntityList) {
        this.usersEntityList = usersEntityList;
    }

    public UsersService getUsersService() {
        return usersService;
    }

    public void setUsersService(UsersService usersService) {
        this.usersService = usersService;
    }

    public WorkingTimesService getWorkingTimesService() {
        return workingTimesService;
    }

    public void setWorkingTimesService(WorkingTimesService workingTimesService) {
        this.workingTimesService = workingTimesService;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }
}
