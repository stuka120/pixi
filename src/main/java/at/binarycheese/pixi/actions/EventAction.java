package at.binarycheese.pixi.actions;

import at.binarycheese.pixi.domain.ContactsEntity;
import at.binarycheese.pixi.domain.EventcategoriesEntity;
import at.binarycheese.pixi.domain.EventsEntity;
import at.binarycheese.pixi.repositories.ContactsRepository;
import at.binarycheese.pixi.repositories.UsersRepository;
import at.binarycheese.pixi.service.ContactsService;
import at.binarycheese.pixi.service.EventcategoriesService;
import at.binarycheese.pixi.service.EventsService;
import at.binarycheese.pixi.domain.*;
import at.binarycheese.pixi.service.*;
import at.binarycheese.pixi.service.interfaces.EventActionInterface;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;


import net.fortuna.ical4j.data.CalendarBuilder;
import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.Component;
import net.fortuna.ical4j.model.Property;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.commons.io.FileUtils;
import org.apache.poi.xwpf.model.XWPFHeaderFooterPolicy;
import org.apache.poi.xwpf.usermodel.*;
import org.apache.struts2.ServletActionContext;
import org.h2.engine.User;
import org.joda.time.*;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONObject;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTP;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTR;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSectPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTText;
import org.springframework.beans.factory.annotation.Autowired;
import org.joda.time.format.ISODateTimeFormat;
import org.springframework.format.datetime.joda.LocalDateParser;
import org.springframework.format.datetime.joda.LocalDateTimeParser;
import org.springframework.security.core.context.SecurityContextHolder;

import java.io.*;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.*;
import java.time.temporal.TemporalUnit;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Tortuga on 08.11.2014.
 */
public class EventAction extends ActionSupport implements ModelDriven, EventActionInterface {
    //EventEntity
    EventsEntity event = new EventsEntity();
    List<EventsEntity> eventList = new ArrayList<EventsEntity>();
    //Used by most of the Events
    int eventId;

    int eventContactPerson;
    //Daos

    @Autowired
    EventsService eventsService;
    @Autowired
    ContactsService contactsService;
    @Autowired
    EventcategoriesService eventcategoriesService;
    @Autowired
    UsersService usersService;
    @Autowired
    ProjectsService projectsService;
    @Autowired
    UsersRepository usersRepository;
    //EventCategory
    public List<EventcategoriesEntity> categoriesList;
    int eventCategory;
    //Contacts
    public List<ContactsEntity> contactsList;
    //Projects
    public List<ProjectEntity> projectList;

    //Message for Edit
    String message;
    //For Upload
    File uploadedFile;
    int selectedCategorie;
    int amountWrong;
    //For changeCategorieColor
    EventcategoriesEntity eventcategorie;
    String fileName;
    JSONArray eventMediaJson;
    private InputStream downloadEventMedia;

    Integer exportmonth;
    Integer exportyear;

    int exportProjectId;

    Map<String, String> exportmonths;

    InputStream fileInputStream;
    private String filename;
    private File fileUpload;
    private String contentType;
    private boolean fullday;


    Time endtime;
    Time starttime;

    boolean showChecklist;

    public String execute() {
        return SUCCESS;
    }

    public String createEvent() {
        categoriesList = eventcategoriesService.findAll();
        projectList = projectsService.findAllActive();
        return SUCCESS;
    }

    public String userWork() {
        try {
            this.event = eventsService.findByIdWithUserworks(eventId);
            return Action.SUCCESS;
        } catch (Exception e) {
            return Action.ERROR;
        }
    }

    public String addEvent() {
        executeFullDay();
        return eventsService.saveEntity(this);
    }

    public String editEvent() {
        event = eventsService.findById(eventId);
        categoriesList = eventcategoriesService.findAll();
        projectList = projectsService.findAllActive();
        if (event == null) {
            return ERROR;
        }
        return SUCCESS;
    }

    public String eventMediaJson() {
        eventMediaJson = new JSONArray();
        event = eventsService.findById(eventId);
        if (event == null) {
            return ERROR;
        }
        File folder = null;
        try {
            String path = ServletActionContext.getServletContext().getRealPath("/upload/" + eventId);
            folder = new File(path);
        } catch (NullPointerException e) {
            e.printStackTrace();
            return ERROR;
        }
        if (!folder.exists()) {
            return ERROR;
        }
        for (File f : folder.listFiles()) {
            if (f.isFile()) {
                JSONObject file = new JSONObject();
                file.put("text", f.getName());
                file.put("link", "/event/download?filePath=" + f.getAbsolutePath());
                eventMediaJson.put(file);
            }
        }
        return SUCCESS;
    }

    public String eventMedia() {
        try {
            event = eventsService.findById(eventId);

            File folder = new File(ServletActionContext.getServletContext().getRealPath("/upload/" + eventId));
            if (!folder.exists()) {
                folder.mkdir();
            }
            ArrayList<String> list = new ArrayList<String>();
            for (File f : folder.listFiles()) {
                list.add("/upload/" + eventId + "/" + f.getName());
            }
        } catch (Exception e) {
            return ERROR;
        }
        if (event == null) {
            return ERROR;
        }
        return SUCCESS;
    }

    public String deleteMedia() {
        String falseFilePath = "/" + "upload" + "/" + eventId;
        String filePath = ServletActionContext.getServletContext().getRealPath(falseFilePath);
        File file = new File(filePath + File.separator + fileName);
        if (FileUtils.deleteQuietly(file)) {
            return SUCCESS;
        }
        return ERROR;
    }

    public String updateEvent() {
        executeFullDay();
        return eventsService.update(this);
    }

    public String deleteEvent() {
        try {
            eventsService.delete(eventId);
        } catch (RuntimeException e) {
            return ERROR;
        }
        return SUCCESS;
    }

    public String listEvents() {
        Date today = new Date();
        Date todayPlusWeek = new Date(today.getTime() + (1000 * 60 * 60 * 24) * 7);

        showChecklist = usersRepository.isResponsible(SecurityContextHolder.getContext().getAuthentication().getName(), new Timestamp(today.getTime()), new Timestamp(todayPlusWeek.getTime()));
        if (showChecklist) {
            eventList = eventsService.getResponsibleEvents(SecurityContextHolder.getContext().getAuthentication().getName());
        }
        exportmonths = new HashMap<String, String>();
        categoriesList = eventcategoriesService.findAll();
        for (EventsEntity e : eventsService.findAll()) {
            java.util.Calendar cal = java.util.Calendar.getInstance(this.getLocale());
            cal.setTimeInMillis(e.getStartDate().getTime());
            exportmonths.put(cal.get(java.util.Calendar.YEAR) + "" + String.format("%02d", cal.get(java.util.Calendar.MONTH)), getMonthForInt(e.getStartDate().getMonth()) + " - " + cal.get(java.util.Calendar.YEAR));
        }
        exportmonths = new TreeMap<String, String>(exportmonths);

        projectList = projectsService.findAll();
        return SUCCESS;
    }

    String getMonthForInt(int num) {
        String month = "wrong";
        DateFormatSymbols dfs = new DateFormatSymbols(getLocale());
        String[] months = dfs.getMonths();
        if (num >= 0 && num <= 11) {
            month = months[num];
        }
        return month;
    }

    public String getUpload() {
        JSONObject array = new JSONObject();
        String falseFilePath = "/" + "upload" + "/" + eventId;
        String filePath = ServletActionContext.getServletContext().getRealPath(falseFilePath);
        File newFile = new File(filePath + File.separator + filename);
        try {
            FileUtils.copyFile(this.uploadedFile, newFile);
        } catch (IOException e) {
            e.printStackTrace();
            array.append("error", "File Not Uploaded");
            return ERROR;
        }
        array.append("filepath", File.separator + "upload" + File.separator + eventId + File.separator + newFile.getName());
        return SUCCESS;
    }

    public String changeCategorieColor() {
        try {
            EventcategoriesEntity a = eventcategoriesService.findById(eventcategorie.getId());
            a.setColor(eventcategorie.getColor());
            eventcategoriesService.update(a);
        } catch (Exception e) {
        }
        return SUCCESS;
    }

    public String importXmlFromGoogle() {
        return eventsService.importXmlFromGoogle(this);
    }

    public List<EventsEntity> readFromIcsAndReturnEventEntities() {
        List<EventsEntity> list = new ArrayList<>();
        Calendar calendar = null;
        try {
            FileInputStream fileInputStream = new FileInputStream(uploadedFile);
            CalendarBuilder builder = new CalendarBuilder();
            calendar = builder.build(fileInputStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            addActionError("Das File konnte nicht gelesen werden");
        } catch (net.fortuna.ical4j.data.ParserException e) {
            addFieldError("errorMessage", getText("import_wrongFormat"));
        } catch (IOException e) {
            e.printStackTrace();
            addActionError("Das File konnte nicht gelesen werden");
        }
        DateFormat formatSimple = new SimpleDateFormat("yyyyMMdd");
        DateTimeFormatter formatIso = ISODateTimeFormat.basicDateTimeNoMillis();
        amountWrong = calendar.getComponents().size();
        int id = -1;
        long millisecondsOfADay = 86400000;
        for (Iterator i = calendar.getComponents().iterator(); i.hasNext(); ) {
            id ++;
            Component component = (Component) i.next();
            EventsEntity a = new EventsEntity();
            String title = "";
            String startDate = null;
            String endDate = null;
            String location = "";
            String description = "";
            boolean fullday = false;
            String internTitle = ""+id;

            //Get Title
            try {
                title = component.getProperty(Property.SUMMARY).getValue();
                internTitle = title;
            } catch (NullPointerException e){}

            //GET STARTDATE: IF NOT ACCESSABLE -> LINE CONVERSION FAILS
            try {
                startDate = component.getProperty(Property.DTSTART).getValue();

            } catch (Exception e) {
                addActionError(internTitle +":"+"Startdate is missing!");
                continue;
            }
            //GET ENDDATUM: IF NOT ACCESSABLE -> FULLDAY EVENT
            try {
                endDate = component.getProperty(Property.DTEND).getValue();

            } catch (Exception e) {
                fullday = true;
            }

            try {
                location = component.getProperty(Property.LOCATION).getValue();
            } catch (NullPointerException e) {
                location = "";
            }
            try {
                description = component.getProperty(Property.DESCRIPTION).getValue();
            } catch (NullPointerException e) {
                description = "";
            }
            //CHECK IF IS FULLDAY
            if(!fullday && startDate.length() == 8 && endDate.length() == 8) {
                try {
                    Date startldate = formatSimple.parse(startDate);
                    Date endldate = formatSimple.parse(endDate);
                    if(endldate.getTime() - startldate.getTime() == millisecondsOfADay){
                        fullday = true;
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            if (startDate.length() == 8) {
                try {
                    a.setStartDate(new Timestamp(formatSimple.parse(startDate).getTime()));
                    if(!fullday) {
                        a.setEndDate(new Timestamp(formatSimple.parse(endDate).getTime()));
                    }
                } catch (ParseException e) {
                    addActionError(internTitle+": "+startDate+" couldn't be parsed to Date");
                    e.printStackTrace();
                }
            } else {
                try {
                    a.setStartDate(new Timestamp(formatIso.parseLocalDateTime(startDate).plusHours(2).toDateTime().getMillis()));
                    if(!fullday) {
                        a.setEndDate(new Timestamp(formatIso.parseLocalDateTime(endDate).plusHours(2).toDateTime().getMillis()));
                    }
                } catch (Exception e) {
                    addActionError(internTitle+": "+startDate+" couldn't be parsed to Date");
                    e.printStackTrace();
                }
            }
            a.setTitle(title);
            a.setLocation(location);
            a.setDescription(description);
            a.setEventcategoriesByEventCategorieId(eventcategoriesService.findById(eventCategory));
            if (a.getStartDate() != null) {
                list.add(a);
            }
        }
        return list;
    }

    public String export() throws Exception {
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("Sample sheet");

        Map<Integer, Object[]> data = new HashMap<Integer, Object[]>();
        data.put(1, new Object[]{"Datum", "Verantwortlicher", "vhd", "Veranstaltung", "Bundesland", "Art", "Team"});
        int start = 2;
        List<EventsEntity> events = null;
        if (exportProjectId == 0) {
            events = eventsService.findAll();
        } else {
            events = eventsService.findAllByProjectId(exportProjectId);
        }
        Collections.sort(events, new Comparator<EventsEntity>() {
            @Override
            public int compare(EventsEntity o1, EventsEntity o2) {
                return o1.getStartDate().compareTo(o2.getStartDate());
            }
        });
        if (events != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
            for (EventsEntity e : events) {
                java.util.Calendar cal = java.util.Calendar.getInstance();
                cal.setTimeInMillis(e.getStartDate().getTime());
                if ((exportmonth == null || exportyear == null) || (cal.get(java.util.Calendar.YEAR) == exportyear && cal.get(java.util.Calendar.MONTH) == exportmonth)) {
                    String surname = "";
                    String name = "";
                    String organisation = "";
                    if (e.getUsersByResponsible() != null) {
                        ContactsEntity contactsEntity = contactsService.findByUserEntity_Username(e.getUsersByResponsible().getUsername());
                        surname = contactsEntity.getSurname();
                        name = contactsEntity.getName();
                        if (contactsEntity.getOrganisationsByOrganisationsId() != null) {
                            organisation = contactsEntity.getOrganisationsByOrganisationsId().getName();
                        }
                    }
                    data.put(start, new Object[]{
                            dateFormat.format(e.getStartDate()),
                            surname + " " + name, "",
                            e.getTitle() + (e.getDescription().trim().equals("") ? "" : " - " + e.getDescription()),
                            e.getLocation(), "",
                            organisation
                    });
                    start++;
                }
            }
            data = new TreeMap<Integer, Object[]>(data);
            Set<Integer> keyset = data.keySet();
            int rownum = 0;
            for (Integer key : keyset) {
                Row row = sheet.createRow(rownum++);
                Object[] objArr = data.get(key);
                int cellnum = 0;
                for (Object obj : objArr) {
                    Cell cell = row.createCell(cellnum++);
                    if (obj instanceof Date)
                        cell.setCellValue((Date) obj);
                    else if (obj instanceof Boolean)
                        cell.setCellValue((Boolean) obj);
                    else if (obj instanceof String)
                        cell.setCellValue((String) obj);
                    else if (obj instanceof Double)
                        cell.setCellValue((Double) obj);
                }
            }
            try {

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                workbook.write(baos);
                fileInputStream = new ByteArrayInputStream(baos.toByteArray());
                for (String s : ServletActionContext.getResponse().getHeaderNames()) {
                    System.err.println(s);
                }
                fileName = exportyear + "_" + ((exportmonth.intValue() + 1 < 10) ? "0" + (exportmonth.intValue() + 1) : (exportmonth.intValue() + 1)) + "_Termine.xls";

            } catch (Exception e) {
                e.printStackTrace();
            }
            return SUCCESS;
        }
        return INPUT;
    }

    @Override
    public void validate() {
        try {
            if (event != null) {
                if (event.getTitle().equals("")) {
                    addFieldError("event.title", getText("createEdit_missingTitle"));
                }
                if (event.getStartDate() == null) {
                    addFieldError("startDate", getText("createEdit_missingDate"));
                } else if (event.getStartDate().after(event.getEndDate()) && !fullday) {
                    addFieldError("startDate", getText("createEdit_StartdateAfterEnddate"));
                }
            }
        } catch (Exception e) {

        }
    }

    public String setChecklistShown() {
        eventsService.setChecklistShown(eventId);
        return SUCCESS;
    }

    public void addTimesToDates() {
        if (starttime != null) {
            this.event.setStartDate(fromDateAndTime(this.event.getStartDate(), starttime));
        }
        if (endtime != null) {
            this.event.setEndDate(fromDateAndTime(this.event.getEndDate(), endtime));
        }
    }

    public static Timestamp fromDateAndTime(Timestamp date, Time time) {
        DateTime t = new DateTime(date);
        t = t.plusHours(time.getHours());
        t = t.plusMinutes(time.getMinutes());
        return new Timestamp(t.getMillis());
    }

    //Functions
    public void executeFullDay() {
        if (fullday) {
            event.setEndDate(null);
        }
    }

    //--------------Getters/Setters-------------------
    //A Methode to look if Event exists
    //If not a entity is created
    public void ensureEventsExistence() {
        //IF Event Not Exists
        if (event == null) {
            //SET IT
            event = new EventsEntity();
        }

    }

    //Converts the Parameter (Time and Date) From the View to  one Timestamp
    public Timestamp timeAndDateStringToTimestamp(String container) {
        String[] values = container.split(",");
        DateTimeFormatter datePattern = DateTimeFormat.forPattern("dd.MM.yyyy");
        DateTimeFormatter timePattern = DateTimeFormat.forPattern("HH:mm");
        LocalDate date = null;
        LocalTime time = new LocalTime("00:00");
        for (String value : values) {
            try {
                date = datePattern.parseLocalDate(value.trim());
            } catch (Exception e) {
            }
            try {
                time = timePattern.parseLocalTime(value.trim());
            } catch (Exception e) {
            }
        }
        if (date == null) {
            return null;
        }
        LocalDateTime dateTime = date.toLocalDateTime(time);
        return new Timestamp(dateTime.toDateTime().getMillis());
    }

    public void setStartDate(String values) {
        //Ensure Events Existence
        ensureEventsExistence();

        //Save them as new StartDate
        event.setStartDate(timeAndDateStringToTimestamp(values));
        //If Enddate does not exist right now set it to the end of the same date
        if (event.getEndDate() == null) {
            Timestamp enddate = (Timestamp) event.getStartDate().clone();
            enddate.setHours(23);
            enddate.setMinutes(59);
            event.setEndDate(enddate);
        }

    }

    public void setEndDate(String values) {
        //Ensure Events Existence
        ensureEventsExistence();

        //Save them as new StartDate
        event.setEndDate(timeAndDateStringToTimestamp(values));
    }

    public File getUploadedFile() {
        return uploadedFile;
    }

    public void setUploadedFile(File uploadedFile) {
        this.uploadedFile = uploadedFile;
    }

    public int getSelectedCategorie() {
        return selectedCategorie;
    }

    public void setSelectedCategorie(int selectedCategorie) {
        this.selectedCategorie = selectedCategorie;
    }

    @Override
    public Object getModel() {
        return event;
    }

    public void setEvent(EventsEntity event) {
        this.event = event;
    }

    public EventsEntity getEvent() {
        return event;
    }

    public void setEventList(List<EventsEntity> eventList) {
        this.eventList = eventList;
    }

    public List<EventsEntity> getEventList() {
        return eventList;
    }

    public List<EventcategoriesEntity> getCategoriesList() {
        return categoriesList;
    }

    public void setCategoriesList(List<EventcategoriesEntity> categoriesList) {
        this.categoriesList = categoriesList;
    }

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public String getStartDate() {
        Format formatter = new SimpleDateFormat("dd.MM.yyyy");
        return formatter.format(event.getStartDate());
    }

    public List<ContactsEntity> getContactsList() {
        return contactsList;
    }

    public void setContactsList(List<ContactsEntity> contactsList) {
        this.contactsList = contactsList;
    }

    public int getEventCategory() {
        return eventCategory;
    }

    public void setEventCategory(int eventCategory) {
        this.eventCategory = eventCategory;
    }

    public int getEventContactPerson() {
        return eventContactPerson;
    }

    public void setEventContactPerson(int eventContactPerson) {
        this.eventContactPerson = eventContactPerson;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getAmountWrong() {
        return amountWrong;
    }

    public void setAmountWrong(int amountWrong) {
        this.amountWrong = amountWrong;
    }

    public EventcategoriesEntity getEventcategorie() {
        return eventcategorie;
    }

    public void setEventcategorie(EventcategoriesEntity eventcategorie) {
        this.eventcategorie = eventcategorie;
    }

    public InputStream getFileInputStream() {
        return fileInputStream;
    }

    public void setExportMonth(String monthyear) {
        int year = 0;
        int month = 0;
        try {
            year = Integer.parseInt(monthyear.substring(0, 4));
            month = Integer.parseInt(monthyear.substring(4, 6));
        } catch (Exception e) {
            return;
        }
        if (!(month < 12 && month >= 0)) {
            return;
        }
        this.exportyear = year;
        this.exportmonth = month;
    }

    public Map<String, String> getExportmonths() {
        return exportmonths;
    }

    public String getCurrentMonth() {
        java.util.Calendar cal = java.util.Calendar.getInstance();
        return cal.get(java.util.Calendar.YEAR) + "" + String.format("%02d", cal.get(java.util.Calendar.MONTH));
    }

    public void setEndtime(String endtime) {
        String pattern = "(\\d{2})[:](\\d{2})";
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(endtime);
        if (m.find()) {
            int hour = Integer.parseInt(m.group(1));
            int minutes = Integer.parseInt(m.group(2));
            Time enddate = new Time(hour, minutes, 0);
            this.endtime = enddate;
        }
    }

    public void setStarttime(String time) {
        String pattern = "(\\d{2})[:](\\d{2})";
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(time);
        if (m.find()) {
            int hour = Integer.parseInt(m.group(1));
            int minutes = Integer.parseInt(m.group(2));
            Time date = new Time(hour, minutes, 0);
            this.starttime = date;
        }
    }

    public String getStarttime() {
        LocalTime time = new DateTime(this.event.getStartDate(), DateTimeZone.getDefault()).toLocalTime();
        return ISODateTimeFormat.hourMinute().print(time);
    }

    public String getEndtime() {
        LocalTime time = new DateTime(this.event.getEndDate(), DateTimeZone.getDefault()).toLocalTime();
        return ISODateTimeFormat.hourMinute().print(time);
    }

    public String getFileName() {
        return fileName;
    }

    public List getEventMediaJson() {
        return EventJsonAction.toList(eventMediaJson);
    }

    public InputStream getDownloadEventMedia() {
        return downloadEventMedia;
    }

    public void setFilePath(String path) {
        try {
            String pattern = "upload.(\\d+).(.+)";

            // Create a Pattern object
            Pattern r = Pattern.compile(pattern);
            Matcher m = r.matcher(path);
            if (m.find()) {
                int id = Integer.parseInt(m.group(1));
                String filename = m.group(2);
                String file = ServletActionContext.getServletContext().getRealPath("/upload/" + id + "/" + filename);
                downloadEventMedia = new FileInputStream(file);
                fileName = filename;
            }
        } catch (Exception e) {
        }
    }

    public String generateReport() {
        try {
            event = eventsService.findByIdWithUserworks(eventId);

            XWPFDocument document = new XWPFDocument(new FileInputStream(ServletActionContext.getServletContext().getRealPath("/assets/wordTemplate/EventReport_Template.docx")));

            XWPFParagraph authorInformationHeadline = document.createParagraph();
            XWPFRun authorInformationHeadlineRun = authorInformationHeadline.createRun();

            authorInformationHeadlineRun.setText("Sachbearbeiter/in:");
            authorInformationHeadlineRun.setUnderline(UnderlinePatterns.SINGLE);
            authorInformationHeadlineRun.setFontSize(9);
            authorInformationHeadlineRun.setBold(true);
            authorInformationHeadline.setSpacingAfter(0);
            authorInformationHeadline.setIndentationLeft(6000);

            XWPFParagraph authorInformation = document.createParagraph();
            XWPFRun authorInformationRun = authorInformation.createRun();

            authorInformationRun.addBreak();
            if (event.getUsersByResponsible() == null) {
                authorInformationRun.setText("<Name hier>");
            } else {
                authorInformationRun.setText(event.getUsersByResponsible().getContactsByContactsId().getName() + " " + event.getUsersByResponsible().getContactsByContactsId().getSurname().toUpperCase());
            }
            authorInformationRun.addBreak();
            authorInformationRun.setText(getText("vereinName"));
            authorInformationRun.addBreak();
            authorInformationRun.setText("Wien 12., Hufelandgasse 4");
            authorInformationRun.addBreak();
            authorInformationRun.setText("Tel.: +43(1) 313-10 DW 45905");
            authorInformationRun.setFontSize(9);
            authorInformation.setSpacingAfter(0);
            authorInformation.setIndentationLeft(6000);

            XWPFParagraph blankspace11 = document.createParagraph();
            XWPFRun blankspace11run = blankspace11.createRun();

            blankspace11run.setFontSize(11);
            blankspace11run.addBreak();
            blankspace11.setSpacingAfter(0);

            XWPFParagraph headline = document.createParagraph();
            headline.setAlignment(ParagraphAlignment.CENTER);
            XWPFRun headlineRun = headline.createRun();

            headlineRun.setFontSize(14);
            headlineRun.setBold(true);
            headlineRun.setText("BERICHT " + event.getTitle());
            headline.setSpacingAfter(0);

            XWPFParagraph blankspace12 = document.createParagraph();
            XWPFRun blankspace12run = blankspace12.createRun();
            blankspace12run.setFontSize(12);
            blankspace12run.addBreak();
            blankspace12.setSpacingAfter(0);


            XWPFTable eventInfoTable = document.createTable();

            XWPFTableRow eventInfoTableRowOne = eventInfoTable.getRow(0);
            eventInfoTableRowOne.getCell(0).setText("Datum:");
            eventInfoTableRowOne.addNewTableCell().setText(new SimpleDateFormat("dd.MM.yyyy, hh:mm").format(event.getStartDate()) + " Uhr");

            XWPFTableRow eventInfoTableRowTwo = eventInfoTable.createRow();
            eventInfoTableRowTwo.getCell(0).setText(getText("tableLocation"));

            XWPFTableRow eventInfoTableRowThree = eventInfoTable.createRow();
            eventInfoTableRowThree.getCell(0).setText("Adresse:");
            eventInfoTableRowThree.getCell(1).setText(event.getLocation());

            XWPFTableRow eventInfoTableRowFour = eventInfoTable.createRow();
            eventInfoTableRowFour.getCell(0).setText("Kontaktperson:");
            if (event.getContactsByContactPerson() != null) {
                eventInfoTableRowFour.getCell(1).setText(getFullname(event.getContactsByContactPerson()));
            } else {
                eventInfoTableRowFour.getCell(1).setText("keine Kontaktperson gefunden!");
            }

            XWPFTableRow eventInfoTableRowFive = eventInfoTable.createRow();
            eventInfoTableRowFive.getCell(0).setText("Team vor Ort: ");
            String teamVorOrt = null;
            for (UserworksEntity userwork : event.getUserworksesById()) {
                ContactsEntity contact = userwork.getUser().getContactsByContactsId();
                if (teamVorOrt == null) {
                    teamVorOrt = getFullname(contact);
                } else {
                    teamVorOrt += ", " + getFullname(contact);
                }
            }
            eventInfoTableRowFive.getCell(1).setText(teamVorOrt);

            XWPFTableRow eventInfoTableRowSix = eventInfoTable.createRow();
            eventInfoTableRowSix.getCell(0).setText("Organistationsteam:");

            XWPFTableRow eventInfoTableRowSeven = eventInfoTable.createRow();
            eventInfoTableRowSeven.getCell(0).setText("Anzahl der anwesenden Personen:");

            XWPFTableRow eventInfoTableRowEight = eventInfoTable.createRow();
            eventInfoTableRowEight.getCell(0).setText(getText("persoenlichkeiten"));

            XWPFTableRow eventInfoTableRowNine = eventInfoTable.createRow();
            eventInfoTableRowNine.getCell(0).setText("Dauer der Veranstaltung:");
            eventInfoTableRowNine.getCell(1).setText(getDuration(event.getStartDate(), event.getEndDate()));

            eventInfoTable.getCTTbl().getTblPr().unsetTblBorders();

            XWPFParagraph bottomLine = document.createParagraph();
            bottomLine.setBorderBottom(Borders.BASIC_BLACK_DASHES);
            bottomLine.setSpacingAfter(0);

            XWPFParagraph reportParagraph = document.createParagraph();
            XWPFRun reportParagraphRun = reportParagraph.createRun();
            reportParagraphRun.addBreak();
            reportParagraphRun.setText("Bericht:");
            reportParagraphRun.setBold(true);
            reportParagraph.setSpacingAfter(0);

            XWPFParagraph blankspaceEnd = document.createParagraph();
            XWPFRun blankspaceEndRun = blankspaceEnd.createRun();
            blankspaceEndRun.addBreak();
            blankspaceEnd.setSpacingAfter(0);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            document.write(baos);
            fileInputStream = new ByteArrayInputStream(baos.toByteArray());
            for (String s : ServletActionContext.getResponse().getHeaderNames()) {
                System.err.println(s);
            }
            fileName = "Eventreport.docx";
            return SUCCESS;
        } catch (Exception e) {
            return ERROR;
        }
    }

    private static String getFullname(ContactsEntity contact) {
        String fullname = "";
        if (contact.getAcademicDegreeBefore() != null) {
            fullname += contact.getAcademicDegreeBefore() + " ";
        }
        fullname += contact.getName() + " " + contact.getSurname();
        if (contact.getAcademicDegreeAfter() != null && !contact.getAcademicDegreeAfter().contains("-")) {
            String[] acaDegrees = contact.getAcademicDegreeAfter().split(",");
            fullname += ",";
            for (String acaDegree : acaDegrees) {
                fullname += " " + acaDegree;
            }

        }
        return fullname;
    }

    private static String getDuration(Timestamp startTime, Timestamp endTime) {
        long MILLISECONDS_IN_HOURS = 60 * 60 * 1000;
        long MILLISECONDS_IN_DAY = 24 * 60 * 60 * 1000;

        if (endTime == null) {
            return "1 Tag";
        } else {
            String durationString = null;
            int duration = (int) ((endTime.getTime() - startTime.getTime()) / MILLISECONDS_IN_HOURS);
            if (duration > 24) {
                duration = (int) ((endTime.getTime() - startTime.getTime()) / MILLISECONDS_IN_DAY);
                if (duration > 1) {
                    durationString = duration + " Tage";
                } else {
                    durationString = duration + " Tag";
                }
            } else {
                if (duration > 1) {
                    durationString = duration + " Stunden";
                } else {
                    durationString = duration + " Stunde";
                }
            }
            return durationString;
        }
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public void setFileContentType(String contentType) {
        this.contentType = contentType;
    }

    public void setFileFileName(String filename) {
        this.filename = filename;
    }

    public void setFile(File fileUpload) {
        this.uploadedFile = fileUpload;
    }

    public EventcategoriesService getEventcategoriesService() {
        return eventcategoriesService;
    }

    public void setEventcategoriesService(EventcategoriesService eventcategoriesService) {
        this.eventcategoriesService = eventcategoriesService;
    }

    public ContactsService getContactsService() {
        return contactsService;
    }

    public void setContactsService(ContactsService contactsService) {
        this.contactsService = contactsService;
    }

    public EventsService getEventsService() {
        return eventsService;
    }

    public void setEventsService(EventsService eventsService) {
        this.eventsService = eventsService;
    }

    public boolean isShowChecklist() {
        return showChecklist;
    }

    public void setShowChecklist(boolean showChecklist) {
        this.showChecklist = showChecklist;
    }

    public List<UsersEntity> getUsers() {
        return usersService.findAll();
    }

    public void setEventById(int id) {
        event = eventsService.findByIdWithUserworks(id);
    }

    public void setFullday(String fulldays) {
        this.fullday = fulldays.equals("true") || fulldays.equals("on");
    }

    public boolean getFullday() {
        return fullday || event.getEndDate() == null;
    }

    public ProjectsService getProjectsService() {
        return projectsService;
    }

    public void setProjectsService(ProjectsService projectsService) {
        this.projectsService = projectsService;
    }

    public String getSuccess() {
        return ActionSupport.SUCCESS;
    }

    public String getError() {
        return ActionSupport.ERROR;
    }

    public void setFileUrl(String url) {
        String[] urlparts = url.split("\\\\");
        if (urlparts.length == 0) {
            return;
        }
        fileName = urlparts[urlparts.length - 1];
    }

    public int getExportProjectId() {
        return exportProjectId;
    }

    public void setExportProjectId(int exportProjectId) {
        this.exportProjectId = exportProjectId;
    }
}
