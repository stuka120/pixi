package at.binarycheese.pixi.actions;

import at.binarycheese.pixi.domain.ContactsEntity;
import at.binarycheese.pixi.domain.OrganisationsEntity;
import at.binarycheese.pixi.service.ContactsService;
import at.binarycheese.pixi.service.OrganisationsService;
import at.binarycheese.pixi.service.interfaces.OrganisationsActionInterface;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Dominik on 05.12.2014.
 */
public class OrganisationsAction extends ActionSupport implements OrganisationsActionInterface {

    //Basic Entity
    private OrganisationsEntity organisationsEntity;
    private List<OrganisationsEntity> organisationsEntityList;

    //ID for the Get Requests
    private int selectedOrganisation;

    //Contacts Select List Values
    private List<ContactsEntity> contactsEntitiesList;
    private String selectedContacts;
    private List<Integer> preSelectedContacts;

    //This is the JSON the gets Returned for the CreateAndEdit Forms and Contains the unreferenced Contacts the User is looking for
    private List<HashMap<String, String>> unreferencedContacts;
    private String contactSearch;

    @Autowired
    OrganisationsService organisationsService;
    @Autowired
    ContactsService contactsService;

    public String index() {
        organisationsService.findAllOrgsAndSetContactCount(this);
        return SUCCESS;
    }

    public String create() {
        if (organisationsEntity == null) {
            //contactsEntitiesList = organisationsService.findUnreferencedContacts();
            return INPUT;
        } else {
            organisationsService.saveEntity(this);
            PrintWriter out = null;
            try {
                out = ServletActionContext.getResponse().getWriter();
            } catch (IOException e) {
                e.printStackTrace();
            }
            out.write("success");
            return Action.NONE;
            //return Action.SUCCESS;
        }
    }

    public String edit() {
        if (organisationsEntity == null) {
            organisationsEntity = organisationsService.findById(selectedOrganisation);
            Iterator<ContactsEntity> iterator = organisationsEntity.getContactsesById().iterator();
            selectedContacts = "";
            while (iterator.hasNext()) {
                ContactsEntity next = iterator.next();
                if (iterator.hasNext()) {
                    selectedContacts += next.getId() + ",";
                } else {
                    selectedContacts += next.getId();
                }
            }
            //contactsEntitiesList = organisationsService.findUnreferencedContacts();
            return INPUT;
        } else {
            organisationsService.update(this);
            PrintWriter out = null;
            try {
                out = ServletActionContext.getResponse().getWriter();
            } catch (IOException e) {
                e.printStackTrace();
            }
            out.write("success");
            return Action.NONE;
        }
    }

    public String delete() {
        organisationsService.delete(organisationsEntity.getId());
        return NONE;
    }

    public String table() {
        organisationsService.findAllOrgsAndSetContactCount(this);
        return SUCCESS;
    }

    public String findUnreferencedContacts() {
        List<ContactsEntity> contactsEntityList = organisationsService.findUnreferencedContactsBySurnameOrName("%" + contactSearch + "%");
        unreferencedContacts = new ArrayList<>();

        for (ContactsEntity contactsEntity : contactsEntityList) {
            HashMap<String, String> map = new HashMap<>();
            map.put("id", String.valueOf(contactsEntity.getId()));
            map.put("text", String.valueOf(contactsEntity.getName() + " " + contactsEntity.getSurname()));
            unreferencedContacts.add(map);
        }

        return SUCCESS;
    }

    @Override
    public void validate() {
        if (organisationsEntity != null) {
            if (organisationsEntity.getName() == null || organisationsEntity.getName().equals("")) {
                if (!selectedContacts.isEmpty()) {
                    organisationsService.addSelectedContactsToEntity(this);
                }
                addFieldError("nameErr", getText("nameErr"));
            } else if (!organisationsService.checkOrgNameAvailable(organisationsEntity.getName())) {
                if (organisationsEntity.getId() != 0) {
                    OrganisationsEntity orgFromDB = organisationsService.findById(organisationsEntity.getId());
                    if (!orgFromDB.getName().equals(organisationsEntity.getName())) {
                        if (!selectedContacts.isEmpty()) {
                            organisationsService.addSelectedContactsToEntity(this);
                        }
                        addFieldError("nameErr", getText("doubleNameErr"));
                    }
                } else {
                    if (!selectedContacts.isEmpty()) {
                        organisationsService.addSelectedContactsToEntity(this);
                    }
                    addFieldError("nameErr", getText("doubleNameErr"));
                }

            }
        }
    }

    //Getters and Setters

    public OrganisationsEntity getOrganisationsEntity() {
        return organisationsEntity;
    }

    public void setOrganisationsEntity(OrganisationsEntity organisationsEntity) {
        this.organisationsEntity = organisationsEntity;
    }

    public List<OrganisationsEntity> getOrganisationsEntityList() {
        return organisationsEntityList;
    }

    public void setOrganisationsEntityList(List<OrganisationsEntity> organisationsEntityList) {
        this.organisationsEntityList = organisationsEntityList;
    }

    public int getSelectedOrganisation() {
        return selectedOrganisation;
    }

    public void setSelectedOrganisation(int selectedOrganisation) {
        this.selectedOrganisation = selectedOrganisation;
    }

    public List<ContactsEntity> getContactsEntitiesList() {
        return contactsEntitiesList;
    }

    public void setContactsEntitiesList(List<ContactsEntity> contactsEntitiesList) {
        this.contactsEntitiesList = contactsEntitiesList;
    }

    public String getSelectedContacts() {
        return selectedContacts;
    }

    public void setSelectedContacts(String selectedContacts) {
        this.selectedContacts = selectedContacts;
    }

    public List<Integer> getPreSelectedContacts() {
        List<Integer> contacts = new ArrayList<>();
        if (organisationsEntity.getContactsesById() != null) {
            for (ContactsEntity contactsEntity : organisationsEntity.getContactsesById()) {
                contacts.add(contactsEntity.getId());
                contactsEntitiesList.add(contactsEntity);
            }
        }
        return contacts;
    }

    public void setPreSelectedContacts(List<Integer> preSelectedContacts) {
        this.preSelectedContacts = preSelectedContacts;
    }

    public OrganisationsService getOrganisationsService() {
        return organisationsService;
    }

    public void setOrganisationsService(OrganisationsService organisationsService) {
        this.organisationsService = organisationsService;
    }

    public ContactsService getContactsService() {
        return contactsService;
    }

    public void setContactsService(ContactsService contactsService) {
        this.contactsService = contactsService;
    }

    public List<HashMap<String, String>> getUnreferencedContacts() {
        return unreferencedContacts;
    }

    public void setContactSearch(String contactSearch) {
        this.contactSearch = contactSearch;
    }
}
