package at.binarycheese.pixi.actions.JSON;

import at.binarycheese.pixi.domain.ContactsEntity;
import at.binarycheese.pixi.domain.EmailsEntity;
import at.binarycheese.pixi.domain.TelnumbersEntity;
import at.binarycheese.pixi.service.ContactsService;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.ServletActionContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.util.*;

public class ContactFilterAction extends ActionSupport {

    private String searchString;
    private String booleanAndOr = "";
    private boolean showAll;

    @Autowired
    ContactsService contactsService;

    public List getFilteredContacts() throws ParseException {

        List<String> queryParams = new ArrayList<>();
        List<String> queryParamsDatatypes = new ArrayList<>();
        List<String> queryBooleanAndOr = new ArrayList<>();

        //Here i check, if the uses send use at least one "and" or "or. If there is one, create a new Array list that contains that And or Ors"
        if(!booleanAndOr.isEmpty()) {
            queryBooleanAndOr = Arrays.asList(booleanAndOr.substring(1).split(","));
        }

        String queryString = "select distinct con " +
                    "from ContactsEntity as con left join con.emails as emails left join con.telnumbers as tels left join con.organisationsByOrganisationsId as org left join con.countriesByCountriesId as cou left join con.postleitzahlByPlz as plz" +
                    " where ";

        List<String> params = Arrays.asList(searchString.split("( and | or )"));

        //IF there's no filter selected, then show all Contacts
        if (params.get(0).isEmpty()) {
            queryString = queryString.replace("where ", "");
        } else {


            for (int x = 0; x < params.size(); x++) {
                List<String> paramDetails = new ArrayList<>();
                String operator = "";
                for (int i = 0; paramDetails.size() < 2; i++) {
                    switch (i) {
                        case 0:
                            paramDetails = Arrays.asList(params.get(x).split(">"));
                            operator = ">";
                            break;
                        case 1:
                            paramDetails = Arrays.asList(params.get(x).split("!="));
                            operator = "!=";
                            break;
                        case 2:
                            paramDetails = Arrays.asList(params.get(x).split("<"));
                            operator = "<";
                            break;
                        case 3:
                            paramDetails = Arrays.asList(params.get(x).split("not like"));
                            operator = "not like";
                            break;
                        case 4:
                            paramDetails = Arrays.asList(params.get(x).split(" like "));
                            operator = " like ";
                            break;
                        case 5:
                            paramDetails = Arrays.asList(params.get(x).split("="));
                            operator = "=";
                            break;
                    }
                }

                switch (paramDetails.get(0).trim()) {
                    case "con.title":
                    case "con.name":
                    case "con.surname":
                    case "con.academicDegreeBefore":
                    case "con.academicDegreeAfter":
                    case "con.address":
                    case "con.plz":
                    case "con.state":
                    case "con.hompage":
                    case "con.info":
                    case "org.name":
                    case "tels.value":
                    case "emails.value":
                    case "add.value":
                    case "cou.germanName":
                        queryParamsDatatypes.add("string");
                        break;
                    case "con.id":
                    case "plz.plz":
                        queryParamsDatatypes.add("int");
                        break;
                    case "con.createDate":
                    case "con.updateDate":
                        queryParamsDatatypes.add("date");
                        break;
                    case "con.newsletter":
                        queryParamsDatatypes.add("boolean");
                        break;
                }

                if (x == params.size() - 1) {
                    if(queryParamsDatatypes.get(queryParamsDatatypes.size()-1).equalsIgnoreCase("string")) {
                        queryString += "lower(" + paramDetails.get(0) + ") ";
                        queryString += operator + " ";
                        queryString += "? ";
                        queryParams.add(paramDetails.get(1));
                    } else {
                        queryString += paramDetails.get(0) + " ";
                        queryString += operator + " ";
                        queryString += "? ";
                        queryParams.add(paramDetails.get(1));
                    }
                } else {
                    if(queryParamsDatatypes.get(queryParamsDatatypes.size()-1).equalsIgnoreCase("string") && operator.trim().equals("like")) {
                        queryString += "lower(" + paramDetails.get(0) + ") ";
                        queryString += operator + " ";
                        queryString += "? ";
                        queryParams.add(paramDetails.get(1));
                        queryString += queryBooleanAndOr.get(x) + " ";
                    } else {
                        queryString += paramDetails.get(0) + " ";
                        queryString += operator + " ";
                        queryString += "? ";
                        queryParams.add(paramDetails.get(1));
                        queryString += queryBooleanAndOr.get(x) + " ";
                    }
                }
            }
        }

        List<ContactsEntity> contactsEntityList = new ArrayList<>();
        try {
            contactsEntityList = contactsService.searchByParams(queryString, queryParams, queryParamsDatatypes, showAll);
        } catch (ClassCastException e) {
            System.err.println("Beim Übergebenen Datentyp handelt es sich vermutlich nicht um eine Zahl!");
            HttpServletResponse response = ServletActionContext.getResponse();
            try {
                response.sendError(400, "Bei einem übergebenem Wert wurde eine Zahl erwartet, aber keine gefunden!");
                throw e;
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        } catch (NumberFormatException e) {
            System.err.println("Beim Übergebenen Datentyp handelt es sich vermutlich nicht um eine Zahl!");
            HttpServletResponse response = ServletActionContext.getResponse();
            try {
                response.sendError(400, "Bei einem übergebenem Wert wurde eine Zahl erwartet, aber keine gefunden!");
                throw e;
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        } catch (ParseException e) {
            System.err.println("Es wurde vermutlich ein Datum übergeben, dessen Formatierung nicht akzeptiert wird!");
            HttpServletResponse response = ServletActionContext.getResponse();
            try {
                response.sendError(400, "Es wurde vermutlich ein Datum übergeben, dessen Formatierung nicht akzeptiert wird!");
                throw e;
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
        JSONArray contacts = new JSONArray();
        for (ContactsEntity contactsEntity : contactsEntityList) {
            JSONObject contact = new JSONObject();
            contact.put("id", contactsEntity.getId());
            contact.put("name", contactsEntity.getName());
            contact.put("surname", contactsEntity.getSurname());
            List<String> mails = new ArrayList<>();
            if(contactsEntity.getEmails() != null) {
                for (EmailsEntity mail : contactsEntity.getEmails()) {
                    if(mail.getImportant()) {
                        mails.add(mail.getValue());
                        break;
                    }
                }
                contact.put("mails", mails);
            } else {
                contact.put("mails", "");
            }
            if(contactsEntity.getOrganisationsByOrganisationsId() != null) {
                contact.put("organisation", contactsEntity.getOrganisationsByOrganisationsId().getName());
            } else {
                contact.put("organisation", "");
            }
            List<String> tels = new ArrayList<>();
            if(contactsEntity.getTelnumbers() != null) {
                for (TelnumbersEntity telnumbersEntity : contactsEntity.getTelnumbers()) {
                    if(telnumbersEntity.getImportant()) {
                        tels.add(telnumbersEntity.getValue());
                        break;
                    }
                }
                contact.put("tels", tels);
            } else {
                contact.put("tels", "");
            }

            contacts.put(contact);
        }
        return toList(contacts);
    }

    public String execute() {
        return Action.SUCCESS;
    }

    public static Map toMap(JSONObject object) throws JSONException {
        Map<String, Object> map = new HashMap<String, Object>();

        Iterator<String> keysItr = object.keys();
        while (keysItr.hasNext()) {
            String key = keysItr.next();
            Object value = object.get(key);

            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            map.put(key, value);
        }
        return map;
    }

    public static List toList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<Object>();
        for (int i = 0; i < array.length(); i++) {
            Object value = array.get(i);
            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            list.add(value);
        }
        return list;
    }

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }

    public ContactsService getContactsService() {
        return contactsService;
    }

    public void setContactsService(ContactsService contactsService) {
        this.contactsService = contactsService;
    }

    public boolean isShowAll() {
        return showAll;
    }

    public void setShowAll(boolean showAll) {
        this.showAll = showAll;
    }

    public String getBooleanAndOr() {
        return booleanAndOr;
    }

    public void setBooleanAndOr(String booleanAndOr) {
        this.booleanAndOr = booleanAndOr;
    }
}