package at.binarycheese.pixi.actions.JSON;

import at.binarycheese.pixi.domain.ContactsEntity;
import at.binarycheese.pixi.domain.EmailsEntity;
import at.binarycheese.pixi.domain.TelnumbersEntity;
import at.binarycheese.pixi.service.ContactsService;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * Created by Dominik on 02.02.2015.
 */
public class ContactTableAction extends ActionSupport implements ServletRequestAware {

    //response
    static int drawCounter;
    int draw;
    int recordsTotal;
    int recordsFiltered;
    ArrayList<ArrayList<String>> data = new ArrayList<>();

    //request
    int start;
    int length;
    String search;
    String sortColumn = "id";
    Sort.Direction direction = Sort.Direction.ASC;

    //Everything else
    HttpServletRequest request;
    @Autowired
    ContactsService contactsService;


    public String processDataTable() {
        try{

            List<ContactsEntity> contactsEntityList;

            //Parse the unnormal RequestValues
            search = "%"+request.getParameter("search[value]")+"%";
            //parse the sorting params and set them, so that also the sorting works fine
            String sortRequestDirection = request.getParameter("order[0][dir]");
            int sortRequestColumn = 0;
            try{
                sortRequestColumn = Integer.parseInt(request.getParameter("order[0][column]"));
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }

            switch (sortRequestColumn) {
                case 0:
                    sortColumn = "surname";
                    break;
                case 1:
                    sortColumn = "name";
                    break;
                case 2:
                    sortColumn = "Emails_value";
                    break;
                case 3:
                    sortColumn = "organisationsByOrganisationsId_name";
                    break;
                case 4:
                    sortColumn = "Telnumbers_value";
                    break;
            }
            switch (sortRequestDirection) {
                case "asc":
                    direction = Sort.Direction.ASC;
                    break;
                case "desc":
                    direction = Sort.Direction.DESC;
                    break;
            }

            if(search.equals("%%")) {
                contactsEntityList = contactsService.findContactsWithPaging(start/length,length, sortColumn, direction);
                recordsFiltered = contactsService.getContactsCount();
            } else {
                contactsEntityList = contactsService.findFilteredContacts(search, start / length, length, sortColumn,direction);
                recordsFiltered = contactsService.findFilteredContactsCount(search);
            }

            drawCounter += 1;
            draw = drawCounter;
            recordsTotal = contactsService.getContactsCount();
            for (Iterator<ContactsEntity> iterator = contactsEntityList.iterator(); iterator.hasNext(); ) {
                ContactsEntity contactsEntity = iterator.next();

                ArrayList<String> contact = new ArrayList<>();

                String formToDetails = "<form id=\"details_action\" class=\"contactsId\" name=\"details_action\" action=\"/contacts/details.action\" method=\"get\">\n" +
                        "                        <input name=\"selectedContact\" value=\""+ contactsEntity.getId() +"\" type=\"hidden\">\n" +
                        "                    </form>" + contactsEntity.getSurname();
                contact.add(formToDetails);
                contact.add(contactsEntity.getName());
                String telString = "";
                //Mail part
                String mailString = "";
                for (Iterator<EmailsEntity> mails = contactsEntity.getEmails().iterator(); mails.hasNext(); ) {
                    EmailsEntity mail = mails.next();
                    if(mail.getImportant()) {
                        mailString = mail.getValue();
                        break;
                    }
                }
                contact.add(mailString);
                //Organisation part;
                if(contactsEntity.getOrganisationsByOrganisationsId() != null) {
                    contact.add(contactsEntity.getOrganisationsByOrganisationsId().getName());
                } else {
                    contact.add("");
                }
                //TelNr part
                for (Iterator<TelnumbersEntity> telnr = contactsEntity.getTelnumbers().iterator(); telnr.hasNext(); ) {
                    TelnumbersEntity tel = telnr.next();
                    if(tel.getImportant()) {
                        telString = tel.getValue();
                        break;
                    }
                }
                contact.add(telString);


                contact.add("<a href=\"/contacts/edit?selectedContact="+contactsEntity.getId()+"\"><i class=\"fa fa-pencil-square-o fa-lg\"></i></i></a>");
                contact.add("<a href=\"/contacts/delete?selectedContact="+contactsEntity.getId()+"\"><i class=\"fa fa-times fa-lg\"></i></a>");
                data.add(contact);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        return Action.SUCCESS;
    }

    public String execute() {
        return Action.SUCCESS;
    }

    public static Map toMap(JSONObject object) throws JSONException {
        Map<String, Object> map = new HashMap<String, Object>();

        Iterator<String> keysItr = object.keys();
        while (keysItr.hasNext()) {
            String key = keysItr.next();
            Object value = object.get(key);

            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            map.put(key, value);
        }
        return map;
    }

    public static List toList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<Object>();
        for (int i = 0; i < array.length(); i++) {
            Object value = array.get(i);
            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            list.add(value);
        }
        return list;
    }

    //Getters and Setters

    public static int getDrawCounter() {
        return drawCounter;
    }

    public static void setDrawCounter(int drawCounter) {
        ContactTableAction.drawCounter = drawCounter;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public int getRecordsTotal() {
        return recordsTotal;
    }

    public void setRecordsTotal(int recordsTotal) {
        this.recordsTotal = recordsTotal;
    }

    public int getRecordsFiltered() {
        return recordsFiltered;
    }

    public void setRecordsFiltered(int recordsFiltered) {
        this.recordsFiltered = recordsFiltered;
    }

    public int getDraw() {
        return draw;
    }

    public void setDraw(int draw) {
        this.draw = draw;
    }

    public ArrayList<ArrayList<String>> getData() {
        return data;
    }

    public void setData(ArrayList<ArrayList<String>> data) {
        this.data = data;
    }

    public ContactsService getContactsService() {
        return contactsService;
    }

    public void setContactsService(ContactsService contactsService) {
        this.contactsService = contactsService;
    }

    @Override
    public void setServletRequest(HttpServletRequest httpServletRequest) {
        this.request = httpServletRequest;
    }
}
