package at.binarycheese.pixi.actions;

import at.binarycheese.pixi.domain.AcademicDegree;
import at.binarycheese.pixi.domain.ContactsEntity;
import at.binarycheese.pixi.domain.PostleitzahlEntity;
import at.binarycheese.pixi.service.AcademicDegreeService;
import at.binarycheese.pixi.service.ContactsService;
import at.binarycheese.pixi.service.PostleitzahlService;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.omg.CORBA.NameValuePair;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;

public class ContactJsonAction extends ActionSupport {

    private String academicDegreeQuery;
    private String searchString;
    private String ort;
    private String postleitzahl;
    private List<HashMap<String,String>> findAcademicDegree;
    private List<HashMap<String,String>> findOrtAndPostleitzahl;

    @Autowired
    ContactsService contactsService;
    @Autowired
    AcademicDegreeService academicDegreeService;
    @Autowired
    PostleitzahlService postleitzahlService;

    public List getContacts() {
        List<ContactsEntity> contactsEntityList = contactsService.searchByNameAndSurname(searchString);
        JSONArray contacts = new JSONArray();
        for (ContactsEntity contactsEntity : contactsEntityList) {
            JSONObject contact = new JSONObject();
            contact.put("id", contactsEntity.getId());
            contact.put("name", contactsEntity.getName());
            contact.put("surname", contactsEntity.getSurname());
            contacts.put(contact);
        }

        return toList(contacts);
    }

    public String findAcademicDegreeasdf() {
        findAcademicDegree = new ArrayList<>();
        List<AcademicDegree> degrees = academicDegreeService.findAcademicDegree("%" + academicDegreeQuery + "%");
        for (AcademicDegree degree : degrees) {
            HashMap<String, String> a = new HashMap<>();
            a.put("id",degree.getDegree());
            a.put("text",degree.getDegree() + " - " + degree.getDescription());
            findAcademicDegree.add(a);
        }
        return SUCCESS;
    }

    public String findOrtAndPostleitzahl() {
        findOrtAndPostleitzahl = new ArrayList<>();
        List<PostleitzahlEntity> postleitzahlEntities = new ArrayList<>();
        if(postleitzahl != null && !postleitzahl.equalsIgnoreCase("")) {
            int plz = 0;
            try {
                plz = Integer.parseInt(postleitzahl);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            postleitzahlEntities = postleitzahlService.findByPlzLike(plz);
            for (PostleitzahlEntity postleitzahlEntity : postleitzahlEntities) {
                HashMap<String, String> a = new HashMap<>();
                a.put("id", String.valueOf(postleitzahlEntity.getPlz()));
                a.put("text",String.valueOf(postleitzahlEntity.getPlz()) + " - " +postleitzahlEntity.getName());
                a.put("ort", postleitzahlEntity.getName());
                a.put("bundesland", postleitzahlEntity.getBundesland());
                findOrtAndPostleitzahl.add(a);
            }
            return SUCCESS;
        } else if(!ort.equalsIgnoreCase("") && ort != null ) {
            postleitzahlEntities = postleitzahlService.findByOrtLike("%" + ort + "%");
            for (PostleitzahlEntity postleitzahlEntity : postleitzahlEntities) {
                HashMap<String, String> a = new HashMap<>();
                a.put("id", postleitzahlEntity.getName() + " - " + postleitzahlEntity.getPlz());
                a.put("id2", String.valueOf(postleitzahlEntity.getName()));
                a.put("text",postleitzahlEntity.getName() + " - " + postleitzahlEntity.getPlz());
                a.put("plz", String.valueOf(postleitzahlEntity.getPlz()));
                a.put("bundesland", postleitzahlEntity.getBundesland());
                findOrtAndPostleitzahl.add(a);
            }
            return SUCCESS;
        } else {
            return ERROR;
        }
    }

    public String execute() {
        return Action.SUCCESS;
    }

    public static Map toMap(JSONObject object) throws JSONException {
        Map<String, Object> map = new HashMap<String, Object>();

        Iterator<String> keysItr = object.keys();
        while (keysItr.hasNext()) {
            String key = keysItr.next();
            Object value = object.get(key);

            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            map.put(key, value);
        }
        return map;
    }

    public static List toList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<Object>();
        for (int i = 0; i < array.length(); i++) {
            Object value = array.get(i);
            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            list.add(value);
        }
        return list;
    }

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        String s = "%";
        s += searchString;
        s += "%";
        this.searchString = s;
    }

    public ContactsService getContactsService() {
        return contactsService;
    }

    public void setContactsService(ContactsService contactsService) {
        this.contactsService = contactsService;
    }

    public String getAcademicDegreeQuery() {
        return academicDegreeQuery;
    }

    public void setAcademicDegreeQuery(String academicDegreeQuery) {
        this.academicDegreeQuery = academicDegreeQuery;
    }

    public AcademicDegreeService getAcademicDegreeService() {
        return academicDegreeService;
    }

    public void setAcademicDegreeService(AcademicDegreeService academicDegreeService) {
        this.academicDegreeService = academicDegreeService;
    }

    public List<HashMap<String, String>> getFindAcademicDegree() {
        return findAcademicDegree;
    }

    public List<HashMap<String, String>> getFindOrtAndPostleitzahl() {
        return findOrtAndPostleitzahl;
    }

    public void setOrt(String ort) {
        this.ort = ort;
    }

    public void setPostleitzahl(String postleitzahl) {
        this.postleitzahl = postleitzahl;
    }

    public PostleitzahlService getPostleitzahlService() {
        return postleitzahlService;
    }

    public void setPostleitzahlService(PostleitzahlService postleitzahlService) {
        this.postleitzahlService = postleitzahlService;
    }
}