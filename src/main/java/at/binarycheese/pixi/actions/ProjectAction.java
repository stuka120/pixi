package at.binarycheese.pixi.actions;

import at.binarycheese.pixi.domain.OrganisationsEntity;
import at.binarycheese.pixi.domain.ProjectEntity;
import at.binarycheese.pixi.domain.ProjectstagesEntity;
import at.binarycheese.pixi.service.OrganisationsService;
import at.binarycheese.pixi.service.ProjectStagesService;
import at.binarycheese.pixi.service.ProjectsService;
import at.binarycheese.pixi.service.interfaces.ProjectActionInterface;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Tortuga on 13.01.2015.
 */
public class ProjectAction extends ActionSupport implements ProjectActionInterface {
    private List<ProjectEntity> projectEntityList;
    private List<OrganisationsEntity> organisationsEntityList;
    private List<ProjectstagesEntity> projectStageList;
    private ProjectEntity projectEntity;
    private String orgId;
    private int id;
    private int projectStagesId;
    private Logger logger = org.apache.log4j.LogManager.getLogger("at.binarycheese.pixi." + this.getClass().getSimpleName());
    private boolean showDisabled;

    @Autowired
    ProjectsService projectsService;
    @Autowired
    OrganisationsService organisationsService;
    @Autowired
    ProjectStagesService projectStagesService;

    //For File Upload
    private File file;
    private String fileContentType;
    private String fileFileName;
    private String destPath;
    private List<File> fileList;

    //For Download
    private String documentName;
    private InputStream fileInputStream;
    private List<File> templateList;

    //For searching through Projects
    String query;
    int page;
    int size;

    public String browse() {
        if (!showDisabled) {
            projectEntityList = projectsService.findAllActive();
        } else {
            projectEntityList = projectsService.findAllDeactivated();
        }
        projectStageList = projectStagesService.findAllActiveStages();
        return SUCCESS;
    }

    public void validate() {
        organisationsEntityList = organisationsService.findAll();
        if (projectEntity != null) {
            if (projectEntity.getName() == null || projectEntity.getName().isEmpty()) {
                addFieldError("nameErr", getText("nameErr"));
            }
            if (projectEntity.getProjectnumber() == null || projectEntity.getProjectnumber().isEmpty()) {
                addFieldError("projNrErr", getText("projNrErrNull"));
            }
            if (projectsService.containsProjectNumber(projectEntity.getProjectnumber()) && projectEntity.getId() == 0) {
                addFieldError("projNrErr", getText("projNrErr"));
            }
        }
    }

    public String input() {
        organisationsEntityList = organisationsService.findAll();
        if (id != 0) {
            projectEntity = projectsService.findById(id);
            projectStagesId = projectEntity.getProjectstagesByProjectStage().getId();
        }
        return INPUT;
    }

    public String create() throws Exception {
        String result = projectsService.saveOrUpdateEntity(this);
        try{
            PrintWriter out = ServletActionContext.getResponse().getWriter();
            if(result == "edited"){
                out.write("edited");
            }else{
                out.write("created");
            }
        } catch (IOException e) {
            throw e;
        }

        return Action.NONE;
    }

    public String table() {
        if (!showDisabled) {
            projectEntityList = projectsService.findAllActive();
        } else {
            projectEntityList = projectsService.findAllDeactivated();
        }
        return SUCCESS;
    }

    public String delete() {
        projectsService.deactivated(id);
        return SUCCESS;
    }

    public String reactivate() {
        projectsService.reactivate(id, projectStagesId);
        return Action.NONE;
    }

    public String details() {
        if (id == 0) {
            return ERROR;
        }
        projectEntity = projectsService.findById(id);
        File folder = new File(ServletActionContext.getServletContext().getRealPath("/") +
                File.separator + "upload" + File.separator + "project" + File.separator + id);
        fileList = new ArrayList<>();

        File templateFolder = new File(ServletActionContext.getServletContext().getRealPath("/") + File.separator + "templates");
        templateList = new ArrayList<>();
        if (templateFolder.listFiles() != null) {
            for (File fileEntry : templateFolder.listFiles()) {
                if (!fileEntry.isDirectory()) {
                    templateList.add(fileEntry);
                }
            }
        }

        if (folder.listFiles() == null) {
            return SUCCESS;
        }
        for (File fileEntry : folder.listFiles()) {
            if (!fileEntry.isDirectory()) {
                fileList.add(fileEntry);
            }
        }
        return SUCCESS;
    }

    public String uploadReport() throws Exception {
        destPath = File.separator + "upload" + File.separator + "project" + File.separator + projectEntity.getId() + File.separator;
        destPath = ServletActionContext.getServletContext().getRealPath("/") + destPath;
        PrintWriter out = ServletActionContext.getResponse().getWriter();
        if (file == null) {
            out.write("error");
            return Action.NONE;
        }

        try {
            File destFile = new File(destPath, fileFileName);
            FileUtils.copyFile(file, destFile);
        } catch (IOException e) {
            e.printStackTrace();
            logger.error("Exception occureed while uploading project report: " + fileFileName, e);
            out.write("error");
            return Action.NONE;
        }

        out.write("success");
        return Action.NONE;
    }

    public String fileList() {
        File folder = new File(ServletActionContext.getServletContext().getRealPath("/") +
                File.separator + "upload" + File.separator + "project" + File.separator + id);
        fileList = new ArrayList<>();
        projectEntity = projectsService.findById(id);
        if (folder.listFiles() == null) {
            return SUCCESS;
        }
        for (File fileEntry : folder.listFiles()) {
            if (!fileEntry.isDirectory()) {
                fileList.add(fileEntry);
            }
        }
        return SUCCESS;
    }

    public String download() {
        File downFile = new File(ServletActionContext.getServletContext().getRealPath("/") +
                File.separator + "upload" + File.separator + "project" +
                File.separator + id +
                File.separator + documentName);
        try {
            fileInputStream = new FileInputStream(downFile);
        } catch (FileNotFoundException e) {
            logger.error("file " + documentName + "couldn't be downloaded ");
            e.printStackTrace();
        }

        return SUCCESS;
    }

    public String deleteFile() {
        File fileToDelete = new File(ServletActionContext.getServletContext().getRealPath("/") +
                File.separator + "upload" + File.separator + "project" +
                File.separator + id +
                File.separator + documentName);
        fileToDelete.delete();

        PrintWriter out = null;
        try {
            out = ServletActionContext.getResponse().getWriter();
        } catch (IOException e) {
            logger.error("Exception occured while creating PrintWriter for Response");
            e.printStackTrace();
        }
        out.write("success");
        return Action.NONE;
    }

    public String nextStage() {
        projectsService.nextStage(this);
        return details();
    }

    public String getSearchAllProjects() {
        JSONObject asdf = projectsService.searchByName(query, page, size);
        try {
            ServletActionContext.getResponse().setContentType("application/json");
            ServletActionContext.getResponse().getWriter().write(asdf.toString());
        } catch (IOException e) {
        }
        return Action.NONE;
    }

    //-------Getter------Setter------------

    public List<ProjectEntity> getProjectEntityList() {
        return projectEntityList;
    }

    public void setProjectEntityList(List<ProjectEntity> projectEntityList) {
        this.projectEntityList = projectEntityList;
    }

    public ProjectEntity getProjectEntity() {
        return projectEntity;
    }

    public void setProjectEntity(ProjectEntity projectEntity) {
        this.projectEntity = projectEntity;
    }

    public List<OrganisationsEntity> getOrganisationsEntityList() {
        return organisationsEntityList;
    }

    public void setOrganisationsEntityList(List<OrganisationsEntity> organisationsEntityList) {
        this.organisationsEntityList = organisationsEntityList;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProjectStagesId() {
        return projectStagesId;
    }

    public void setProjectStagesId(int projectStagesId) {
        this.projectStagesId = projectStagesId;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public String getFileContentType() {
        return fileContentType;
    }

    public void setFileContentType(String fileContentType) {
        this.fileContentType = fileContentType;
    }

    public String getFileFileName() {
        return fileFileName;
    }

    public void setFileFileName(String fileFileName) {
        this.fileFileName = fileFileName;
    }

    public List<File> getFileList() {
        return fileList;
    }

    public void setFileList(List<File> fileList) {
        this.fileList = fileList;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public InputStream getFileInputStream() {
        return fileInputStream;
    }

    public void setFileInputStream(InputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }

    public List<File> getTemplateList() {
        return templateList;
    }

    public void setTemplateList(List<File> templateList) {
        this.templateList = templateList;
    }

    public OrganisationsService getOrganisationsService() {
        return organisationsService;
    }

    public void setOrganisationsService(OrganisationsService organisationsService) {
        this.organisationsService = organisationsService;
    }

    public ProjectStagesService getProjectStagesService() {
        return projectStagesService;
    }

    public void setProjectStagesService(ProjectStagesService projectStagesService) {
        this.projectStagesService = projectStagesService;
    }

    public ProjectsService getProjectsService() {
        return projectsService;
    }

    public void setProjectsService(ProjectsService projectsService) {
        this.projectsService = projectsService;
    }

    public String getDestPath() {
        return destPath;
    }

    public void setDestPath(String destPath) {
        this.destPath = destPath;
    }

    public boolean isShowDisabled() {
        return showDisabled;
    }

    public void setShowDisabled(boolean showDisabled) {
        this.showDisabled = showDisabled;
    }

    public List<ProjectstagesEntity> getProjectStageList() {
        return projectStageList;
    }

    public void setProjectStageList(List<ProjectstagesEntity> projectStageList) {
        this.projectStageList = projectStageList;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

}