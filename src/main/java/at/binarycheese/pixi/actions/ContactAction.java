package at.binarycheese.pixi.actions;

import at.binarycheese.pixi.domain.*;
import at.binarycheese.pixi.service.ContactsService;
import at.binarycheese.pixi.service.CountriesService;
import at.binarycheese.pixi.service.LanguagesService;
import at.binarycheese.pixi.service.OrganisationsService;
import at.binarycheese.pixi.service.interfaces.ContactActionInterface;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.json.annotations.JSON;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.BooleanSupplier;

/**
 * Created by Dominik on 15.11.2014.
 */
public class ContactAction extends ActionSupport implements ContactActionInterface {

    //Regex for email
    private String emailRegex = "(?:(?:\\r\\n)?[ \\t])*(?:(?:(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*))*@(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*|(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*)*\\<(?:(?:\\r\\n)?[ \\t])*(?:@(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*(?:,@(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*)*:(?:(?:\\r\\n)?[ \\t])*)?(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*))*@(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*\\>(?:(?:\\r\\n)?[ \\t])*)|(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*)*:(?:(?:\\r\\n)?[ \\t])*(?:(?:(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*))*@(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*|(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*)*\\<(?:(?:\\r\\n)?[ \\t])*(?:@(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*(?:,@(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*)*:(?:(?:\\r\\n)?[ \\t])*)?(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*))*@(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*\\>(?:(?:\\r\\n)?[ \\t])*)(?:,\\s*(?:(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*))*@(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*|(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*)*\\<(?:(?:\\r\\n)?[ \\t])*(?:@(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*(?:,@(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*)*:(?:(?:\\r\\n)?[ \\t])*)?(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*))*@(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*\\>(?:(?:\\r\\n)?[ \\t])*))*)?;\\s*)";

    //ContactEntity Values
    private ContactsEntity contactsEntity;
    private List<ContactsEntity> contactsEntityList;
    private int selectedContact;

    //Country Select List Values
    private List<CountriesEntity> countriesEntitiesList;
    private String selectedCountry;
    private List<String> preSelectedCountry;

    //Organisation SelectLis tValues
    private List<OrganisationsEntity> organisationsEntityList;
    private int selectedOrganisation;
    private List<Integer> preSelectedOrganisation;

    //Languages Select2List Values;
    private List<LanguagesEntity> languagesEntityList;
    private List<String> selectedLanguages;
    private List<Integer> preSelectedLanguages;

    //Phone Select2List
    private String selectedPhones;
    private String preSelectedPhones;
    private String selectedPhonesPrivate;
    private String preSelectedPhonesPrivate;

    //EMail Select2List
    private String selectedEmails;
    private String preSelectedEmails;
    private String selectedEmailsPrivate;
    private String preSelectedEmailsPrivate;

    //Newsletter Selection
    private String selectedNewsletter;

    //AdditionalInformation Fields
    private List<AdditionalinformationEntity> selectedAdditionalinformationEntityList = new ArrayList<>();
    private String addName;
    private String addValue;

    //For searching through Kontakts
    String query;
    int page;
    int size;

    //For export
    private List<String> selectedColumns;
    private InputStream fileInputStream;
    private List<Integer> contactsToExport;

    @Autowired
    private ContactsService contactsService;
    @Autowired
    private CountriesService countriesService;
    @Autowired
    private OrganisationsService organisationsService;
    @Autowired
    private LanguagesService languagesService;


    public String index() {

        //contactsEntityList = contactsService.findAll();

        return SUCCESS;
    }

    public String details() throws Exception {
        contactsEntity = contactsService.findById(selectedContact);
        if (contactsEntity == null) {
            return ERROR;
        }
        return "success";
    }

    public String create() {
        if (contactsEntity == null) {
            countriesEntitiesList = countriesService.findAll();
            Collections.sort(countriesEntitiesList, new Comparator<CountriesEntity>() {
                @Override
                public int compare(CountriesEntity o1, CountriesEntity o2) {
                    return o1.getGermanName().compareTo(o2.getGermanName());
                }
            });
            organisationsEntityList = organisationsService.findAll();
            Collections.sort(organisationsEntityList, new Comparator<OrganisationsEntity>() {
                @Override
                public int compare(OrganisationsEntity o1, OrganisationsEntity o2) {
                    if (o1.getName().equalsIgnoreCase("Fair & Sensibel"))
                        return -1;
                    else if (o2.getName().equalsIgnoreCase("Fair & Sensibel")) {
                        return 1;
                    } else
                        return o1.getName().compareTo(o2.getName());
                }
            });
            languagesEntityList = languagesService.findAll();
            return INPUT;
        } else {
            //Sett the Foreign Key Entities from the selected List
            //Exception means, that entity is no longer available

            contactsService.saveEntity(this);
            return Action.SUCCESS;
        }
    }

    public String delete() {
        if (contactsEntity == null) {
            contactsEntity = contactsService.findById(selectedContact);
            return INPUT;
        } else {
            contactsService.delete(contactsEntity.getId());
            return SUCCESS;
        }
    }

    public String edit() {
        if (contactsEntity == null) {
            contactsEntity = contactsService.findById(selectedContact);
            prepForCreateView();
            return INPUT;
        } else {
            //Sett the Foreign Key Entities from the selected List
            //Exception means, that entity is no longer available
            try {
                contactsService.update(this);
            } catch (Exception e) {
                e.printStackTrace();
                return ERROR;
            }
            return "success";
        }
    }

    public void prepForCreateView(){

        countriesEntitiesList = countriesService.findAll();
        Collections.sort(countriesEntitiesList, new Comparator<CountriesEntity>() {
            @Override
            public int compare(CountriesEntity o1, CountriesEntity o2) {
                return o1.getGermanName().compareTo(o2.getGermanName());
            }
        });
        organisationsEntityList = organisationsService.findAll();
        Collections.sort(organisationsEntityList, new Comparator<OrganisationsEntity>() {
            @Override
            public int compare(OrganisationsEntity o1, OrganisationsEntity o2) {
                if (o1.getName().equalsIgnoreCase("Fair & Sensibel"))
                    return -1;
                else if (o2.getName().equalsIgnoreCase("Fair & Sensibel")) {
                    return 1;
                } else
                    return o1.getName().compareTo(o2.getName());
            }
        });
        languagesEntityList = languagesService.findAll();
    }

    public void validate(){
        if(contactsEntity != null){
            if(contactsEntity.getName().isEmpty()){
                prepForCreateView();
                addFieldError("emptyName", getText("nameRequired"));
            }
            if(contactsEntity.getSurname().isEmpty()){
                if(getFieldErrors().isEmpty()){
                    prepForCreateView();
                }
                addFieldError("emptySurName", getText("surnameRequried"));
            }
            if(contactsEntity.getTitle().isEmpty()){
                if(getFieldErrors().isEmpty()){
                    prepForCreateView();
                }
                addFieldError("emptyTitle", getText("titleRequired"));
            }
            if(selectedEmails != null && !selectedEmails.isEmpty()){
                if(!selectedEmails.matches(emailRegex)){
                    if(getFieldErrors().isEmpty()){
                        prepForCreateView();
                    }
                    addFieldError("emailErr", getText("emailErr"));
                }
            }
            if(selectedEmailsPrivate != null && !selectedEmailsPrivate.isEmpty()){
                if(!selectedEmailsPrivate.matches(emailRegex)){
                    if(getFieldErrors().isEmpty()){
                        prepForCreateView();
                    }
                    addFieldError("emailErrP", getText("emailErr"));
                }
            }
        }
    }

    /**
     * For getting every Kontakt as a JSON
     */
    public String getSearchAllContacts() {
        JSONObject asdf = contactsService.searchByNameAndSurnameWithPageable(query, page, size,  false);
        try {
            ServletActionContext.getResponse().setContentType("application/json");
            ServletActionContext.getResponse().getWriter().write(asdf.toString());
        } catch (IOException e) {
        }
        return Action.NONE;
    }
    
    /**
     * For getting every Kontakt without the Contacts which also habe a User as a JSON
     */
    public String getSearchAllContactsWithoutUser() {
        JSONObject asdf = contactsService.searchByNameAndSurnameWithPageable(query, page, size, true);
        try {
            ServletActionContext.getResponse().setContentType("application/json");
            ServletActionContext.getResponse().getWriter().write(asdf.toString());
        } catch (IOException e) {
        }
        return Action.NONE;
    }
    public String export() {
        if (selectedColumns == null || selectedColumns.size() == 0) {
            HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFSheet sheet = workbook.createSheet("Kontakte");
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try {
                workbook.write(baos);
            } catch (IOException e) {
                e.printStackTrace();
                return ERROR;
            }
            fileInputStream = new ByteArrayInputStream(baos.toByteArray());

            return SUCCESS;
        }

        //fetch all selected contacts from the database
        List<ContactsEntity> contacts = contactsService.findUsersByIDs(contactsToExport);

        //create excel file, the sheet and the headline with the selected columns
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("Kontakte");
        HSSFCellStyle styleHeadline = workbook.createCellStyle();
        styleHeadline.setBorderBottom(CellStyle.BORDER_THIN);
        Font f = workbook.createFont();
        f.setBoldweight(Font.BOLDWEIGHT_BOLD);
        styleHeadline.setFont(f);

        int rownum = 0;
        Row row = sheet.createRow(rownum);
        int cellnum = 0;
        for (Object obj : selectedColumns) {
            Cell cell = row.createCell(cellnum++);
            if (obj instanceof String) {
                String x = (String) obj;
                cell.setCellValue(x.substring(x.indexOf(':')+1));
            }
            cell.setCellStyle(styleHeadline);
        }
        rownum++;
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");


        //iterate through the contacts and add a line for each one
        for (ContactsEntity contact : contacts) {
            row = sheet.createRow(rownum);
            Class<?> contactClass = contact.getClass();
            cellnum = 0;
            for (String column : selectedColumns) {
                Cell cell = row.createCell(cellnum++);
                try {
                    String methodname = "get"+ column.substring(0,1).toUpperCase() + column.substring(1,column.indexOf(':'));
                    Method method = contactClass.getDeclaredMethod(methodname);
                    Object obj = method.invoke(contact);
                    if (obj instanceof String) {
                        if(((String)obj).isEmpty() || ((String)obj).equals("null")){
                            cell.setCellValue("-");
                        }else{
                            cell.setCellValue((String) obj);
                        }
                    } else if (obj instanceof Timestamp) {
                        cell.setCellValue(dateFormat.format((Timestamp) obj));
                    } else if (obj instanceof Boolean){
                        if((Boolean) obj){
                            cell.setCellValue("NL");
                        }
                    } else if (obj instanceof CountriesEntity){
                        cell.setCellValue(((CountriesEntity) obj).getGermanName());
                    } else if(obj instanceof OrganisationsEntity){
                        cell.setCellValue(((OrganisationsEntity) obj).getName());
                    } else if (obj instanceof PostleitzahlEntity){
                        cell.setCellValue(((PostleitzahlEntity)obj).getPlz() + "/" + ((PostleitzahlEntity)obj).getName());
                    } else if (obj instanceof Collection){
                        Collection collection = (Collection) obj;
                        if(collection.size() == 0){
                            continue;
                        }
                        String cellValue = "";
                        if(collection.toArray()[0] instanceof EmailsEntity){
                            for(Object email : collection){
                                cellValue += ((EmailsEntity)email).getValue() + "; ";
                            }
                        }else if(collection.toArray()[0] instanceof TelnumbersEntity){
                            for(Object telnr : collection){
                                cellValue += ((TelnumbersEntity)telnr).getValue() + "; ";
                            }
                        }else if(collection.toArray()[0] instanceof SpeaksEntity){
                            for(Object language : collection){
                                cellValue += ((SpeaksEntity)language).getLanguagesByLanguagesId().getGermanName() + "; ";
                            }
                        }
                        cellValue = cellValue.substring(0, cellValue.length()-2);
                        cell.setCellValue(cellValue);
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                    continue;
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                    continue;
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                    continue;
                }
            }

            rownum++;
        }
        for(int i = 0; i < selectedColumns.size(); i++){
            sheet.autoSizeColumn(i);
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            workbook.write(baos);
        } catch (IOException e) {
            e.printStackTrace();
            return ERROR;
        }
        fileInputStream = new ByteArrayInputStream(baos.toByteArray());

        return SUCCESS;
    }

    /**
     * Getters and Setters
     */
    public ContactsEntity getContactsEntity() {
        return contactsEntity;
    }

    public void setContactsEntity(ContactsEntity contactsEntity) {
        this.contactsEntity = contactsEntity;
    }

    public List<ContactsEntity> getContactsEntityList() {
        return contactsEntityList;
    }

    public void setContactsEntityList(List<ContactsEntity> contactsEntityList) {
        this.contactsEntityList = contactsEntityList;
    }

    public List<CountriesEntity> getCountriesEntitiesList() {
        return countriesEntitiesList;
    }

    public void setCountriesEntitiesList(List<CountriesEntity> countriesEntitiesList) {
        this.countriesEntitiesList = countriesEntitiesList;
    }

    public String getSelectedCountry() {
        return selectedCountry;
    }

    public void setSelectedCountry(String selectedCountry) {
        this.selectedCountry = selectedCountry;
    }

    public List<OrganisationsEntity> getOrganisationsEntityList() {
        return organisationsEntityList;
    }

    public void setOrganisationsEntityList(List<OrganisationsEntity> organisationsEntityList) {
        this.organisationsEntityList = organisationsEntityList;
    }

    public int getSelectedOrganisation() {
        return selectedOrganisation;
    }

    public void setSelectedOrganisation(int selectedOrganisation) {
        this.selectedOrganisation = selectedOrganisation;
    }

    public int getSelectedContact() {
        return selectedContact;
    }

    public void setSelectedContact(int selectedContact) {
        this.selectedContact = selectedContact;
    }

    public List<LanguagesEntity> getLanguagesEntityList() {
        return languagesEntityList;
    }

    public void setLanguagesEntityList(List<LanguagesEntity> languagesEntityList) {
        this.languagesEntityList = languagesEntityList;
    }

    public List<String> getSelectedLanguages() {
        return selectedLanguages;
    }

    public void setSelectedLanguages(List<String> selectedLanguages) {
        this.selectedLanguages = selectedLanguages;
    }

    public String getSelectedPhones() {
        return selectedPhones;
    }

    public void setSelectedPhones(String selectedPhones) {
        this.selectedPhones = selectedPhones;
    }

    public String getSelectedEmails() {
        return selectedEmails;
    }

    public void setSelectedEmails(String selectedEmails) {
        this.selectedEmails = selectedEmails;
    }

    public String getSelectedNewsletter() {
        return selectedNewsletter;
    }

    public void setSelectedNewsletter(String selectedNewsletter) {
        this.selectedNewsletter = selectedNewsletter;
    }

    public List<Integer> getPreSelectedLanguages() {
        List<Integer> items = new ArrayList<>();
        for (LanguagesEntity languagesEntity : contactsEntity.getLanguages()) {
            items.add(languagesEntity.getId());
        }
        return items;
    }

    public void setPreSelectedLanguages(List<Integer> preSelectedLanguages) {
        this.preSelectedLanguages = preSelectedLanguages;
    }

    public String getPreSelectedPhones() {
        String s = "";
        for (TelnumbersEntity contactaddressesEntity : contactsEntity.getTelnumbers()) {
            if (contactaddressesEntity.getImportant())
                s += contactaddressesEntity.getValue() + ",";
        }
        s = s.substring(0, s.length() - 1);
        return s;
    }

    public void setPreSelectedPhones(String preSelectedPhones) {
        this.preSelectedPhones = preSelectedPhones;
    }

    public String getPreSelectedEmails() {
        String s = "";
        for (EmailsEntity contactaddressesEntity : contactsEntity.getEmails()) {
            if (contactaddressesEntity.getImportant())
                s += contactaddressesEntity.getValue() + ",";
        }
        s = s.substring(0, s.length() - 1);
        return s;
    }

    public void setPreSelectedEmails(String preSelectedEmails) {
        this.preSelectedEmails = preSelectedEmails;
    }

    public List<String> getPreSelectedCountry() {
        return Arrays.asList(contactsEntity.getCountriesByCountriesId().getId());
    }

    public void setPreSelectedCountry(List<String> preSelectedCountry) {
        this.preSelectedCountry = preSelectedCountry;
    }

    public List<Integer> getPreSelectedOrganisation() {
        return Arrays.asList(contactsEntity.getOrganisationsByOrganisationsId().getId());
    }

    public void setPreSelectedOrganisation(List<Integer> preSelectedOrganisation) {
        this.preSelectedOrganisation = preSelectedOrganisation;
    }

    public String getAddName() {
        return addName;
    }

    public void setAddName(String addName) {
        this.addName = addName;
    }

    public String getAddValue() {
        return addValue;
    }

    public void setAddValue(String addValue) {
        List<String> nameList = Arrays.asList(addName.split(","));
        List<String> valueList = Arrays.asList(addValue.split(","));
        for (int i = 0; i < nameList.size(); i++) {
            if (valueList.get(i).trim().isEmpty())
                continue;
            else {
                AdditionalinformationEntity additionalinformationEntity = new AdditionalinformationEntity();
                additionalinformationEntity.setName(nameList.get(i));
                additionalinformationEntity.setValue(valueList.get(i));
                this.selectedAdditionalinformationEntityList.add(additionalinformationEntity);
            }
        }
    }

    public LanguagesService getLanguagesService() {
        return languagesService;
    }

    public void setLanguagesService(LanguagesService languagesService) {
        this.languagesService = languagesService;
    }

    public OrganisationsService getOrganisationsService() {
        return organisationsService;
    }

    public void setOrganisationsService(OrganisationsService organisationsService) {
        this.organisationsService = organisationsService;
    }

    public CountriesService getCountriesService() {
        return countriesService;
    }

    public void setCountriesService(CountriesService countriesService) {
        this.countriesService = countriesService;
    }

    public ContactsService getContactsService() {
        return contactsService;
    }

    public void setContactsService(ContactsService contactsService) {
        this.contactsService = contactsService;
    }

    public List<AdditionalinformationEntity> getSelectedAdditionalinformationEntityList() {
        return selectedAdditionalinformationEntityList;
    }

    public void setSelectedAdditionalinformationEntityList(List<AdditionalinformationEntity> selectedAdditionalinformationEntityList) {
        this.selectedAdditionalinformationEntityList = selectedAdditionalinformationEntityList;
    }

    public String getSelectedEmailsPrivate() {
        return selectedEmailsPrivate;
    }

    public void setSelectedEmailsPrivate(String selectedEmailsPrivate) {
        this.selectedEmailsPrivate = selectedEmailsPrivate;
    }

    public String getPreSelectedEmailsPrivate() {
        String s = "";
        for (EmailsEntity emailsEntity : contactsEntity.getEmails()) {
            if (!emailsEntity.getImportant())
                s += emailsEntity.getValue() + ",";
        }
        s = s.substring(0, s.length() - 1);
        return s;
    }

    public void setPreSelectedEmailsPrivate(String preSelectedEmailsPrivate) {
        this.preSelectedEmailsPrivate = preSelectedEmailsPrivate;
    }

    public String getSelectedPhonesPrivate() {
        return selectedPhonesPrivate;
    }

    public void setSelectedPhonesPrivate(String selectedPhonesPrivate) {
        this.selectedPhonesPrivate = selectedPhonesPrivate;
    }

    public String getPreSelectedPhonesPrivate() {
        String s = "";
        for (TelnumbersEntity telnumbersEntity : contactsEntity.getTelnumbers()) {
            if (!telnumbersEntity.getImportant())
                s += telnumbersEntity.getValue() + ",";
        }
        s = s.substring(0, s.length() - 1);
        return s;
    }

    public void setPreSelectedPhonesPrivate(String preSelectedPhonesPrivate) {
        this.preSelectedPhonesPrivate = preSelectedPhonesPrivate;
    }

    public String preSelectedAcademicDegreesBefore() {
        return contactsEntity.getAcademicDegreeBefore();
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public List<String> getSelectedColumns() {
        return selectedColumns;
    }

    public void setSelectedColumns(List<String> selectedColumns) {
        this.selectedColumns = selectedColumns;
    }

    public InputStream getFileInputStream() {
        return fileInputStream;
    }

    public void setFileInputStream(InputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }

    public List<Integer> getContactsToExport() {
        return contactsToExport;
    }

    public void setContactsToExport(List<Integer> contactsToExport) {
        this.contactsToExport = contactsToExport;
    }
}
