package at.binarycheese.pixi.config.spring;

import at.binarycheese.pixi.domain.RolesEntity;
import at.binarycheese.pixi.domain.UsersEntity;
import at.binarycheese.pixi.repositories.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;


@Service("userDetailsService")
public class MyUserDetailsService implements UserDetailsService {

    @Autowired
    UsersRepository usersRepository;

    @Transactional(readOnly = true)
    @Override
    public UserDetails loadUserByUsername(final String username)
            throws UsernameNotFoundException {
        UsersEntity user = usersRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("Username not Found");
        }
        List<GrantedAuthority> authorities =
                buildUserAuthority(user.getRoles());

        return buildUserForAuthentication(user, authorities);
    }


    // Converts UserEntity to
    // org.springframework.security.core.userdetails.User
    public static MyUser buildUserForAuthentication(UsersEntity user, List<GrantedAuthority> authorities) {
        MyUser userForSpring = new MyUser(user.getUsername(), user.getPassword(), !user.getDeactivated(), true, true, true, authorities);
        userForSpring.setName(user.getContactsByContactsId().getName());
        userForSpring.setSurname(user.getContactsByContactsId().getSurname());
        userForSpring.setTitle(user.getContactsByContactsId().getTitle());
        return userForSpring;
    }

    public static List<GrantedAuthority> buildUserAuthority(Collection<RolesEntity> userPermissions) {
        Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>();
        // Build user's authorities
        for (RolesEntity permission : userPermissions) {
            setAuths.add(new SimpleGrantedAuthority(permission.getName()));
        }
        List<GrantedAuthority> Result = new ArrayList<GrantedAuthority>(setAuths);

        return Result;
    }


}