package at.binarycheese.pixi.repositories;

import at.binarycheese.pixi.domain.LanguagesEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Dominik on 29.01.2015.
 */
public interface LanguagesRepository extends CrudRepository<LanguagesEntity, Integer> {
}
