package at.binarycheese.pixi.repositories;

import at.binarycheese.pixi.domain.CountriesEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Dominik on 29.01.2015.
 */
public interface CountriesRepository extends CrudRepository<CountriesEntity, String> {
}
