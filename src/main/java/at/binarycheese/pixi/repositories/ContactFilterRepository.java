package at.binarycheese.pixi.repositories;

import at.binarycheese.pixi.domain.ContactsEntity;

import java.text.ParseException;
import java.util.List;

/**
 * Created by Dominik on 30.01.2015.
 */
public interface ContactFilterRepository {
    public List<ContactsEntity> searchByParams(String searchString, List<String> params, List<String> paramsDatatypes, boolean showAll) throws ParseException,RuntimeException;
}
