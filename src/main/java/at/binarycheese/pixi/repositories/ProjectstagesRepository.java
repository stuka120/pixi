package at.binarycheese.pixi.repositories;

import at.binarycheese.pixi.domain.ProjectstagesEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Dominik on 30.01.2015.
 */
public interface ProjectstagesRepository extends CrudRepository<ProjectstagesEntity, Integer> {
}
