package at.binarycheese.pixi.repositories;

import at.binarycheese.pixi.domain.ContactsEntity;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created by Dominik on 29.01.2015.
 */
public interface ContactsRepository extends JpaRepository<ContactsEntity, Integer> {
    Long deleteById(int id);

    Page<ContactsEntity> findDistinctBy(Pageable pageable);

    ContactsEntity findDistinctById(Integer id);

    int countByOrganisationsByOrganisationsId_Id(Integer id);

    //This part gives me the Contacts without an Organisation

    List<ContactsEntity> findByOrganisationsByOrganisationsIdIsNull();

    @Query("select c from ContactsEntity as c where c.organisationsByOrganisationsId is null and (c.surname like ?1 or c.name like ?2)")
    List<ContactsEntity> findByOrganisationsByOrganisationsIdIsNullAndNameLikeOrSurnameLikeAllIgnoreCase(String name, String surname);


    List<ContactsEntity> findByOrganisationsByOrganisationsId_Id(Integer id);

    List<ContactsEntity> findByNameLikeOrSurnameLikeAllIgnoreCase(String name, String surname);

    List<ContactsEntity> findDistinctByNameLikeOrSurnameLikeOrOrganisationsByOrganisationsId_NameLikeOrEmails_ValueLikeOrTelnumbers_ValueLikeAllIgnoreCase(String name, String surname, String organisation, String telnumber, String email, Pageable page);

    int countDistinctByNameLikeOrSurnameLikeOrOrganisationsByOrganisationsId_NameLikeOrEmails_ValueLikeOrTelnumbers_ValueLikeAllIgnoreCase(String name, String surname, String organisation, String telnumber, String email);

    //Telnumbers and Email Ordering
    Page<ContactsEntity> findDistinctByOrderByEmails_importantDesc(Pageable pageable);

    Page<ContactsEntity> findByNameLikeAndSurnameLikeOrNameLikeAndSurnameLikeAllIgnoreCase(String name1, String surname1, String name2, String surname2, Pageable pageable);

    Long countByNameLikeOrSurnameLikeAllIgnoreCase(String name, String surname);

    ContactsEntity findByUsersesById_Username(String username);

    List<ContactsEntity> findAllByIdIn(List<Integer> ids);
}
