package at.binarycheese.pixi.repositories;

import at.binarycheese.pixi.domain.RolesEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Dominik on 29.01.2015.
 */
public interface RolesRepository extends CrudRepository<RolesEntity, Integer> {
    List<RolesEntity> findByNameNotIn(String[] rolesName);

    @Query("select users.size from RolesEntity where name = ?1")
    int countRoles(String name);
}
