package at.binarycheese.pixi.repositories;

import at.binarycheese.pixi.domain.PostleitzahlEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Joyce on 24.02.2015.
 */
public interface PostleitzahlRepository extends JpaRepository<PostleitzahlEntity, Integer> {
    PostleitzahlEntity findByPlz(int plz);

    List<PostleitzahlEntity> findDistinctByNameLike(String ort);

    List<PostleitzahlEntity> findDistinctByPlzLike(int plz);
}
