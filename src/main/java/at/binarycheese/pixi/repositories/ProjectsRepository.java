package at.binarycheese.pixi.repositories;

import at.binarycheese.pixi.domain.ProjectEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Dominik on 30.01.2015.
 */
public interface ProjectsRepository extends CrudRepository<ProjectEntity, Integer> {
    int countByProjectnumber(String projektNumber);

    List<ProjectEntity> findAllByProjectstagesByProjectStage_nameNot(String name);

    List<ProjectEntity> findAllByProjectstagesByProjectStage_name(String name);

    Page<ProjectEntity> findByNameLikeAndProjectnumberLikeOrNameLikeAndProjectnumberLike(String name1, String projectnumber1, String name2, String projectnumber2, Pageable pageable);

    long countByNameLikeAndProjectnumberLikeOrNameLikeAndProjectnumberLike(String name1, String projectnumber1, String name2, String projectnumber2);

    List<ProjectEntity> findByProjectnumber(String projectnumber);
}
