package at.binarycheese.pixi.repositories.impl;

import at.binarycheese.pixi.domain.ContactsEntity;
import at.binarycheese.pixi.repositories.ContactFilterRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Dominik on 30.01.2015.
 */

@Repository
public class ContactFilterRepositoryImpl implements ContactFilterRepository {

    protected EntityManager entityManager;

    public EntityManager getEntityManager() {
        return entityManager;
    }
    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public List<ContactsEntity> searchByParams(String searchString, List<String> params, List<String> paramsDatatypes, boolean showAll) throws ParseException,RuntimeException {
        try {
            Query query = getEntityManager().createQuery(searchString);
            for (int i = 0; i < params.size(); i++) {
                if (paramsDatatypes.get(i) == "date") {
                    SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
                    Date date = formatter.parse(params.get(i));
                    query.setParameter(i+1, date);
                } else if (paramsDatatypes.get(i) == "int") {
                    query.setParameter(i+1, Integer.parseInt(params.get(i).trim()));
                } else if (paramsDatatypes.get(i) == "string") {
                    query.setParameter(i+1, params.get(i).trim().replace("'", "").toLowerCase());
                } else if (paramsDatatypes.get(i) == "boolean") {
                    query.setParameter(i+1, Boolean.parseBoolean(params.get(i).trim()));
                }
            }
            List erg;
            if(showAll) {
                erg = query.getResultList();
            } else {
                erg = query.setMaxResults(100).getResultList();
            }
            return erg;
        } catch (ClassCastException | ParseException e) {
            throw e;
        } catch (NumberFormatException e) {
            throw e;
        } catch (RuntimeException e) {
            throw e;
        }
    }
}
