package at.binarycheese.pixi.repositories;

import at.binarycheese.pixi.domain.WorkingtimesEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by Dominik on 28.01.2015.
 */
@Transactional
public interface WorkingTimesRepository extends CrudRepository<WorkingtimesEntity, Integer> {
    List<WorkingtimesEntity> findByUsersByUserId_Id (int userId);

    List<WorkingtimesEntity> findByUsersByUserId_IdAndStartTimeBetweenAndDesc(int userId, Date startDate, Date endDate, String description);

    WorkingtimesEntity findByUsersByUserId_UsernameAndStartTime(String name, Date startDate);
}
