package at.binarycheese.pixi.repositories;

import at.binarycheese.pixi.domain.EventsEntity;
import org.springframework.data.repository.CrudRepository;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created by Dominik on 29.01.2015.
 */
public interface EventsRepository extends CrudRepository<EventsEntity, Integer> {
    EventsEntity findDistinctById(Integer id);

    List<EventsEntity> findByUsersByResponsible_UsernameAndStartDateBetweenAndChecklistShown(String username,Timestamp startDate, Timestamp endDate, Byte checklistShown);

    List<EventsEntity> findByTitleAndLocation(String title, String location);

    List<EventsEntity> findByStartDateBetweenOrEndDateBetweenAndEventcategoriesByEventCategorieId_IdIn(Timestamp startbeginn, Timestamp startend, Timestamp endbeginn, Timestamp endend, List<Integer> ids);

    //List<EventsEntity> findByStartDateAndEndDateAndEventcategoriesByEventCategorieId_idIn(Timestamp startDate,Timestamp endDate, int[] id);

    //List<EventsEntity> findByStartDateAndEndDateAndEventcategoriesByEventCategorieId_idIn(Timestamp startDate,Timestamp endDate, int[] id);
}
