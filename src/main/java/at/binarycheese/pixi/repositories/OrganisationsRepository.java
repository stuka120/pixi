package at.binarycheese.pixi.repositories;

import at.binarycheese.pixi.domain.OrganisationsEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Dominik on 29.01.2015.
 */
public interface OrganisationsRepository extends CrudRepository<OrganisationsEntity, Integer> {
    public List<OrganisationsEntity> findByName(String name);
}
