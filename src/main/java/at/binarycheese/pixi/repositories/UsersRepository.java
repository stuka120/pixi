package at.binarycheese.pixi.repositories;

import at.binarycheese.pixi.domain.UsersEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import at.binarycheese.pixi.domain.UserworksEntity;
import org.springframework.data.repository.CrudRepository;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import java.util.List;

/**
 * Created by Dominik on 28.01.2015.
 */

public interface UsersRepository extends CrudRepository<UsersEntity, Integer> {
    UsersEntity findByUsername(String username);

    @Query("SELECT CASE WHEN COUNT(u) > 0 THEN 'true' ELSE 'false' END FROM UsersEntity u WHERE u.username = ?1")
    public Boolean usernameExists(String username);

    @Query("select CASE WHEN COUNT(e) > 0 THEN 'true' ELSE 'false' END from EventsEntity e where e.usersByResponsible.username = ?1 and e.startDate>?2 and e.startDate<?3 and e.checklistShown = 0")
    public Boolean isResponsible(String username, Timestamp from, Timestamp to);

    List<UsersEntity> findByUsernameLike(String search);

    List<UsersEntity> findByDeactivated(boolean deactivated);

    Page<UsersEntity> findByContactsByContactsId_NameLikeOrContactsByContactsId_SurnameLikeAllIgnoreCase(String name, String surname, Pageable pageable);

    Long countByContactsByContactsId_NameLikeOrContactsByContactsId_SurnameLikeAllIgnoreCase(String name,String surname);
}
