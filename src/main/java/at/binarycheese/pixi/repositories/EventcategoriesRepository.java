package at.binarycheese.pixi.repositories;

import at.binarycheese.pixi.domain.EventcategoriesEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Dominik on 29.01.2015.
 */
public interface EventcategoriesRepository extends CrudRepository<EventcategoriesEntity, Integer> {
}
