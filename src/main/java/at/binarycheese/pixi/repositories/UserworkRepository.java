package at.binarycheese.pixi.repositories;

import at.binarycheese.pixi.domain.UserworksEntity;
import org.springframework.data.repository.CrudRepository;

import javax.jws.soap.SOAPBinding;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created by Lukas on 10.02.2015.
 */
public interface UserworkRepository extends CrudRepository<UserworksEntity, Integer>{
    public List<UserworksEntity> findByEvent_Id(int id);
    public List<UserworksEntity> findByStartTimeLessThanAndEndTimeGreaterThanAndUser_Id(Timestamp startTime, Timestamp endTime, int user_Id);

    public List<UserworksEntity> findByUser_UsernameAndEvent_Title(String username, String title);
}
