package at.binarycheese.pixi.repositories;

import at.binarycheese.pixi.domain.AcademicDegree;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Dominik on 15.03.2015.
 */
public interface AcademicDegreeRepository  extends JpaRepository<AcademicDegree, Integer>{

    List<AcademicDegree> findByDegreeLikeOrDescriptionLikeAllIgnoreCase(String degree, String description);

}
