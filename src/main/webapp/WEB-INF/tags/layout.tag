<%@tag description="Overall Page template" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@attribute name="navbarLeft" fragment="true" %>
<%@attribute name="search" fragment="true" %>
<%@attribute name="footer" fragment="true" %>
<%@attribute name="head" fragment="true" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="/assets/css/foundation.css">
    <link rel="stylesheet" href="/assets/css/normalize.css">
    <link rel="stylesheet" href="/assets/css/style.css">
    <link rel="stylesheet" href="/assets/css/font-awesome.css">
    <link rel="stylesheet" href="../../assets/css/jquery-ui.css"/>
    <link rel="shortcut icon" href="../../assets/img/pixi-logo.png" />
    <link rel="stylesheet" type="text/css" href="../../assets/css/sweet-alert.css">
    <link rel="stylesheet" type="text/css" href="../../assets/css/iosOverlay.css">
    <script src="/assets/js/jquery-2.1.1.js"></script>
    <script src="/assets/js/modernizr.js"></script>
    <script src="/assets/js/foundation/foundation.js"></script>
    <script src="/assets/js/foundation/foundation.dropdown.js"></script>
    <script src="/assets/js/foundation/foundation.topbar.js"></script>
    <script type="text/javascript" src="../../assets/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../../assets/js/spin.min.js"></script>
    <script type="text/javascript" src="../../assets/js/iosOverlay.js"></script>
    <script type="text/javascript" src="../../assets/js/sweet-alert.js"></script>

    <jsp:invoke fragment="head"/>
    <sec:authorize access="hasRole('New User')">
        <script type="text/javascript" src="../../assets/js/foundation/foundation.alert.js"></script>
        <style>
            .reveal-modal.open {
                top: 25% !important;
            }

            .modal-title {
                color: white;
                height: 57px;
                position: relative;
                text-shadow: rgba(0, 0, 0, 0.247059) 0px 1px 3px;
                perspective-origin: 280px 28.5px;
                transform-origin: 280px 28.5px;
                background: #333333;
                border-top: 0px none rgb(255, 255, 255);
                border-right: 0px none rgb(255, 255, 255);
                border-bottom: 1px solid rgba(0, 0, 0, 0);
                border-left: 0px none rgb(255, 255, 255);
                font: normal normal normal normal 22px/normal sans-serif;
                outline: rgb(255, 255, 255) none 0px;
                overflow: hidden;
                padding: 15px 20px;
            }

            .modal-title h3 {
                color: rgb(255, 255, 255);
                display: inline-block;
                height: 26px;
                text-shadow: rgba(0, 0, 0, 0.247059) 0px 1px 3px;
                width: 300px;
                perspective-origin: 44.640625px 13px;
                transform-origin: 44.640625px 13px;
                border: 0px none rgb(255, 255, 255);
                font: normal normal normal normal 22px/normal sans-serif;
                margin: 0px;
                outline: rgb(255, 255, 255) none 0px;
            }

            .modal-text {
                color: rgb(68, 68, 68);
                perspective-origin: 280px 29px;
                transform-origin: 280px 29px;
                border: 0px none rgb(68, 68, 68);
                font: normal normal normal normal 14px/18px sans-serif;
                outline: rgb(68, 68, 68) none 0px;
                overflow: auto;
                padding: 20px 20px 0 20px;
            }

            .modal-buttons {
                color: rgb(68, 68, 68);
                height: 63px;
                text-align: right;
                perspective-origin: 280px 31.5px;
                transform-origin: 280px 31.5px;
                background: rgb(250, 250, 250) none repeat scroll 0% 0% / auto padding-box border-box;
                border-top: 1px solid rgb(221, 221, 221);
                border-right: 0px none rgb(68, 68, 68);
                border-bottom: 0px none rgb(68, 68, 68);
                border-left: 0px none rgb(68, 68, 68);
                font: normal normal normal normal 16px/normal sans-serif;
                outline: rgb(68, 68, 68) none 0px;
                padding: 15px 20px;
            }

            .modal-buttons > a {
                box-shadow: rgba(0, 0, 0, 0.0784314) 0px 1px 1px 0px;
                color: rgb(255, 255, 255);
                cursor: pointer;
                display: inline-block;
                height: 32px;
                text-align: center;
                text-decoration: none;
                vertical-align: middle;
                white-space: nowrap;
                perspective-origin: 22px 16px;
                transform-origin: 22px 16px;
                /*background: rgb(66, 139, 202) none repeat scroll 0% 0% / auto padding-box border-box;*/
                background: #333333;
                font: normal normal bold normal 12px/normal sans-serif;
                outline: rgb(255, 255, 255) none 0px;
                padding: 8px 12px;
            }

            .modal-inner {
                color: rgb(68, 68, 68);
                /*width: 560px;*/
                perspective-origin: 280px 89px;
                transform-origin: 280px 89px;
                border: 0px none rgb(68, 68, 68);
                font: normal normal normal normal 16px/normal sans-serif;
                outline: rgb(68, 68, 68) none 0px;
            }
        </style>
        <script>
            $(document).ready(function () {
                <sec:authorize access="hasRole('New User')">
                    hideOverlay();
                </sec:authorize>
                $('#pwResetModel').foundation('reveal', 'open');
                $("#savePassword").click(function () {
                    var passwort = $("#passwort").val();
                    var passwortWH = $("#passwortWH").val();
                    if (passwort == "") {
                        $("#pwResetModel .alert-box").html("<s:text name="emptyPw" />");
                        $("#pwResetModel .alert-box").show();
                    } else if (passwort != passwortWH) {
                        $("#pwResetModel .alert-box").html("<s:text name="diffPw" />");
                        $("#pwResetModel .alert-box").show();
                    } else if (passwort.length < 6) {
                        $("#pwResetModel .alert-box").html("<s:text name="shortPw" />");
                    } else if (passwort == passwortWH) {
                        $("#pwResetModel .alert-box").hide();
                        $.ajax({
                            type: "GET",
                            url: "<s:url namespace="/user" action="changePassword"></s:url>",
                            data: {
                                confirmPW: $("#passwort").val()
                            },
                            cache: false,
                            success: function (data) {
                                swal({
                                    title: "<s:text name="savedPW" />",
                                    text: "<s:text name="changedPw" />",
                                    type: "success"
                                }, function () {
                                    window.location.reload();
                                });
                            }
                        });
                    }
                });
                $("#passwort, #passwortWH").keyup(function(event){
                    if(event.keyCode == 13){
                        $("#savePassword").click();
                    }
                });
            });
        </script>
    </sec:authorize>
</head>
<body>
<s:action name="navbar" flush="true" executeResult="true" namespace="/"/>
<jsp:doBody/>
<sec:authorize access="hasRole('New User')">
    <div id="pwResetModel" class="reveal-modal small" data-reveal aria-labelledby="modalTitle" aria-hidden="true"
         style="padding: 0" data-options="close_on_background_click:false;close_on_esc:false;" role="dialog">
        <div class="modal-inner">
            <div class="modal-title">
                <h3><s:text name="passwordReset" /></h3>
            </div>
            <div class="modal-text">
                <s:text name="passwordResetMsg" /><br/><br/>

                <div data-alert class="alert-box alert radius" style="display: none;">
                    <s:text name="passwordResetErr" />
                </div>
                <input type="password" id="passwort" placeholder="<s:text name="pw" />" tabindex="1">
                <input type="password" id="passwortWH" placeholder="<s:text name="pwRepeat" />" tabindex="2">
            </div>
            <div class="modal-buttons">
                <a href="#" id="savePassword" tabindex="3"><s:text name="savePw" /></a>
            </div>
        </div>
    </div>
</sec:authorize>
<script>
    $(document).foundation();
    var spinner = null;
    var overlay = null;
    function showOverlay() {
        if(overlay != null){
            return;
        }
        var opts = {
            lines: 13, // The number of lines to draw
            length: 11, // The length of each line
            width: 5, // The line thickness
            radius: 17, // The radius of the inner circle
            corners: 1, // Corner roundness (0..1)
            rotate: 0, // The rotation offset
            color: '#FFF', // #rgb or #rrggbb
            speed: 1, // Rounds per second
            trail: 60, // Afterglow percentage
            shadow: false, // Whether to render a shadow
            hwaccel: false, // Whether to use hardware acceleration
            className: 'spinner', // The CSS class to assign to the spinner
            zIndex: 2e9, // The z-index (defaults to 2000000000)
            top: 'auto', // Top position relative to parent in px
            left: 'auto' // Left position relative to parent in px
        };
        var target = document.createElement("div");
        document.body.appendChild(target);
        spinner = new Spinner(opts).spin(target);
        overlay = iosOverlay({
            text: "Loading",
            spinner: spinner
        });
    }
    function hideOverlay() {
        if(overlay!=null){
            overlay.hide();
            overlay = null;
        }
    }
</script>
</body>
</html>