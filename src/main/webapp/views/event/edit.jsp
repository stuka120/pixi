<%@ page contentType="text/html;charset=iso-8859-1" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<style>
    .topfix {
        display: block;
        position: relative;
        z-index: 2;
        text-align: center;
        width: 100%;
        padding-top: 0;
        padding-bottom: 0;
        border-style: solid;
        border-width: 1px;
        overflow: hidden;
        font-size: 0.875rem;
        height: 2.3125rem;
        line-height: 2.3125rem;
        background: #f2f2f2;
        border-bottom: none;
        color: #333333;
        border-color: #cccccc;
    }
</style>
<script type="text/javascript">
    //# sourceURL=EditEvent.js
    $(".timepicker").timepicker({
        'timeFormat': 'H:i',
        'scrollDefault': 'now',
        'step': 15
    });
    $("#startdate").datepicker({
        dateFormat: "dd.mm.yy",
        onSelect: function () {
            $("#enddate").datepicker("option", "minDate", $(this).val());
            $(this).blur();
        }
    }, $.datepicker.regional['<s:text name="languageCode" />']).datepicker('widget').wrap('<div class="ll-skin-cangas"/>');
    $("#enddate").datepicker({
        dateFormat: "dd.mm.yy",
        onSelect: function () {
            $("#startdate").datepicker("option", "maxDate", $(this).val());
            $(this).blur();
        }
    }, $.datepicker.regional['<s:text name="languageCode" />']).datepicker('widget').wrap('<div class="ll-skin-cangas"/>');
    $("#deleteEventForm").ajaxForm();
    $("#enddate").datepicker("option", "minDate", $("#startdate").datepicker('getDate'));
    $("#startdate").datepicker("option", "maxDate", $("#enddate").datepicker('getDate'));
    $("#deleteEvent").on("click", function () {
        swal({
            title: "<s:text name="are_you_sure"></s:text>",
            text: "<s:text name="will_be_permanent"></s:text>",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "<s:text name="yes"></s:text>",
            cancelButtonText: "<s:text name="cancel"></s:text>",
            closeOnConfirm: false
        }, function () {
            $("#deleteEventForm").ajaxSubmit({
                success: function () {
                    swal({
                        title: "<s:text name="event_deleted"></s:text>",
                        type: "success"
                    }, function () {
                        $("#eventInfo").foundation('reveal', 'close');
                        applyEventFilter();
                    });
                },
                error: function () {
                    swal({
                        title: "<s:text name="event_not_deleted"></s:text>",
                        type: "error"
                    }, function () {
                        $("#eventInfo").foundation('reveal', 'close');
                    });
                }
            });
        });
    });

    $.subscribe('beforeSubmit', function (event, data) {
        $("#formerrors").empty();
        $("#formerrors").hide();
        $(".prefix").css("background-color", "#f2f2f2");
        $("#stateName").val($("#stateList option:selected").val());
    });
    $.subscribe('onSuccess', function (event, data) {
        if (parseInt(event.originalEvent.data.toString().trim()) > 0) {
            $("#eventInfo").foundation('reveal', 'close');
            swal({
                title: "<s:text name="event_saved"></s:text>",
                text: "",
                type: "success"
            }, function () {
                applyEventFilter();
            });
        } else {
            swal("<s:text name="SomethingWentWrong"></s:text>", "", "error");
        }
    });

    $("#fullday").change(function () {
        fullday(this.checked);
    });
    function fullday(isfullday) {
        var fulldate = isfullday;
        $('.notfullday').each(function (index) {
            $(this).val("");
            $(this).prop('disabled', fulldate);
        });
    }
    function customValidation(form, errors) {
        var list = $('#formerrors');
        list.show();
        if (errors.errors) {
            $.each(errors.errors, function (index, value) {
                list.append('<li>' + value + '</li>\n');
            });
        }
        if (errors.fieldErrors) {
            $.each(errors.fieldErrors, function (index, value) {
                list.append('<li>' + value + '</li>\n');
                $("input[name='" + index + "']").closest(".row").find(".prefix").css("background-color", "#f04124");
            });
        }
    }
    $("#eventContactPerson2").select2({
        placeholder: "<s:text name="searchForContact"></s:text>",
        allowClear: true,
        ajax: {
            url: "<s:url namespace="/contacts" action="searchAllContacts"></s:url>",
            dataType: 'json',
            quietMillis: 400,
            data: function (term, page) { // page is the one-based page number tracked by Select2
                return {
                    query: term, //search term
                    page: page - 1, // page number
                    size: 20
                };
            },
            results: function (data, page) {
                var more = (page * 20) < data.total_count;
                var result = [];
                $.each(data.items, function (index, item) {
                    result.push({
                        id: index,
                        text: item
                    });
                });
                return {results: result, more: more};
            }
        }
    });
    $("#eventProjects").select2({
        placeholder: "<s:text name="searchForProject"></s:text>",
        allowClear: true,
        ajax: {
            url: "<s:url namespace="/project" action="searchAllProjects"></s:url>",
            dataType: 'json',
            quietMillis: 400,
            data: function (term, page) { // page is the one-based page number tracked by Select2
                return {
                    query: term, //search term
                    page: page - 1, // page number
                    size: 20
                };
            },
            results: function (data, page) {
                var more = (page * 20) < data.total_count;
                var result = [];
                $.each(data.items, function (index, item) {
                    result.push({
                        id: index,
                        text: item
                    });
                });
                return {results: result, more: more};
            }
        }
    });
    $("#usersByResponsible").select2({
        placeholder: "<s:text name="searchForResponsible"></s:text>",
        allowClear: true,
        ajax: {
            url: "<s:url namespace="/user" action="searchAllUsers"></s:url>",
            dataType: 'json',
            quietMillis: 400,
            data: function (term, page) { // page is the one-based page number tracked by Select2
                return {
                    query: term, //search term
                    page: page - 1, // page number
                    size: 20
                };
            },
            results: function (data, page) {
                var more = (page * 20) < data.total_count;
                var result = [];
                $.each(data.items, function (index, item) {
                    result.push({
                        id: index,
                        text: item
                    });
                });
                return {results: result, more: more};
            }
        }
    });
    $('#eventContactPerson2').select2('data', {
        id: $("#contactId").val(),
        text: $("#contactName").val()
    });
    $('#eventProjects').select2('data', {
        id: $("#projectId").val(),
        text: $("#projectDesc").val()
    });
    $('#usersByResponsible').select2('data', {
        id: $("#userResponsibleId").val(),
        text: $("#userResponsibleName").val()
    });
    $(document).ready(function () {
        var fullday2 = <s:property value="fullday"></s:property>;
        if ($("#enddate").val() == "" || fullday2) {
            $("#fullday").attr("checked", "checked");
            fullday(fullday2);
        }
        if ($("#stateName").val().trim() != "") {
            $("#stateList option[value=" + $("#stateName").val() + "]").prop('selected', 'wurscht');
        }
    });

</script>

<h2><s:text name="edit_event"></s:text></h2>

<div id="result"></div>
<div data-alert class="alert-box alert radius" id="formerrors" style="display: none"></div>
<input type="hidden" id="contactId" value="<s:property value="event.contactsByContactPerson.id"></s:property>">
<input type="hidden" id="contactName"
       value="<s:property value="event.contactsByContactPerson.surname"></s:property> <s:property value="event.contactsByContactPerson.name"></s:property>">
<input type="hidden" id="projectId" value="<s:property value="event.projectByProjectId.id"></s:property>">
<input type="hidden" id="projectDesc"
       value="<s:property value="event.projectByProjectId.projectnumber"></s:property> - <s:property value="event.projectByProjectId.name"></s:property>">
<input type="hidden" id="userResponsibleId" value="<s:property value="event.usersByResponsible.id"></s:property>">
<input type="hidden" id="userResponsibleName"
       value="<s:property value="event.usersByResponsible.contactsByContactsId.surname"></s:property> <s:property value="event.usersByResponsible.contactsByContactsId.name"></s:property>">

<s:form action="update" method="POST" id="editevent" cssStyle="width:100%" theme="simple">
    <sec:csrfInput></sec:csrfInput>
    <s:hidden name="event.id"></s:hidden>
    <div class="row">
        <div class="large-6 columns">
            <div class="row collapse prefix-radius">
                <div class="small-3 columns">
                    <span class="prefix"><s:text name="title"></s:text></span>
                </div>
                <div class="small-9 columns">
                    <s:textfield name="event.title" id="create_eventtitle"></s:textfield>
                </div>
            </div>
        </div>
        <div class="large-6 columns">
            <div class="row collapse prefix-radius">
                <div class="small-3 columns">
                    <span class="prefix">Ort</span>
                </div>
                <div class="small-9 columns">
                    <s:textfield name="event.location"/>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="margin-bottom: 10px">
        <div class="small-12 large-12 columns">
            <div class="row collapse prefix-radius">
                <div class="small-3 columns">
                    <span class="prefix"><s:text name="state"/> </span>
                </div>
                <div class="small-9 columns">
                    <input type="hidden" name="wtf" id="stateName"
                           value="<s:property value="event.state"></s:property>">
                    <select id="stateList" name="event.state">
                        <option value="">-</option>
                        <option value="Wien">Wien</option>
                        <option value="Niederösterreich">Niederösterreich</option>
                        <option value="Oberösterreich">Oberösterreich</option>
                        <option value="Burgenland">Burgenland</option>
                        <option value="Kärnten">Kärnten</option>
                        <option value="Steiermark">Steiermark</option>
                        <option value="Tirol">Tirol</option>
                        <option value="Salzburg">Salzburg</option>
                        <option value="Vorarlberg">Vorarlberg</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="margin-bottom: 10px">
        <div class="small-10 columns">
            <div class="row">
                <div class="large-6 small-12 columns">
                    <div class="row collapse prefix-radius">
                        <div class="small-3 large-5 columns">
                            <span class="prefix"><s:text name="startdate"></s:text></span>
                        </div>
                        <div class="small-9 large-7 columns">
                            <s:textfield name="startDate" value="%{event.startDate}" id="startdate"/>
                        </div>
                    </div>
                </div>
                <div class="large-6 small-12 columns">
                    <div class="row collapse prefix-radius">
                        <div class="small-3 large-5 columns">
                            <span class="prefix"><s:text name="event_starttime"></s:text></span>
                        </div>
                        <div class="small-9 large-7 columns">
                            <s:textfield name="startDate" value="%{starttime}" cssClass="timepicker notfullday"
                                         id="starttime"></s:textfield>
                        </div>
                    </div>
                </div>
                <div class="large-6 small-12 columns">
                    <div class="row collapse prefix-radius">
                        <div class="small-3 large-5 columns">
                            <span class="prefix"><s:text name="enddate"></s:text></span>
                        </div>
                        <div class="small-9 large-7 columns">
                            <s:textfield name="endDate" value="%{event.endDate}" id="enddate" cssClass="notfullday"/>
                        </div>
                    </div>
                </div>
                <div class="large-6 small-12 columns">
                    <div class="row collapse prefix-radius">
                        <div class="small-3 large-5 columns">
                            <span class="prefix"><s:text name="event_endtime"></s:text></span>
                        </div>
                        <div class="small-9 large-7 columns">
                            <s:textfield name="endDate" value="%{endtime}" cssClass="timepicker notfullday"
                                         id="endtime"></s:textfield>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="small-2 columns ">
            <div class="row valign-middle">
                <div class="small-12 columns">
                    <input id="fullday" name="fullday" <s:if test="fullday">checked="checked"</s:if>
                           type="checkbox">
                    <span>Ganztags</span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="large-6 columns">
            <div class="row collapse prefix-radius">
                <div class="small-3 columns">
                    <span class="prefix"><s:text name="category"></s:text></span>
                </div>
                <div class="small-9 columns">
                    <s:select list="categoriesList" listKey="id" listValue="name" name="eventCategory" id="catlist"
                              value="event.eventcategoriesByEventCategorieId.id"/>
                </div>
            </div>
        </div>
        <div class="large-6 columns">
            <div class="row collapse prefix-radius">
                <div class="small-3 columns">
                    <span class="prefix"><s:text name="contactPerson"></s:text></span>
                </div>
                <div class="small-9 columns">
                    <s:hidden id="eventContactPerson2" name="eventContactPerson"
                              value="event.contactsByContactPerson.id"
                              cssStyle="width: 100%;"></s:hidden>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 0.5em">
        <div class="large-6 columns">
            <div class="row collapse prefix-radius">
                <div class="small-3 columns">
                    <span class="prefix"><s:text name="event_project"/></span>
                </div>
                <div class="small-9 columns">
                    <s:hidden id="eventProjects" name="event.projectByProjectId.id" value=""
                              cssStyle="width: 100%;"></s:hidden>
                </div>
            </div>
        </div>
        <div class="large-6 columns">
            <div class="row collapse prefix-radius">
                <div class="small-3 columns">
                    <span class="prefix"><s:text name="event_responsible"></s:text></span>
                </div>
                <div class="small-9 columns">
                    <s:hidden id="usersByResponsible" name="event.usersByResponsible.id"
                              value="%{event.usersByResponsible.id}"
                              cssStyle="width: 100%;"></s:hidden>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 0.5em">
        <div class="small-3 columns">
            <label class="topfix"><s:text name="event_description"/></label>
        </div>
        <div class="large-12 columns">
            <s:textarea name="event.description"/>
        </div>
    </div>
    <div class="row" style="margin-top: 0.7em">
        <div class="small-4 columns text-left">
            <a id="deleteEvent" class="button radius small"><s:text name="event_delete"></s:text></a>
        </div>
        <div class="small-6 columns text-right">
            <div class="right text-right">
                <s:text name="save_event" var="save_event"/>
                <sj:submit
                        cssClass="button radius"
                        targets="result"
                        button="true"
                        validate="true"
                        validateFunction="customValidation"
                        onBeforeTopics="beforeSubmit"
                        onSuccessTopics="onSuccess"
                        value="%{save_event}"
                        />
            </div>
        </div>

    </div>
</s:form>
<s:form action="delete" method="POST" id="deleteEventForm" theme="simple">
    <sec:csrfInput></sec:csrfInput>
    <input type="hidden" name="eventId" value="<s:property value="event.id"></s:property>"/>
</s:form>
