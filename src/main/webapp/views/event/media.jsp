<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<script type="text/javascript" src="../../assets/lib/dropzone.js"></script>
<link rel="stylesheet" href="../../assets/css/dropzone.css"/>
<link rel="stylesheet" href="../../assets/css/basic.css"/>

<style>
    .dropzone {
        border: 5px dotted lightskyblue;
        max-height: 25px;
    }
</style>

<sec:csrfMetaTags></sec:csrfMetaTags>
<div class="row" style="max-height: 300px; overflow-x: auto;">
    <table id="files" class="small-12 columns table">
    </table>
</div>

<div class="row">
    <div class="small-12 columns" style="padding-left: 0px !important; padding-right: 0px !important;">
        <form action="/event/upload" class="dropzone" id="my-awesome-dropzone" enctype="multipart/form-data" style="overflow-x: auto">
            <s:hidden name="eventId"></s:hidden>
            <sec:csrfInput></sec:csrfInput>
        </form>
    </div>
</div>
