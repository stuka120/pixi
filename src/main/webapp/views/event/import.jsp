<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<t:layout>
    <jsp:attribute name="head">
        <title>Import</title>
        <sec:csrfMetaTags></sec:csrfMetaTags>
        <script type="application/javascript" src="../../assets/js/jquery.form.js"></script>
        <script>
            $(document).ready(function () {
                var csrfToken = $("meta[name='_csrf']").attr("content");
                $("#uploadForm").attr("action", $("#uploadForm").attr("action") + "?_csrf=" + csrfToken);
                $("#uploadedFileTxt").on("click", function () {
                    $("#uploadedFile").click();
                });
                $("#uploadedFile").on("change", function () {
                    $("#uploadedFileTxt").val($(this).val());
                });
            });
        </script>
        <style>
            ul {
                list-style: none;
            }
        </style>
    </jsp:attribute>
    <jsp:body>
        <s:form id="uploadForm" action="import" method="post" enctype="multipart/form-data" theme="simple">
            <div class="row">
                <div class="large-12 columns">
                    <div class="row">
                        <h2><s:text name="import_from_google"></s:text></h2>
                    </div>
                    <div class="row">
                        <s:fielderror name="errorMessage" cssClass="alert-box alert radius"></s:fielderror>
                        <s:actionerror cssClass="alert-box alert radius"></s:actionerror>
                    </div>
                    <div class="row collapse">
                        <div class="small-2 columns">
                            <span class="prefix"><s:text name="category"/></span>
                        </div>
                        <div class="small-9 columns">
                            <s:select list="categoriesList" listKey="id" listValue="name" name="eventCategory"
                                      label="Kategorie"/>
                        </div>
                        <div class="small-1 right">
                        </div>
                    </div>
                    <div class="row collapse">
                        <div class="small-2 columns">
                            <span class="prefix"><s:text name="choose_file"/></span>
                        </div>
                        <div class="small-9 columns">
                            <input type="url" id="uploadedFileTxt" readonly>
                            <s:file name="uploadedFile" id="uploadedFile" label="File" cssStyle="visibility: hidden"/>
                        </div>
                        <div class="small-1 right">
                            <s:text name="import" var="import"/>
                            <s:submit cssClass="button postfix" value="%{import}"/>
                        </div>
                    </div>
                    <div class="row collapse">
                        <div class="small-2 columns"></div>
                        <div class="small-9 columns">
                        </div>
                        <div class="small-1 right"></div>
                    </div>
                    <div class="row" id="uploadProgress" style="visibility: hidden;">
                        <label><s:text name="progress"/></label>

                        <div class="progress small-12 success round">
                            <span class="meter" style="width: 0%"></span>
                        </div>
                    </div>
                </div>
            </div>
        </s:form>
    </jsp:body>
</t:layout>