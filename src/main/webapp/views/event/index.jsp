<%@ page contentType="text/html;charset=ISO-8859-1" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<t:layout>
    <jsp:attribute name="head">
        <title><s:text name="packetName"/></title>
        <link rel="stylesheet" href="../../assets/css/fullcalendar.css"/>
        <link rel="stylesheet" href="../../assets/css/livevalidation.css"/>
        <link rel="stylesheet" href="../../assets/css/jquery.simplecolorpicker.css"/>
        <link rel="stylesheet" href="../../assets/css/jquery.timepicker.css"/>
        <link rel="stylesheet" href="../../assets/css/dataTables.foundation.css"/>
        <link rel="stylesheet" href="../../assets/css/jquery.dataTables.css"/>
        <link rel="stylesheet" href="../../assets/css/select2-foundation5.css"/>
        <link type="text/css" rel="stylesheet" href="../../assets/css/cangas.datepicker.css"/>

        <script type="text/javascript" src="../../assets/js/moment.js"></script>
        <script type="text/javascript" src="../../assets/js/fullcalendar.js"></script>
        <script type="text/javascript" src="../../assets/js/lang-all.js"></script>
        <script type="text/javascript" src="../../assets/js/foundation/foundation.reveal.js"></script>
        <script type="text/javascript" src="../../assets/js/foundation/foundation.tab.js"></script>
        <script type="text/javascript" src="../../assets/js/foundation/foundation.clearing.js"></script>
        <script type="text/javascript" src="/struts/js/plugins/jquery.subscribe.js?s2j=3.7.1"></script>
        <script type="text/javascript" src="../../assets/js/struts/jquery.ui.struts.js"></script>
        <script type="text/javascript" src="../../assets/js/struts/jquery.struts.js"></script>
        <script type="text/javascript" src="../../assets/js/livevalidation.js"></script>
        <script type="text/javascript" src="../../assets/js/jquery.form.js"></script>
        <script type="text/javascript" src="../../assets/js/jquery.simplecolorpicker.js"></script>
        <script type="text/javascript" src="../../assets/js/jquery.timepicker.js"></script>
        <script type="text/javascript" src="../../assets/js/imgpreview.full.jquery.js"></script>
        <script type="text/javascript" src="../../assets/js/jquery.dataTables.js"></script>
        <script type="text/javascript" src="../../assets/js/dataTables.foundation.js"></script>
        <script type="text/javascript" src="../../assets/js/select2.js"></script>
        <script type="text/javascript" src="../../assets/js/jquery.form_populator.js"></script>
        <script src="../../assets/js/datepicker-<s:text name="languageCode" />.js"></script>
        <script type="text/javascript" src="../../assets/js/select2.js"></script>
        <s:if test="showChecklist">
            <link rel="stylesheet" href="../../assets/css/animate.css"/>
            <script type="text/javascript" src="../../assets/js/jquery.noty.js"></script>
        </s:if>
        <style>
            .valign-middle {
                display: table;
            }

            .valign-middle .columns {
                display: table-cell;
                vertical-align: middle;
            }

            .valign-middle .columns,
            .valign-middle [class*="column"] + [class*="column"]:last-child {
                float: none;
            }

            .topfix {
                display: block;
                position: relative;
                z-index: 2;
                text-align: center;
                width: 100%;
                padding-top: 0;
                padding-bottom: 0;
                border-style: solid;
                border-width: 1px;
                overflow: hidden;
                font-size: 0.875rem;
                height: 2.3125rem;
                line-height: 2.3125rem;
                background: #f2f2f2;
                border-bottom: none;
                color: #333333;
                border-color: #cccccc;
            }

            .fa {
                font-size: 20px;
                padding-top: 2px;
                margin-left: 5px;
            }

            #imgPreviewWithStyles {
                background: #222;
                -moz-border-radius: 10px;
                -webkit-border-radius: 10px;
                padding: 15px;
                z-index: 2000;
                border: none;
            }

            button:hover {
                color: #333;
                background-color: #f5f5f5;
            }

            #eventInfo {
                background-color: white;
            }

            .categories {
                height: 28px;
            }

            .categories:hover {
                background-color: #eee;
            }

            .simplecolorpicker {
                border: 1px solid #003f54 !important;
            }
        </style>
        <script type="text/javascript">
            //@ sourceURL=IndexEvent.js
            $(document).ready(function () {
                $("body").on('click', '.deletemedia', function () {
                    var event_id = $(this).attr('event_id');
                    var fileurl = $(this).attr('fileurl');
                    $.ajax({
                        url: "/event/deleteMedia?eventId=" + event_id + "&fileUrl=" + fileurl
                    }).done(function (data) {
                        if (data == "success") {
                            swal("<s:text name="media_deleted"/>", "", "success")
                        } else {
                            swal("<s:text name="media_not_deleted"/>", "", "error")
                        }
                        loadFilelist(event_id);
                    });

                });
            });

            function resizeFullCalendar(view) {
                var newheight = $(window).height() * 0.90;
                if ($(window).width() < 1170) {
                    newheight = $(window).height() * 0.84;
                }
                if ($(window).width() < 920) {
                    newheight = $(window).height() * 0.77
                }
                $('#cal').fullCalendar('option', 'height', newheight);
            }
            $.datepicker.setDefaults($.extend($.datepicker.regional['de']));
            var source;
            function openDialog() {
                $("#eventInfo").foundation('reveal', 'open');
            }
            function applyEventFilter() {
                $('#cal').fullCalendar('refetchEvents');
            }
            function returnLoader() {
                return "<div class='row'><div class='small-12 columns text-center'>"
                        + "<img src='../../assets/img/grid.svg' align='center' style='margin-left: auto;margin-right: auto;'></div></div>";
            }
            function loadFilelist(id) {
                //showOverlay();
                $.ajax({
                    async: true,
                    type: "GET",
                    url: "/event/mediajson?eventId=" + id,
                    dataType: "json",
                    success: function (json) {
                        //hideOverlay();
                        if (json.length > 0) {
                            $("#files").html('');
                            $("#files").append("<tr class='row preview'><td colspan='3'><button colspan='3' class='tiny button radius right downloadall' style='margin-bottom: 0px'><s:text name="download_all"/></button></td></tr>")
                            json.forEach(function (element, index, array) {
                                $("#files").append("<tr>" +
                                "<td><i class='fa fa-file preview' rel='/upload/" + id + "/" + element.text + "' style='margin-right: 10px'></i>" + element.text + "</td>" +
                                "<td>" +
                                "<a class='right' href='" + element.link + "'><i class='fa fa-download'></i></a>" +
                                "<a class='deletemedia right' style='color:red' event_id='" + id + "' fileUrl='" + element.link + "'><i class='fa fa-times'></i></a>"
                                + "</td>" +
                                "</tr>");
                                try {
                                    $(".preview:last-child").imgPreview({
                                        srcAttr: 'rel',
                                        containerID: 'imgPreviewWithStyles',
                                        imgCSS: {
                                            // Limit preview size:
                                            height: 150
                                        },
                                        // When container is shown:
                                        onShow: function (link) {
                                            // Animate link:
                                            $(link).stop().animate({opacity: 0.4});
                                            // Reset image:
                                            $('img', this).stop().css({opacity: 0});
                                        },
                                        // When image has loaded:
                                        onLoad: function () {
                                            // Animate image
                                            $(this).animate({opacity: 1}, 300);
                                        },
                                        // When container hides:
                                        onHide: function (link) {
                                            // Animate link:
                                            $(link).stop().animate({opacity: 1});
                                        }
                                    });
                                } catch (err) {
                                }
                            });
                            $(".downloadall").click(function () {
                                $('#files a').each(function (index) {
                                    window.open($(this).attr('href'));
                                });
                            })
                        } else {
                            $("#files").html('');
                            $("#files").append("<tr><td colspan='3'><s:text name="no_media_data_found"/></td><tr/>");
                        }
                    }
                });
            }
            window.downloadFile = function (sUrl) {
                //If in Chrome or Safari - download via virtual link click
                if (window.downloadFile.isChrome || window.downloadFile.isSafari) {
                    //Creating new link node.
                    var link = document.createElement('a');
                    link.href = sUrl;
                    if (link.download !== undefined) {
                        //Set HTML5 download attribute. This will prevent file from opening if supported.
                        var fileName = sUrl.substring(sUrl.lastIndexOf('/') + 1, sUrl.length);
                        link.download = fileName;
                    }
                    //Dispatching click event.
                    if (document.createEvent) {
                        var e = document.createEvent('MouseEvents');
                        e.initEvent('click', true, true);
                        link.dispatchEvent(e);
                        return true;
                    }
                }
                // Force file download (whether supported by server).
                var query = '?download';
                window.open(sUrl + query);
            }
            String.prototype.contains = function (it) {
                return this.indexOf(it) != -1;
            };
            $(document).ready(function () {
                $('#cal').fullCalendar({
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'month,agendaWeek,agendaDay'
                    },
                    windowResize: resizeFullCalendar,
                    lang: '<s:text name="fulllcalender_lang"/>',
                    editable: true,
                    eventLimit: true,
                    events: function (start, end, timezone, callback) {
                        var url = "/event/json?";
                        $('input[name="category"]').each(function (index) {
                            if ($(this).is(":checked")) {
                                url += "categories=" + $(this).val() + "&";
                            }
                        });
                        $.ajax({
                            url: url,
                            dataType: 'json',
                            data: {
                                start: start.format(),
                                end: end.format()
                            },
                            success: function (doc) {
                                callback(doc);
                            }, error: function () {
                                console.log("error");
                            }
                        });
                    },
                    eventClick: function (event, jsEvent, view) {
                        showOverlay();
                        document.generateReport.eventId.value = event.id;
                        //set the values and open the modal
//                        $("#event_edit").html(returnLoader());
                        $("#event_edit").load("edit.action?eventId=" + event.id, function () {
                            hideOverlay();
                            openDialog();
                        });
                        $("#event_workers").html(returnLoader());
                        $("#event_workers").load("/userwork/modalview.action?eventId=" + event.id, function () {
                        });
                        $("#event_media").html(returnLoader());
                        $("#event_media").load("media?eventId=" + event.id, function () {
                            loadFilelist(event.id);
                            myAwesomeDropzone = new Dropzone("#my-awesome-dropzone", {
                                maxFilesize: 100,
                                headers: {"X-CSRF-TOKEN": $("meta[name=_csrf]").attr('content'), "eventId": event.id}
                            });
                            myAwesomeDropzone.on("complete", function (file) {
                                loadFilelist(event.id);
                            });
                            myAwesomeDropzone.on('sending', function (file, xhr, formData) {
                                formData.append('fileName', file.name);
                            });
                            myAwesomeDropzone.options.uiDZResume = {
                                success: function (file, response) {
                                    alert(response);
                                }
                            };
                            $(document).foundation('clearing', 'reflow');
                        });
                    },
                    loading: function (isLoading, view) {
                        if (isLoading) {
                            showOverlay();
                        } else {
                            hideOverlay();
                        }
                    }
                });
                resizeFullCalendar("lel");
                $("#create").click(function () {
                    showOverlay();
                    $("#eventCreate > div").load("create.action", function () {
                        $("#eventCreate").foundation('reveal', 'open');
                        hideOverlay();
                    });

                });
                $(".categorie").click(function () {
                    $(this).parent().find("input[type=checkbox]").click();
                });
                $("input[name='category']").click(function () {
                    applyEventFilter();
                    if ($(this).is(":checked")) {
                        $(this).parent().find(".simple-color-picker").simplecolorpicker('selectColor', $(this).parent().find(".categorie-color").val());
                    } else {
                        $(this).parent().find(".simple-color-picker").simplecolorpicker('selectColor', "#FFFFFF");
                    }
                });
                $.each($(".categories"), function (index, data) {
                    var color = $(this).find(".categorie-color").val();
                    $(this).find(".simple-color-picker").append($('<option></option>').val(color).html(color));
                    $(this).find(".simple-color-picker").simplecolorpicker({picker: true, theme: 'glyphicons'})
                    $(this).find(".simple-color-picker").simplecolorpicker('selectColor', $(this).find(".categorie-color").val());
                });
                $(".simple-color-picker").on("change", function () {
                    $(this).parent().find(".eventcategorie-color").val($(this).val());
                    $(this).parent().find("form").submit();
                });
                <s:if test="showChecklist">
                var array = [];
                <s:iterator value="eventList">
                var e = {};
                e.id = "<s:property value="id"></s:property>";
                e.title = "<s:property value="title"></s:property>";
                e.startDate = "<s:date name="startDate" format="YYYY-MM-dd" />";
                e.endDate = "<s:date name="startDate" format="YYYY-MM-dd"></s:date>";
                e.location = "<s:property value="location"></s:property>";
                array.push(e);
                </s:iterator>
                array.forEach(function (data) {
                    var n = noty({
                        layout: 'topRight',
                        text: '<s:text name="you_are_resposnible_for"/>: ' + data.title + "\n Datum: " + moment(data.startDate).format("D.MMM.YYYY"),
                        theme: 'relax',
                        type: 'alert',
                        animation: {
                            open: 'animated fadeInDown', // jQuery animate function property object
                            close: 'animated fadeOutDown', // jQuery animate function property object
                            easing: 'swing', // easing
                            speed: 500 // opening & closing animation speed
                        },
                        callback: {
                            onClose: function () {
                            },
                            afterClose: function () {
                            },
                            onCloseClick: function () {
                            }
                        },
                        buttons: [
                            {
                                addClass: 'button tiny', text: 'Ok', onClick: function ($noty) {
                                var id = data.id;
                                $.ajax({
                                    async: true,
                                    type: "GET",
                                    url: "/event/setChecklistShown.action",
                                    dataType: "json",
                                    data: {
                                        eventId: id
                                    },
                                    success: function (json) {

                                    }
                                })
                                $noty.close();
                            }
                            },
                            {
                                addClass: 'button tiny info',
                                text: '<s:text name="download_checklist"/>',
                                onClick: function ($noty) {
                                    $noty.close();
                                    //TODO 7Hier muss man noch eine Weiterleitung auf die Datei machen
                                }
                            },
                            {
                                addClass: 'button tiny alert',
                                text: '<s:text name="cancel"/>',
                                onClick: function ($noty) {
                                    $noty.close();
                                }
                            }
                        ]
                    });
                });
                </s:if>
                $("#export_eventProjects").select2({
                    placeholder: "<s:text name="exportProjectPlaceholder"></s:text>",
                    ajax: {
                        url: "<s:url namespace="/project" action="searchAllProjects"></s:url>",
                        dataType: 'json',
                        quietMillis: 400,
                        data: function (term, page) { // page is the one-based page number tracked by Select2
                            return {
                                query: term, //search term
                                page: page - 1, // page number
                                size: 20
                            };
                        },
                        results: function (data, page) {
                            var more = (page * 20) < data.total_count;
                            var result = [];
                            result.push({
                                id: 0,
                                text: "<s:text name="exportAllProjects"></s:text>"
                            });
                            $.each(data.items, function (index, item) {
                                result.push({
                                    id: index,
                                    text: item
                                });
                            });
                            return {results: result, more: more};
                        }
                    }
                });
            });


        </script>
    </jsp:attribute>
    <jsp:body>
        <div class="row" style="max-width: none">
            <div class="small-12 medium-3 large-2 columns">
                <h3><s:text name="event_calendar"/></h3>

                <div class="tiny button radius expand" id="create"><s:text name="create_event"/></div>
                <hr/>
                <h4><s:text name="event_export"/></h4>

                <form action="/event/export" id="exportEventForm">
                    <div class="row collapse postfix-radius">
                        <div class="large-9 small-12 columns">
                            <s:select list="exportmonths" name="ExportMonth" cssStyle="margin-bottom:5px"
                                      value='%{currentMonth}'></s:select>
                        </div>
                        <div class="large-3 show-for-large-up columns">
                            <span class="postfix"><s:text name="month"></s:text></span>
                        </div>
                        <div class="large-12 small-12 columns">
                            <s:hidden id="export_eventProjects" name="exportProjectId" value="%{projectId}"
                                      cssStyle="width: 100%;"></s:hidden>
                        </div>

                    </div>
                    <input type="submit" value="<s:text name="event_export"/>" class="tiny button radius expand">
                </form>
                <hr/>
                <div class="row" style="padding-left: 0.9375rem">
                    <h4><s:text name="categories"/></h4>
                    <s:iterator value="categoriesList">
                        <div class="small-10 columns left categories">
                            <div class="row">
                                <input checked="checked" type="checkbox" value='<s:property value="id"></s:property>'
                                       name="category" style="display: none;">
                                <input type="hidden" class="categorie-id" value="<s:property value="id"></s:property>">
                                <input type="hidden" class="categorie-color"
                                       value="<s:property value="color"></s:property>">
                                <select class="simple-color-picker">
                                    <option value="#FFFFFF">#FFFFFF</option>
                                    <option value="#ac725e">#ac725e</option>
                                    <option value="#d06b64">#d06b64</option>
                                    <option value="#f83a22">#f83a22</option>
                                    <option value="#fa573c">#fa573c</option>
                                    <option value="#ff7537">#ff7537</option>
                                    <option value="#ffad46">#ffad46</option>
                                    <option value="#42d692">#42d692</option>
                                    <option value="#16a765">#16a765</option>
                                    <option value="#7bd148">#7bd148</option>
                                    <option value="#b3dc6c">#b3dc6c</option>
                                    <option value="#fbe983">#fbe983</option>
                                    <option value="#fad165">#fad165</option>
                                    <option value="#92e1c0">#92e1c0</option>
                                    <option value="#9fe1e7">#9fe1e7</option>
                                    <option value="#9fc6e7">#9fc6e7</option>
                                    <option value="#4986e7">#4986e7</option>
                                    <option value="#9a9cff">#9a9cff</option>
                                    <option value="#b99aff">#b99aff</option>
                                    <option value="#c2c2c2">#c2c2c2</option>
                                    <option value="#cabdbf">#cabdbf</option>
                                    <option value="#cca6ac">#cca6ac</option>
                                    <option value="#f691b2">#f691b2</option>
                                    <option value="#cd74e6">#cd74e6</option>
                                    <option value="#a47ae2">#a47ae2</option>
                                </select>

                                <div class="categorie" style="display: inline-block">
                                    <label>
                                        <s:text name="%{name}"/>
                                    </label>
                                    <s:form action="setCategorieColor" method="GET">
                                        <input type="hidden" name="eventcategorie.id"
                                               value="<s:property value="id"></s:property>">
                                        <input class="eventcategorie-color" type="hidden" name="eventcategorie.color"
                                               value="<s:property value="color"></s:property>">
                                    </s:form>
                                </div>
                            </div>
                        </div>
                    </s:iterator>
                </div>
                <hr/>
                <sec:authorize access="hasRole('Admin')">
                    <div class="row">
                        <div class="small-12 columns">
                            <s:a action="import" namespace="/event" cssClass="button tiny expand"><s:text
                                    name="import_from_google"></s:text></s:a>
                        </div>
                    </div>
                </sec:authorize>
            </div>
            <div class="small-12 medium-9 large-10 columns" style="margin-top: 4px;">
                <div id="cal" class="small-centered column"></div>
            </div>
        </div>
        <div id="eventInfo" class="reveal-modal" data-reveal>

            <a class="close-reveal-modal">&#215;</a>
            <dl class="tabs" data-tab>
                <dd class="active"><a href="#event_edit"><s:text name="edit_event"></s:text></a></dd>
                <dd><a href="#event_media"><s:text name="media"/></a></dd>
                <dd><a href="#event_workers"><s:text name="workers"/></a></dd>
                <dd>
                    <a href="#generate" onclick="$(this).parent().find('form').submit();"><s:text name="generateReport"/></a>

                    <form name="generateReport" id="generateReport" action="generateReport" method="POST">
                        <s:hidden name="eventId" id="eventId" value=""></s:hidden>
                            <%--<p id="eventId"/>--%>
                        <sec:csrfInput></sec:csrfInput>
                    </form>
                </dd>

            </dl>
            <div class="tabs-content">
                <div class="content active" id="event_edit">
                </div>
                <div class="content" id="event_media">
                </div>
                <div class="content" id="event_workers">
                </div>
            </div>

        </div>
        <div id="eventCreate" class="reveal-modal" data-reveal>
            <a class="close-reveal-modal">&#215;</a>

            <div>
            </div>
        </div>
    </jsp:body>
</t:layout>
