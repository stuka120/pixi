<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<script type="text/javascript">
    //@ sourceURL=CreateEvent.js
    //StartDate
    $(function () {
        $("#starttime").timepicker({
            'timeFormat': 'H:i',
            'scrollDefault': 'now',
            'step': 15
        });
        $("#endtime").timepicker({
            'timeFormat': 'H:i',
            'scrollDefault': 'now',
            'step': 15
        });

    $("#startdate").datepicker({
        dateFormat: "dd.mm.yy",
        onSelect: function () {
            $("#enddate").datepicker("option", "minDate", $(this).val());
            $(this).blur();
        }
    }, $.datepicker.regional['<s:text name="languageCode" />']).datepicker('widget').wrap('<div class="ll-skin-cangas"/>');
    $("#enddate").datepicker({
        dateFormat: "dd.mm.yy",
        onSelect: function () {
            $("#startdate").datepicker("option", "maxDate", $(this).val());
            $(this).blur();
        }
    }, $.datepicker.regional['<s:text name="languageCode" />']).datepicker('widget').wrap('<div class="ll-skin-cangas"/>');
    $.subscribe('beforeSubmit', function (event, data) {
        $("#formerrors").empty();
        $("#formerrors").hide();
        $(".prefix").css("background-color", "#f2f2f2");
    });
        $.subscribe('onSuccess', function (event, data) {
            if (parseInt(event.originalEvent.data.toString().trim()) > 0) {
                $("#eventInfo").foundation('reveal', 'close');
                swal({
                    title: "<s:text name="event_saved"></s:text>",
                    text: "",
                    type: "success"
                }, function () {
                    applyEventFilter();
                });
            } else {
                swal("<s:text name="SomethingWentWrong"></s:text>", "", "error");
            }
        });
        $("#fullday").change(function(){
        fullday(this.checked);
    });
    });
    $(document).ready(function () {
        $("#eventContactPerson").select2({
            placeholder: "<s:text name="searchForContact"></s:text>",
            allowClear: true,
            ajax: {
                url: "<s:url namespace="/contacts" action="searchAllContacts"></s:url>",
                dataType: 'json',
                quietMillis: 400,
                data: function (term, page) { // page is the one-based page number tracked by Select2
                    return {
                        query: term, //search term
                        page: page - 1, // page number
                        size: 20
                    };
                },
                results: function (data, page) {
                    var more = (page * 20) < data.total_count;
                    var result = [];
                    $.each(data.items, function (index, item) {
                        result.push({
                            id: index,
                            text: item
                        });
                    });
                    return {results: result, more: more};
                }
            }
        });
        $("#eventProjects").select2({
            placeholder: "<s:text name="searchForProject"></s:text>",
            allowClear: true,
            ajax: {
                url: "<s:url namespace="/project" action="searchAllProjects"></s:url>",
                dataType: 'json',
                quietMillis: 400,
                data: function (term, page) { // page is the one-based page number tracked by Select2
                    return {
                        query: term, //search term
                        page: page - 1, // page number
                        size: 20
                    };
                },
                results: function (data, page) {
                    var more = (page * 20) < data.total_count;
                    var result = [];
                    $.each(data.items, function (index, item) {
                        result.push({
                            id: index,
                            text: item
                        });
                    });
                    return {results: result, more: more};
                }
            }
        });
        $("#usersByResponsible").select2({
            placeholder: "<s:text name="searchForResponsible"></s:text>",
            allowClear: true,
            ajax: {
                url: "<s:url namespace="/user" action="searchAllUsers"></s:url>",
                dataType: 'json',
                quietMillis: 400,
                data: function (term, page) { // page is the one-based page number tracked by Select2
                    return {
                        query: term, //search term
                        page: page - 1, // page number
                        size: 20
                    };
                },
                results: function (data, page) {
                    var more = (page * 20) < data.total_count;
                    var result = [];
                    $.each(data.items, function (index, item) {
                        result.push({
                            id: index,
                            text: item
                        });
                    });
                    return {results: result, more: more};
                }
            }
        });
    });
    function fullday(isfullday) {
        var fulldate = isfullday;
        $('.notfullday').each(function (index) {
            $(this).val("");
            $(this).prop('disabled', fulldate);
        });
    }

    function customValidation(form, errors) {
        var list = $('#formerrors');
        list.show();
        if (errors.errors) {
            $.each(errors.errors, function (index, value) {
                list.append('<li>' + value + '</li>\n');
            });
        }
        if (errors.fieldErrors) {
            $.each(errors.fieldErrors, function (index, value) {
                list.append('<li>' + value + '</li>\n');
                $("input[name='" + index + "']").closest(".row").find(".prefix").css("background-color", "#f04124");
            });
        }
    }
</script>
<h2><s:text name="create_event"></s:text>:</h2>

<div id="result"></div>
<div data-alert class="alert-box alert radius" id="formerrors" style="display: none"></div>
<s:form id="createForm" action="add" method="POST" cssStyle="width:100%" theme="simple">
    <sec:csrfInput></sec:csrfInput>
    <div class="row">
        <div class="large-6 columns">
            <div class="row collapse prefix-radius">
                <div class="small-3 columns">
                    <span class="prefix"><s:text name="title"></s:text></span>
                </div>
                <div class="small-9 columns">
                    <s:textfield name="event.title" id="create_eventtitle"/>
                </div>
            </div>
        </div>
        <div class="large-6 columns">
            <div class="row collapse prefix-radius">
                <div class="small-3 columns">
                    <span class="prefix"><s:text name="location"></s:text></span>
                </div>
                <div class="small-9 columns">
                    <s:textfield name="event.location"/>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="margin-bottom: 10px">
        <div class="small-12 large-12 columns">
            <div class="row collapse prefix-radius">
                <div class="small-3 columns">
                    <span class="prefix"><s:text name="state"/> </span>
                </div>
                <div class="small-9 columns">
                    <%--<s:textfield name="event.state" id="state"/>--%>
                    <select name="event.state">
                        <option value="">-</option>
                        <option value="Wien">Wien</option>
                        <option value="Niederösterreich">Niederösterreich</option>
                        <option value="Oberösterreich">Oberösterreich</option>
                        <option value="Burgenland">Burgenland</option>
                        <option value="Kärnten">Kärnten</option>
                        <option value="Steiermark">Steiermark</option>
                        <option value="Tirol">Tirol</option>
                        <option value="Salzburg">Salzburg</option>
                        <option value="Vorarlberg">Vorarlberg</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="margin-bottom: 10px">
        <div class="small-10 columns">
            <div class="row">
                <div class="large-6 small-12 columns">
                    <div class="row collapse prefix-radius">
                        <div class="small-3 large-5 columns">
                            <span class="prefix"><s:text name="startdate"></s:text></span>
                        </div>
                        <div class="small-9 large-7 columns">
                            <s:textfield name="startDate" id="startdate"/>
                        </div>
                    </div>
                </div>
                <div class="large-6 small-12 columns">
                    <div class="row collapse prefix-radius">
                        <div class="small-3 large-5 columns">
                            <span class="prefix"><s:text name="event_starttime"></s:text></span>
                        </div>
                        <div class="small-9 large-7 columns">
                            <s:textfield name="startDate" id="starttime" cssClass="notfullday"></s:textfield>
                        </div>
                    </div>
                </div>
                <div class="large-6 small-12 columns">
                    <div class="row collapse prefix-radius">
                        <div class="small-3 large-5 columns">
                            <span class="prefix"><s:text name="enddate"></s:text></span>
                        </div>
                        <div class="small-9 large-7 columns">
                            <s:textfield name="endDate" id="enddate" cssClass="notfullday"/>
                        </div>
                    </div>
                </div>
                <div class="large-6 small-12 columns">
                    <div class="row collapse prefix-radius">
                        <div class="small-3 large-5 columns">
                            <span class="prefix"><s:text name="event_endtime"></s:text></span>
                        </div>
                        <div class="small-9 large-7 columns">
                            <s:textfield name="endDate" id="endtime" cssClass="notfullday"></s:textfield>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="small-2 columns ">
            <div class="row valign-middle">
                <div class="small-12 columns">
                    <input id="fullday" name="fullday" value="<s:property value="fullday"></s:property>" type="checkbox"
                           tabindex="-1">
                    <span>Ganztags</span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="large-6 columns">
            <div class="row collapse prefix-radius">
                <div class="small-3 columns">
                    <span class="prefix"><s:text name="category"></s:text></span>
                </div>
                <div class="small-9 columns">
                    <s:select list="categoriesList" listKey="id" listValue="name" name="eventCategory"
                              id="catlist"/>
                </div>
            </div>
        </div>
        <div class="large-6 columns">
            <div class="row collapse prefix-radius">
                <div class="small-3 columns">
                    <span class="prefix"><s:text name="contactPerson"></s:text></span>
                </div>
                <div class="small-9 columns">
                    <s:hidden id="eventContactPerson" name="eventContactPerson" value=""
                              cssStyle="width: 100%;"></s:hidden>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 0.5em">
        <div class="large-6 columns">
            <div class="row collapse prefix-radius">
                <div class="small-3 columns">
                    <span class="prefix"><s:text name="event_project"/></span>
                </div>
                <div class="small-9 columns">
                    <s:hidden id="eventProjects" name="event.projectByProjectId.id" value=""
                              cssStyle="width: 100%;"></s:hidden>
                </div>
            </div>
        </div>
        <div class="large-6 columns">
            <div class="row collapse prefix-radius">
                <div class="small-3 columns">
                    <span class="prefix"><s:text name="event_responsible"></s:text></span>
                </div>
                <div class="small-9 columns">
                    <s:hidden id="usersByResponsible" name="event.usersByResponsible.id" value=""
                              cssStyle="width: 100%;"></s:hidden>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 0.5em">
        <div class="large-12 columns">
            <label><s:text name="event_description"></s:text>
                <s:textarea name="event.description"/>
            </label>
        </div>
    </div>
    <div class="row" style="margin-top: 0.7em">
        <div class="columns right text-right">
            <s:text name="save_event" var="save_event"></s:text>
            <sj:submit
                    cssClass="button radius"
                    targets="result"
                    button="true"
                    validate="true"
                    validateFunction="customValidation"
                    onBeforeTopics="beforeSubmit"
                    onSuccessTopics="onSuccess"
                    value='%{save_event}'
                    />
        </div>
    </div>
</s:form>

