<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="row">
    <script type="text/javascript">
        $(document).ready(function () {
            $(".deleteevent").click(function () {
                $.ajax({url: "/event/delete.action?eventId=" + $(this).attr("id")})
                        .done(function (data) {
                            $("#cal").fullCalendar('refetchEvents')
                            $(".eventInfo").hide();
                        });
            });
        });
    </script>
    <div style="background-color: red" class="tiny alert button right" id="<s:property value="event.id"></s:property>">
        <s:text name="event_delete"/>
    </div>
</div>
<div class="eventcontent">
    <jsp:include page="edit.jsp"></jsp:include>
</div>