<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<style>
    .bottom-align-left {
        margin-left: -230px !important;
    }

    .bottom-align-left::before {
        margin-left: 250px;
    }

    .bottom-align-left::after {
        margin-left: 250px;
    }

    .ui-state-focus {
        color: #000000 !important;
        background: transparent !important;
        border: none !important;
        font-weight: normal !important;
        outline: none;
    }

    .ui-autocomplete-loading {
        background-image: url(../../assets/img/loading.gif) !important;
        background-position: right center !important;
        background-repeat: no-repeat !important;
        background-size: 1.6rem 1.6rem !important;
    }

    .ui-autocomplete-loading:focus {
        background-image: url(../../assets/img/loading.gif) !important;
        background-position: right center !important;
        background-repeat: no-repeat !important;
        background-size: 1.6rem 1.6rem !important;
    }
    <sec:authorize access="isAnonymous()">
    nav{
        visibility: hidden;
    }
    </sec:authorize>
</style>
<div class="sticky">
    <nav class="top-bar" data-topbar role="navigation" style="z-index: 5;">
        <ul class="title-area">
            <li class="name">
                <h1>
                    <a id="HomeButton" href="">
                        <h3 class="three columns" style="font: normal 1.8em Verdana, sans-serif;margin-bottom: 0;">
                            <span style="color:red">Pi</span>
                        <span style="color:green">Xi
                            <sup style="color: #777777;font-size: 75%;">beta</sup></span>
                        </h3>
                    </a>
                </h1>
            </li>
            <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
        </ul>
        <section class="top-bar-section">
            <ul class="left">
                <sec:authorize access="hasRole('Admin')">
                    <li><a href="<s:url action='index' namespace='/user'/>"
                           style="font-size: medium;"><s:text name="usermanagement"/></a></li>
                </sec:authorize>
                <sec:authorize access="hasRole('Employee')">
                    <li class="has-dropdown">
                        <a href="<s:url action='index' namespace='/contacts'/>" style="font-size: medium;"><s:text
                                name="contacts"/></a>
                        <ul class="dropdown">
                            <li><a href="<s:url action='index' namespace='/contacts'/>" style="font-size: medium;">
                                <s:text name="contactmanagement"/> </a>
                            </li>
                            <li><a href="<s:url action='index' namespace='/organisations'/>" style="font-size: medium;">
                                Organisation
                            </a></li>
                        </ul>
                    </li>
                    <li><a href="<s:url action='index' namespace='/workingtime'/>"
                           style="font-size: medium;"><s:text name="workingtime"/></a></li>
                    <li><a href="<s:url action='index' namespace='/event'/>" style="font-size: medium;">Events</a></li>
                </sec:authorize>
                <sec:authorize access="hasRole('Employee')">
                    <li><a href="<s:url action='index' namespace='/project'/>" style="font-size: medium;"><s:text
                            name="project"/></a></li>
                </sec:authorize>
            </ul>

            <ul class="right">
                <sec:authorize access="isAuthenticated()">
                    <li class="has-form">
                        <div class="row collapse">
                            <div class="large-12 small-12 columns">
                                <input id="search" type="text" placeholder="<s:text name="contactSearch" />" autocomplete="off"/>
                            </div>
                        </div>
                    </li>
                    <li class="has-dropdown">
                        <a href="#" style="font-size: medium;"><s:text name="language" /></a>
                        <ul class="dropdown">
                            <li>
                                <a href="<s:url namespace="/" action="changeLanguage"><s:param name="lang">en</s:param></s:url>">
                                    <img src="../../assets/img/flag_gb.jpg" alt="" style="width: 2.4em;"/> English
                                </a>
                            </li>
                            <li>
                                <a href="<s:url namespace="/" action="changeLanguage"><s:param name="lang">de</s:param></s:url>">
                                    <img src="../../assets/img/flag_de.jpg" alt="" style="width: 2.4em;"/> Deutsch
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a data-dropdown="drop2" aria-controls="drop2" aria-expanded="false">
                            Hi, <sec:authentication property="principal.username"/>
                        </a>

                        <div id="drop2" data-dropdown-content class="f-dropdown content small bottom-align-left"
                             aria-hidden="true" tabindex="-1">
                            <div class="row">
                                <div class="small-4 columns" style="padding: 0">
                                    <img src="/assets/img/fairundsensibel-logo.jpg"/>
                                </div>
                                <div class="small-8 columns">
                                    <sec:authorize access="isAuthenticated()">
                                        <div style="font-weight: bold;margin: -4px 0 1px 0;">
                                            <sec:authentication property="principal.title"></sec:authentication>,
                                            <sec:authentication property="principal.name"></sec:authentication>
                                            <sec:authentication property="principal.surname"></sec:authentication>
                                        </div>
                                        <div style="color: #999999;"><sec:authentication
                                                property="principal.username"/></div>
                                    </sec:authorize>
                                </div>
                            </div>
                            <hr style="margin-bottom: 5px;"/>
                            <div class="row">
                                <div class="small-7 columns">
                                    <a href="#" class="button expand" id="showProfil"><s:text name="editProfile" /></a>
                                </div>
                                <div class="small-5 columns" style="padding: 0;">
                                    <a href="#" class="button tiny" onclick="$(this).parent().find('form').submit()">
                                        <s:text name="logout" />
                                    </a>

                                    <form action="/j_spring_security_logout" method="post">
                                        <sec:csrfInput></sec:csrfInput>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </li>
                </sec:authorize>
            </ul>
        </section>
    </nav>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        $("#search").autocomplete({
            source: function (request, response) {
                $.ajax({
                    type: "GET",
                    url: "<s:url action="searchAllContacts" namespace="/contacts"/>",
                    data: {
                        query: request.term,
                        page: 0,
                        size: 20
                    },
                    cache: false,
                    success: function (data) {
                        var result = [];
                        $.each(data.items, function (index, item) {
                            result.push({
                                id: index,
                                text: item
                            });
                        })
                        response(result);
                    }
                });
            },
            select: function (event, ui) {
                $(this).val(ui.item.name + " " + ui.item.surname);
                window.location.href = "/contacts/details.action?selectedContact=" + ui.item.id;
            }
        }).data('ui-autocomplete')._renderItem = function (ul, item) {
            return $("<li>")
                    .append("<a>" + item.text + "</a>")
                    .data("id", item.id)
                    .appendTo(ul);
        };
        $('body > .ui-autocomplete').addClass('f-dropdown');
        $('body > .ui-autocomplete').removeClass('ui-widget-content');
        <sec:authorize access="isAuthenticated()">
        $("#showProfil").click(function () {
            showOverlay();
            $("body").append("<div id='profilBearbeitenModel' class='reveal-modal' data-reveal aria-labelledby='modalTitle' aria-hidden='true' role='dialog'>" +
            "<a class='close-reveal-modal' aria-label='Close'>&#215;</a>" +
            "<div></div>" +
            "</div>");
            $("#profilBearbeitenModel > div").load("<s:url namespace="/user" action="editProfil"></s:url>", function () {
                $('#profilBearbeitenModel').foundation('reveal', 'open');
                hideOverlay();
            });
        });
        </sec:authorize>
    });
</script>
