<%@ page contentType="text/html;charset=ISO-8859-1" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:if test="#context['struts.actionMapping'].name=='editProfil'">
    <link rel="stylesheet" type="text/css" href="../../assets/css/select2-foundation5.css">
    <link rel="stylesheet" href="../../assets/css/sweet-alert.css"/>

    <script type="text/javascript" src="../../assets/js/select2.js"></script>
    <script type="text/javascript" src="/struts/js/plugins/jquery.subscribe.js?s2j=3.7.1"></script>
    <script type="text/javascript" src="../../assets/js/struts/jquery.ui.struts.js"></script>
    <script type="text/javascript" src="../../assets/js/struts/jquery.struts.js"></script>
    <script type="text/javascript" src="../../assets/js/jquery.form.js"></script>
    <script type="text/javascript" src=../../assets/js/sweet-alert.js></script>
</s:if>
<style>
    .select2-search__field {
        box-shadow: none !important;
        padding: 0 !important;
        margin: 0 !important;
    }

    .select2-selection--single {
        height: 2.3125rem !important;
    }

    .select2-input {
        margin: 0 !important;
    }

    .select2-search-field {
        height: 2.15rem;
    }

    .select2-search-choice {
        margin-top: 2px !important;
    }
</style>
<script type="text/javascript">
    //@ sourceURL=EditUser.js
    $(function () {
        $.subscribe('beforeSubmit', function (event, data) {
            $("#formerrors").empty();
            $("#formerrors").hide();
            $(".prefix").css("background-color", "#f2f2f2");
        });
        $.subscribe('onSuccess', function (event, data) {
            $("#reveal-modal").foundation('reveal', 'close');
            swal({title: "<s:text name="userEdited"/>", type: "success"}, function () {
                window.location.reload();
            });
        });
    });
    function customValidation(form, errors) {
        var list = $('#formerrors');
        list.show();
        if (errors.errors) {
            $.each(errors.errors, function (index, value) {
                list.append('<li>' + value + '</li>\n');
            });
        }
        if (errors.fieldErrors) {
            $.each(errors.fieldErrors, function (index, value) {
                list.append('<li>' + value + '</li>\n');
                $("input[name='" + index + "']").closest(".row").find(".prefix").css("background-color", "#f04124");
            });
        }
    }
    $(document).ready(function () {
        $("#rolesList").select2({
            placeholder: "Rolen"
        });
        $("#selectedContact").select2({
            placeholder: "Suchen Sie nach einem Kontakt",
            allowClear: true,
            minimumInputLength: 1,
            ajax: {
                url: "<s:url namespace="/contacts" action="searchAllContactsWithoutUser"></s:url>",
                dataType: 'json',
                quietMillis: 400,
                data: function (term, page) { // page is the one-based page number tracked by Select2
                    return {
                        query: term, //search term
                        page: page - 1, // page number
                        size: 20
                    };
                },
                results: function (data, page) {
                    var more = (page * 20) < data.total_count;
                    var result = [];
                    $.each(data.items, function (index, item) {
                        result.push({
                            id: index,
                            text: item
                        });
                    });
                    return {results: result, more: more};
                }
            }
        });
        $("#plannedHours").trigger("keyup");
        $("#selectedContact").select2("data", {
            id: $("#contactId").val(),
            text: $("#contactName").val()
        });
        <s:if test="username == userEntity.username">
        $("#newPasswortOption").click(function () {
            $(".new-passwort-div").show();
            $("#newPasswort").val("true");
        });
        </s:if>
    });
</script>
<div class="row">
    <div class="small-12 colums">
        <h2 style="display: inline;">User bearbeiten:</h2>
        <s:if test="username == userEntity.username">
            <button class="button secondary small" style="display: inline;" id="newPasswortOption">Neues Passwort
                einstellen
            </button>
        </s:if>
    </div>
</div>
<s:hidden name="userEntity.contactsByContactsId.id" id="contactId"></s:hidden>
<input type="hidden" id="contactName"
       value="<s:property value="userEntity.contactsByContactsId.surname"></s:property> <s:property value="userEntity.contactsByContactsId.name"></s:property>">

<div id="result"></div>
<div data-alert class="alert-box alert radius" id="formerrors" style="display: none"></div>
<s:form action="update" namespace="/user" theme="simple" method="POST">
    <s:hidden name="userEntity.id"/>

    <div class="row collapse">
        <div class="small-2 columns">
            <span class="prefix"><s:text name="userEntity.username"/></span>
            <input type="hidden" name="userEntity.username"
                   value="<s:property value="userEntity.username"></s:property>">
        </div>
        <div class="small-5 columns end">
            <s:textfield name="userEntity.username" disabled="true"/>
        </div>
    </div>
    <s:if test="username == userEntity.username">
        <div class="row collapse">
            <div class="small-2 columns">
                <span class="prefix"><s:text name="userEntity.oldPassword"/></span>
            </div>
            <div class="small-5 columns end">
                <s:password name="oldPW"/>
            </div>
        </div>
        <input type="hidden" name="newPasswort" id="newPasswort" value="false">

        <div class="row collapse new-passwort-div" style="display: none">
            <div class="small-2 columns">
                <span class="prefix"><s:text name="userEntity.newPassword"/></span>
            </div>
            <div class="small-5 columns end">
                <s:password name="userEntity.password"/>
            </div>
        </div>
        <div class="row collapse new-passwort-div" style="display: none">
            <div class="small-2 columns">
                <span class="prefix"><s:text name="repeatPW"/></span>
            </div>
            <div class="small-5 columns end">
                <s:password name="confirmPW"/>
            </div>
        </div>
    </s:if>
    <div class="row collapse">
        <div class="small-2 columns">
            <span class="prefix"><s:text name="userEntity.plannedHours"/></span>
        </div>
        <div class="small-5 columns end">
            <s:textfield name="userEntity.plannedHours" type="text" id="plannedHours"
                         onkeyup="$(this).val($(this).val().replace('.', ','));"/>
        </div>
    </div>
    <sec:authorize access="hasRole('Admin')">
        <div class="row collapse">
            <div class="small-2 columns">
                <span class="prefix"><s:text name="userEntity.role"/></span>
            </div>
            <div class="small-5 columns end">
                <s:select multiple="true"
                          id="rolesList"
                          list="rolesEntityList"
                          name="selectedRoles"
                          listValue="name"
                          listKey="id"
                          value="preSelectedRoles"
                          cssStyle="width: 100%"/>
            </div>
        </div>
        <div class="row collapse" style="margin-bottom: 1em;">
            <div class="small-2 columns">
                <span class="prefix"><s:text name="userEntity.contact"/></span>
            </div>
            <div class="small-5 columns end">
                <s:hidden id="selectedContact" name="selectedContact" value="" cssStyle="width: 100%;"></s:hidden>
            </div>
        </div>
    </sec:authorize>
    <div class="row left">
        <s:text name="saveUserLabel" var="asdf"/>
        <sj:submit
                cssClass="button radius"
                targets="result"
                button="true"
                validate="true"
                validateFunction="customValidation"
                onBeforeTopics="beforeSubmit"
                onSuccessTopics="onSuccess"
                value="%{asdf}"
                >
        </sj:submit>
    </div>
    <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</s:form>