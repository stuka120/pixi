<%@ page contentType="text/html;charset=ISO-8859-1" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<t:layout>
    <jsp:attribute name="footer">
      <p id="copyright">Binary Cheese</p>
    </jsp:attribute>
    <jsp:attribute name="head">
        <sec:csrfMetaTags/>
        <title><s:text name="UserTitle"/> </title>
        <link rel="stylesheet" type="text/css" href="../../assets/css/select2-foundation5.css">
        <link rel="stylesheet" href="../../assets/css/sweet-alert.css"/>

        <script src="../../assets/js/foundation/foundation.reveal.js"></script>
        <script type="text/javascript" src="../../assets/js/select2.js"></script>
        <script type="text/javascript" src="/struts/js/plugins/jquery.subscribe.js?s2j=3.7.1"></script>
        <script type="text/javascript" src="../../assets/js/struts/jquery.ui.struts.js"></script>
        <script type="text/javascript" src="../../assets/js/struts/jquery.struts.js"></script>
        <script type="text/javascript" src="../../assets/js/jquery.form.js"></script>
        <script type="text/javascript" src=../../assets/js/sweet-alert.js></script>
        <style>
            .fa1-5x {
                font-size: 1.5em;;
            }
        </style>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#createUser").click(function (event) {
                    event.preventDefault();
                    showOverlay();
                    $("#reveal-modal").load("create.action", function () {
                        hideOverlay();
                        $("#reveal-modal").foundation('reveal', 'open');
                    });
                });
                $(".editUser").click(function (event) {
                    event.preventDefault();
                    showOverlay();
                    $("#reveal-modal").load("edit.action?selectedUser=" + $(this).data("id"), function () {
                        hideOverlay();
                        $("#reveal-modal").foundation('reveal', 'open');
                    });
                });
                $(".resetPWBtn").click(function (e) {
                    e.preventDefault();
                    showSweetAlert(true, $(this).data("id"), null);
                });
                $(".deleteUser").click(function (e) {
                    e.preventDefault();
                    showSweetAlert(false, $(this).data("id"), $(this).find(".deactivated").val() == "true");
                });
            });

            function showSweetAlert(reset, userId, activate) {
                if (!reset) {
                    var deactivateUserTitle = "<s:text name="deactivateUserTitle"/>";
                    var deactivateUserConfirmmessage = "<s:text name="deactivateUserConfirmmessage"/>";
                    var activateUserTitle = "<s:text name="activateUserTitle"/>";
                    var activateUserConfirmmessage = "<s:text name="activateUserConfirmmessage"/>";
                }
                swal({
                    title: reset ? "<s:text name="passwordResetConfirmTitle"/>" : activate ? activateUserTitle : deactivateUserTitle,
                    text: reset ? "<s:text name="passwordResetConfirmMessage"/>" : activate ? activateUserConfirmmessage : deactivateUserConfirmmessage,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "<s:text name="continue"/>",
                    cancelButtonText: "<s:text name="cancel"/>",
                    closeOnConfirm: false
                }, function () {
                    $.ajax({
                        url: reset ? "<s:url namespace="/user" action="resetPW"></s:url>" : "<s:url namespace="/user" action="toggleDeactivateUser"></s:url>",
                        type: 'post',

                        data: {
                            selectedUser: userId
                        },
                        success: function (data, status) {
                            if (reset) {
                                swal("<s:text name="passwortResetet"/>", "<s:text name="yourNewPasswordIs"/> " + data, "success");
                            } else {
                                if (activate) {
                                    swal({
                                        title: "<s:text name="activatedUser"/>",
                                        text: "<s:text name="activatedUserConfirmmessage"/> ",
                                        type: "success"
                                    }, function () {
                                        location.reload();
                                    });
                                } else {
                                    swal({
                                        title: "<s:text name="deactivatedUser"/>",
                                        text: "<s:text name="deactivatedUserConfirmmessage"/> ",
                                        type: "success"
                                    }, function () {
                                        location.reload();
                                    });
                                }
                            }
                        },
                        error: function (xhr, desc, err) {
                            swal("<s:text name="error"/>", "<s:text name="somethingWentWrong"/>", "error")
                        }
                    });
                });
            }
        </script>
    </jsp:attribute>
    <jsp:body>
        <div class="row" style="max-width: 100%; width: 100%">
            <div class="small-2 columns">
                <a href="#" class="button radius small expand" style="margin-top: 0.67em;"
                   id="createUser"><s:text name="newUserLabel"/></a>
                <h5><s:text name="showDeactivatedUsers"/></h5>

                <ul class="button-group round even-2">
                    <s:if test="deactivated">
                        <li><a href="#" class="button small secondary disabled">Ja</a></li>
                        <li><a href="<s:url action="index" namespace="/user"></s:url>"
                               class="button small info">Nein</a></li>
                    </s:if>
                    <s:else>
                        <s:url action="index" namespace="/user" var="urlWithParams">
                            <s:param name="deactivated">true</s:param>
                        </s:url>
                        <li><a href="<s:property value="urlWithParams"></s:property> " class="button small info"
                               aria-disabled="true">Ja</a></li>
                        <li><a href="#" class="button small secondary disabled">Nein</a></li>
                    </s:else>
                </ul>
            </div>
            <div class="small-10 columns">
                <h1>Benutzerverwaltung</h1>

                <s:if test="userList.size() > 0">
                    <table id="userTable" class="table" align="center" style="width: 100%">
                        <thead>
                        <tr>
                            <th><s:text name="userEntity.username"/></th>
                            <th><s:text name="userEntity.role"/></th>
                            <th><s:text name="userEntity.name"/></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <s:iterator value="userList" status="statusParent">
                            <tr>
                                <td>
                                    <s:hidden value="%{deactivated}" name="deactivated"/>
                                    <s:hidden value="%{username}" name="username"/>
                                    <s:property value="username"/>
                                </td>
                                <td>
                                    <s:iterator value="roles" status="status">
                                        <s:if test="#status.last == false">
                                            <s:property value="name"/><br/>
                                        </s:if>
                                        <s:if test="#status.last == true">
                                            <s:property value="name"/>
                                        </s:if>
                                    </s:iterator>
                                </td>
                                <td>
                                    <s:property value="contactsByContactsId.surname"/>
                                    <s:property value="contactsByContactsId.name"/>
                                </td>
                                <td>
                                    <a class="editUser" data-id="<s:property value="Id"/>">
                                        <div class="show-for-medium-down">
                                            <i class="fa fa-edit fa1-5x"></i>
                                        </div>
                                        <div class="show-for-large-up" style="min-width: 72px;">
                                            <i class="fa fa-edit fa1-5x"></i>
                                        </div>
                                    </a>
                                </td>
                                <td>
                                    <a href="#" class="resetPWBtn" data-id="<s:property value="Id"/>">
                                        <div class="show-for-medium-down">
                                            <i class="fa fa-undo fa1-5x"></i>
                                        </div>
                                        <div class="show-for-large-up">
                                            <i class="fa fa-undo fa1-5x"></i>
                                        </div>
                                    </a>
                                </td>
                                <td>
                                    <a href="#" class="deleteUser"
                                       data-id="<s:property value="Id"/>">
                                        <input type="hidden" name="deactivated" class="deactivated"
                                               value="<s:property value="deactivated"></s:property>">
                                        <s:if test="deactivated">
                                            <div class="show-for-medium-down">
                                                <i class="fa fa-unlock fa1-5x"></i>
                                            </div>
                                            <div class="show-for-large-up" style="min-width: 76px;">
                                                <i class="fa fa-unlock fa1-5x"></i>
                                            </div>
                                        </s:if>
                                        <s:else>
                                            <s:hidden value="deactivated" name="active"></s:hidden>
                                            <div class="show-for-medium-down">
                                                <i class="fa fa-lock fa1-5x"></i>
                                            </div>
                                            <div class="show-for-large-up">
                                                <i class="fa fa-lock fa1-5x"></i>
                                            </div>
                                        </s:else>
                                    </a>
                                </td>
                            </tr>
                        </s:iterator>
                        </tbody>
                    </table>
                </s:if>
                <s:else>
                    <p><s:text name="noUsersFound"/></p>
                </s:else>
            </div>
        </div>

        <div id="reveal-modal" class="reveal-modal" data-reveal>
        </div>
    </jsp:body>
</t:layout>
