<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<style>
    .select2-search__field {
        box-shadow: none !important;
        padding: 0 !important;
        margin: 0 !important;
    }

    .select2-selection--single {
        height: 2.3125rem !important;
    }

    .select2-input {
        margin: 0 !important;
    }

    .select2-search-field {
        height: 2.15rem;
    }

    .select2-search-choice {
        margin-top: 2px !important;
    }
</style>
<script>
    //@ sourceURL=CreateUser.js
    $.subscribe('beforeSubmit', function (event, data) {
        $("#formerrors").empty();
        $("#formerrors").hide();
        $(".prefix").css("background-color", "#f2f2f2");
    });
    $.subscribe('onSuccess', function (event, data) {
        var password = data.innerHTML;
        $("#reveal-modal").foundation('reveal', 'close');
        swal({
            title: "<s:text name="userCreated"/>",
            text: "<s:text name="yourNewPasswordIs"></s:text> " + password,
            type: "success"
        }, function () {
            window.location.reload();
        });
    });
    function customValidation(form, errors) {
        var list = $('#formerrors');
        list.show();
        if (errors.errors) {
            $.each(errors.errors, function (index, value) {
                list.append('<li>' + value + '</li>\n');
            });
        }
        if (errors.fieldErrors) {
            $.each(errors.fieldErrors, function (index, value) {
                list.append('<li>' + value + '</li>\n');
                $("input[name='" + index + "']").closest(".row").find(".prefix").css("background-color", "#f04124");
            });
        }
    }

    $(document).ready(function () {
        $("#rolesList").select2({
            placeholder: "Rolen"
        });
        $("#plannedHours").trigger("keyup");
        $("#selectedContact").select2({
            placeholder: "Suchen Sie nach einem Kontakt",
            allowClear: true,
            minimumInputLength: 1,
            ajax: {
                url: "<s:url namespace="/contacts" action="searchAllContactsWithoutUser"></s:url>",
                dataType: 'json',
                quietMillis: 400,
                data: function (term, page) { // page is the one-based page number tracked by Select2
                    return {
                        query: term, //search term
                        page: page - 1, // page number
                        size: 20
                    };
                },
                results: function (data, page) {
                    var more = (page * 20) < data.total_count;
                    var result = [];
                    $.each(data.items, function (index, item) {
                        result.push({
                            id: index,
                            text: item
                        });
                    });
                    return {results: result, more: more};
                }
            }
        });
        $("#plannedHours").trigger("keyup");
    });
</script>
<div id="result"></div>
<div data-alert class="alert-box alert radius" id="formerrors" style="display: none"></div>
<s:form action="add" theme="simple">
    <h2>Benutzer anlegen</h2>

    <div class="row collapse">
        <div class="small-2 columns">
            <span class="prefix"><s:text name="userEntity.username"/></span>
        </div>
        <div class="small-5 columns end">
            <s:textfield name="userEntity.username"/>
        </div>
    </div>
    <div class="row collapse">
        <div class="small-2 columns">
            <span class="prefix"><s:text name="userEntity.plannedHours"/></span>
        </div>
        <div class="small-5 columns end">
            <s:textfield name="userEntity.plannedHours" type="text" id="plannedHours" onkeyup="$(this).val($(this).val().replace('.', ','));"/>
        </div>
    </div>
    <div class="row collapse">
        <div class="small-2 columns">
            <span class="prefix"><s:text name="userEntity.role"/></span>
        </div>
        <div class="small-5 columns end noinput">
            <s:select multiple="true"
                      id="rolesList"
                      list="rolesEntityList"
                      name="selectedRoles"
                      listValue="name"
                      listKey="id"
                      cssStyle="width: 100%"/>
        </div>
    </div>
    <div class="row collapse">
        <div class="small-2 columns">
            <span class="prefix"><s:text name="userEntity.contact"/></span>
        </div>
        <div class="small-5 columns end noinput">
            <s:hidden id="selectedContact" name="selectedContact" value="" cssStyle="width: 100%;"></s:hidden>
        </div>
    </div>
    <div class="row left">
        <s:text name="createUserLabel" var="LabelCreateSubmit"/>
        <sj:submit
                targets="result"
                button="true"
                validate="true"
                validateFunction="customValidation"
                onBeforeTopics="beforeSubmit"
                onSuccessTopics="onSuccess"
                value="%{LabelCreateSubmit}"
                cssClass="button radius"/>
    </div>
    <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</s:form>