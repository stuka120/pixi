<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<s:if test="projectEntityList.size() > 0">
    <table role="grid" class="responsive display" id="table">
        <thead>
        <tr>
            <th><s:text name="projectNr"/></th>
            <th><s:text name="projectName"/></th>
            <th><s:text name="description"/></th>
            <th><s:text name="projectStage"/></th>
            <th><s:text name="organisation"/></th>
            <sec:authorize access="hasRole('Projectcontroller')">
                <th style="width: 0.8em;"></th>
                <th style="width: 0.8em;"></th>
            </sec:authorize>
        </tr>
        </thead>
        <tbody>
        <s:iterator value="projectEntityList">
            <tr>
                <td>
                    <s:form theme="simple" action="details.action">
                        <s:hidden key="id"/>
                        <sec:csrfInput></sec:csrfInput>
                    </s:form>
                    <s:property value="projectnumber"/>
                </td>
                <td><s:property value="name"/></td>
                <td class="descriptionCell"><s:property value="description"/></td>
                <td><s:property value="projectstagesByProjectStage.name"/></td>
                <td>
                    <s:if test="organisationsByOrganisationsId == null">
                        -
                    </s:if>
                    <s:else>
                        <s:property value="organisationsByOrganisationsId.name"/>
                    </s:else>
                </td>
                <sec:authorize access="hasRole('Projectcontroller')">
                    <td>
                        <s:if test="projectstagesByProjectStage.name != 'Deaktiviert'">
                            <a data-id="<s:property value="id" />" class="editButton">
                                <i class="fa fa-pencil-square-o fa-lg"></i>
                            </a>
                        </s:if>
                    </td>
                    <td>
                        <s:if test="projectstagesByProjectStage.name != 'Deaktiviert'">
                            <a data-id="<s:property value="id" />" class="deleteButton">
                                <i class="fa fa-lock fa-lg"></i>
                            </a>
                        </s:if>
                        <s:else>
                            <a data-id="<s:property value="id" /> " class="reactivateButton">
                                <i class="fa fa-unlock fa-lg"></i>
                            </a>
                        </s:else>
                    </td>
                </sec:authorize>
            </tr>
        </s:iterator>
        </tbody>
    </table>
</s:if>
<s:else>
    <h4 class="text-center subheader alert-box warning radius"
        style="width: 40%;margin-left: auto; margin-right: auto">
        <s:text name="nodata"/>
    </h4>
</s:else>
