<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<s:if test="templateList.size() > 0">
    <s:iterator value="templateList">
        <div class="block" style="font-size: 10px">
            <s:if test="%{@org.apache.commons.io.FilenameUtils@getExtension(name) == 'docx' || @org.apache.commons.io.FilenameUtils@getExtension(name) == 'doc' }">
                <i class="fa fa-file-word-o fa-5x hover-file"></i>
            </s:if>
            <s:if test="%{@org.apache.commons.io.FilenameUtils@getExtension(name) == 'pdf'}">
                <i class="fa fa-file-pdf-o fa-5x hover-file"></i>
            </s:if>
            <s:form theme="simple" cssStyle="display: none;" action="downloadTemplate">
                <sec:csrfInput></sec:csrfInput>
                <s:hidden name="documentName" value="%{name}"/>
            </s:form>
            <i class="fa fa-download fa-5x download" onclick="$(this).parent().find('form').submit();"
               data-name="<s:property value="name" />"></i>
            <i class="fa fa-times deleteTemplate"></i>

            <div><s:property value="name"/></div>
        </div>
    </s:iterator>
</s:if>
<s:else>
    <h4 class="text-center subheader alert-box warning radius"
        style="width: 40%;margin-left: auto; margin-right: auto">
        <s:text name="noTemplates"/>
    </h4>
</s:else>
