<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<s:form theme="simple" action="save" id="inputForm">
    <sec:csrfInput></sec:csrfInput>
    <s:hidden name="projectEntity.id"/>
    <s:hidden name="projectStagesId"/>
    <div class="row">
        <div class="large-5 columns">
            <div class="row collapse prefix-radius">
                <div class="small-4 columns">
                    <span class="prefix"><s:text name="projectNr"/></span>
                </div>
                <div class="small-8 columns ">
                    <s:textfield tabindex="1" name="projectEntity.projectnumber" id="projectNr"/>
                </div>
            </div>
        </div>
        <div class="large-7 columns">
            <s:fielderror fieldName="projNrErr" cssClass="error"/>
        </div>
    </div>
    <div class="row">
        <div class="large-5 columns">
            <div class="row collapse prefix-radius">
                <div class="small-4 columns">
                    <span class="prefix"><s:text name="projectName"/></span>
                </div>
                <div class="small-8 columns ">
                    <s:textfield tabindex="2" name="projectEntity.name" id="projectName"/>
                </div>
            </div>
        </div>
        <div class="large-7 columns">
            <s:fielderror fieldName="nameErr" cssClass="error"/>
        </div>
    </div>

    <div class="row">
        <div class="large-6 columns">
            <label><s:text name="description"/>
                <s:textarea tabindex="3" name="projectEntity.description" id="description"/>
            </label>
        </div>
        <div class="large-6 columns">
            <s:fielderror fieldName="descErr" cssClass="error"/>
        </div>
    </div>

    <div class="row" style="margin-top: 1em;">
        <div class="large-5 columns">
            <div class="row collapse prefix-radius">
                <div class="small-4 columns">
                    <span class="prefix"><s:text name="organisation"/></span>
                </div>
                <div class="small-8 columns ">
                    <s:select list="organisationsEntityList" listValue="name" listKey="id" name="orgId"
                              headerKey="-1" headerValue="-" tabindex="4"
                              value="%{projectEntity.organisationsByOrganisationsId.id}"/>

                </div>
            </div>
        </div>
        <div class="large-7 columns">
            <s:fielderror fieldName="orgErr" cssClass="error"/>
        </div>
    </div>
    <div class="row">
        <div class="large-6 columns">
            <div class="small-6 columns">
                <a class="button radius" tabindex="5" id="submitButton" style="margin: 0.4em"><s:text name="save"/></a>
            </div>
        </div>
    </div>
    <a class="close-reveal-modal">&#215;</a>
</s:form>
