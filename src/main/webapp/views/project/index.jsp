<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<t:layout>
  <jsp:attribute name="head">
      <title><s:text name="project"/></title>

      <link rel="stylesheet" type="text/css" href="../../assets/css/jquery.dataTables.css"/>
      <link rel="stylesheet" type="text/css" href="../../assets/css/dataTables.foundation.css"/>
      <link rel="stylesheet" type="text/css" href="../../assets/css/iosOverlay.css" />

      <script type="text/javascript" src="../../assets/js/foundation/foundation.reveal.js"></script>
      <script type="text/javascript" src="../../assets/js/jquery.dataTables.js"></script>
      <script type="text/javascript" src="../../assets/js/dataTables.foundation.js"></script>
      <script type="text/javascript" src="../../assets/js/jquery-ui.min.js"></script>
      <script type="text/javascript" src="../../assets/js/jquery.autosize.js"></script>
      <script type="text/javascript" src="../../assets/js/trunk8.js"></script>
      <script type="text/javascript" src="../../assets/js/iosOverlay.js"></script>
      <script type="text/javascript" src="../../assets/js/spin.min.js"></script>

      <script>
          var opts = {
              lines: 13, // The number of lines to draw
              length: 11, // The length of each line
              width: 5, // The line thickness
              radius: 17, // The radius of the inner circle
              corners: 1, // Corner roundness (0..1)
              rotate: 0, // The rotation offset
              color: '#FFF', // #rgb or #rrggbb
              speed: 1, // Rounds per second
              trail: 60, // Afterglow percentage
              className: 'spinner', // The CSS class to assign to the spinner
              zIndex: 2e9, // The z-index (defaults to 2000000000)
              top: 'auto', // Top position relative to parent in px
              left: 'auto' // Left position relative to parent in px
          };
          $(document).ready(function () {
              initDataTableAndButtons();
              $("#reactivateSubmitButton").on('click', function(){
                  $.ajax({
                      url: "/project/reactivate.action",
                      data: {projectStagesId: $("#stageIdSelect").val(), id: $("#hiddenProjectId").val() }
                  }).done(function () {
                      $.ajax({
                          url: "/project/table.action"
                      }).done(function (data) {
                          var container = $("#tableParent");
                          container.empty();
                          container.append(data);
                          initDataTableAndButtons();
                      });
                  });
                  $("#deactivatedSwitch").prop("checked", false);
                  swal("<s:text name="activated" />", "<s:text name="reactivatedMessage" />", "success");
                  $("#reactivateModal").foundation('reveal', 'close');
              });
              $("#newButton").on('click', function (e) {
                  e.preventDefault();
                  var newModal = $("#newModal");
                  newModal.empty();

                  showLoading();
                  $.ajax({
                      url: "/project/create.action"
                  }).done(function (data) {
                      newModal.foundation('reveal', 'open');
                      $(".ios-overlay-show").remove();
                      newModal.empty();
                      newModal.append(data);
                      initModalJavascript();
                  });
              });

              $("#deactivatedSwitch").on('change', function(){
                  showLoading();
                  $.ajax({
                      url: "/project/table.action",
                      data: {showDisabled: $("#deactivatedSwitch").prop("checked")}
                  }).done(function (data) {
                      var container = $("#tableParent");
                      container.empty();
                      container.append(data);
                      initDataTableAndButtons();
                      $(".ios-overlay-show").remove();
                  });
              });
          });

          function showLoading(){
              var target = document.createElement("div");
              document.body.appendChild(target);
              var spinner = new Spinner(opts).spin(target);
              iosOverlay({
                  text: "Loading",
                  duration: 2e5,
                  spinner: spinner
              });
          }

          function initModalJavascript() {
              $("#inputForm").submit(function (e) {
                  var postData = $(this).serializeArray();
                  var formURL = $(this).attr("action");
                  $.ajax({
                      url: formURL,
                      type: "POST",
                      data: postData,
                      success: function (data) {
                          if (data == "created" || data == "edited") {
                              $.ajax({
                                  url: "/project/table.action"
                              }).done(function (data) {
                                  var container = $("#tableParent");
                                  container.empty();
                                  container.append(data);
                                  initDataTableAndButtons();
                              });

                              $("#inputForm").foundation('reveal', 'close');
                              $("#inputForm").empty();
                              if(data == "created"){
                                  swal("<s:text name="created" />", "<s:text name="createdMessage" />", "success");
                              }else{
                                  swal("<s:text name="edited" />", "<s:text name="editedMessage" />", "success");
                              }
                          } else {
                              $("#inputForm").replaceWith(data);
                              initModalJavascript();
                          }
                      }
                  });
                  e.preventDefault();
              });
              $("#submitButton").on('click', function () {
                  $("#inputForm").submit();
              });
              $("#description").autosize();
          }

          function initDataTableAndButtons() {
              $("#table").dataTable({
                  "language": {
                      "url": "../../assets/js/dataTablesLang/<s:text name="dataTablesLang" />"
                  }
              });
              $(".editButton").on('click', function (e) {
                  e.preventDefault();
                  var editModal = $("#newModal");
                  editModal.empty();
                  showLoading();

                  $.ajax({
                      url: "/project/edit.action",
                      data: {id: $(this).data("id")}
                  }).done(function (data) {
                      editModal.foundation('reveal', 'open');
                      editModal.append(data);
                      $(".ios-overlay-show").remove();
                      initModalJavascript();
                  });
              });
              $("#table > tbody").on('click', "tr", function(event){
                  if(event.target.nodeName == "TD"){
                      $(this).find("form").submit();
                  }
              });
              $(".deleteButton").on('click', function(e){
                  e.preventDefault();
                  var id = $(this).data("id");
                  swal({
                              title: "<s:text name="delProject"/>",
                              text: "<s:text name="areYouSure"/>",
                              type: "warning",
                              showCancelButton: true,
                              confirmButtonColor: "#DD6B55",
                              confirmButtonText: "<s:text name="deactivate" />",
                              closeOnConfirm: false
                          },
                          function(){
                              $.ajax({
                                  url: "/project/delete.action",
                                  data: {id: id}
                              }).done(function(){
                                  $.ajax({
                                      url: "/project/table.action"
                                  }).done(function (data) {
                                      var container = $("#tableParent");
                                      container.empty();
                                      container.append(data);
                                      initDataTableAndButtons();
                                  });
                              });
                              swal("<s:text name="deactivated" />", "<s:text name="deletedMessage" />", "success");
                          });
              });

              $('.descriptionCell').trunk8({
                  fill: '&hellip; <a class="read-more" >read more</a>'
              });

              $('#table').on('click', '.read-more', function (event) {
                  $(this).parent().trunk8('revert').append(' <a class="read-less" >read less</a>');

                  return false;
              });

              $('#table').on('click', '.read-less',function (event) {
                  $(this).parent().trunk8();

                  return false;
              });
              $(".reactivateButton").on('click', function(){
                  $("#reactivateModal").foundation('reveal', 'open');
                  $("#hiddenProjectId").val($(this).data("id"));
              });

          }
      </script>
      <style>
          .dataTables_length label select {
              margin-bottom: 1em;
          }

          .dataTables_wrapper div.row {
              max-width: none;
              margin-left: 0px;
              margin-right: 0px;
          }

          .dataTables_filter label {
              max-height: 1.1em;
              margin-bottom: 1em;
          }

          .error {
              color: #D8000C;
              background-color: #FFBABA;
          }
      </style>
  </jsp:attribute>
    <jsp:body>
        <div id="newModal" class="reveal-modal" data-reveal>
        </div>

        <div id="reactivateModal" class="reveal-modal" data-reveal>
            <h1><s:text name="enProject" /></h1>
            <s:text name="chooseStage" />
            <s:form id="reactivateForm" theme="simple">
                <s:hidden id="hiddenProjectId" />
                <s:select list="projectStageList" listValue="name" listKey="id" name="stageId" id="stageIdSelect"/>
                <a id="reactivateSubmitButton" class="button radius"> <s:text name="activate" /></a>
            </s:form>
            <a class="close-reveal-modal">&#215;</a>
        </div>
        <div class="row" style="max-width: none">
            <div class="small-12 medium-3 large-2 columns">
                <h3 class="text-center" style="display: inline-flex">
                    <s:text name="project"/>
                </h3>
                <sec:authorize access="hasRole('Projectcontroller')">
                    <a class="button radius tiny expand" id="newButton">
                        <s:text name="new"/>
                    </a>
                    <hr/>
                    <h4><s:text name="showDisabled" /></h4>
                    <div class="switch radius">
                        <input id="deactivatedSwitch" type="checkbox">
                        <label for="deactivatedSwitch"><s:text name="showDisabled" /></label>
                    </div>
                </sec:authorize>
            </div>
            <div class="small-12 medium-9 large-10 columns">
                <div id="tableParent">
                    <s:include value="table.jsp" />
                </div>
            </div>
        </div>
    </jsp:body>
</t:layout>