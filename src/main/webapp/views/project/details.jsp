<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<t:layout>
  <jsp:attribute name="head">
      <title><s:text name="project"/></title>
      <sec:csrfMetaTags></sec:csrfMetaTags>

      <link type="text/css" rel="stylesheet" href="../../assets/css/progress.css"/>
      <link type="text/css" rel="stylesheet" href="../../assets/css/font-awesome.css"/>

      <script type="text/javascript" src="../../assets/js/jquery.form.js"></script>
      <script type="text/javascript" src="../../assets/js/foundation/foundation.reveal.js"></script>

      <script>
          $(document).ready(function () {
              initFileListJavaScript();
              var csrfToken = $("meta[name='_csrf']").attr("content");
              $("#uploadReportForm").attr("action", $("#uploadReportForm").attr("action") + "?_csrf=" + csrfToken);

              $("#uploadReportForm").ajaxForm(function (x) {
                  if (x == "success") {
                      sweetAlert('<s:text name="uploadSuccessfull" />', '<s:text name="uploadSuccessfullMessage" />', "success");
                      $.ajax({
                          url: "/project/fileList.action",
                          data: {id: <s:property value="projectEntity.id" />}
                      }).done(function (data) {
                          var container = $("#fileListContainer");
                          container.empty();
                          container.append(data);
                          initFileListJavaScript();
                      });
                  } else {
                      sweetAlert('<s:text name="uploadError" />', '<s:text name="uploadErrorMessage" />', "error");
                  }

              });
              $("#uploadReportButton").on('click', function () {
                  $("#uploadReport").trigger('click');
              });
              $("#uploadReport").on('change', function () {
                  if (this.files[0].size > 20000000) {
                      sweetAlert('<s:text name="uploadError" />', '<s:text name="uploadSizeError" />', "error");
                      $(this).val('');
                  } else {
                      $("#uploadReportForm").submit();
                  }
              });
              $("#showEvent").on('click', function () {
                  $("#eventModal").foundation('reveal', 'open');
              });
          });
          function initFileListJavaScript() {
              $(".fa-download").on('click', function () {
                  $(this).parent().parent().submit();
              });
              $(".deleteDocument").on('click', function () {
                  var form = $(this).parent().parent();
                  swal({
                              title: "<s:text name="delDocument"/>",
                              text: "<s:text name="areYouSureDoc"/>",
                              type: "warning",
                              showCancelButton: true,
                              confirmButtonColor: "#DD6B55",
                              confirmButtonText: "<s:text name="del" />",
                              closeOnConfirm: false
                          },
                          function () {
                              var postData = form.serialize();
                              $.ajax({
                                  url: "/project/deleteFile.action",
                                  data: postData
                              }).done(function (data) {
                                  if (data == "success") {
                                      swal("<s:text name="deleted" />", "<s:text name="deletedMessageDoc" />", "success");
                                      $.ajax({
                                          url: "/project/fileList.action",
                                          data: {id: <s:property value="projectEntity.id" />}
                                      }).done(function (data) {
                                          var container = $("#fileListContainer");
                                          container.empty();
                                          container.append(data);
                                          initFileListJavaScript();
                                      });
                                  }
                              });
                          });
              });
          }
      </script>
  </jsp:attribute>
    <jsp:body>

        <div id="eventModal" class="reveal-modal" data-reveal>
            <h1><s:text name="events"/></h1>
            <table style="width: 100%">
                <tr>
                    <th><s:text name="title"/></th>
                    <th><s:text name="startDate"/></th>
                    <th><s:text name="location"/></th>
                    <th><s:text name="contactPerson"/></th>
                </tr>
                <s:iterator value="projectEntity.eventsesById">
                    <tr>
                        <td>
                            <s:property value="title"/>
                        </td>
                        <td>
                            <s:date name="startDate" format="dd.MM.yyyy HH:mm"/>
                        </td>
                        <td>
                            <s:property value="location"/>
                        </td>
                        <td>
                            <s:form theme="simple" action="details" namespace="/contacts">
                                <sec:csrfInput/>
                                <input type="hidden" value="<s:property value='contactsByContactPerson.id'  />"
                                       name="selectedContact"/>
                                <s:url action="details" namespace="/contacts" var="url">
                                    <s:param name="selectedContact"><s:property value="contactsByContactPerson.Id"></s:property></s:param>
                                </s:url>
                                <a class="viewContact" href="<s:property value="url"></s:property> "><s:property
                                        value="contactsByContactPerson.name + ' ' + contactsByContactPerson.surname"/> </a>
                            </s:form>
                        </td>
                    </tr>
                </s:iterator>
            </table>
            <a class="close-reveal-modal">&#215;</a>
        </div>

        <div id="backButton" style="position: fixed;top: 50px; left: 10px;">
            <a href="index.action" style="color: #000000;">
                <i class="fa fa-chevron-circle-left fa-5x"></i>
            </a>
        </div>

        <div id="fullpage">
            <div class="section">
                <div class="row" style="max-width: 80%">
                    <div class="large-4 columns">
                        <div class="row">
                            <h1>
                                <s:property value="projectEntity.name"/>
                            </h1>
                        </div>
                        <dl>
                            <dt><s:text name="projectNr"/>:</dt>
                            <dd><s:property value="projectEntity.projectnumber"/></dd>
                            <s:if test="projectEntity.organisationsByOrganisationsId != null">
                                <dt><s:text name="organisation"/>:</dt>
                                <dd><s:property value="projectEntity.organisationsByOrganisationsId.name"/></dd>
                            </s:if>
                            <dt><s:text name="eventCount"/>:</dt>
                            <dd>
                                <a id="showEvent"> <s:property value="projectEntity.eventsesById.size()"/> </a>
                            </dd>
                            <dt><s:text name="description"/>:</dt>
                            <dd><s:property value="projectEntity.description"/></dd>
                        </dl>
                    </div>
                    <div class="large-8 columns">
                        <div class="row checkout-wrap" style="margin-top: 2em;height: 100px;">
                            <ul class="checkout-bar">
                                <li class="<s:if test='projectEntity.projectstagesByProjectStage.id == 1'>active</s:if><s:if test='projectEntity.projectstagesByProjectStage.id > 1'>visited</s:if>">
                                    Antrag
                                </li>
                                <li class="<s:if test='projectEntity.projectstagesByProjectStage.id == 2'>active</s:if><s:if test='projectEntity.projectstagesByProjectStage.id > 2'>visited</s:if>">
                                    Vertrag
                                </li>
                                <li class="<s:if test='projectEntity.projectstagesByProjectStage.id == 3'>active</s:if><s:if test='projectEntity.projectstagesByProjectStage.id > 3'>visited</s:if>">
                                    Anpassung
                                </li>
                                <li class="<s:if test='projectEntity.projectstagesByProjectStage.id == 4'>active</s:if> last">
                                    Abrechnung
                                </li>
                            </ul>
                        </div>
                        <div class="row" style="margin-top: 2em;" align="center">
                            <sec:authorize access="hasRole('Projectcontroller')">
                                <s:if test='projectEntity.projectstagesByProjectStage.id != 5'>
                                    <s:form action="uploadReport" method="POST" cssStyle="display: inline;"
                                            theme="simple" id="uploadReportForm">
                                        <s:hidden key="projectEntity.id"/>
                                        <input type="file" name="file" id="uploadReport"
                                               accept=".pdf,.docx,.doc,.odt"
                                               style="display: none"/>
                                        <a class="button radius small" id="uploadReportButton">
                                            <s:text name="uploadReport"/>
                                        </a>
                                    </s:form>
                                    <s:if test="projectEntity.projectstagesByProjectStage.id < 4">
                                        <s:form action="nextStage" method="post" cssStyle="display: inline;"
                                                theme="simple"
                                                id="nextStageForm">
                                            <sec:csrfInput></sec:csrfInput>
                                            <s:hidden key="projectEntity.id" name="id"/>
                                            <s:submit key="nextStage" cssClass="button radius small"/>
                                        </s:form>
                                    </s:if>
                                </s:if>
                            </sec:authorize>
                        </div>

                        <div id="fileListContainer">
                            <s:include value="fileList.jsp"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </jsp:body>
</t:layout>