<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<table style="width: 90%;">
    <s:iterator value="fileList">
        <tr>
            <td>
                <s:property value="name"/>
            </td>
            <td style="width: 1em;">
                <s:form theme="simple" action="download">
                    <sec:csrfInput></sec:csrfInput>
                    <s:hidden value="%{name}" name="documentName"/>
                    <s:hidden value="%{projectEntity.id}" name="id"/>
                    <a>
                        <i class="fa fa-download"></i>
                    </a>
                </s:form>
            </td>
            <sec:authorize access="hasRole('Projectcontroller')">
                <s:if test='projectEntity.projectstagesByProjectStage.id != 5'>
                    <td style="width: 1em;">
                        <s:form theme="simple" action="deleteFile">
                            <sec:csrfInput></sec:csrfInput>
                            <s:hidden value="%{name}" name="documentName"/>
                            <s:hidden value="%{projectEntity.id}" name="id"/>
                            <a>
                                <i class="fa fa-times deleteDocument"></i>
                            </a>
                        </s:form>
                    </td>
                </s:if>
            </sec:authorize>
        </tr>
    </s:iterator>
</table>