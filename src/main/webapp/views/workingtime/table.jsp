<%@ taglib prefix="s" uri="/struts-tags" %>
<s:if test="workingtimesList.size() > 0">
    <table role="grid" class="responsive display"
           id="table">
        <thead>
        <tr>
            <th style="width: 10em;"><s:text name="user"/></th>
            <th style="width: 5em;"><s:text name="date"/></th>
            <th style="width: 5em;"><s:text name="starttime"/></th>
            <th style="width: 5em"><s:text name="endtime"/></th>
            <th style="width: 3em;"><s:text name="duration"/></th>
            <th><s:text name="description"/></th>
            <th style="width: 0.8em"></th>
            <th style="width: 0.8em"></th>
        </tr>
        </thead>
        <tbody>
        <s:iterator value="workingtimesList">
            <tr>
                <td><s:property value="usersByUserId.username"/></td>
                <td><s:date name="startTime" format="dd.MM.yyyy"/></td>
                <td>
                    <s:if test="%{desc != 'Urlaub'}">
                        <s:date name="startTime" format="HH:mm"/> <s:text name="oclock" />
                    </s:if>
                    <s:else>
                        -
                    </s:else>
                </td>
                <td>
                    <s:if test="%{desc != 'Urlaub'}">
                        <s:if test="%{endTime.hours == 0 && endTime.minutes == 0}">
                            24:00 <s:text name="oclock" />
                        </s:if>
                        <s:else>
                            <s:date name="endTime" format="HH:mm"/> <s:text name="oclock" />
                        </s:else>
                    </s:if>
                    <s:else>
                        -
                    </s:else>
                </td>
                <td>
                    <s:if test="%{duration != null}">
                        <s:property value="duration"/>
                    </s:if>
                    <s:else>
                        <div style="text-align: center">-</div>
                    </s:else>
                </td>
                <td>
                    <div class="descriptionCell">
                        <s:if test="%{desc != ''}">
                            <s:property value="desc"/>
                        </s:if>
                        <s:else>
                            <div style="text-align: center">-</div>
                        </s:else>
                    </div>
                </td>
                <td>
                    <s:if test="%{desc != 'Urlaub'}">
                        <a data-id="<s:property value="id" />" class="editButton">
                            <i class="fa fa-pencil-square-o fa-lg"></i>
                        </a>
                    </s:if>
                </td>
                <td>
                    <a data-id="<s:property value="id"/>" class="deleteButton">
                        <i class="fa fa-times fa-lg"></i>
                    </a>
                </td>
            </tr>
        </s:iterator>
        </tbody>
    </table>
</s:if>
<s:else>
    <h4 class="text-center subheader alert-box warning radius"
        style="width: 40%;margin-left: auto; margin-right: auto">
        <s:text name="nodata"/>
    </h4>
</s:else>