<%@ taglib prefix="s" uri="/struts-tags" %>
<ul id="dateMenu" data-name="selectedDates" style="background-color: lightgrey;">
    <s:set var="currentDate" value="%{new java.util.Date()}"/>
    <s:iterator value="menuDates">
        <li><s:property value="key"/>
            <ul>
                <s:iterator value="value" var="dateItem">
                    <li data-value="<s:if test="%{#request.locale.language == 'en'}"><s:date name="dateItem" format="MM/dd/yyyy" /></s:if><s:else><s:date name="dateItem" /></s:else>"
                        class="dateMenuItem"
                        <s:if test="%{(#currentDate.month == #dateItem.month && #currentDate.year == #dateItem.year) || selectedDates.contains(dateItem)}">data-checked='1'</s:if>>
                        <s:date name="dateItem" format="MMMMM"/>
                    </li>
                </s:iterator>
            </ul>
        </li>
    </s:iterator>
</ul>
