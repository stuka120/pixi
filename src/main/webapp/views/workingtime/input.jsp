<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<s:form action="save" theme="simple" id="myForm">
    <sec:csrfInput></sec:csrfInput>
    <s:hidden name="workingtimesEntity.id"/>
    <div class="row">
        <div class="row">
            <div class="large-6 small-12 columns">
                <div class="row collapse prefix-radius">
                    <div class="small-4 columns">
                        <span class="prefix"><s:text name="date"/></span>
                    </div>
                    <div class="small-8 columns ">
                        <s:if test="date == null" >
                            <s:hidden name="date" id="hiddenDate" value="%{@java.util.Calendar@getInstance().time}"/>
                            <s:textfield tabindex="1" name="inDate"  cssClass="ll-skin-cangas" id="date" value="%{@java.util.Calendar@getInstance().time}"/>
                        </s:if>
                        <s:else>
                            <s:hidden name="date" id="hiddenDate"/>
                            <s:textfield tabindex="1" name="inDate" cssClass="ll-skin-cangas" id="date"/>
                        </s:else>
                    </div>
                </div>
                <div class="row">
                    <s:fielderror fieldName="dateErr" cssClass="alert-box alert radius"/>
                </div>
            </div>
            <div class="large-6 small-12 columns">
                <s:if test='%{(workingtimesEntity.id + "") != "" && (workingtimesEntity.id + "") != 0}'>
                </s:if>
                <s:else>
                    <input id="__checkbox_holidayCheckbox" name="__checkbox_holiday" type="hidden">
                    <h5 class="left" style="margin-right: 0.375em"><s:text name="holiday"/></h5>

                    <div class="switch radius">
                        <input name="holiday" value="true" id="holidayCheckbox" type="checkbox" <s:if test="holiday">checked="true" </s:if>>
                        <label for="holidayCheckbox"><s:text name="holiday"/></label>
                    </div>
                </s:else>

            </div>
        </div>
        <div class="row">
            <div class="large-6 small-12 columns" id="starttimeRow" <s:if test="holiday">style="display: none"</s:if>>
                <div class="row collapse prefix-radius">
                    <div class="small-4 columns">
                        <span class="prefix"><s:text name="workingtimesEntity.startTime"/></span>
                    </div>
                    <div class="small-8 columns">
                        <s:textfield tabindex="2" name="starttime" id="starttime"/>
                    </div>
                </div>
                <div class="row">
                    <s:fielderror fieldName="starttimeErr" cssClass="alert-box alert radius"/>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="large-6 small-12 columns" id="endtimeRow" <s:if test="holiday">style="display: none"</s:if>>
                <div class="row collapse prefix-radius">
                    <div class="small-4 columns">
                        <span class="prefix"><s:text name="workingtimesEntity.endTime"/></span>
                    </div>
                    <div class="small-8 columns">
                        <s:textfield tabindex="3" name="endtime" id="endtime"/>
                    </div>
                </div>
                <div class="row">
                    <s:fielderror fieldName="endtimeErr" cssClass="alert-box alert radius"/>
                </div>
            </div>
            <div class="large-6 small-12 columns" id="durationRow" <s:if test="holiday">style="display: none"</s:if>>
                <div class="row collapse prefix-radius">
                    <div class="small-4 columns">
                        <span class="prefix"><s:text name="workingtimesEntity.duration"/></span>
                    </div>
                    <div class="small-8 columns">
                        <s:textfield tabindex="4" name="workingtimesEntity.duration" id="duration"/>
                    </div>
                </div>
                <div class="row">
                    <s:fielderror fieldName="workingtimesEntity.durationErr" cssClass="alert-box alert radius"/>
                    <div class="error" id="durationError"></div>
                </div>
            </div>
            <div class="large-6 small-12 columns" id="untilHolidayDate" <s:if test="!holiday">style="display: none"</s:if>>
                <div class="row collapse prefix-radius">
                    <div class="small-4 columns">
                        <span class="prefix"><s:text name="endHolidayDate"/></span>
                    </div>
                    <div class="small-8 columns">
                        <s:hidden tabindex="2" name="endHolidayDate" id="hiddenEndHolidayDate"/>
                        <s:textfield name="inEndHolidayDate" cssClass="ll-skin-cangas" id="endHolidayDate"/>
                    </div>
                </div>
                <div class="row">
                    <s:fielderror fieldName="endHolidayDateErr" cssClass="alert-box alert radius"/>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="small-12 columns" id="descriptionRow">
                <label><s:text name="description" />
                    <s:textarea name="workingtimesEntity.desc"/>
                </label>
                <div class="row">
                    <s:fielderror fieldName="workingtimesEntity.descErr" cssClass="alert-box alert radius" cssStyle="max-width: 1000px"/>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="large-6 columns">
            <div class="small-6 columns">
                <a class="button radius" tabindex="6" id="submitButton" style="margin: 0.4em"><s:text name="save"/></a>
            </div>
                <%--<div class="small-6 columns">--%>
                <%--<a class="button radius text-right" id="closeModal" style="margin: 0.4em"><s:text name="cancel"/></a>--%>
                <%--</div>--%>
        </div>
    </div>
    <a class="close-reveal-modal">&#215;</a>
</s:form>