<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<t:layout>
    <jsp:attribute name="head">
        <title><s:text name="LabelTitle"/></title>
        <link type="text/css" rel="stylesheet" href="../../assets/css/jquery-ui.css"/>
        <link type="text/css" rel="stylesheet" href="../../assets/css/cangas.datepicker.css"/>
        <link type="text/css" rel="stylesheet" href="../../assets/css/jquery.timepicker.css"/>
        <link rel="stylesheet" type="text/css" href="../../assets/css/jquery.dataTables.css"/>
        <link rel="stylesheet" type="text/css" href="../../assets/css/dataTables.foundation.css"/>
        <link rel="stylesheet" type="text/css" href="../../assets/css/jquery.bonsai.css"/>
        <link rel="stylesheet" type="text/css" href="../../assets/css/iosOverlay.css"/>

        <script type="text/javascript" charset="utf-8" src="../../assets/js/jquery.dataTables.js"></script>
        <script type="text/javascript" charset="utf-8" src="../../assets/js/dataTables.foundation.js"></script>
        <script type="text/javascript" charset="utf-8" src="../../assets/js/date-de.js"></script>
        <script type="text/javascript" src="../../assets/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="../../assets/js/moment.js"></script>
        <script type="text/javascript" src="../../assets/js/jquery.timepicker.js"></script>
        <script type="text/javascript" src="../../assets/js/foundation/foundation.reveal.js"></script>
        <script type="text/javascript" src="../../assets/js/jquery.bonsai.js"></script>
        <script type="text/javascript" src="../../assets/js/jquery.qubit.js"></script>
        <script src="../../assets/js/datepicker-<s:text name="languageCode" />.js"></script>
        <script type="text/javascript" src="../../assets/js/watch.js"></script>
        <script type="text/javascript" src="../../assets/js/trunk8.js"></script>
        <script type="text/javascript" src="../../assets/js/iosOverlay.js"></script>
        <script type="text/javascript" src="../../assets/js/spin.min.js"></script>

        <script>
            $(document).ready(function () {
                initDataTableAndButtons();
                $("#newButton").on('click', function (e) {
                    e.preventDefault();
                    var newModal = $("#newModal");
                    newModal.empty();
                    showOverlay();

                    $.ajax({
                        url: "/workingtime/create.action"
                    }).done(function (data) {
                        newModal.append(data);
                        newModal.foundation('reveal', 'open');
                        hideOverlay();
                        initModalJavascipt();
                    });
                });
                $("#closeDeleteModal").on('click', function (e) {
                    $("#deleteModal").foundation('reveal', 'close');
                });
                initMenu();
                $("#menuForm_selectedUserForExport").on('change', function () {
                    showOverlay();
                    $.ajax({
                        url: "/workingtime/menu.action",
                        data: {selectedUserForExport: $(this).val()}
                    }).done(function (data) {
                        $("#menuContainer").empty();
                        $("#menuContainer").append(data);
                        initMenu();
                        var postData = $("#menuForm").serialize();
                        $.ajax({
                            url: "/workingtime/table.action",
                            data: postData
                        }).done(function (data) {
                            var container = $("#tableParent");
                            container.empty();
                            container.append(data);
                            initDataTableAndButtons();
                            hideOverlay();
                            var checkedCount = $("#dateMenu").find("input[name='selectedDates']:checked").length;
                            var submitButton = $("#menuForm_export");
                            if (checkedCount == 0) {
                                submitButton.prop("disabled", true);
                            } else {
                                submitButton.prop("disabled", false);
                            }
                        });
                    });
                });
            });
            var running = false;
            function initMenu() {
                $("#dateMenu").bonsai({
                    checkboxes: true,
                    createCheckboxes: true
                });
                $(".dateMenuItem input").on('change', function (event) {
                    event.stopPropagation();
                    showOverlay();
                    var postData = $("#menuForm").serialize();
                    $.ajax({
                        url: "/workingtime/table.action",
                        data: postData
                    }).done(function (data) {
                        var container = $("#tableParent");
                        container.empty();
                        container.append(data);
                        initDataTableAndButtons();
                        hideOverlay();
                    });
                });
                if (!!navigator.userAgent.match(/Trident\/7\./)) {
                    $(".has-children").each(function () {
                        watch($(this).find("input").first()[0], "indeterminate", function () {
                            $(this).find("input").first().trigger("change");
                        });
                    });
                }
                $("input[name='selectedDates']").on('change', function () {
                    var checkedCount = $("#dateMenu").find("input[name='selectedDates']:checked").length;
                    var submitButton = $("#menuForm_export");
                    if (checkedCount == 0) {
                        submitButton.prop("disabled", true);
                    } else {
                        submitButton.prop("disabled", false);
                    }
                });
            }
            function initDataTableAndButtons() {
                $('#table').dataTable({
                    columnDefs: [
                        {type: 'de_date', targets: 1}
                    ],
                    "order": [1, 'asc'],
                    "language": {
                        "url": "../../assets/js/dataTablesLang/<s:text name="dataTablesLang" />"
                    }
                });
                $('.descriptionCell').trunk8({
                    fill: '&hellip; <a class="read-more" >read more</a>',
                    lines: 1
                });

                $('#table').on('click', '.read-more', function (event) {
                    $(this).parent().trunk8('revert').append(' <a class="read-less" >read less</a>');

//                    return false;
                });

                $('#table').on('click', '.read-less', function (event) {
                    $(this).parent().trunk8();

//                    return false;
                });
                $(".deleteButton").on('click', function (x) {
                    x.preventDefault();
                    var id = $(this).data("id");
                    swal({
                                title: "<s:text name="delEntry"/>",
                                text: "<s:text name="areYouSure"/>",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "<s:text name="del" />",
                                closeOnConfirm: false
                            },
                            function () {
                                $.ajax({
                                    url: "/workingtime/delete.action",
                                    data: {id: id}
                                }).done(function () {
                                    var postData = $("#menuForm").serialize();
                                    $.ajax({
                                        url: "/workingtime/table.action",
                                        data: postData
                                    }).done(function (data) {
                                        var container = $("#tableParent");
                                        container.empty();
                                        container.append(data);
                                        initDataTableAndButtons();
                                    });
                                    $.ajax({
                                        url: "/workingtime/menu.action",
                                        data: {selectedUserForExport: $("#menuForm_selectedUserForExport").val()}
                                    }).done(function (data) {
                                        $("#menuContainer").empty();
                                        $("#menuContainer").append(data);
                                        initMenu();
                                    });
                                });
                                swal("<s:text name="deleted" />", "<s:text name="deletedMessage" />", "success");
                            });
                });
                $(".editButton").on('click', function (e) {
                    e.preventDefault();
                    var editModal = $("#newModal");
                    editModal.empty();
                    var opts = {
                        lines: 13, // The number of lines to draw
                        length: 11, // The length of each line
                        width: 5, // The line thickness
                        radius: 17, // The radius of the inner circle
                        corners: 1, // Corner roundness (0..1)
                        rotate: 0, // The rotation offset
                        color: '#FFF', // #rgb or #rrggbb
                        speed: 1, // Rounds per second
                        trail: 60, // Afterglow percentage
                        className: 'spinner', // The CSS class to assign to the spinner
                        zIndex: 2e9, // The z-index (defaults to 2000000000)
                        top: 'auto', // Top position relative to parent in px
                        left: 'auto' // Left position relative to parent in px
                    };
                    var target = document.createElement("div");
                    document.body.appendChild(target);
                    var spinner = new Spinner(opts).spin(target);
                    iosOverlay({
                        text: "Loading",
                        duration: 2e5,
                        spinner: spinner
                    });

                    $.ajax({
                        url: "/workingtime/edit.action",
                        data: {id: $(this).data("id")}
                    }).done(function (data) {
                        editModal.append(data);
                        editModal.foundation('reveal', 'open');
                        $(".ios-overlay-show").remove();
                        initModalJavascipt();
                    });
                });
            }
            function initModalJavascipt() {
                var locale = "<s:property value="#request.locale.language" />";
                if(locale == "en"){
                    var dateFormat = "mm/dd/yy";
                }else{
                    var dateFormat = "dd.mm.yy";
                }
                $("#date").datepicker({
                            dateFormat: dateFormat,
                            beforeShowDay: function (date) {
                                day = date.getDate();
                                month = date.getMonth() + 1;
                                year = date.getYear();
                                return CheckHoliday(year, month, day);
                            }
                        },
                        $.datepicker.regional['<s:text name="languageCode" />']).datepicker('widget').wrap('<div class="ll-skin-cangas"/>');
                $("#endHolidayDate").datepicker({
                            dateFormat: dateFormat,
                            beforeShowDay: function (date) {
                                day = date.getDate();
                                month = date.getMonth() + 1;
                                year = date.getYear();
                                return CheckHoliday(year, month, day);
                            }
                        },
                        $.datepicker.regional['<s:text name="languageCode" />']).datepicker('widget').wrap('<div class="ll-skin-cangas"/>');
                $("#starttime , #endtime").on("change", function () {
                    var a = moment($("#starttime").val(), "HH:mm");
                    var b = moment($("#endtime").val(), "HH:mm");
                    var diff = moment.duration(b.diff(a)).asHours();
                    if ($("#starttime").val() && $("#endtime").val()) {
                        if (diff > 0) {
                            $("#duration").val(diff);
                            $("#durationError").empty();
                        } else {
                            $("#durationError").empty();
                            $("#durationError").append('<s:text name="negativeDuration" />');
                        }
                    }
                });
                $("#starttime").timepicker({
                    'timeFormat': 'H:i',
                    'scrollDefault': 'now',
                    'step': 15,
                    'maxTime': '23:45'
                });
                $("#endtime").timepicker({
                    'timeFormat': 'H:i',
                    'scrollDefault': 'now',
                    'step': 15
                });
                $("#starttime").on('changeTime', function () {
                    $("#endtime").timepicker('option', 'disableTimeRanges', [['00:00', $("#starttime").val()]]);
                });
                $("#date").on("change", function () {
                    $("#hiddenDate").val($("#date").val());
                });
                $("#endHolidayDate").on("change", function () {
                    $("#hiddenEndHolidayDate").val($("#endHolidayDate").val());
                })
                $("#myForm").submit(function (e) {
                    var postData = $("#myForm, #menuForm").serialize();
                    var formURL = $(this).attr("action");
                    $.ajax({
                        url: formURL,
                        type: "POST",
                        data: postData,
                        success: function (data) {
                            if (data == "success") {
                                $.ajax({
                                    url: "/workingtime/menu.action",
                                    data: $("#menuForm").serialize()
                                }).done(function (data) {
                                    $("#menuContainer").empty();
                                    $("#menuContainer").append(data);
                                    initMenu();
                                    var postData = $("#menuForm").serialize();
                                    $.ajax({
                                        url: "/workingtime/table.action",
                                        data: postData
                                    }).done(function (data) {
                                        var container = $("#tableParent");
                                        container.empty();
                                        container.append(data);
                                        initDataTableAndButtons();
                                    });
                                });
                                $("#newModal").foundation('reveal', 'close');
                                $("#newModal").empty();
                            } else {
                                $("#myForm").replaceWith(data);
                                initModalJavascipt();
                            }
                        }
                    });
                    e.preventDefault();
                });
                $("#submitButton").on('click', function () {
                    var locale = "<s:property value="#request.locale.language" />";
                    if(locale == "en"){
                        $("#duration").val($("#duration").val().replace(/\,/g, '.'));
                    }else{
                        $("#duration").val($("#duration").val().replace(/\./g, ','));
                    }
                    $("#myForm").submit();
                });
                $("#closeModal").on('click', function (e) {
                    $("#newModal").foundation('reveal', 'close');
                    $("#newModal").empty();
                });
                $("#holidayCheckbox").on('change', function () {
                    $("#endtimeRow").toggle()
                    $("#starttimeRow").toggle();
                    $("#durationRow").toggle();
                    $("#descriptionRow").toggle();
                    $("#untilHolidayDate").toggle();
                });
            }
            function CheckHoliday(year, monat, tag) {
                year += 1900;
                //Check fixed Holidays
                if (tag == 1 && monat == 1) {
                    return [true, 'holiday', '<s:text name="newyear"/>'];
                }
                if (tag == 6 && monat == 1) {
                    return [true, 'holiday', '<s:text name="threeKings"/>'];
                }
                if (tag == 1 && monat == 5) {
                    return [true, 'holiday', '<s:text name="nationalHoliday"/>'];
                }
                if (tag == 15 && monat == 8) {
                    return [true, 'holiday', '<s:text name="assumptionDay"/>'];
                }
                if (tag == 26 && monat == 10) {
                    return [true, 'holiday', '<s:text name="AustrianNationalDay"/>'];
                }
                if (tag == 1 && monat == 11) {
                    return [true, 'holiday', '<s:text name="AllHallows"/>'];
                }
                if (tag == 15 && monat == 11) {
                    return [true, 'holiday', '<s:text name="StLeopoldsday"/>'];
                }
                if (tag == 8 && monat == 12) {
                    return [true, 'holiday', '<s:text name="ImmaculateConception"/>'];
                }
                if (tag == 25 && monat == 12) {
                    return [true, 'holiday', '<s:text name="christmasDay"/>'];
                }
                if (tag == 26 && monat == 12) {
                    return [true, 'holiday', '<s:text name="boxingDay"/>'];
                }

                //Check easterSunday and Dependent Holidays
                var day;
                var month;

                var a = year % 19;
                var b = year % 4;
                var c = year % 7;
                var k = Math.floor(year / 100);
                var p = Math.floor((8 * k + 13) / 25);
                var q = Math.floor(k / 4);
                var M = (15 + k - p - q) % 30;
                var d = (19 * a + M) % 30;
                var N = (4 + k - q) % 7;
                var e = (2 * b + 4 * c + 6 * d + N) % 7;
                var ostern = (22 + d + e);
                if (ostern > 31) {
                    month = 4;
                    day = ostern - 31;
                } else {
                    month = 3;
                    day = ostern;
                }
                var eastersunday = Date.UTC(year, month - 1, day, 12);
                var isDate = Date.UTC(year, monat - 1, tag, 12);
                var timeDiff = isDate / (1000 * 60 * 60 * 24) - eastersunday / (1000 * 60 * 60 * 24);
                var diffDays = timeDiff;
                if (diffDays == 0) {
                    return [true, 'holiday', '<s:text name="easterSunday" />'];
                } else if (diffDays == 1) {
                    return [true, 'holiday', '<s:text name="easterMonday" />'];
                } else if (diffDays == 39) {
                    return [true, 'holiday', '<s:text name="AscensionDay" />'];
                } else if (diffDays == 49) {
                    return [true, 'holiday', '<s:text name="whitsunday" />'];
                } else if (diffDays == 50) {
                    return [true, 'holiday', '<s:text name="whitmonday" />'];
                } else if (diffDays == 60) {
                    return [true, 'holiday', '<s:text name="corpusChristi" />'];
                } else {
                    return [true, ''];
                }
            }
        </script>
        <style>
            .dataTables_length label select {
                margin-bottom: 1em;
            }

            .dataTables_wrapper div.row {
                max-width: none;
                margin-left: 0px;
                margin-right: 0px;
            }

            .dataTables_filter label {
                max-height: 1.1em;
                margin-bottom: 1em;
            }

            .error {
                color: #D8000C;
                background-color: #FFBABA;
            }

            .holiday a {
                background: none repeat scroll 0% 0% #e8540a !important;
            }
        </style>
    </jsp:attribute>
    <jsp:body>
        <div id="newModal" class="reveal-modal" data-reveal>
        </div>

        <div class="row" style="max-width: none">
            <div class="small-12 medium-3 large-2 columns">
                <h3 class="text-center" style="display: inline-flex"><s:text
                        name="workingtimeTitle"/></h3>

                <a class="button radius tiny expand" id="newButton"><s:text
                        name="recordWorkingtime"/></a>
                <hr/>
                <s:form id="menuForm" theme="simple" action="export">
                    <sec:csrfInput></sec:csrfInput>
                    <h4>Filter</h4>

                    <div id="menuContainer">
                        <s:include value="menu.jsp"/>
                    </div>
                    <hr>
                    <sec:authorize access="hasRole('Projectcontroller')">
                        <h4><s:text name="export" /></h4>
                        <s:select list="usersEntityList"
                                  listValue="contactsByContactsId.name + ' ' + contactsByContactsId.surname"
                                  listKey="id" name="selectedUserForExport"
                                  value="selectedUserForExport"/>
                        <s:submit key="export" cssClass="button tiny radius expand"/>
                    </sec:authorize>
                </s:form>
            </div>
            <div class="small-12 medium-9 large-10 columns">
                <div id="tableParent">
                    <s:include value="table.jsp"/>
                </div>
            </div>
        </div>
        </div>
    </jsp:body>
</t:layout>