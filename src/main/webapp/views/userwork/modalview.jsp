<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<script type="text/javascript">
      //@ sourceURL=userwork.js
      function showList(){
        $('#worktable_div').fadeIn();
        $('#creatework').fadeOut();
      }
      function showForm(){
        $('#worktable_div').fadeOut();
        $('#creatework').fadeIn();
      }
      function reloadList(eventid){
          $('#worktable_div').html(returnLoader());
          $('#worktable_div').load("/userwork/list?eventId="+eventid)
      }
      function reloadCreate(eventid){
          $('#creatework').html(returnLoader());
          $('#creatework').load("/userwork/create?eventId="+eventid)
      }

    $(document).ready(function(){
        var eventid = <s:property value="event"></s:property>;
        reloadList(eventid);
        reloadCreate(eventid);
    });
</script>
<style>
  .confirm_delete,.confirm_delete:hover{
    color:red;
  }
</style>
<div id="worktable_div">
</div>
<div id="creatework" style="display: none">
</div>