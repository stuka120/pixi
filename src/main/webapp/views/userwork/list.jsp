<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<script type="text/javascript">
  //@ sourceURL=userworklist.js
  $('#switchcreate').click(function(){
    showForm();
  });
  $('.delete_userwork').on('click', function(){
      var userwork_id = $(this).attr('userwork_id');
      swal({title: "<s:text name="userwork_are_you_sure"/>",
          text: "<s:text name="userwork_delete_confirm"/>",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "<s:text name="userwork_remove"/>",
          cancelButtonText: "<s:text name="userwork_cancel"/>",
          closeOnConfirm: false
      }, function(){
          var eventid = <s:property value="event"></s:property>;
          $.ajax({
              url: "/userwork/delete",
              statusCode: {
                  200: function() {
                      swal("<s:text name="userwork_deleted"/>", "<s:text name="userwork_deleted_text"/>", "success");
                      reloadList(eventid);
                  },
                  500:function() {
                      swal("<s:text name="userwork_not_deleted"/>", "<s:text name="userwork_error"/>", "error");
                      reloadList(eventid);
                  }
              },
              data:{
                  "userwork": userwork_id
              }
          });

      });
  });
  $('#switchcreate').click(function(){
      showForm();
  });
  $('#worktable').DataTable({
      "language": {
          "url": "../../assets/js/dataTablesLang/<s:text name="dataTablesLang" />"
      }
  });
</script>
<button class="small button radius" id="switchcreate"><s:text name="create_userwork"/></button>
<table class="row display" cellspacing="0" id="worktable">
  <thead>
  <tr>
    <th><s:text name="userwork_workers"></s:text></th>
    <th><s:text name="userwork_startdate"/></th>
    <th><s:text name="userwork_starttime"/></th>
    <th><s:text name="userwork_enddate"/></th>
    <th><s:text name="userwork_endtime"/></th>
    <th> </th>
  </tr>
  </thead>
  <tbody>
  <s:iterator value="userworks">
    <tr>
      <td><s:property value="User.contactsByContactsId.name"></s:property> <s:property value="User.contactsByContactsId.surname"></s:property></td>
      <td><s:date name="startTime" format="dd.MM.yyyy"></s:date></td>
      <td><s:date name="startTime" format="HH:mm"></s:date></td>
      <td><s:date name="endTime" format="dd.MM.yyyy"></s:date></td>
      <td><s:date name="endTime" format="HH:mm"></s:date></td>
      <td><a class="delete_userwork" userwork_id='<s:property value="id"></s:property>'><i class="fa fa-times fa-lg"></i></a></td>
    </tr>
  </s:iterator>
  </tbody>
</table>