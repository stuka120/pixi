<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<style>
    .select2-search__field {
        box-shadow: none !important;
        padding: 0 !important;
        margin: 0 !important;
    }

    .select2-selection--multiple {
        min-height: 2.3125rem !important;
    }
</style>
<script>
    //@ sourceURL=userworkcreate.js
    function uwcustomValidation(form, errors) {
        var list = $('#userworkformerrors');
        list.show();
        if (errors.errors) {
            $.each(errors.errors, function (index, value) {
                list.append('<li>' + value + '</li>\n');
            });
        }
        if (errors.fieldErrors) {
            $.each(errors.fieldErrors, function (index, value) {
                list.append('<li>' + value + '</li>\n');
                $("input[name='" + index + "']").closest(".row").find(".prefix, .topfix").css("background-color", "#f04124");
            });
        }
    }
    $.subscribe('uwbeforeSubmit', function (event, data) {
        $("#userworkformerrors").empty();
        $("#userworkformerrors").hide();
        $(".prefix, .topfix").css("background-color", "#f2f2f2");
    });
    $.subscribe('uwonSuccess', function (event, data) {
        var json = JSON.parse(data.innerText);
        var saarray = [];
        function isLastAlert(index){
            return saarray.length - 2 == index;
        }
        function success(index){
            var arbeitszeit = "<s:text name="worktimes_saved"/>";
            if(json.success.length == 1){
                arbeitszeit = "<s:text name="worktime_saved"/>";
            }
            swal({
                        title:json.success.length + " "+arbeitszeit,
                        type:"success",
                        closeOnConfirm: isLastAlert(index)},

                    function(){
                        saarray[index+1](index+1);
                    });
        }
        function error(index){
            var arbeitszeit = "<s:text name="worktimes_not_saved"/>";
            if(json.errors.length == 1){
                arbeitszeit = "<s:text name="worktime_not_saved"/>";
            }
            swal({
                        title: json.errors.length + " "+arbeitszeit,
                        text: "<s:text name="userwork_conflict_with"/>:"+json.conflicts,
                        type: "error",
                        closeOnConfirm: isLastAlert(index)},
                    function(){
                        saarray[index+1](index+1);
                    });
        }
        function confirm(index){
            var arbeitszeit = "<s:text name="merges_confirm"/>";
            if(json.confirm.length == 1){
                arbeitszeit = "<s:text name="merge_confirm"/>";
            }
            swal({
                        title: json.confirm.length + " "+ arbeitszeit,
                        text: "<s:text name="merge_text"/>",
                        type: "warning",
                        showConfirmButton: true,
                        confirmButtonText: "<s:text name="userwork_save"/>",
                        showCancelButton: true,
                        cancelButtonText: "<s:text name="userwork_cancel"/>",
                        closeOnCancel: true,
                        closeOnConfirm: true
                    },
                    function () {
                        $.ajax({
                            url: "/userwork/merge",
                            data: {confirm: JSON.stringify(json.confirm)}
                        }).success(function( data, textStatus, jqXHR )  {
                            swal({title: "<s:text name="userwork_saved"/>", type: "success"}, function(){
                                saarray[index+1](index+1);
                            });
                        }).error(function(jqXHR, textStatus, errorThrown ){
                            swal({title: "<s:text name="userwork_error"/>", type: "error"}, function(){
                                saarray[index+1](index+1);
                            });
                        });

                    });
        }
        function end(index){
            var event = <s:property value="event"></s:property>;
            reloadList(event);
            reloadCreate(event);
            showList();
        }
        if (json.success.length > 0) {
            saarray.push(success);
        }
        if (json.errors.length > 0) {
            saarray.push(error);
        }
        if (json.confirm.length > 0) {
            saarray.push(confirm);
        }
        saarray.push(end);
        saarray[0](0);
    });
    $("#userchoice").select2({
        multiple: true,
        ajax: {
            url: "/user/searchapi",
            dataType: 'json',
            quietMillis: 50,
            data: function (params) {
                return {
                    userSearchString: params
                };
            },
            results: function (data, page) { // parse the results into the format expected by Select2.
                // since we are using custom formatting functions we do not need to alter the remote JSON data
                return { results: data };
            },
            cache: true
        },
        minimumInputLength: 1
    });
    $('#workstarttime').timepicker({
    'timeFormat': 'H:i',
    'scrollDefault': 'now',
    'step': 15
    });
    $('#workendtime').timepicker({
    'timeFormat': 'H:i',
    'scrollDefault': 'now',
    'step': 15
    });
    $("#workstartdate").datepicker({
    dateFormat: "dd.mm.yy",
    onSelect: function () {
        var e = $.Event("keyup");
        e.keyCode = 13;
        $(this).trigger(e);
        enddate.disable();
        enddate.enable();
        $("#workenddate").datepicker("option", "minDate", $(this).val());
    }
    });
    $("#workenddate").datepicker({
    dateFormat: "dd.mm.yy",
    onSelect: function () {
        var e = $.Event("keyup");
        e.keyCode = 13;
        $(this).trigger(e);
        startdate.disable();
        startdate.enable();
        $("#workstartdate").datepicker("option", "maxDate", $(this).val());
    }
    });
</script>
<div id="result2" class="hidden" style="display: none;"></div>
<div data-alert class="alert-box alert radius" id="userworkformerrors" style="display: none"></div>
<form action="/userwork/createSubmit" id="createworkform">
    <sec:csrfInput></sec:csrfInput>
  <div class="row">
    <div class="row">
      <div class="small-12 large-12 columns">
        <div class="row collapse">
          <div class="small-4 large-2 columns">
            <span class="topfix"><s:text name="userwork_workers"/></span>
          </div>
          <div class="small-12 columns">
              <input type="hidden" id="userchoice" style="width:100%" name="users"></div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="small-12 large-6 columns">
        <div class="row collapse">
          <div class="small-4 columns">
            <span class="prefix"><s:text name="userwork_startdate"/></span>
          </div>
          <div class="small-8 columns"><input type="text" name="workstartdate" id="workstartdate"></div>
        </div>
      </div>

      <div class="small-12 large-6 columns">
        <div class="row collapse">
          <div class="small-4 columns">
            <span class="prefix"><s:text name="userwork_starttime"/></span>
          </div>
          <div class="small-8 columns"><input type="text" name="workstartdate" id="workstarttime"></div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="small-12 large-6 columns">
        <div class="row collapse">
          <div class="small-4 columns">
            <span class="prefix"><s:text name="userwork_enddate"/></span>
          </div>
          <div class="small-8 columns"><input type="text" name="workenddate" id="workenddate"></div>
        </div>
      </div>
      <div class="small-12 large-6 columns">
        <div class="row collapse">
          <div class="small-4 columns">
            <span class="prefix"><s:text name="userwork_endtime"/></span>
          </div>
          <div class="small-8 columns"><input type="text" name="workenddate" id="workendtime"></div>
        </div>
      </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
          <s:hidden value="%{event}" name="event"></s:hidden>
            <button class="small button radius alert" onclick="showList();"><s:text name="userwork_back"/></button>
            <s:text name="userwork_create" var="create"/>
            <sj:submit
                    cssClass="button radius small right"
                    targets="result2"
                    button="true"
                    validate="true"
                    validateFunction="uwcustomValidation"
                    onBeforeTopics="uwbeforeSubmit"
                    onSuccessTopics="uwonSuccess"
                    value="%{create}"
                    />
        </div>
    </div>
  </div>
</form>
