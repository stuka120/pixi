<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>


<title><s:text name="LabelEditHeader"/></title>
<script src="../../assets/js/select2.js"></script>
<form method="get" action="<s:url action="create" namespace="/organisations"/>" id="orgCreateForm">
    <sec:csrfInput></sec:csrfInput>
    <div class="row">
        <div class="large-6 medium-6 small-12 columns">
            <h3><s:text name="OrganisationCreateLabelCreateOrganisation"/></h3>

            <div class="row collapse">
                <s:fielderror fieldName="nameErr" cssClass="alert-box alert radius"/>
                <div class="small-4 columns">
                    <span class="prefix"><s:text name="organisationsEntity.name"/></span>
                </div>
                <div class="small-8 columns end">
                    <s:textfield name="organisationsEntity.name"/>
                </div>
            </div>
            <div class="row">
                <div class="small-4 columns">
                    <label><s:text name="organisationsEntity.desc"/>:</label>
                </div>
                <div class="small-12 columns end">
                    <s:textarea name="organisationsEntity.desc" cssStyle="height: 12em;"/>
                </div>
            </div>
        </div>
        <div class="large-6 medium-6 small-12 columns">
            <h3><s:text name="ORganisationCreateLabelAddContact"/></h3>

            <div class="row">
                <div class="small-4 columns">
                    <span><s:text name="organisationsEntity.contacts"/></span>
                </div>
                <div class="small-12 columns end">
                    <s:hidden id="contactsList"
                              name="selectedContacts"
                              cssStyle="margin-bottom: 16px; width: 100%"/>
                </div>
            </div>
            <s:if test="organisationsEntity.contactsesById.size > 0">
                <div class="row" style="max-height: 250px; overflow:auto">
                    <table class="table small-12 columns">
                        <tr>
                            <th><s:text name="orgContactsEntity.title"/></th>
                            <th><s:text name="orgContactsEntity.name"/></th>
                            <th><s:text name="orgContactsEntity.surname"/></th>
                        </tr>
                        <s:iterator value="organisationsEntity.contactsesById" status="status">
                            <tr>
                                <td><s:property value="title"/></td>
                                <td><s:property value="name"/></td>
                                <td><s:property value="surname"/></td>
                            </tr>
                        </s:iterator>
                    </table>
                </div>
            </s:if>
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <s:hidden name="organisationsEntity.Id"/>
            <input type="submit" class="button radius small" value="<s:text name="OrganisationCreateLAbelCreateContact"/>">
        </div>
    </div>
    <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</form>
<script type="text/javascript">
    //# sourceURL=CreateOrg.js
    $(document).ready(function () {
        $("#contactsList").select2({
            placeholder: 'Kontakt',
            minimumInputLength: 1,
            multiple: true,
            ajax: {
                url: "/organisations/unreferencedContacts",
                dataType: 'json',
                quietMillis: 250,
                data: function (term, page) {
                    return {
                        contactSearch: term
                    };
                },
                results: function (data, page) { // parse the results into the format expected by Select2.
                    // since we are using custom formatting functions we do not need to alter the remote JSON data
                    return { results: data };
                },
                cache: true
            }
        });
    });
</script>