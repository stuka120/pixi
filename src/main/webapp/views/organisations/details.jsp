<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:layout>
    <jsp:attribute name="footer">
      <p id="copyright">Binary Cheese</p>
    </jsp:attribute>
    <jsp:attribute name="head">
        <script type="text/javascript" src="../../assets/js/foundation.reveal.js"></script>

    </jsp:attribute>
    <jsp:body>
        <h1><s:text name="OrganisationDetailsHeader"/> </h1>
    </jsp:body>
</t:layout>
