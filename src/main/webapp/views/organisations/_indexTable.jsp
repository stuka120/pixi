<%@ taglib prefix="s" uri="/struts-tags" %>
<table id="organisationTable" <%--align="center" style="max-width: 80%; width: 80%" --%> role="grid"
       class="responsive display" cellspacing="0">
    <thead>
    <tr>
        <th><s:text name="OrganisationTableHeaderName"/></th>
        <th><s:text name="OrganisationTableHeaderDescription"/></th>
        <th><s:text name="OrganisationTableHeaderNumberofcontacts"/></th>
        <th></th>
        <th></th>
    </tr>
    </thead>
    <tfoot>
    <tr>
        <th><s:text name="OrganisationTableHeaderName"/></th>
        <th><s:text name="OrganisationTableHeaderDescription"/></th>
        <th><s:text name="OrganisationTableHeaderNumberofcontacts"/></th>
        <th></th>
        <th></th>
    </tr>
    </tfoot>
    <tbody>
    <s:iterator value="organisationsEntityList" status="status">
        <tr>
            <td><s:property value="name"/></td>
            <td><s:property value="desc"/></td>
            <td><s:property value="contactMembers"/></td>
            <td>
                <a class="editOrg" data-id="<s:property value="Id" /> "><i class="fa fa-pencil-square-o fa-lg"></i></a>
            </td>
            <td>
                <s:if test="Id != 1">
                    <a class="deleteOrgansiation" data-id="<s:property value="Id"/>">
                        <i class="fa fa-times fa-lg"></i>
                    </a>
                </s:if>
            </td>
        </tr>
    </s:iterator>
    </tbody>
</table>