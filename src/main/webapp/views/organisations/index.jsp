<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:layout>
    <jsp:attribute name="footer">
      <p id="copyright">Binary Cheese</p>
    </jsp:attribute>
    <jsp:attribute name="head">
        <title><s:text name="IndexPageTitle"/></title>
        <link type="text/css" href="../../assets/css/jquery.dataTables.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="../../assets/css/dataTables.foundation.css"/>
        <link rel="stylesheet" type="text/css" href="../../assets/css/select2-foundation5.css"/>
        <script type="text/javascript" src="../../assets/js/jquery.dataTables.js"></script>
        <script type="text/javascript" charset="utf-8" src="../../assets/js/dataTables.foundation.js"></script>
        <script type="text/javascript" charset="utf-8" src="../../assets/js/foundation/foundation.reveal.js"></script>
        <script>
            $(document).ready(function () {
                initDataTableAndButtons();
                $("#modal").on("submit", "#orgCreateForm", function(e){
                    e.preventDefault();
                    var postData = $("#orgCreateForm").serialize();
                    var formURL = $(this).attr("action");
                    $.ajax({
                        url: formURL,
                        type: "POST",
                        data: postData,
                        success: function (data) {
                            if (data == "success") {
                                $.ajax({
                                    url: "/organisations/table.action",
                                    data: postData
                                }).done(function (data) {
                                    var container = $("#tableParent");
                                    container.empty();
                                    container.append(data);
                                    initDataTableAndButtons();
                                });
                                $("#modal").foundation('reveal', 'close');
                                $("#modal").empty();
                            } else {
                                $("#orgCreateForm").replaceWith(data);
                            }
                        }
                    });
                });
                $("#newOrgButton").click(function () {
                    showOverlay();
                    $('#modal').foundation('reveal', 'open', {
                        url: '<s:url action="create" namespace="/organisations"/>',
                        success: function (data) {
                            hideOverlay();
                        },
                        error: function () {
                            hideOverlay();
                        }
                    });
                });
                $("body").on("click",".editOrg",function () {
                    showOverlay();
                    var id = $(this).data("id");
                    var url = "<s:url action='edit' namespace='/organisations'/>"+"?selectedOrganisation="+id;
                    console.log(id + url);
                    $('#modal').foundation('reveal', 'open', {
                        url: url,
                        success: function (data) {
                            hideOverlay();
                        },
                        error: function () {
                            hideOverlay();
                        }
                    });
                })
            });
            function initDataTableAndButtons() {
                $("#organisationTable").dataTable({
                    "language": {
                        "url": "../../assets/js/dataTablesLang/<s:text name="dataTablesLang" />"
                    }
                });
                $("body").on('click', ".deleteOrgansiation", function (x) {
                    x.preventDefault();
                    var id = $(this).data("id");
                    swal({
                                title: "<s:text name="OrganisationDeleteHeaderLabel"/>",
                                text: "<s:text name="areYouSure"/>",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "<s:text name="OrganisationIndexTableDeleteLabel" />",
                                closeOnConfirm: false
                            },
                            function () {
                                $.ajax({
                                    url: "<s:url action='delete' namespace='/organisations' />",
                                    data: {"organisationsEntity.Id": id}
                                }).success(function () {
                                    $.ajax({
                                        url: "/organisations/table.action"
                                    }).done(function (data) {
                                        var container = $("#tableParent");
                                        container.empty();
                                        container.append(data);
                                        initDataTableAndButtons();
                                    });
                                    swal("<s:text name="deleted" />", "<s:text name="deletedMessage" />", "success");
                                });
                            });
                });
            }
        </script>
        <style>
            .dataTables_length label select {
                margin-bottom: 1em;
            }

            .dataTables_wrapper div {
                margin-left: 0px;
                margin-right: 0px;
                max-width: none;
            }

            .dataTables_filter label input {
                margin-top: 0.5em;
            }
        </style>
    </jsp:attribute>
    <jsp:body>
        <div>
            <div class="small-12 medium-3 large-2 columns">
                <h3 style="float: left; margin-bottom: 0px;margin-top: 0px;"><s:text
                        name="OrganisationIndexH1Header"/></h3>
                <a data-reveal-ajax="true" data-reveal-id="modal"
                   href="<s:url action="create" namespace="/organisations"/>"
                   class="button expand radius tiny"><s:text name="OrganisationIndexButtonCreateTop"/></a>
                <hr/>
            </div>
            <div class="small-12 medium-9 large-10 columns" id="tableParent">
                <s:include value="_indexTable.jsp"/>
            </div>
        </div>
        <div id="modal" class="reveal-modal" data-reveal></div>
    </jsp:body>
</t:layout>
