<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<t:layout>
    <jsp:attribute name="footer">
      <p id="copyright">Binary Cheese</p>
    </jsp:attribute>
    <jsp:attribute name="head">
        <title>Login - Pixi - Fair & Sensibel</title>
        <script type="text/javascript" src="../../assets/js/foundation/foundation.alert.js"></script>
        <style>
            html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, dl, dt, dd, ol, nav ul, nav li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td, article, aside, canvas, details, embed, figure, figcaption, footer, header, hgroup, menu, nav, output, ruby, section, summary, time, mark, audio, video {
                margin: 0;
                padding: 0;
                border: 0;
                font-size: 100%;
                font: inherit;
                vertical-align: baseline;
            }

            article, aside, details, figcaption, figure, footer, header, hgroup, menu, nav, section {
                display: block;
            }

            ol, ul {
                list-style: none;
                margin: 0px;
                padding: 0px;
            }

            blockquote, q {
                quotes: none;
            }

            blockquote:before, blockquote:after, q:before, q:after {
                content: '';
                content: none;
            }

            table {
                border-collapse: collapse;
                border-spacing: 0;
            }

            a {
                text-decoration: none;
            }

            .txt-rt {
                text-align: right;
            }

            .txt-lt {
                text-align: left;
            }

            .txt-center {
                text-align: center;
            }

            .float-rt {
                float: right;
            }

            .float-lt {
                float: left;
            }

            .clear {
                clear: both;
            }

            .pos-relative {
                position: relative;
            }

            .pos-absolute {
                position: absolute;
            }

            .vertical-base {
                vertical-align: baseline;
            }

            .vertical-top {
                vertical-align: top;
            }

            nav.vertical ul li {
                display: block;
            }

            nav.horizontal ul li {
                display: inline-block;
            }

            img {
                max-width: 100%;
            }

            body {
                background: url(../../assets/img/bg.jpg) no-repeat center fixed;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;
                position: relative;
                font-family: 'Open Sans', sans-serif;

            }

            .login-form {
                background: #fff;
                width: 32%;
                margin: 9% auto 4% auto;
                position: relative;
            }

            .head {
                position: absolute;
                top: -15%;
                left: 35%;
            }

            .head img {
                border-radius: 50%;
                -webkit-border-radius: 50%;
                -o-border-radius: 50%;
                -moz-border-radius: 50%;
                width: 9em;
                border: 6px solid rgba(221, 218, 215, 0.23);
                background-color: #ffffff;
            }

            form {
                padding: 24% 2.5em;
            }

            .text, input[type=password] {
                -webkit-box-shadow: none !important;
                -moz-box-shadow: none !important;
                box-shadow: none !important;
            }

            form li {
                border: 1px solid #B4B2B2;
                list-style: none;
                margin-bottom: 25px;
                width: 100%;
                height: 50px;
                background: none;
                border-radius: 0.3em;
                -webkit-border-radius: 0.3em;
                -o-border-radius: 0.3em;
                -moz-border-radius: 0.3em;

            }

            .icon {
                background: url(../../assets/img/icons.png) no-repeat 0px 0px;
                height: 30px;
                width: 30px;
                display: block;
                float: left;
                margin: 5px -11px 0px 0px
            }

            .user {
                background: url(../../assets/img/icons.png) no-repeat 12px 11px;

            }

            .lock {
                background: url(../../assets/img/icons.png) no-repeat -17px 11px;
            }

            .text, .password {
                padding-bottom: 0em !important;
                padding-top: 0 !important;
                margin-top: 8px !important;
                height: 30px !important;
            }

            input[type="text"], input[type="password"] {
                font-family: 'Open Sans', sans-serif;
                width: 90%;
                padding: 0.7em 2em 0.7em 1.7em;
                margin-top: 5px;
                margin-bottom: 0;
                color: #858282;
                font-size: 18px;
                outline: none;
                background: none;
                border: none;
                font-weight: 600;
            }

            input[type="text"]:focus, input[type="password"]:focus {
                box-shadow: none;
            }

            form li:hover {
                border: 1px solid #40A9DF;
                box-shadow: 0 0 1em #40A9DF;
                -webkit-box-shadow: 0 0 1em #40A9DF;
                -o-box-shadow: 0 0 1em #40A9DF;
                -moz-box-shadow: 0 0 1em #40A9DF;
            }

            input[type="submit"] {
                float: right;
                font-size: 18px;
                display: inline-block;
                font-weight: 600;
                color: #fff;
                transition: 0.1s all;
                -webkit-transition: 0.1s all;
                -moz-transition: 0.1s all;
                -o-transition: 0.1s all;
                cursor: pointer;
                outline: none;
                padding: 15px 10px;
                margin-top: 3px;
                font-family: 'Open Sans', sans-serif;
                width: 40%;
                border: none;
                border-radius: 0.3em;
                -webkit-border-radius: 0.3em;
                -o-border-radius: 0.3em;
                -moz-border-radius: 0.3em;
                background: #ffe49e;
                background: -moz-linear-gradient(top, #ffe49e 0%, #ffb700 3%, #ffad00 35%, #ff9000 97%, #e27f00 100%);
                background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #ffe49e), color-stop(3%, #ffb700), color-stop(35%, #ffad00), color-stop(97%, #ff9000), color-stop(100%, #e27f00));
                background: -webkit-linear-gradient(top, #ffe49e 0%, #ffb700 3%, #ffad00 35%, #ff9000 97%, #e27f00 100%);
                background: -o-linear-gradient(top, #ffe49e 0%, #ffb700 3%, #ffad00 35%, #ff9000 97%, #e27f00 100%);
                background: -ms-linear-gradient(top, #ffe49e 0%, #ffb700 3%, #ffad00 35%, #ff9000 97%, #e27f00 100%);
                background: linear-gradient(to bottom, #ffe49e 0%, #ffb700 3%, #ffad00 35%, #ff9000 97%, #e27f00 100%);
                filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffe49e', endColorstr='#e27f00', GradientType=0);
            }

            input[type="submit"]:hover {
                background: #e27f00;
                background: -moz-linear-gradient(top, #e27f00 0%, #ff9000 3%, #ffad00 65%, #ffb700 97%, #ffe49e 100%);
                background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #e27f00), color-stop(3%, #ff9000), color-stop(65%, #ffad00), color-stop(97%, #ffb700), color-stop(100%, #ffe49e));
                background: -webkit-linear-gradient(top, #e27f00 0%, #ff9000 3%, #ffad00 65%, #ffb700 97%, #ffe49e 100%);
                background: -o-linear-gradient(top, #e27f00 0%, #ff9000 3%, #ffad00 65%, #ffb700 97%, #ffe49e 100%);
                background: -ms-linear-gradient(top, #e27f00 0%, #ff9000 3%, #ffad00 65%, #ffb700 97%, #ffe49e 100%);
                background: linear-gradient(to bottom, #e27f00 0%, #ff9000 3%, #ffad00 65%, #ffb700 97%, #ffe49e 100%);
                filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#e27f00', endColorstr='#ffe49e', GradientType=0);
            }

            .p-container {
                margin-top: 2em;
            }

            .p-container .checkbox input {
                position: absolute;
                left: -9999px;
            }

            .p-container.checkbox i {
                border-color: #fff;
                transition: border-color 0.3s;
                -o-transition: border-color 0.3s;
                -ms-transition: border-color 0.3s;
                -moz-transition: border-color 0.3s;
                -webkit-transition: border-color 0.3s;
            }

            .p-container.checkbox i:hover {
                border-color: red;
            }

            .p-container i:before {
                background-color: #2da5da;
            }

            .p-container .rating label {
                color: #ccc;
                transition: color 0.3s;
                -o-transition: color 0.3s;
                -ms-transition: color 0.3s;
                -moz-transition: color 0.3s;
                -webkit-transition: color 0.3s;
            }


            .p-container .checkbox input + i:after {
                position: absolute;
                opacity: 0;
                transition: opacity 0.1s;
                -o-transition: opacity 0.1s;
                -ms-transition: opacity 0.1s;
                -moz-transition: opacity 0.1s;
                -webkit-transition: opacity 0.1s;
            }

            .p-container .checkbox input + i:after {
                content: url(../../assets/img/tick.png);
                top: -15px;
                width: 15px;
                height: 15px;
            }

            .p-container .checkbox {
                float: left;
                margin-right: 30px;
            }

            .p-container .checkbox {
                padding-left: 40px;
                font-size: 16px;
                line-height: 60px;
                color: #858282;
                cursor: pointer;
                font-family: 'Open Sans', sans-serif;
                font-weight: 600;
            }

            .p-container .checkbox {
                position: relative;
                display: block;
            }

            .p-container .checkbox i {
                position: absolute;
                top: 18px;
                left: 5px;
                display: block;
                width: 22px;
                height: 22px;
                outline: none;
                border: 1px solid #999;
                background: #fff;
                border-radius: 4px;
                -webkit-border-radius: 4px;
                -moz-border-radius: 4px;
                -o-border-radius: 4px;

            }

            .p-container .checkbox input + i:after {
                position: absolute;
                opacity: 0;
                transition: opacity 0.1s;
                -o-transition: opacity 0.1s;
                -ms-transition: opacity 0.1s;
                -moz-transition: opacity 0.1s;
                -webkit-transition: opacity 0.1s;
            }

            .p-container .checkbox input + i:after {
                color: #2da5da;
            }

            .p-container .checkbox input:checked + i,
            .p-container . input:checked + i {
                border-color: #2da5da;
            }

            .p-container .rating input:checked ~ label {
                color: #2da5da;
            }

            .p-container .checkbox input:checked + i:after {
                opacity: 1;
            }

            /*-----start-responsive-design------*/
            @media (max-width: 1024px) {
                .login-form {
                    margin: 8% auto 0;
                    width: 45%;
                }

                input[type="text"], input[type="password"] {
                    width: 90%;
                }
            }

            @media (max-width: 768px) {
                .login-form {
                    margin: 12% auto 0;
                    width: 57%;
                }
            }

            @media (max-width: 640px) {
                .login-form {
                    margin: 13% auto 0;
                    width: 70%;
                }
            }

            @media (max-width: 480px) {
                .login-form {
                    margin: 20% auto 0;
                    width: 90%;
                }

                .head img {
                    width: 8em;
                }
            }

            @media (max-width: 320px) {
                .login-form {
                    margin: 20% auto 0;
                    width: 95%;
                }

                form {
                    padding: 19% 1.5em;
                }

                form li {
                    margin-top: 10px;
                }

                input[type="text"], input[type="password"] {
                    font-size: 15px;
                    padding: 0.5em 1em 0.5em 1em;
                }

                input[type="submit"] {
                    float: none;
                    font-size: 15px;
                    padding: 10px 10px;
                    width: 40%;
                    margin-left: 28%;
                }

                .head {
                    top: -12%;
                    left: 35.5%;
                }

                .head img {
                    width: 69%;
                    width: 7em;
                }

                .user {
                    background: url(../../assets/img/icons.png) no-repeat 12px 4px;
                }

                .lock {
                    background: url(../../assets/img/icons.png) no-repeat -20px 3px;
                }

                .p-container .checkbox {
                    float: none;
                    text-align: center;
                }

                .p-container {
                    margin-top: 0;
                }

                .icon {
                    margin: 6px -11px 0px 0px;
                }

                .p-container .checkbox i {
                    position: absolute;
                    top: 18px;
                    left: 43px;
                }
            }
        </style>
    </jsp:attribute>
    <jsp:body>
        <div class="login-form">
            <div class="head">
                <img src="../../assets/img/pixi-logo.png" alt="">
            </div>
            <form method="post" action="/j_spring_security_check">
                <sec:csrfInput></sec:csrfInput>
                <ul>
                    <s:if test="%{#parameters.error != null}">
                        <s:iterator value="%{#session}">
                            <s:if test="key == 'SPRING_SECURITY_LAST_EXCEPTION'">
                                <s:if test="%{ value instanceof org.springframework.security.authentication.DisabledException }">
                                    <div data-alert class="alert-box alert radius">
                                        Der Benutzer ist deaktiviert, bitte den Admin deinen Account zu aktivieren
                                        <a href="#" class="close">&times;</a>
                                    </div>
                                </s:if>
                                <s:if test="%{ value instanceof org.springframework.security.authentication.BadCredentialsException }">
                                    <div data-alert class="alert-box alert radius">
                                        Der eingegebene Benutzername oder das Passwort ist falsch
                                        <a href="#" class="close">&times;</a>
                                    </div>
                                </s:if>
                            </s:if>
                        </s:iterator>
                    </s:if>
                    <li>
                        <span class="icon user"></span>
                        <input tabindex="1" type="text" class="text" value="USERNAME" name="j_username"
                               onfocus="if (this.value == 'USERNAME') {this.value = ''}"
                               autocomplete="off" onblur="if (this.value == '') {this.value = 'USERNAME'}">
                    </li>
                    <li>
                        <span class="icon lock"></span>
                        <input tabindex="2" type="password" value="Password" class="password" name="j_password"
                               onfocus="if(this.value == 'Password'){this.value =''}"
                               autocomplete="off" onblur="if (this.value == '') {this.value = 'Password'}">
                    </li>
                </ul>
                <div class="p-container">
                    <label class="checkbox">
                        <input type="checkbox" name="_spring_security_remember_me" checked="">
                        <i></i>Angemeldet bleiben
                    </label>
                    <input type="submit" value="Anmelden">

                    <div class="clear"></div>
                </div>
            </form>
        </div>
    </jsp:body>
</t:layout>

