<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<t:layout>
    <jsp:attribute name="footer">
      <p id="copyright">Binary Cheese</p>
    </jsp:attribute>
    <jsp:attribute name="head">
            <title><s:text name="LabelHomeTitle"/></title>
            <style>
                td {
                    overflow: auto;
                }
            </style>
        <script>
            <s:if test="contactsEntity.usersesById.size > 0">
            $(document).ready(function () {
                $("#deleteForm").submit(function (e) {
                    e.preventDefault();
                    swal({
                        title: "<s:text name="contactDeleteTitle"></s:text>",
                        text: "<s:text name="contactDeleteText"></s:text>",
                        type: "warning",
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "<s:text name="contactDeleteConfirmMessage"></s:text>",
                        closeOnConfirm: true
                    });
                });
            });
            </s:if>
        </script>
    </jsp:attribute>
    <jsp:body>
        <form action="<s:url action='delete' namespace='/contacts'><s:param name="contactsEntity.id"><s:property value="contactsEntity.Id"/></s:param></s:url>"
              method="get" id="deleteForm">

            <div class="row" style="max-width:120rem;">
                <div class="columns small-6">
                    <table style="margin-top: 50px; table-layout: fixed; width: 85%; " align="center" class="table">
                        <th colspan="2" style="text-align: center;  border-bottom:1pt solid black;">
                            Kontaktinformationen
                        </th>
                        <tr>
                            <td><s:text name="contactsEntity.title"/></td>
                            <td><s:property value="contactsEntity.title"/></td>
                        </tr>
                        <tr>
                            <td><s:text name="contactsEntity.surname"/></td>
                            <td><s:property value="contactsEntity.surname"/></td>
                        </tr>
                        <tr>
                            <td><s:text name="contactsEntity.name"/></td>
                            <td><s:property value="contactsEntity.name"/></td>
                        </tr>
                        <tr>
                            <td><s:text name="contactsEntity.email"/></td>
                            <td><s:property value="contactsEntity.importantMails.value"/></td>
                        </tr>
                        <tr>
                            <td><s:text name="contactsEntity.tel"/></td>
                            <td><s:property value="contactsEntity.importantTelnumbers.value"/></td>
                        </tr>
                        <tr>
                            <td><s:text name="contactsEntity.languages"/></td>
                            <td>
                                <s:iterator value="contactsEntity.languages" status="status">
                                    <s:if test="#status.last == false">
                                        <s:property value="germanName"/><br/>
                                    </s:if>
                                    <s:if test="#status.last == true">
                                        <s:property value="germanName"/>
                                    </s:if>
                                </s:iterator>
                            </td>
                        </tr>
                        <tr>
                            <td><s:text name="contactsEntity.academicDegreeBefore"/></td>
                            <td><s:property value="contactsEntity.academicDegreeBefore"/></td>
                        </tr>
                        <tr>
                            <td><s:text name="contactsEntity.academicDegreeAfter"/></td>
                            <td><s:property value="contactsEntity.academicDegreeAfter"/></td>
                        </tr>
                        <tr>
                            <td><s:text name="contactsEntity.info"/></td>
                            <td><s:property value="contactsEntity.info"/></td>
                        </tr>
                        <tr>
                            <td><s:text name="contactsEntity.newsletter"/></td>
                            <s:if test="contactsEntity.newsletter==true">
                                <td><s:text name="contactsEntity.newsletterYes"/></td>
                            </s:if>
                            <s:else>
                                <td><s:text name="contactsEntity.newsletterNo"/></td>
                            </s:else>
                        </tr>
                        <tr>
                            <td><s:text name="contactsEntity.email-private"/></td>
                            <td>
                                <s:iterator value="contactsEntity.notImportantMails" status="status">
                                    <s:if test="#status.last == false">
                                        <s:property value="value"/><br/>
                                    </s:if>
                                    <s:if test="#status.last == true">
                                        <s:property value="value"/>
                                    </s:if>
                                </s:iterator>
                            </td>
                        </tr>
                        <tr>
                            <td><s:text name="contactsEntity.tel-private"/></td>
                            <td>
                                <s:iterator value="contactsEntity.NotImportantTelnumbers" status="status">
                                    <s:if test="#status.last == false">
                                        <s:property value="value"/><br/>
                                    </s:if>
                                    <s:if test="#status.last == true">
                                        <s:property value="value"/>
                                    </s:if>
                                </s:iterator>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="columns small-6">
                    <table style="margin-top: 50px; width: 85% " align="center" class="table">
                        <th colspan="2" style="text-align: center;  border-bottom:1pt solid black;">Kontaktadresse</th>
                        <tr>
                            <td><s:text name="contactsEntity.address"/></td>
                            <td><s:property value="contactsEntity.address"/></td>
                        </tr>
                        <tr>
                            <td><s:text name="contactsEntity.plz"/></td>
                            <td><s:property value="contactsEntity.postleitzahlByPlz.plz"/></td>
                        </tr>
                        <tr>
                            <td><s:text name="contactsEntity.state"/></td>
                            <td><s:property value="contactsEntity.state"/></td>
                        </tr>
                        <tr>
                            <td><s:text name="contactsEntity.country"/></td>
                            <td><s:property value="contactsEntity.countriesByCountriesId.germanName"/></td>
                        </tr>
                        <tr>
                            <td><s:text name="contactsEntity.town"/></td>
                            <td><s:property value="contactsEntity.postleitzahlByPlz.name"/></td>
                        </tr>
                        <tr>
                            <td><s:text name="contactsEntity.createDate"/></td>
                            <td><s:date name="contactsEntity.createDate" format="dd.MM.yyyy HH:mm"/></td>
                        </tr>
                        <tr>
                            <td><s:text name="contactsEntity.updateDate"/></td>
                            <td><s:date name="contactsEntity.updateDate" format="dd.MM.yyyy HH:mm"/></td>
                        </tr>
                        <tr>
                            <td><s:text name="contactsEntity.homepage"/></td>
                            <td><s:property value="contactsEntity.hompage"/></td>
                        </tr>
                        <tr>
                            <td><s:text name="contactsEntity.Organisation"/></td>
                            <td><s:property value="contactsEntity.organisationsByOrganisationsId.name"/></td>
                        </tr>
                        <tr>
                            <td><s:text name="contactsEntity.additionalinformations"/></td>
                            <td>
                                <s:iterator value="contactsEntity.additionalinformationsById" status="status">
                                    <s:if test="#status.last == false">
                                        <s:property value="name"/>: <s:property value="value"/><br/>
                                    </s:if>
                                    <s:if test="#status.last == true">
                                        <s:property value="name"/>: <s:property value="value"/>
                                    </s:if>
                                </s:iterator>
                            </td>
                        </tr>
                            <%--
                                                    <tr>
                                                        <td>
                                                            <input type="button" id="deleteButton"
                                                                   value="<s:text name="LabelDeleteContact"></s:text>" class="button left">
                                                        </td>
                                                        <td></td>
                                                    </tr>
                            --%>
                        <s:text id="asdf" name="LabelDeleteContact" var="asdf"/>
                        <s:submit value="%{asdf}" cssClass="button left" id="deleteButton"/>
                    </table>
                </div>
            </div>
                <%--Hidden Fields--%>
            <s:hidden name="contactsEntity.Id"/>

        </form>
    </jsp:body>
</t:layout>
