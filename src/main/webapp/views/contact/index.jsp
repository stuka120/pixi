<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:layout>
    <jsp:attribute name="footer">
      <p id="copyright">Binary Cheese</p>
    </jsp:attribute>
    <jsp:attribute name="head">
        <title><s:text name="LabelHomeTitle"/></title>
        <link rel="stylesheet" href="../../assets/css/dataTables.foundation.css">
        <link rel="stylesheet" href="../../assets/css/jquery.dataTables.css">
        <link rel="stylesheet" href="../../assets/css/multi-select.css"/>
        <script type="text/javascript" src="../../assets/js/foundation/foundation.reveal.js"></script>
        <script type="text/javascript" src="../../assets/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="../../assets/js/struts/jquery.struts.js"></script>
        <script type="text/javascript" src="../../assets/js/jquery.dataTables.js"></script>
        <script type="text/javascript" src="../../assets/js/dataTables.foundation.js"></script>
        <script type="text/javascript" src="../../assets/js/foundation.offcanvas.js"></script>
        <script type="text/javascript" src="../../assets/js/jquery.multi-select.js"></script>

        <script>
            var table;
            $(document).ready(function () {
                table = $("#contactTable")
                        .on('processing.dt', function (e, settings, processing) {
                            if (processing) {
                                showOverlay();
                            } else {
                                hideOverlay();
                            }
                        })
                        .DataTable({
                            serverSide: true,
                            processing: true,
                            ajax: {
                                url: '/contacts/DataTable',
                                type: 'GET'
                            },
                            "language": {
                                "url": "../../assets/js/dataTablesLang/<s:text name="dataTablesLang" />"
                            }
                        });
                $("#contactTable > tbody").on('click', "tr", function (event) {
                    if (event.target.nodeName == "TD") {
                        var form = $(this).find("form");
                        if (form.attr("disabled") == null) {
                            form.submit();
                        } else {
                            var contactsId = $(this).find("form input").val();
                            var name = $(this).children("td:nth-child(2)").text();
                            var surname = $(this).children("td:nth-child(1)").text();
                            $("#canvasList").append('<li class="exportEntry"><a href="#" style="float: left; width: 88%;" data-id="' + contactsId + '">' + contactsId + ' ' + name + ' ' + surname + '</a><a href="#" style="float: right; width: 12%" id="lol" class="exportRemove">X</a></li>');
                        }
                    }
                });
                $("#columnsSelect").multiSelect({
                    "cssClass": "myMultiSelect",
                    "selectableHeader": "<s:text name="available" />",
                    "selectionHeader": "<s:text name="chosen" />"
                });
                $("#exportForm").on('submit', function () {
                    $("#exportModal").foundation("reveal", "close");
                    $.each($(".exportEntry a:not(.exportRemove)"), function (index, value) {
                        $('<input />').attr('type', 'hidden')
                                .attr('name', "contactsToExport")
                                .attr('value', $(value).data('id'))
                                .appendTo('#exportForm');
                    });
                });
                $("#exportButton").on('click', function () {
                    if($("li.exportEntry").length > 0){
                        $("#exportModal").foundation('reveal', 'open');
                        $("input[name='contactsToExport']").remove();
                    }
                });
                $('#select-all').click(function () {
                    $('#columnsSelect').multiSelect('select_all');
                });
                $('#deselect-all').click(function () {
                    $('#columnsSelect').multiSelect('deselect_all');
                });
            });
        </script>
        <style>
            .myMultiSelect {
                margin-left: auto;
                margin-right: auto;
            }
        </style>
    </jsp:attribute>
    <jsp:body>


        <div class="off-canvas-wrap" data-offcanvas>
            <div class="inner-wrap" style="height: calc(100% - 45px);">


                <!-- Off Canvas Menu -->
                <aside class="right-off-canvas-menu"
                       style="position: fixed; margin-top: 45px; width: 28rem; padding-bottom: 7rem;">
                    <!-- whatever you want goes here -->
                    <div class="row" style="height: 100%; overflow-y: auto;">
                        <ul class="off-canvas-list" id="canvasList">
                            <li>
                                <label style="float: left; width: 12%;;" class="right-off-canvas-toggle"
                                       aria-expanded="true"> &gt; </label>
                                <label style="cursor:default; float: left; width: 76%">To Export</label>
                                <label style=" float: right; width: 12%;cursor: pointer;"
                                       id="removeAllExportEntries">X</label>
                            </li>
                        </ul>
                    </div>
                    <div class="row"
                         style="width: inherit; text-align: center; bottom: 0px; position: absolute; height: 5rem">
                        <div>
                            <a href="#" id="importShownContacts" class="button radius">Kontakte auswählen</a>
                            <a href="#" id="exportButton" class="button radius">Exportieren</a>
                        </div>
                    </div>

                </aside>

                <!-- main content goes here -->
                <p>
                    <div class="row" style="max-width: none">
                        <div class="small-12 medium-3 large-2 columns">
                            <h3><s:text name="LabelContactsIndex"/></h3>
                            <a href="<s:url action="create" namespace="/contacts"/>" style="margin-top: 10px"
                               class="radius button tiny expand"><s:text name="newContactLabel"/></a>
                            <hr/>
                            <a class="right-off-canvas-toggle radius button tiny expand"
                               href="#"
                               id="exportContacts">Export</a>
                            <a href="#" data-reveal-id="myModal" class="radius button tiny expand"><s:text
                                    name="ButtonFilterContacts"/></a>
                        </div>
                        <div class="small-12 medium-9 large-10 columns" style="margin-top: 4px;">
                            <s:include value="_indexTable.jsp"/>
                        </div>
                    </div>

                <div id="myModal" class="reveal-modal" data-reveal>
                    <s:include value="_filter.jsp"/>
                </div>
                </p>

                <!-- close the off-canvas menu -->
                <%--<a class="exit-off-canvas"></a>--%>

            </div>
        </div>

        <div id="exportModal" class="reveal-modal" aria-labelledby="modalTitle" data-reveal>
            <h2 id="modalTitle"><s:text name="chooseColumns"/></h2>
            <s:form id="exportForm" action="export.action" theme="simple">
                <sec:csrfInput/>
                <div class="row">
                    <div class="large-3 columns">
                        <a id='select-all' class="button tiny radius"><s:text name="all"/></a>
                    </div>
                    <div class="large-6 columns">
                        <select multiple="multiple" id="columnsSelect" name="selectedColumns"
                                style="margin-left: auto; margin-right: auto">
                            <option value="title:<s:text name="contactsEntity.title"/>" selected>
                                <s:text name="contactsEntity.title"/>
                            </option>
                            <option value="name:<s:text name="contactsEntity.name"/>" selected>
                                <s:text name="contactsEntity.name"/>
                            </option>
                            <option value="surname:<s:text name="contactsEntity.surname"/>" selected>
                                <s:text name="contactsEntity.surname"/>
                            </option>
                            <option value="academicDegreeBefore:<s:text name="contactsEntity.academicDegreeBefore"/>">
                                <s:text name="contactsEntity.academicDegreeBefore"/>
                            </option>
                            <option value="academicDegreeAfter:<s:text name="contactsEntity.academicDegreeAfter"/>">
                                <s:text name="contactsEntity.academicDegreeAfter"/>
                            </option>
                            <option value="address:<s:text name="contactsEntity.address"/>">
                                <s:text name="contactsEntity.address"/>
                            </option>
                            <option value="postleitzahlByPlz:<s:text name="contactsEntity.plz"/>">
                                <s:text name="contactsEntity.plz"/>
                            </option>
                            <option value="state:<s:text name="contactsEntity.state"/>">
                                <s:text name="contactsEntity.state"/>
                            </option>
                            <option value="createDate:<s:text name="contactsEntity.createDate"/>">
                                <s:text name="contactsEntity.createDate"/>
                            </option>
                            <option value="updateDate:<s:text name="contactsEntity.updateDate"/>">
                                <s:text name="contactsEntity.updateDate"/>
                            </option>
                            <option value="hompage:<s:text name="contactsEntity.homepage"/>">
                                <s:text name="contactsEntity.homepage"/>
                            </option>
                            <option value="info:<s:text name="contactsEntity.info"/>">
                                <s:text name="contactsEntity.info"/>
                            </option>
                            <option value="organisationsByOrganisationsId:<s:text name="contactsEntity.Organisation"/>">
                                <s:text name="contactsEntity.Organisation"/>
                            </option>
                            <option value="countriesByCountriesId:<s:text name="contactsEntity.country"/>">
                                <s:text name="contactsEntity.country"/>
                            </option>
                            <option value="newsletter:<s:text name="contactsEntity.newsletter"/>">
                                <s:text name="contactsEntity.newsletter"/>
                            </option>
                            <option value="emails:<s:text name="contactsEntity.email"/>">
                                <s:text name="contactsEntity.email"/>
                            </option>
                            <option value="telnumbers:<s:text name="contactsEntity.tel"/>">
                                <s:text name="contactsEntity.tel"/>
                            </option>
                            <option value="speaksesById:<s:text name="contactsEntity.languages"/>">
                                <s:text name="contactsEntity.languages"/>
                            </option>
                        </select>
                    </div>
                    <div class="large-3 columns">
                        <a id='deselect-all' class="button tiny radius"><s:text name="none"/></a>
                    </div>
                </div>

                <s:submit key="export" cssClass="button small radius" cssStyle="margin-top: 1em;"/>
            </s:form>
            <a class="close-reveal-modal">&#215;</a>
        </div>

        <script>
            $(document).foundation({
                offcanvas: {
                    // Sets method in which offcanvas opens.
                    // [ move | overlap_single | overlap ]
                    open_method: 'overlap',
                    // Should the menu close when a menu link is clicked?
                    // [ true | false ]
                    close_on_click: false
                }
            });
            $(document).on('open.fndtn.offcanvas', '[data-offcanvas]', function ($event) {
                var target = $($event.target).filter("div.off-canvas-wrap");
                if ($(target).length != 0) {
                    $("#exportContacts").attr("data-active", true);
                    var forms = $(".contactsId");
                    if (forms.length != 0) {
                        forms.each(function (index, value) {
                            var form = $(value);
                            form.removeAttr('action');
                            form.attr("disabled", true);
                        });
                    }
                }
            });

            $(document).on('close.fndtn.offcanvas', '[data-offcanvas]', function () {
                $("#exportContacts").attr("data-active", false);
                var forms = $(".contactsId");
                if (forms.length != 0) {
                    forms.each(function (index, value) {
                        var form = $(value);
                        form.attr('action', '/contacts/details');
                        form.removeAttr("disabled");
                    });
                }
            });

            $("#contactTable").on('draw.dt', function () {
                var asdf = $("#exportContacts").attr("data-active");
                if ($("#exportContacts").attr("data-active") == "true") {
                    var forms = $(".contactsId");
                    if (forms.length != 0) {
                        forms.each(function (index, value) {
                            var form = $(value);
                            form.removeAttr('action');
                            form.attr("disabled", true);
                        });
                    }
                }
            });

            $("#canvasList").on('click', 'a.exportRemove', function () {
                var asdf = $(this).parent();
                asdf.detach();
            })


            $("#importShownContacts").on('click', function () {
                var rows = $("#contactTable tr");
                rows.each(function (index, value) {
                    if ($(value).hasClass("odd") || $(value).hasClass("even")) {
                        var contactsId = $(value).find("form input").val()
                        var name = $(value).children("td:nth-child(2)").text();
                        var surname = $(value).children("td:nth-child(1)").text();
                        $("#canvasList").append('<li class="exportEntry"><a href="#" style="float: left; width: 88%;" data-id="' + contactsId + '">'+ contactsId + ' ' + name + ' ' + surname + '</a><a href="#" style="float: right; width: 12%" id="lol" class="exportRemove">X</a></li>');
                    }
                });
            });

            $("#removeAllExportEntries").on('click', function () {
                $(".exportEntry").each(function () {
                    $(this).detach();
                });
            });
        </script>
    </jsp:body>
</t:layout>
