<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<t:layout>
   <jsp:attribute name="footer">
      <p id="copyright">Binary Cheese</p>
    </jsp:attribute>
    <jsp:attribute name="head">
        <title><s:text name="LabelHomeTitle"/></title>
        <link rel="stylesheet" type="text/css" href="../../assets/css/select2-foundation5.css">
        <script type="text/javascript" src="../../assets/js/select2-3.5.2.js"></script>
        <style>
            .select2-search__field {
                box-shadow: none !important;
                padding: 0 !important;
                margin: 0 !important;
            }

            .select2-input {
                margin: 0 !important;
            }

            .select2-search-field {
                height: 2.2rem;
            }

            .select2-search-choice {
                margin-top: 2px !important;
            }

            .error {
                color: #D8000C;
                background-color: #FFBABA;
            }
        </style>
    </jsp:attribute>
    <jsp:body>
        <form method="get" action="<s:url action="create" namespace="/contacts"/>" onkeypress="return event.keyCode != 13;">
            <div class="row" style="max-width: 100rem !important;">
                <div class="columns small-12 medium-12 large-6">
                    <div class="row">
                        <h1><s:text name="createContactTitle"/></h1>

                        <h3><s:text name="createContactGeneralInformation"/></h3>
                    </div>
                    <div class="row collapse">
                        <s:fielderror fieldName="emptyTitle" cssClass="error"/>
                        <div class="small-3 columns">
                            <span class="prefix"><s:text name="contactsEntity.title"/></span>
                        </div>
                        <div class="small-9 columns end">
                            <s:hidden id="title"
                                      name="contactsEntity.title"
                                      cssStyle="margin-bottom: 16px; width: 100%" />
                        </div>
                    </div>
                    <div class="row collapse">
                        <div class="small-3 columns">
                            <span class="prefix"><s:text name="contactsEntity.academicDegreeBefore"/></span>
                        </div>
                        <div class="small-9 columns end">
                            <s:hidden name="contactsEntity.academicDegreeBefore"
                                      id="academicDegreeBefore"
                                      cssStyle="margin-bottom: 16px; width: 100%"/>
                        </div>
                    </div>
                    <div class="row collapse">
                        <s:fielderror fieldName="emptyName" cssClass="error"/>
                        <div class="small-3 columns">
                            <span class="prefix"><s:text name="contactsEntity.name"/></span>
                        </div>
                        <div class="small-9 columns end">
                            <s:textfield tabindex="3" name="contactsEntity.name" placeholder="%{getText('contactsEntity.name')}"/>
                        </div>
                    </div>
                    <div class="row collapse">
                        <s:fielderror fieldName="emptySurName" cssClass="error"/>
                        <div class="small-3 columns">
                            <span class="prefix"><s:text name="contactsEntity.surname"/></span>
                        </div>
                        <div class="small-9 columns end">
                            <s:textfield tabindex="4" name="contactsEntity.surname" placeholder="%{getText('contactsEntity.surname')}"/>
                        </div>
                    </div>
                    <div class="row collapse">
                        <div class="small-3 columns">
                            <span class="prefix"><s:text name="contactsEntity.academicDegreeAfter"/></span>
                        </div>
                        <div class="small-9 columns end">
                            <s:hidden name="contactsEntity.academicDegreeAfter"
                                      id="academicDegreeAfter"
                                      cssStyle="margin-bottom: 16px; width: 100%"/>
                        </div>
                    </div>
                    <div class="row">
                        <h3><s:text name="createContactAdditionalInformation"/></h3>
                    </div>

                    <div class="row collapse">
                        <div class="small-3 columns">
                            <span class="prefix"><s:text name="contactsEntity.languages"/></span>
                        </div>
                        <div class="small-9 columns end">
                            <s:select multiple="true"
                                      id="languagesList"
                                      list="languagesEntityList"
                                      name="selectedLanguages"
                                      listValue="germanName"
                                      listKey="id"
                                      cssStyle="margin-bottom: 16px; width: 100%"/>
                        </div>
                    </div>

                    <div class="row collapse">
                        <div class="small-3 columns">
                            <span class="prefix"><s:text name="contactsEntity.tel"/></span>
                        </div>
                        <div class="small-9 columns end">
                            <s:hidden id="telephone"
                                      name="selectedPhones"
                                      cssStyle="margin-bottom: 16px; width: 100%"/>
                        </div>
                    </div>

                    <div class="row collapse">
                        <div class="small-3 columns">
                            <span class="prefix"><s:text name="contactsEntity.tel-private"/></span>
                        </div>
                        <div class="small-9 columns end">
                            <s:hidden id="telephonePrivate"
                                      name="selectedPhonesPrivate"
                                      cssStyle="margin-bottom: 16px; width: 100%"/>
                        </div>
                    </div>

                    <div class="row collapse">
                        <s:fielderror fieldName="emailErr" cssClass="error"/>
                        <div class="small-3 columns">
                            <span class="prefix"><s:text name="contactsEntity.email"/></span>
                        </div>
                        <div class="small-9 columns end">
                            <s:hidden id="email"
                                      name="selectedEmails"
                                      cssStyle="margin-bottom: 16px; width: 100%"/>
                        </div>
                    </div>

                    <div class="row collapse">
                        <s:fielderror fieldName="emailErrP" cssClass="error"/>
                        <div class="small-3 columns">
                            <span class="prefix"><s:text name="contactsEntity.email-private"/></span>
                        </div>
                        <div class="small-9 columns end">
                            <s:hidden id="email-private"
                                      name="selectedEmailsPrivate"
                                      cssStyle="margin-bottom: 16px; width: 100%"/>
                        </div>
                    </div>

                    <div class="row collapse">
                        <div class="small-3 columns">
                            <span class="prefix"><s:text name="contactsEntity.homepage"/></span>
                        </div>
                        <div class="small-9 columns end">
                            <s:textfield tabindex="17" name="contactsEntity.hompage" placeholder="%{getText('contactsEntity.homepage')}"/>
                        </div>
                    </div>

                    <div class="row collapse">
                        <div class="small-3 columns">
                            <span class="prefix"><s:text name="contactsEntity.info"/></span>
                        </div>
                        <div class="small-9 columns end" style="margin-bottom: 16px;">
                            <s:textarea tabindex="18" name="contactsEntity.info"
                                        cssStyle="height: 150px;" placeholder="%{getText('contactsEntity.info')}"/>
                        </div>
                    </div>
                </div>
                <div class="columns small-12 medium-12 large-6" style="margin-top: 130px;">
                    <div class="row collapse">
                        <div class="small-3 columns">
                            <span class="prefix"><s:text name="contactsEntity.address"/></span>
                        </div>
                        <div class="small-9 columns end">
                            <s:textfield tabindex="6" name="contactsEntity.address" placeholder="%{getText('contactsEntity.address')}"/>
                        </div>
                    </div>
                    <div class="row collapse">
                        <div class="small-3 columns">
                            <span class="prefix"><s:text name="contactsEntity.plz-town"/></span>
                        </div>
                        <div class="small-9 columns end">
                            <div class="row">
                                <div class="columns small-6 collapse">
                                    <s:hidden name="contactsEntity.postleitzahlByPlz.plz"
                                              id="postleitzahl"
                                              cssStyle="margin-bottom: 16px; width: 100%"/>
                                </div>
                                <div class="columns small-6 collapse">
                                    <s:hidden name="contactsEntity.postleitzahlByPlz.name"
                                              id="ort"
                                              cssStyle="margin-bottom: 16px; width: 100%"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row collapse">
                        <div class="small-3 columns">
                            <span class="prefix"><s:text name="contactsEntity.state"/></span>
                        </div>
                        <div class="small-9 columns end">
                            <s:select list="#{'':'','W':'W','N':'N','O':'O','B':'B','St':'St','K':'K','Sa':'Sa','T':'T','V':'V'}"
                                      id="stateList"
                                      name="contactsEntity.state"
                                      cssStyle="margin-bottom: 16px; width: 100%"/>
                        </div>
                    </div>

                    <div class="row collapse">
                        <div class="small-3 columns">
                            <span class="prefix"><s:text name="contactsEntity.country"/></span>
                        </div>
                        <div class="small-9 columns end">
                            <s:select multiple="true"
                                      id="countriesList"
                                      list="countriesEntitiesList"
                                      name="selectedCountry"
                                      listValue="germanName"
                                      listKey="id"
                                      cssStyle="margin-bottom: 16px; width: 100%"/>
                        </div>
                    </div>

                    <div class="row collapse">
                        <div class="small-3 columns">
                            <span class="prefix"><s:text name="contactsEntity.Organisation"/></span>
                        </div>
                        <div class="small-9 columns end">
                            <s:select multiple="true"
                                      id="organisationsList"
                                      list="organisationsEntityList"
                                      name="selectedOrganisation"
                                      listValue="name" listKey="id"
                                      cssStyle="margin-bottom: 16px; width: 100%"/>
                        </div>
                    </div>

                    <div class="row collapse">
                        <div class="columns small-3">
                            <span class="prefix"><s:text name="contactsEntity.newsletter"/> </span>
                        </div>
                        <div class="small-9 columns end" style="margin-bottom: 26px; margin-top: 3px">
                            <div class="switch radius">
                                <input type="checkbox" name="selectedNewsletter" id="newsletterCheckbox" tabindex="-1">
                                <label for="newsletterCheckbox" style="margin-left: 3em"></label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <h3><s:text name="LabelCustomInfos"/></h3>
                    </div>

                    <div class="row collapse additionalInformationRow ">
                        <div class="small-3 columns">
                            <span class="prefix"><input name="addName" type="text" tabindex='18'></span>
                        </div>
                        <div class="small-7 columns">
                            <s:textfield name="addValue" cssClass="additionalInformation" tabindex="18"/>
                        </div>
                        <div class="small-2 columns end">
                            <a class="button radius postfix"
                               id="addNewAdditionalInformationField">+</a>
                        </div>
                    </div>

                    <div class="row">
                        <div class="columns small-12 collapse">
                            <s:text name="LabelCreateSubmit" var="LabelCreateSubmit"/>
                            <s:submit value="%{LabelCreateSubmit}" cssClass="button radius left" tabindex="19"/>
                        </div>
                    </div>
                </div>
            </div>


        </form>

        <script type="text/javascript">
            $(document).ready(function () {
                $('#title').select2({
                    tags: ["Herr", "Frau"],
                    tokenSeparators: [",", " "],
                    placeholder: "<s:text name="placeholder.title"/>",
                    maximumSelectionSize: 1
                });
                $('#academicDegreeBefore').select2({
                    placeholder: "<s:text name="placeholder.academicDegreeBefore"/>",
                    minimumInputLength: 1,
                    tokenSeparators: [" "],
                    separator: ",",
                    createSearchChoice:function(term, data) {
                        var vorhanden = $(data).filter(function() {
                            return this.id.toUpperCase().localeCompare(term.toUpperCase())===0;
                        });
                        if (vorhanden.length===0) {
                            return {id:term, text:term};
                        }
                    },
                    multiple: true,
                    ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
                        url: "/contacts/academicDegree",
                        dataType: 'json',
                        quietMillis: 250,
                        data: function (term, page) {
                            return {
                                academicDegreeQuery: term // search term
                            };
                        },
                        results: function (data, page) { // parse the results into the format expected by Select2.
                            // since we are using custom formatting functions we do not need to alter the remote JSON data
                            return { results: data };
                        },
                        cache: true
                    }
                });
                $('#academicDegreeAfter').select2({
                    placeholder: "<s:text name="placeholder.academicDegreeBefore"/>",
                    minimumInputLength: 1,
                    tokenSeparators: [" "],
                    separator: ",",
                    createSearchChoice:function(term, data) {
                        var vorhanden = $(data).filter(function() {
                            return this.id.toUpperCase().localeCompare(term.toUpperCase())===0;
                        });
                        if (vorhanden.length===0) {
                            return {id:term, text:term};
                        }
                    },
                    multiple: true,
                    ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
                        url: "/contacts/academicDegree",
                        dataType: 'json',
                        quietMillis: 250,
                        data: function (term, page) {
                            return {
                                academicDegreeQuery: term // search term
                            };
                        },
                        results: function (data, page) { // parse the results into the format expected by Select2.
                            // since we are using custom formatting functions we do not need to alter the remote JSON data
                            return { results: data };
                        },
                        cache: true
                    }
                });
                $("#languagesList").select2({
                    placeholder: "<s:text name="placeholder.LanguageList"/>"
                });
                $("#telephone").select2({
                    placeholder: "<s:text name="placeholder.telelephone"/>",
                    maximumSelectionSize: 1,
                    tags: [],
                    tokenSeparators: [",", " "]
                });
                $("#telephonePrivate").select2({
                    placeholder: "<s:text name="placeholder.telefone-Private"/>",
                    tags: [],
                    tokenSeparators: [",", " "]
                });
                $("#email").select2({
                    placeholder: "<s:text name="placeholder.Email"/>",
                    maximumSelectionSize: 1,
                    tags: [],
                    tokenSeparators: [",", " "]
                });
                $("#email-private").select2({
                    placeholder: "<s:text name="placeholder.Email-Private"/>",
                    tags: [],
                    tokenSeparators: [",", " "]
                });
                $("#postleitzahl").select2({
                    placeholder: "<s:text name="placeholder.plz"/>",
                    minimumInputLength: 1,
                    ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
                        url: "/contacts/ortByPlz",
                        dataType: 'json',
                        quietMillis: 250,
                        data: function (term, page) {
                            return {
                                postleitzahl: term // search term
                            };
                        },
                        results: function (data, page) { // parse the results into the format expected by Select2.
                            // since we are using custom formatting functions we do not need to alter the remote JSON data
                            return { results: data };
                        },
                        cache: true
                    },
                    createSearchChoice:function(term, data) {
                        var vorhanden = $(data).filter(function() {
                            return this.id.toUpperCase().localeCompare(term.toUpperCase())===0;
                        });
                        if (vorhanden.length===0) {
                            return {id:term, text:term};
                        }
                    }
                });
                $("#ort").select2({
                    placeholder: "<s:text name="placeholder.town"/>",
                    minimumInputLength: 1,
                    ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
                        url: "/contacts/ortByPlz",
                        dataType: 'json',
                        quietMillis: 250,
                        data: function (term, page) {
                            return {
                                ort: term // search term
                            };
                        },
                        results: function (data, page) { // parse the results into the format expected by Select2.
                            // since we are using custom formatting functions we do not need to alter the remote JSON data
                            return { results: data };
                        },
                        cache: true
                    },
                    createSearchChoice:function(term, data) {
                        var vorhanden = $(data).filter(function() {
                            return this.id.toUpperCase().localeCompare(term.toUpperCase())===0;
                        });
                        if (vorhanden.length===0) {
                            return {id: term, id2:term, text:term};
                        }
                    }
                });
                $("#postleitzahl").on('change', function(e) {
                    $("#postleitzahl").select2("data", { id: e.added.id, text: e.added.id });
                    if(e.added.ort != null)
                        $("#ort").select2("data", { id: e.added.ort, text: e.added.ort });
                    if(e.added.bundesland != null)
                        $("#stateList").select2("data", { id: e.added.bundesland, text: e.added.bundesland })
                });
                $("#ort").on('change', function(e) {
                    $("#ort").select2("data", { id: e.added.id2, text: e.added.id2 });
                    if(e.added.plz != null)
                        $("#postleitzahl").select2("data", { id: e.added.plz, text: e.added.plz });
                    if(e.added.bundesland != null)
                        $("#stateList").select2("data", { id: e.added.bundesland, text: e.added.bundesland })
                });
                $("#stateList").select2({
                    placeholder: "<s:text name="placeholder.state"/>",
                    allowClear: true
                });
                $("#organisationsList").select2({
                    placeholder: "<s:text name="contactsEntity.Organisation"/>",
                    maximumSelectionSize: 1,
                    allowClear: true
                });
                $("#countriesList").select2({
                    placeholder: "<s:text name="contactsEntity.country"/>",
                    maximumSelectionSize: 1
                });
                $("#addNewAdditionalInformationField").click(function () {
                    $(".additionalInformationRow").last().after(insertNewDiv());
                });
                $("#s2id_title input").prop('tabindex',1);
                $("#s2id_academicDegreeBefore input").prop('tabindex',2);
                $("#s2id_academicDegreeAfter input").prop('tabindex',5);
                $("#s2id_postleitzahl input").prop('tabindex',7);
                $("#s2id_ort input").prop('tabindex',8);
                $("#s2id_stateList input").prop('tabindex',9);
                $("#s2id_countriesList input").prop('tabindex',10);
                $("#s2id_organisationsList input").prop('tabindex',11);
                $("#s2id_languagesList input").prop('tabindex',12);
                $("#s2id_telephone input").prop('tabindex',13);
                $("#s2id_telephonePrivate input").prop('tabindex',14);
                $("#s2id_email input").prop('tabindex',15);
                $("#s2id_email-private input").prop('tabindex',16);
                function insertNewDiv() {
                    return "<div class='row collapse additionalInformationRow '>" +
                            "       <div class='small-3 columns'>" +
                            "<span class='prefix'><input tabindex='18' type='text' name='addName' value id='addName'></span>" +
                            " </div>" +
                            " <div class='small-9 columns'>" +
                            " <input tabindex='18' type='text' name='addValue' value id='addValue' class='additionalInformation'>" +
                            " </div>" +
                            "  <div class='small-2 columns end'>" +
                            " </div>" +
                            " </div>";
                };
            });
        </script>
    </jsp:body>
</t:layout>
