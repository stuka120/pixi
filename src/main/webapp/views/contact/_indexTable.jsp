<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<table id="contactTable" class="table responsive display dataTable no-footer" align="center">
    <thead>
    <tr>
        <th><s:text name="contactsEntity.surname"/></th>
        <th><s:text name="contactsEntity.name"/></th>
        <th><s:text name="contactsEntity.email"/></th>
        <th><s:text name="contactsEntity.Organisation"/></th>
        <th><s:text name="contactsEntity.tel"/></th>
        <th></th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <s:iterator value="contactsEntityList" status="contactEntity">
        <tr>
            <td>
                <s:property value="surname"/>asdf
                <s:form theme="simple" action="details.action">
                    <s:hidden key="Id" name="selectedContact"/>
                    <sec:csrfInput></sec:csrfInput>
                </s:form>
            </td>
            <td><s:property value="name"/></td>
            <td>
                <s:iterator value="Emails" status="status">
                    <s:if test="#status.last == false">
                        <s:property value="value"/><br/>
                    </s:if>
                    <s:if test="#status.last == true">
                        <s:property value="value"/>
                    </s:if>
                </s:iterator>
            </td>
                <%--<td><s:date name="createDate" format="dd.MM.yyyy"/></td>--%>
                <%--<td><s:date name="updateDate" format="dd.MM.yyyy"/></td>--%>
            <td><s:property value="organisationsByOrganisationsId.name"/></td>
            <td>
                <s:iterator value="TelNr" status="status">
                    <s:if test="#status.last == false">
                        <s:property value="value"/><br/>
                    </s:if>
                    <s:if test="#status.last == true">
                        <s:property value="value"/>
                    </s:if>
                </s:iterator>
            </td>
            <td>
                <a href="<s:url action='edit' namespace='/contacts'><s:param name="selectedContact"><s:property value="Id"/></s:param></s:url>"><s:text
                        name="LabelEditContact"/></a>
            </td>
            <td>
                <a href="<s:url action='delete' namespace='/contacts'><s:param name="selectedContact"><s:property value="Id"/></s:param></s:url>"><s:text
                        name="LabelDeleteContact"/></a>
            </td>
        </tr>
    </s:iterator>
    </tbody>

</table>