<%@ taglib prefix="s" uri="/struts-tags" %>
<h1 align="center">Filter verwalten</h1>

<div id="filterSummery">
    <div class="row">
        <div class="columns small-1">
            <input type="button" value="X" name="removeRow()" onclick="removeRow(event)"/>
        </div>
        <div class="columns small-3 collapse">
            <select onchange="changeSelector(event)" class="felder" name="">
                <option data-datatype="int" value="id"><s:text name="contactsEntity.Id"/></option>
                <option data-datatype="string" value="name"><s:text name="contactsEntity.name"/></option>
                <option data-datatype="string" value="surname"><s:text name="contactsEntity.surname"/></option>
                <option data-datatype="string" value="title"><s:text name="contactsEntity.title"/></option>
                <option data-datatype="string" value="academicDegreeBefore"><s:text name="contactsEntity.academicDegreeBefore"/></option>
                <option data-datatype="string" value="academicDegreeAfter"><s:text name="contactsEntity.academicDegreeAfter"/></option>
                <option data-datatype="int" value="plz.plz"><s:text name="contactsEntity.plz"/></option>
                <option data-datatype="string" value="state"><s:text name="contactsEntity.state"/></option>
                <option data-datatype="string" value="organisation"><s:text name="contactsEntity.Organisation"/></option>
                <option data-datatype="boolean" value="newsletter"><s:text name="contactsEntity.newsletter"/></option>
                <option data-datatype="date" value="createDate"><s:text name="contactsEntity.createDate"/></option>
                <option data-datatype="date" value="updateDate"><s:text name="contactsEntity.updateDate"/></option>
                <option data-datatype="string" value="hompage"><s:text name="contactsEntity.homepage"/></option>
                <option data-datatype="string" value="info"><s:text name="contactsEntity.info"/></option>
                <option data-datatype="string" value="mail"><s:text name="contactsEntity.email"/></option>
                <option data-datatype="string" value="tel"><s:text name="contactsEntity.tel"/></option>
                <option data-datatype="string" value="country"><s:text name="contactsEntity.country"/></option>
            </select>
        </div>
        <div class="columns small-4 collapse">
            <select class="operator" id="idselector" name="">
                <option value="is"><s:text name="operator.is"/></option>
                <option value="isNot"><s:text name="operator.isNot"/></option>
                <option value="smaller"><s:text name="operator.smaller"/></option>
                <option value="bigger"><s:text name="operator.bigger"/></option>
            </select>
            <select class="operator" id="stringselector" name="" hidden>
                <option value="is"><s:text name="operator.is"/></option>
                <option value="isNot"><s:text name="operator.isNot"/></option>
                <option value="like"><s:text name="operator.like"/></option>
                <option value="notLike"><s:text name="notLike"/></option>
                <option value="startsWith"><s:text name="operator.startsWith"/></option>
                <option value="endsWith"><s:text name="operator.endsWith"/></option>
            </select>
            <select class="operator" id="dateselector" name="" hidden>
                <option value="smaller"><s:text name="operator.before"/></option>
                <option value="bigger"><s:text name="operator.after"/></option>
            </select>
            <select class="operator" id="booleanselector" name="" hidden>
                <option value="is"><s:text name="operator.is"/></option>
                <option value="isNot"><s:text name="operator.isNot"/></option>
            </select>
        </div>
        <div class="columns small-4 collapse end">
            <s:textfield value="1" cssClass="werte"/>
            <s:hidden value="10.10.2014" cssClass="hasDatepicker cal"/>
            <select name="" hidden style="margin-bottom: 16px!important;">
                <option value="true"><s:text name="contactsEntity.newsletterYes"/></option>
                <option value="false"><s:text name="contactsEntity.newsletterNo"/></option>
            </select>
        </div>
    </div>
</div>

<div class="row">
    <div class="columns small-6 left end collapse">
        <a href="" onclick="addFilter(event)" class="button radius">Hinzuf&uuml;gen</a>
    </div>
    <div class="columns small-6 end collapse" style="text-align: right">
        <a href="" onclick="applyFilter(event, false)" class="button radius">&Uuml;bernehmen</a>
    </div>
</div>

<a class="close-reveal-modal">&#215;</a>

<script type="text/javascript">

    var searchQuery = "";
    var andOrArray = "";

    function changeSelector(event) {

        var datatype = $(event.target).find(":selected").data("datatype");
        if (datatype == "string") {
            $(event.target).parent().next().children().first().next().show(); //string selector --> show it
            $(event.target).parent().next().children().first().hide(); //idselector --> hide it
            $(event.target).parent().next().children().first().next().next().hide(); //date selector --> hide it
            $(event.target).parent().next().children().first().next().next().next().hide(); //boolean selector --> hide it
            $(event.target).parent().next().next().children().first().next().attr("type", "hidden"); //date value --> hide it
            $(event.target).parent().next().next().children().first().next().removeClass("werte"); //date value --> remove werte class
            $(event.target).parent().next().next().children().first().next().next().hide(); //Boolean value --> hide it
            $(event.target).parent().next().next().children().first().next().next().removeClass("werte"); //boolean value --> remove werte class
            $(event.target).parent().next().next().children().first().show(); //text value --> show it
            $(event.target).parent().next().next().children().first().addClass("werte"); //text value --> add werte class

        } else if (datatype == "int") {
            $(event.target).parent().next().children().first().next().hide(); //string selector --> hide it
            $(event.target).parent().next().children().first().show(); //idselector --> show it
            $(event.target).parent().next().children().first().next().next().hide(); //date selector --> hide it
            $(event.target).parent().next().children().first().next().next().next().hide(); //boolean selector --> hide it
            $(event.target).parent().next().next().children().first().next().attr("type", "hidden"); //date value --> hide it
            $(event.target).parent().next().next().children().first().next().removeClass("werte"); //date value --> remove werte class
            $(event.target).parent().next().next().children().first().next().next().hide(); //Boolean value --> hide it
            $(event.target).parent().next().next().children().first().next().next().removeClass("werte"); //boolean value --> remove werte class
            $(event.target).parent().next().next().children().first().show(); //text value --> show it
            $(event.target).parent().next().next().children().first().addClass("werte"); //text value --> add werte class
        }else if(datatype == "boolean") {
            $(event.target).parent().next().children().first().next().hide(); //string selector --> hide it
            $(event.target).parent().next().children().first().hide(); //idselector --> show it
            $(event.target).parent().next().children().first().next().next().hide(); //date selector --> hide it
            $(event.target).parent().next().children().first().next().next().next().show(); //boolean selector --> show it
            $(event.target).parent().next().next().children().first().next().attr("type", "hidden"); //date value --> hide it
            $(event.target).parent().next().next().children().first().next().removeClass("werte"); //date value --> remove werte class
            $(event.target).parent().next().next().children().first().next().next().show(); //Boolean value --> show it
            $(event.target).parent().next().next().children().first().next().next().addClass("werte"); //boolean value --> remove werte class
            $(event.target).parent().next().next().children().first().hide(); //text value --> show it
            $(event.target).parent().next().next().children().first().removeClass("werte"); //text value --> add werte class
        } else {
            $(event.target).parent().next().children().first().next().hide(); //string selector --> hide it
            $(event.target).parent().next().children().first().hide(); //idselector --> hide it
            $(event.target).parent().next().children().first().next().next().show(); //date selector --> show it
            $(event.target).parent().next().children().first().next().next().next().hide(); //boolean selector --> hide it
            $(event.target).parent().next().next().children().first().next().attr("type", "text"); //date value --> show it
            $(event.target).parent().next().next().children().first().next().addClass("werte"); //date value --> add werte class
            $(event.target).parent().next().next().children().first().next().next().hide(); //Boolean value --> hide it
            $(event.target).parent().next().next().children().first().next().next().removeClass("werte"); //boolean value --> remove werte class
            $(event.target).parent().next().next().children().first().hide(); //text value --> hide it
            $(event.target).parent().next().next().children().first().removeClass("werte"); //text value --> remove werte class
        }

        $.each($('.cal'), function (index, value) {
            $(value).datepicker({
                dateFormat: "dd.mm.yy"
            });
        });
    }

    function addFilter(event) {
        event.preventDefault();
        var filterRow =
                '<div class="row">' +
                '<div class="columns small-1">' +
                '<input type="button" value="X" name="removeRow" onclick="removeRow(event)" class="removeRow"/>' +
                '</div>' +
                '<div class="columns small-2 collapse">'+
                '<select class="booleanAndOr" name="">'+
                '<option value="and">UND</option>'+
                '<option value="or">ODER</option>'+
                '</select>'+
                '</div>'+
                '<div class="columns small-3 collapse">' +
                '<select onchange="changeSelector(event)" class="felder" name="">' +
                '<option data-datatype="int" value="id"><s:text name="contactsEntity.Id"/></option>' +
                '<option data-datatype="string" value="name"><s:text name="contactsEntity.name"/></option>' +
                '<option data-datatype="string" value="surname"><s:text name="contactsEntity.surname"/></option>' +
                '<option data-datatype="string" value="title"><s:text name="contactsEntity.title"/></option>' +
                '<option data-datatype="string" value="academicDegreeBefore"><s:text name="contactsEntity.academicDegreeBefore"/></option>' +
                '<option data-datatype="string" value="academicDegreeAfter"><s:text name="contactsEntity.academicDegreeAfter"/></option>' +
                '<option data-datatype="int" value="plz.plz"><s:text name="contactsEntity.plz"/></option>' +
                '<option data-datatype="string" value="state"><s:text name="contactsEntity.state"/></option>' +
                '<option data-datatype="string" value="organisation"><s:text name="contactsEntity.Organisation"/></option>' +
                '<option data-datatype="boolean" value="newsletter"><s:text name="contactsEntity.newsletter"/></option>' +
                '<option data-datatype="date" value="createDate"><s:text name="contactsEntity.createDate"/></option>' +
                '<option data-datatype="date" value="updateDate"><s:text name="contactsEntity.updateDate"/></option>' +
                '<option data-datatype="string" value="hompage"><s:text name="contactsEntity.homepage"/></option>' +
                '<option data-datatype="string" value="info"><s:text name="contactsEntity.info"/></option>' +
                '<option data-datatype="string" value="mail"><s:text name="contactsEntity.email"/></option>' +
                '<option data-datatype="string" value="tel"><s:text name="contactsEntity.tel"/></option>' +
                '<option data-datatype="string" value="country"><s:text name="contactsEntity.country"/></option>' +
                '</select>' +
                '</div>' +
                '<div class="columns small-2 collapse">' +
                '<select class="operator" id="idselector" name="">' +
                '<option value="is"><s:text name="operator.is"/></option>' +
                '<option value="isNot"><s:text name="operator.isNot"/></option>' +
                '<option value="smaller"><s:text name="operator.smaller"/></option>' +
                '<option value="bigger"><s:text name="operator.bigger"/></option>' +
                '</select>' +
                '<select class="operator" id="stringselector" name="" hidden="">' +
                '<option value="is"><s:text name="operator.is"/></option>' +
                '<option value="isNot"><s:text name="operator.isNot"/></option>' +
                '<option value="like"><s:text name="operator.like"/></option>' +
                '<option value="notLike"><s:text name="notLike"/></option>' +
                '<option value="startsWith"><s:text name="operator.startsWith"/></option>' +
                '<option value="endsWith"><s:text name="operator.endsWith"/></option>' +
                '</select>' +
                '<select class="operator" id="dateselector" name="" hidden>' +
                '<option value="smaller"><s:text name="operator.before"/></option>' +
                '<option value="bigger"><s:text name="operator.after"/></option>' +
                '</select>' +
                '<select class="operator" id="booleanselector" name="" hidden>'+
                '<option value="is"><s:text name="operator.is"/></option>'+
                '<option value="isNot"><s:text name="operator.isNot"/></option>'+
                '</select>'+
                '</div>' +
                '<div class="columns small-4 collapse end">' +
                '<input class="werte" type="text" name="" value="LOL">' +
                '<input type="hidden" value="10.10.2014" class="hasDatepicker cal"/>' +
                '<select name="" hidden style="margin-bottom: 16px!important;">'+
                '<option value="true"><s:text name="contactsEntity.newsletterYes"/></option>'+
                '<option value="false"><s:text name="contactsEntity.newsletterNo"/></option>'+
                '</select>'+
                '</div>' +
                '</div>';
        if($("#filterSummery .row").length == 0){
            var filterRow =
                    '<div class="row">' +
                    '<div class="columns small-1">' +
                    '<input type="button" value="X" name="removeRow" onclick="removeRow(event)" class="removeRow"/>' +
                    '</div>' +

                    '<div class="columns small-3 collapse">' +
                    '<select onchange="changeSelector(event)" class="felder" name="">' +
                    '<option data-datatype="int" value="id"><s:text name="contactsEntity.Id"/></option>' +
                    '<option data-datatype="string" value="name"><s:text name="contactsEntity.name"/></option>' +
                    '<option data-datatype="string" value="surname"><s:text name="contactsEntity.surname"/></option>' +
                    '<option data-datatype="string" value="title"><s:text name="contactsEntity.title"/></option>' +
                    '<option data-datatype="string" value="academicDegreeBefore"><s:text name="contactsEntity.academicDegreeBefore"/></option>' +
                    '<option data-datatype="string" value="academicDegreeAfter"><s:text name="contactsEntity.academicDegreeAfter"/></option>' +
                    '<option data-datatype="int" value="plz.plz"><s:text name="contactsEntity.plz"/></option>' +
                    '<option data-datatype="string" value="state"><s:text name="contactsEntity.state"/></option>' +
                    '<option data-datatype="string" value="organisation"><s:text name="contactsEntity.Organisation"/></option>' +
                    '<option data-datatype="boolean" value="newsletter"><s:text name="contactsEntity.newsletter"/></option>' +
                    '<option data-datatype="string" value="town"><s:text name="contactsEntity.town"/></option>' +
                    '<option data-datatype="date" value="createDate"><s:text name="contactsEntity.createDate"/></option>' +
                    '<option data-datatype="date" value="updateDate"><s:text name="contactsEntity.updateDate"/></option>' +
                    '<option data-datatype="string" value="hompage"><s:text name="contactsEntity.homepage"/></option>' +
                    '<option data-datatype="string" value="info"><s:text name="contactsEntity.info"/></option>' +
                    '<option data-datatype="string" value="mail"><s:text name="contactsEntity.email"/></option>' +
                    '<option data-datatype="string" value="tel"><s:text name="contactsEntity.tel"/></option>' +
                    '<option data-datatype="string" value="country"><s:text name="contactsEntity.country"/></option>' +
                    '</select>' +
                    '</div>' +
                    '<div class="columns small-4 collapse">' +
                    '<select class="operator" id="idselector" name="">' +
                    '<option value="is"><s:text name="operator.is"/></option>' +
                    '<option value="isNot"><s:text name="operator.isNot"/></option>' +
                    '<option value="smaller"><s:text name="operator.smaller"/></option>' +
                    '<option value="bigger"><s:text name="operator.bigger"/></option>' +
                    '</select>' +
                    '<select class="operator" id="stringselector" name="" hidden="">' +
                    '<option value="is"><s:text name="operator.is"/></option>' +
                    '<option value="isNot"><s:text name="operator.isNot"/></option>' +
                    '<option value="like"><s:text name="operator.like"/></option>' +
                    '<option value="notLike"><s:text name="notLike"/></option>' +
                    '<option value="startsWith"><s:text name="operator.startsWith"/></option>' +
                    '<option value="endsWith"><s:text name="operator.endsWith"/></option>' +
                    '</select>' +
                    '<select class="operator" id="dateselector" name="" hidden>' +
                    '<option value="smaller"><s:text name="operator.before"/></option>' +
                    '<option value="bigger"><s:text name="operator.after"/></option>' +
                    '</select>' +
                    '<select class="operator" id="booleanselector" name="" hidden>'+
                    '<option value="is"><s:text name="operator.is"/></option>'+
                    '<option value="isNot"><s:text name="operator.isNot"/></option>'+
                    '</select>'+
                    '</div>' +
                    '<div class="columns small-4 collapse end">' +
                    '<input class="werte" type="text" name="" value="LOL">' +
                    '<input type="hidden" value="10.10.2014" class="hasDatepicker cal"/>' +
                    '<select name="" hidden style="margin-bottom: 16px!important;">'+
                    '<option value="true">JA</option>'+
                    '<option value="false">NEIN</option>'+
                    '</select>'+
                    '</div>' +
                    '</div>';
        }
        $("#filterSummery").append(filterRow);
    }


    function removeRow(event) {
        $(event.target).parent().parent().detach();
        var asdf = $(event.target);
        asdf.detach();
        var e = $("#filterSummery .row:first-child .booleanAndOr");
        e.parent().remove();
        $("#filterSummery .row:first-child > div:nth-child(3)").removeClass("small-2");
        $("#filterSummery .row:first-child > div:nth-child(3)").addClass("small-4");
    }

    function applyFilter(event, showAll) {
        event.preventDefault();
        showOverlay();
        andOrArray = "";
        $("#maxEntriesInfo").fadeOut();
        if(!showAll) {
            var booleanAndOr = $(".booleanAndOr").filter(":visible");
            var felder = $(".felder");
            var operatoren = $(".operator").filter(":visible");
            var values = $(".werte");
            if(felder.length === 0) {
                document.location.href = '/contacts/index.action';
            }
            searchQuery = "";
            for (var i = 0; i < felder.length; i++) {
                if (felder[i].value == 'mail') {
                    searchQuery += "emails.value";
                } else if (felder[i].value == 'tel') {
                    searchQuery += "tels.value";
                } else if (felder[i].value == 'organisation') {
                    searchQuery += "org.name";
                } else if (felder[i].value == "country") {
                    searchQuery += "cou.germanName"
                } else if(felder[i].value == "plz.plz") {
                    searchQuery += "plz.plz"
                } else {
                    searchQuery += "con." + felder[i].value;
                }
                if (operatoren[i].value == "is") {
                    searchQuery += "=";
                } else if (operatoren[i].value == "isNot") {
                    searchQuery += "!=";
                } else if (operatoren[i].value == "notLike") {
                    searchQuery += "not like"
                } else if (operatoren[i].value == "like" || operatoren[i].value == "startsWith" || operatoren[i].value == "endsWith") {
                    searchQuery += " like ";
                } else if (operatoren[i].value == "smaller") {
                    searchQuery += "<";
                } else {
                    searchQuery += ">";
                }

                var operator = $(values[i].parentNode).prev().children(":not(:hidden)").children().filter(":selected").val();
                var datatype = $(values[i].parentNode).prev().prev().children().first().children("option").filter(":selected").data("datatype");
                if (datatype == "int") {
                    searchQuery += values[i].value;
                } else if (datatype == "date") {
                    searchQuery += values[i].value;
                } else if(datatype == "boolean") {
                    searchQuery += values[i].value;
                } else if (datatype == "string") {
                    if (operator == "like") {
                        searchQuery += '\'%' + values[i].value + '%\'';
                    } else if (operator == "notLike") {
                        searchQuery += '\'%' + values[i].value + '%\'';
                    } else if (operator == "startsWith") {
                        searchQuery += '\'' + values[i].value + '%\'';
                    } else if (operator == "endsWith") {
                        searchQuery += '\'%' + values[i].value + '\'';
                    } else {
                        searchQuery += '\'' + values[i].value + '\'';
                    }
                }

                if (i != felder.length - 1) {
                    searchQuery += " " + booleanAndOr[i].value +" ";
                }
            }
            $.each(booleanAndOr, function(index, value) {
                andOrArray += ","+value.value;
            });
        }



        $.ajax({
            url: "/contacts/filter",
            method: "GET",
            data: {
                searchString: searchQuery,
                booleanAndOr: andOrArray,
                showAll: showAll
            },
            dataType: "JSON",
            success: function (data) {

                hideOverlay();

                //Check, if we got some Data with the chosen filters
                if (data.filteredContacts.length == 0) {
                    alert("Mit dem ausgew\xE4hlten Filter wurden keine Kontakte gefunden :'(");
                    return;
                }

                $('#myModal').foundation('reveal', 'close');
                $("#maxEntriesInfo").detach();
                if(!showAll){
                    $("#contactTable").parent().parent().prepend("<h3 id=\"maxEntriesInfo\" align=\"center\">Es werden maximal 100 Eintr&auml;ge angezeigt! Um alle anzuzeigen dr&uuml;cken Sie <a href='#' id='showAll' onclick='applyFilter(event, true)'>Hier</a></h3>")
                }
                $("#contactTable").next().detach();
                $("#contactTable").prev().detach();

                table.destroy();

                table = $("#contactTable").DataTable({
                    "language": {
                        "url": "../../assets/js/dataTablesLang/<s:text name="dataTablesLang" />"
                    }
                });

                table.clear().draw();


//                table.api().clear().draw();
//
                $.each(data.filteredContacts, function(index, value) {
                    var s = "";
                    var tels = "";
                    $.each(value.mails,function(indexMail, valueMail) {
                        s += valueMail + "\n";
                    });
                    $.each(value.tels, function(indexTels, valueTels) {
                        tels += valueTels + "\n";
                    });
                    table.row.add([
                    '<form id="details_action" class="contactsId" name="details_action" action="/contacts/details.action" method="get">'+
                    '    <input name="selectedContact" value="' + value.id + '" type="hidden">' +
                    '</form>' + value.surname,
                        value.name,
                        s,
                        value.organisation,
                        tels,
                        "<a href=\"/contacts/edit.action?selectedContact=" + value.id + "\"><i class='fa fa-pencil-square-o fa-lg'></a>",
                        "<a href=\"/contacts/delete.action?selectedContact=" + value.id + "\"><i class='fa fa-times fa-lg'></i></a>"
                    ]).draw();
                });

                <%--var table = $("#contactTable").find("tbody").first();--%>
                <%--var childrenLength = table.children().length;--%>
                <%--for (var i = 0; i < childrenLength; i++) {--%>
                    <%--table.find("tr:nth-child(1)").remove();--%>
                <%--}--%>
                <%--tableString = "";--%>
                <%--for (var i = 0; i < data.filteredContacts.length; i++) {--%>
                    <%--tableString += '<tr>';--%>
                    <%--tableString += '<td>' + data.filteredContacts[i].surname + '</td>';--%>
                    <%--tableString += '<td>' + data.filteredContacts[i].name + '</td>';--%>
                    <%--tableString += '<td>';--%>
                    <%--for (var j = 0; j < data.filteredContacts[i].mails.length; j++) {--%>
                        <%--if (j == data.filteredContacts[i].mails.length - 1) {--%>
                            <%--tableString += data.filteredContacts[i].mails[j];--%>
                        <%--} else {--%>
                            <%--tableString += data.filteredContacts[i].mails[j] + '<br />';--%>
                        <%--}--%>
                    <%--}--%>
                    <%--tableString += '</td>';--%>
                    <%--tableString += '<td>' + data.filteredContacts[i].organisation + '</td>';--%>
                    <%--tableString += '<td>';--%>
                    <%--for (var j = 0; j < data.filteredContacts[i].tels.length; j++) {--%>
                        <%--if (j == data.filteredContacts[i].tels.length - 1) {--%>
                            <%--tableString += data.filteredContacts[i].tels[j];--%>
                        <%--} else {--%>
                            <%--tableString += data.filteredContacts[i].tels[j] + '<br />';--%>
                        <%--}--%>
                    <%--}--%>
                    <%--tableString += '</td>';--%>
                    <%--tableString += '<td><a href="/contacts/edit.action?selectedContact=' + data.filteredContacts[i].id + '"><s:text name="LabelEditContact"/></a></td>';--%>
                    <%--tableString += '<td><a href="/contacts/delete.action?selectedContact=' + data.filteredContacts[i].id + '"><s:text name="LabelDeleteContact"/></a></td>';--%>
                    <%--tableString += '</tr>'--%>
                <%--}--%>
                <%--table.append(tableString);--%>



            },
            error: function (data) {
                hideOverlay();
                alert(data.statusText);
            }
        });
    }




</script>
