$.fn.populate = function populate(json){
    for(key in json)
    {
        if(json.hasOwnProperty(key))
            $(this).find('input[name='+key+']').val(json[key]);
    }
}