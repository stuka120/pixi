package at.binarycheese.pixi.selenium;

import at.binarycheese.pixi.domain.*;
import at.binarycheese.pixi.repositories.ContactsRepository;
import at.binarycheese.pixi.repositories.CountriesRepository;
import at.binarycheese.pixi.repositories.OrganisationsRepository;
import at.binarycheese.pixi.repositories.PostleitzahlRepository;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.fail;

/**
 * Created by Tortuga on 14.03.2015.
 */
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ContactSeleniumTest {

    private static WebDriver driver;
    private static String baseURL = "http://localhost:17001/";
    private static String contactURL = baseURL + "contacts/index.action";
    private static String contactCreateURL = baseURL + "contacts/create.action";

    @Autowired
    ContactsRepository contactsRepository;
    @Autowired
    CountriesRepository countriesRepository;
    @Autowired
    PostleitzahlRepository postleitzahlRepository;
    @Autowired
    OrganisationsRepository organisationsRepository;

    @BeforeClass
    public static void openBrowser() {
        driver = SeleniumTestUtil.getWebDriver();
    }

    @Before
    public void Login() {
        SeleniumTestUtil.login(driver);
        driver.get(contactURL);
    }

    @Test
    public void t201testCreateContactValid() {
        clearDB();
        driver.get(contactCreateURL);
        driver.findElement(By.cssSelector("#s2id_title input")).click();
        driver.findElement(By.cssSelector("#s2id_title input")).sendKeys("Mister");
        driver.findElement(By.cssSelector(".select2-match")).click();
        driver.findElement(By.id("contactsEntity_name")).clear();
        driver.findElement(By.id("contactsEntity_name")).sendKeys("Max");
        driver.findElement(By.id("contactsEntity_surname")).clear();
        driver.findElement(By.id("contactsEntity_surname")).sendKeys("Mustermann");
        driver.findElement(By.cssSelector("#s2id_academicDegreeAfter input")).click();
        driver.findElement(By.cssSelector("#s2id_academicDegreeAfter input")).sendKeys("PhD");
        driver.findElement(By.cssSelector(".select2-match")).click();
        driver.findElement(By.cssSelector("#s2id_postleitzahl a")).click();
        driver.findElement(By.cssSelector("#s2id_autogen9_search")).sendKeys("2151");
        driver.findElement(By.cssSelector(".select2-result-label")).click();
        driver.findElement(By.id("contactsEntity_address")).clear();
        driver.findElement(By.id("contactsEntity_address")).sendKeys("Musterstraße 1");
        driver.findElement(By.cssSelector("#s2id_telephone input")).click();
        driver.findElement(By.cssSelector("#s2id_telephone input")).sendKeys("+4365012345678");
        driver.findElement(By.cssSelector(".select2-match")).click();
        driver.findElement(By.cssSelector("#s2id_email input")).click();
        driver.findElement(By.cssSelector("#s2id_email input")).sendKeys("test@test.at");
        driver.findElement(By.cssSelector(".select2-match")).click();
        driver.findElement(By.cssSelector("#s2id_languagesList input")).click();
        driver.findElement(By.cssSelector("#s2id_languagesList input")).sendKeys("Deutsch");
        driver.findElement(By.cssSelector(".select2-match")).click();
        driver.findElement(By.cssSelector("#s2id_languagesList input")).sendKeys("Englisch");
        driver.findElement(By.cssSelector(".select2-match")).click();
        driver.findElement(By.cssSelector("#s2id_organisationsList input")).click();
        driver.findElement(By.cssSelector("#s2id_organisationsList input")).sendKeys("Fair ");
        driver.findElement(By.cssSelector(".select2-match")).click();
        driver.findElement(By.cssSelector("#s2id_countriesList input")).click();
        driver.findElement(By.cssSelector("#s2id_countriesList input")).sendKeys("Österreich");
        driver.findElement(By.cssSelector(".select2-match")).click();
        driver.findElement(By.id("newsletterCheckbox")).click();
        driver.findElement(By.cssSelector("input[value='Kontakt anlegen']")).click();
        Assert.assertEquals(1,contactsRepository.findByNameLikeOrSurnameLikeAllIgnoreCase("Max", "Mustermann").size());
    }

    @Test
    public void t202testCreateContactInvalidEmail() {
        clearDB();
        driver.get(contactCreateURL);
        driver.findElement(By.cssSelector("#s2id_title input")).click();
        driver.findElement(By.cssSelector("#s2id_title input")).sendKeys("Mister");
        driver.findElement(By.cssSelector(".select2-match")).click();
        driver.findElement(By.id("contactsEntity_name")).clear();
        driver.findElement(By.id("contactsEntity_name")).sendKeys("Max");
        driver.findElement(By.id("contactsEntity_surname")).clear();
        driver.findElement(By.id("contactsEntity_surname")).sendKeys("Mustermann");
        driver.findElement(By.cssSelector("#s2id_academicDegreeAfter input")).click();
        driver.findElement(By.cssSelector("#s2id_academicDegreeAfter input")).sendKeys("PhD");
        driver.findElement(By.cssSelector(".select2-match")).click();
        driver.findElement(By.cssSelector("#s2id_postleitzahl a")).click();
        driver.findElement(By.cssSelector("#s2id_autogen9_search")).sendKeys("2151");
        driver.findElement(By.cssSelector(".select2-result-label")).click();
        driver.findElement(By.id("contactsEntity_address")).clear();
        driver.findElement(By.id("contactsEntity_address")).sendKeys("Cheese street 1");
        driver.findElement(By.cssSelector("#s2id_telephone input")).click();
        driver.findElement(By.cssSelector("#s2id_telephone input")).sendKeys("+4365012345678");
        driver.findElement(By.cssSelector(".select2-match")).click();
        driver.findElement(By.cssSelector("#s2id_email input")).click();
        driver.findElement(By.cssSelector("#s2id_email input")).sendKeys("test");
        driver.findElement(By.cssSelector(".select2-match")).click();
        driver.findElement(By.cssSelector("#s2id_languagesList input")).click();
        driver.findElement(By.cssSelector("#s2id_languagesList input")).sendKeys("Deutsch");
        driver.findElement(By.cssSelector(".select2-match")).click();
        driver.findElement(By.cssSelector("#s2id_languagesList input")).sendKeys("Englisch");
        driver.findElement(By.cssSelector(".select2-match")).click();
        driver.findElement(By.cssSelector("#s2id_organisationsList input")).click();
        driver.findElement(By.cssSelector("#s2id_organisationsList input")).sendKeys("Fair ");
        driver.findElement(By.cssSelector(".select2-match")).click();
        driver.findElement(By.cssSelector("#s2id_countriesList input")).click();
        driver.findElement(By.cssSelector("#s2id_countriesList input")).sendKeys("Österreich");
        driver.findElement(By.cssSelector(".select2-match")).click();
        driver.findElement(By.id("newsletterCheckbox")).click();
        driver.findElement(By.cssSelector("input[value='Kontakt anlegen']")).click();
        Assert.assertEquals(0, contactsRepository.findByNameLikeOrSurnameLikeAllIgnoreCase("Max", "Mustermann").size());
        Assert.assertEquals("Neuen Kontakt erstellen", driver.getTitle());
    }

    @Test
    public void t203testCreateContactEmptyForm() {
        long size = contactsRepository.count();
        driver.get(contactCreateURL);
        driver.findElement(By.cssSelector("input[value='Kontakt anlegen']")).click();
        Assert.assertEquals(size, contactsRepository.count());
        Assert.assertTrue(driver.getCurrentUrl().contains("create"));
    }

    @Test
    public void t204testEditContactAddValidPhoneNumber() throws Exception {
        createTestContact();
        //wait until datatables has loaded
        for (int second = 0; ; second++) {
            if (second >= 60) fail("timeout");
            try {
                if (!driver.findElement(By.id("contactTable_processing")).isDisplayed()) break;
            } catch (Exception e) {
            }
            Thread.sleep(1000);
        }

        driver.findElement(By.cssSelector("input[type=\"search\"]")).sendKeys("Mustermann");

        //wait until datatables search has finished
        for (int second = 0; ; second++) {
            if (second >= 60) fail("timeout");
            try {
                if ("test@test.at".equals(driver.findElement(By.xpath("//table[@id='contactTable']/tbody/tr/td[3]")).getText()))
                    break;
            } catch (Exception e) {
            }
            Thread.sleep(1000);
        }

        driver.findElement(By.cssSelector(".fa-pencil-square-o")).click();
        driver.findElement(By.cssSelector("#s2id_telephonePrivate input")).click();
        driver.findElement(By.cssSelector("#s2id_telephonePrivate input")).sendKeys("+4365098765432");
        driver.findElement(By.cssSelector(".select2-match")).click();
        driver.findElement(By.cssSelector("input.button.left")).click();
        Assert.assertTrue(driver.getCurrentUrl().contains("index"));
        Assert.assertEquals("+4365098765432", contactsRepository.findByNameLikeOrSurnameLikeAllIgnoreCase("Max", "Mustermann").get(0).getNotImportantTelnumbers().get(0).getValue());
    }

    @Test
    public void t206testEditContactEmptyFirstName() throws Exception {
        createTestContact();

        //wait until datatables has loaded
        for (int second = 0; ; second++) {
            if (second >= 60) fail("timeout");
            try {
                if (!driver.findElement(By.id("contactTable_processing")).isDisplayed()) break;
            } catch (Exception e) {
            }
            Thread.sleep(1000);
        }

        driver.findElement(By.cssSelector("input[type=\"search\"]")).sendKeys("Mustermann");

        //wait until datatables search has finished
        for (int second = 0; ; second++) {
            if (second >= 60) fail("timeout");
            try {
                if ("test@test.at".equals(driver.findElement(By.xpath("//table[@id='contactTable']/tbody/tr/td[3]")).getText()))
                    break;
            } catch (Exception e) {
            }
            Thread.sleep(1000);
        }

        driver.findElement(By.cssSelector(".fa-pencil-square-o")).click();
        driver.findElement(By.id("contactsEntity_name")).clear();
        driver.findElement(By.cssSelector("input.button.left")).click();
        Assert.assertTrue(driver.getCurrentUrl().contains("create"));
    }

    @Test
    public void t207testAccessContactDataByFilter() throws Exception {
        createTestContact();
        driver.get(contactURL);
        driver.findElement(By.linkText("Filtern")).click();
        new Select(driver.findElement(By.cssSelector("select.felder"))).selectByVisibleText("Nachname");
        driver.findElement(By.cssSelector("input.werte")).clear();
        driver.findElement(By.cssSelector("input.werte")).sendKeys("Mustermann");
        driver.findElement(By.linkText("Hinzufügen")).click();
        new Select(driver.findElement(By.xpath("(//select[@name=''])[8]"))).selectByVisibleText("Staat");
        driver.findElement(By.xpath("(//input[@name=''])[3]")).clear();
        driver.findElement(By.xpath("(//input[@name=''])[3]")).sendKeys("Österreich");
        driver.findElement(By.linkText("Hinzufügen")).click();
        new Select(driver.findElement(By.xpath("(//select[@name=''])[15]"))).selectByVisibleText("TelNr");
        driver.findElement(By.xpath("(//input[@name=''])[4]")).clear();
        driver.findElement(By.xpath("(//input[@name=''])[4]")).sendKeys("+43 650 12345678");
        driver.findElement(By.linkText("Übernehmen")).click();
        for (int second = 0; ; second++) {
            if (second >= 60) fail("timeout");
            try {
                if ("Mustermann".equals(driver.findElement(By.cssSelector("td.sorting_1")).getText()))
                    break;
            } catch (Exception e) {
            }
            Thread.sleep(1000);
        }
        Assert.assertEquals("Mustermann", driver.findElement(By.cssSelector("td.sorting_1")).getText());
        Assert.assertEquals("+43 650 12345678", driver.findElement(By.xpath("//table[@id='contactTable']/tbody/tr/td[5]")).getText());
    }

    @Test
    public void t208testAccessContactDataBySearch() throws Exception {
        createTestContact();
        for (int second = 0; ; second++) {
            if (second >= 60) fail("timeout");
            try {
                if (!driver.findElement(By.id("contactTable_processing")).isDisplayed()) break;
            } catch (Exception e) {
            }
            Thread.sleep(1000);
        }

        driver.findElement(By.cssSelector("input[type=\"search\"]")).sendKeys("Mustermann");
        for (int second = 0; ; second++) {
            if (second >= 60) fail("timeout");
            try {
                if ("test@test.at".equals(driver.findElement(By.xpath("//table[@id='contactTable']/tbody/tr/td[3]")).getText()))
                    break;
            } catch (Exception e) {
            }
            Thread.sleep(1000);
        }
        Assert.assertEquals("Mustermann", driver.findElement(By.cssSelector("td.sorting_1")).getText());
        Assert.assertEquals("+43 650 12345678", driver.findElement(By.xpath("//table[@id='contactTable']/tbody/tr/td[5]")).getText());
    }

    @Test
    public void t209testAccessContactDataBySearchNotExistingContact() throws Exception {
        for (int second = 0; ; second++) {
            if (second >= 60) fail("timeout");
            try {
                if (!driver.findElement(By.id("contactTable_processing")).isDisplayed()) break;
            } catch (Exception e) {
            }
            Thread.sleep(1000);
        }

        driver.findElement(By.cssSelector("input[type=\"search\"]")).sendKeys("Musterfrau");
        for (int second = 0; ; second++) {
            if (second >= 60) fail("timeout");
            try {
                if ("Keine Einträge vorhanden.".equals(driver.findElement(By.cssSelector("td.dataTables_empty")).getText()))
                    break;
            } catch (Exception e) {
            }
            Thread.sleep(1000);
        }
        Assert.assertEquals("Keine Einträge vorhanden.", driver.findElement(By.cssSelector("td.dataTables_empty")).getText());
    }

    @Test
    public void t210testDeleteContact() throws Exception {
        createTestContact();
        for (int second = 0; ; second++) {
            if (second >= 60) fail("timeout");
            try {
                if (!driver.findElement(By.id("contactTable_processing")).isDisplayed()) break;
            } catch (Exception e) {
            }
            Thread.sleep(1000);
        }

        driver.findElement(By.cssSelector("input[type=\"search\"]")).sendKeys("Mustermann");
        for (int second = 0; ; second++) {
            if (second >= 30) fail("timeout");
            try {
                if ("test@test.at".equals(driver.findElement(By.xpath("//table[@id='contactTable']/tbody/tr/td[3]")).getText()))
                    break;
            } catch (Exception e) {
            }
            Thread.sleep(1000);
        }

        driver.findElement(By.cssSelector(".fa-times")).click();
        driver.findElement(By.cssSelector("input.button.left")).click();
        Assert.assertEquals(0,contactsRepository.findByNameLikeOrSurnameLikeAllIgnoreCase("Max", "Mustermann").size());
    }

    //TODO: T211

    public void createTestContact() {
        ContactsEntity testContact = new ContactsEntity();
        testContact.setTitle("Mister");
        testContact.setName("Max");
        testContact.setSurname("Mustermann");
        testContact.setAcademicDegreeAfter("PhD");
        testContact.setCountriesByCountriesId(countriesRepository.findOne("AT"));
        testContact.setState("N");
        testContact.setAddress("Musterstraße 1");
        testContact.setPostleitzahlByPlz(postleitzahlRepository.findByPlz(2151));
        testContact.setOrganisationsByOrganisationsId(organisationsRepository.findOne(1));
        testContact.setNewsletter(true);
        testContact.setCreateDate(new Timestamp(new Date().getTime()));
        contactsRepository.save(testContact);

        testContact = contactsRepository.findOne(testContact.getId());
        TelnumbersEntity telnumbersEntity = new TelnumbersEntity();
        telnumbersEntity.setValue("+43 650 12345678");
        telnumbersEntity.setImportant(true);;
        telnumbersEntity.setContact(testContact);
        testContact.getTelnumbers().add(telnumbersEntity);
        EmailsEntity emailsEntity = new EmailsEntity();
        emailsEntity.setImportant(true);
        emailsEntity.setValue("test@test.at");
        emailsEntity.setContact(testContact);
        testContact.getEmails().add(emailsEntity);
        contactsRepository.save(testContact);
    }

    @After
    @Transactional
    public void clearDB() {
        for (ContactsEntity contactsEntity : contactsRepository.findByNameLikeOrSurnameLikeAllIgnoreCase("Max", "Mustermann")) {
            contactsRepository.delete(contactsEntity.getId());
        }
    }

    @AfterClass
    public static void closeBrowser() {
        driver.quit();
    }
}
