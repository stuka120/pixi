package at.binarycheese.pixi.selenium;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.fail;

/**
 * Created by Tortuga on 23.03.2015.
 */
public class SeleniumTestUtil {
    public static String baseURL = "http://localhost:17001/";
    private static WebDriver driver;
    public static String username = "asdf";
    public static String password = "qwer";
    public static String testpassword = "Testpa55word";
    public static String testusername = "testuser685646";
    public static String testpasswordHash = new BCryptPasswordEncoder().encode(testpassword);

    public static void login(WebDriver driver) {
        if (driver.getCurrentUrl().contains("login")) {
            driver.findElement(By.name("j_username")).clear();
            driver.findElement(By.name("j_username")).sendKeys(username);
            driver.findElement(By.name("j_password")).clear();
            driver.findElement(By.name("j_password")).sendKeys(password);
            driver.findElement(By.cssSelector("input[type='submit']")).click();
            Assert.assertNotNull("Login failed: Hi, "+ username + " not visible", driver.findElement(By.linkText("Hi, "+ username)));
        }
    }

    public static WebDriver getWebDriver() {
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get(baseURL);
        return driver;
    }

    public static boolean isElementPresent(WebDriver driver, By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public static void waitForElement(WebDriver driver, By by) {
        for (int second = 0; ; second++) {
            if (second >= 60) fail("timeout");
            try {
                if (isElementPresent(driver, by)) {
                    break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
