package at.binarycheese.pixi.selenium;

import at.binarycheese.pixi.domain.ContactsEntity;
import at.binarycheese.pixi.domain.EventsEntity;
import at.binarycheese.pixi.domain.UsersEntity;
import at.binarycheese.pixi.domain.UserworksEntity;
import at.binarycheese.pixi.repositories.*;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Tortuga on 22.03.2015.
 */
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class EventSeleniumTest {
    private static WebDriver driver;
    private static String baseURL = "http://localhost:17001/";
    private static String eventURL = baseURL + "event/index.action";

    @Autowired
    private EventsRepository eventsRepository;
    @Autowired
    private EventcategoriesRepository eventcategoriesRepository;
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private ContactsRepository contactsRepository;
    @Autowired
    private UserworkRepository userworkRepository;

    @BeforeClass
    public static void openBrowser() {
        driver = SeleniumTestUtil.getWebDriver();
    }

    @Before
    public void Login() {
        SeleniumTestUtil.login(driver);
        driver.get(eventURL);
    }

    @Test
    public void testT502CreateEventValid() throws Exception {
        createTestUser();
        int sizeBefore = eventsRepository.findByTitleAndLocation("testevent", "Vienna").size();
        driver.findElement(By.id("create")).click();
        SeleniumTestUtil.waitForElement(driver, By.id("create_eventtitle"));
        driver.findElement(By.id("create_eventtitle")).clear();
        driver.findElement(By.id("create_eventtitle")).sendKeys("testevent");
        driver.findElement(By.id("startdate")).clear();
        driver.findElement(By.id("startdate")).sendKeys("30.11.2014");
        driver.findElement(By.id("enddate")).clear();
        driver.findElement(By.id("enddate")).sendKeys("01.12.2014");
        driver.findElement(By.id("createForm_event_location")).click();
        driver.findElement(By.id("createForm_event_location")).clear();
        driver.findElement(By.id("createForm_event_location")).sendKeys("Vienna");
        driver.findElement(By.id("createForm_event_description")).clear();
        driver.findElement(By.id("createForm_event_description")).sendKeys("This is a test event");
        new Select(driver.findElement(By.id("catlist"))).selectByVisibleText("Verein");
        driver.findElement(By.cssSelector(".select2-choice")).click();
        driver.findElement(By.id("s2id_autogen1_search")).sendKeys("Mustermann");
        driver.findElement(By.cssSelector(".select2-result-selectable")).click();
        driver.findElement(By.cssSelector("#createForm .row:last-child input[type=submit]")).click();
        SeleniumTestUtil.waitForElement(driver, By.cssSelector("button.confirm"));
        driver.findElement(By.cssSelector("button.confirm")).click();
        Thread.sleep(1000);
        List<EventsEntity> resultList;
        resultList = eventsRepository.findByTitleAndLocation("testevent", "Vienna");
        Assert.assertEquals(sizeBefore + 1, resultList.size());
    }

    @Test
    public void testT503CreateEventNoStartDate() throws Exception {
        createTestUser();
        int sizeBefore = eventsRepository.findByTitleAndLocation("testevent", "Vienna").size();
        driver.findElement(By.id("create")).click();
        SeleniumTestUtil.waitForElement(driver, By.id("create_eventtitle"));
        driver.findElement(By.id("create_eventtitle")).clear();
        driver.findElement(By.id("create_eventtitle")).sendKeys("testevent");
        driver.findElement(By.id("enddate")).clear();
        driver.findElement(By.id("enddate")).sendKeys("01.12.2014");
        driver.findElement(By.id("createForm_event_location")).click();
        driver.findElement(By.id("createForm_event_location")).clear();
        driver.findElement(By.id("createForm_event_location")).sendKeys("Vienna");
        driver.findElement(By.id("createForm_event_description")).clear();
        driver.findElement(By.id("createForm_event_description")).sendKeys("This is a test event");
        new Select(driver.findElement(By.id("catlist"))).selectByVisibleText("Verein");
        driver.findElement(By.cssSelector(".select2-choice")).click();
        driver.findElement(By.id("s2id_autogen1_search")).sendKeys("Mustermann");
        driver.findElement(By.cssSelector(".select2-result-selectable")).click();
        driver.findElement(By.cssSelector("#createForm .row:last-child input[type=submit]")).click();
        Thread.sleep(1000);
        assertTrue(SeleniumTestUtil.isElementPresent(driver, By.cssSelector("#formerrors")));
        assertEquals(sizeBefore, eventsRepository.findByTitleAndLocation("testevent", "Vienna").size());
    }

    @Test
    public void testT504CreateEventEmptyFields() throws Exception {
        createTestUser();
        int sizeBefore = eventsRepository.findByTitleAndLocation("testevent", "Vienna").size();
        driver.findElement(By.id("create")).click();
        SeleniumTestUtil.waitForElement(driver, By.id("create_eventtitle"));
        driver.findElement(By.cssSelector("#createForm .row:last-child input[type=submit]")).click();
        Thread.sleep(1000);
        assertTrue(SeleniumTestUtil.isElementPresent(driver, By.cssSelector("#formerrors")));
        assertEquals(sizeBefore, eventsRepository.findByTitleAndLocation("testevent", "Vienna").size());
    }

    @Test
    public void testT505CreateEventStartDateAfterEndDat() throws Exception {
        createTestUser();
        int sizeBefore = eventsRepository.findByTitleAndLocation("testevent", "Vienna").size();
        driver.findElement(By.id("create")).click();
        SeleniumTestUtil.waitForElement(driver, By.id("create_eventtitle"));
        driver.findElement(By.id("create_eventtitle")).clear();
        driver.findElement(By.id("create_eventtitle")).sendKeys("testevent");
        driver.findElement(By.id("startdate")).clear();
        driver.findElement(By.id("startdate")).sendKeys("02.12.2014");
        driver.findElement(By.id("enddate")).clear();
        driver.findElement(By.id("enddate")).sendKeys("01.12.2014");
        driver.findElement(By.id("createForm_event_location")).click();
        driver.findElement(By.id("createForm_event_location")).clear();
        driver.findElement(By.id("createForm_event_location")).sendKeys("Vienna");
        driver.findElement(By.id("createForm_event_description")).clear();
        driver.findElement(By.id("createForm_event_description")).sendKeys("This is a test event");
        new Select(driver.findElement(By.id("catlist"))).selectByVisibleText("Verein");
        driver.findElement(By.cssSelector(".select2-choice")).click();
        driver.findElement(By.id("s2id_autogen1_search")).sendKeys("Mustermann");
        driver.findElement(By.cssSelector(".select2-result-selectable")).click();
        driver.findElement(By.cssSelector("#createForm .row:last-child input[type=submit]")).click();
        Thread.sleep(1000);
        assertTrue(SeleniumTestUtil.isElementPresent(driver, By.cssSelector("#formerrors")));
        assertEquals(sizeBefore, eventsRepository.findByTitleAndLocation("testevent", "Vienna").size());
    }

    @Test
    public void testT506EditEvent() throws Exception {
        createTestUser();
        createTestEvent();
        Assume.assumeTrue(driver instanceof JavascriptExecutor);
        JavascriptExecutor jsdriver = (JavascriptExecutor) driver;
        jsdriver.executeScript("$('#cal').fullCalendar('gotoDate', \"2014/11/30\");");
        SeleniumTestUtil.waitForElement(driver, By.xpath("//*[contains(text(), 'testevent')]"));
        driver.findElement(By.xpath("//*[contains(text(), 'testevent')]")).click();
        driver.findElement(By.id("editevent_event_location")).clear();
        driver.findElement(By.id("editevent_event_location")).sendKeys("Moon");
        driver.findElement(By.cssSelector("#editevent .row:last-child div:last-child input[type=submit]")).click();
        SeleniumTestUtil.waitForElement(driver, By.cssSelector("button.confirm"));
        driver.findElement(By.cssSelector("button.confirm")).click();
        SeleniumTestUtil.waitForElement(driver, By.xpath("//*[contains(text(), 'testevent')]"));
        driver.findElement(By.xpath("//*[contains(text(), 'testevent')]")).click();
        assertEquals("Moon", driver.findElement(By.id("editevent_event_location")).getAttribute("value"));
        assertEquals("Moon", eventsRepository.findByTitleAndLocation("testevent", "Moon").get(0).getLocation());
    }

    @Test
    public void testT507EditEventNoLocation() throws Exception {
        createTestEvent();
        Assume.assumeTrue(driver instanceof JavascriptExecutor);
        JavascriptExecutor jsdriver = (JavascriptExecutor) driver;
        jsdriver.executeScript("$('#cal').fullCalendar('gotoDate', \"2014/11/30\");");
        SeleniumTestUtil.waitForElement(driver, By.xpath("//*[contains(text(), 'testevent')]"));
        driver.findElement(By.xpath("//*[contains(text(), 'testevent')]")).click();
        driver.findElement(By.id("startdate")).clear();
        driver.findElement(By.cssSelector("#editevent .row:last-child div:last-child input[type=submit]")).click();
        SeleniumTestUtil.waitForElement(driver, By.cssSelector("button.confirm"));
        assertTrue(SeleniumTestUtil.isElementPresent(driver, By.cssSelector("#formerrors")));
    }

    @Test
    public void testT508DeleteEvent() throws Exception {
        createTestEvent();
        Assume.assumeTrue(driver instanceof JavascriptExecutor);
        JavascriptExecutor jsdriver = (JavascriptExecutor) driver;
        jsdriver.executeScript("$('#cal').fullCalendar('gotoDate', \"2014/11/30\");");
        SeleniumTestUtil.waitForElement(driver, By.xpath("//*[contains(text(), 'testevent')]"));
        driver.findElement(By.xpath("//*[contains(text(), 'testevent')]")).click();
        driver.findElement(By.id("deleteEvent")).click();
        Thread.sleep(1000);
        driver.findElement(By.cssSelector("button.confirm")).click();
        Thread.sleep(1000);
        driver.findElement(By.cssSelector("button.confirm")).click();
        Thread.sleep(3000);
        Assert.assertEquals(0, eventsRepository.findByTitleAndLocation("tesetevent", "Vienna").size());
    }

    @Test
    public void testT509AssignWorkersToEvents() throws Exception {
        createTestEvent();
        createTestUser();
        JavascriptExecutor jsdriver = (JavascriptExecutor) driver;
        jsdriver.executeScript("$('#cal').fullCalendar('gotoDate', \"2014/11/30\");");
        driver.findElement(By.xpath("//*[contains(text(), 'testevent')]")).click();
        driver.findElement(By.linkText("Mitarbeiter")).click();
        driver.findElement(By.id("switchcreate")).click();
        driver.findElement(By.cssSelector(".select2-choices")).click();
        driver.findElement(By.cssSelector(".select2-search-field input")).sendKeys("Mustermann");
        driver.findElement(By.cssSelector(".select2-result-label")).click();
        driver.findElement(By.id("workstartdate")).click();
        driver.findElement(By.id("workstartdate")).clear();
        driver.findElement(By.id("workstartdate")).sendKeys("30.11.2014");
        driver.findElement(By.id("workstarttime")).clear();
        driver.findElement(By.id("workstarttime")).sendKeys("20:00");
        driver.findElement(By.id("workenddate")).clear();
        driver.findElement(By.id("workenddate")).sendKeys("01.12.2014");
        driver.findElement(By.id("workendtime")).clear();
        driver.findElement(By.id("workendtime")).sendKeys("02:00");
        driver.findElement(By.cssSelector("#createworkform div div.row:last-child input[type=submit]")).click();
        Thread.sleep(3000);
        Assert.assertEquals(1, userworkRepository.findByUser_UsernameAndEvent_Title(SeleniumTestUtil.testusername, "testevent").size());
    }

    @After
    public void cleanDatabase() {
        for (UserworksEntity entity : userworkRepository.findByUser_UsernameAndEvent_Title(SeleniumTestUtil.testusername, "testevent")) {
            userworkRepository.delete(entity);
        }
        for (EventsEntity entity : eventsRepository.findByTitleAndLocation("testevent", "Vienna")) {
            eventsRepository.delete(entity);
        }
        if (usersRepository.findByUsername(SeleniumTestUtil.testusername) != null) {
            usersRepository.delete(usersRepository.findByUsername(SeleniumTestUtil.testusername));
        }
        if (contactsRepository.findByUsersesById_Username(SeleniumTestUtil.testusername) != null) {
            for(ContactsEntity contact : contactsRepository.findByNameLikeOrSurnameLikeAllIgnoreCase("Max", "Mustermann")){
                contactsRepository.delete(contact);
            }
        }
    }

    @AfterClass
    public static void closeBrowser() {
        driver.quit();
    }

    private void createTestEvent() {
        EventsEntity event = new EventsEntity();
        event.setTitle("testevent");
        event.setStartDate(new Timestamp(114, 10, 30, 0, 0, 0, 0));
        event.setEndDate(new Timestamp(114, 11, 1, 0, 0, 0, 0));
        event.setLocation("Vienna");
        event.setDescription("This is a test event.");
        event.setEventcategoriesByEventCategorieId(eventcategoriesRepository.findOne(2));
        eventsRepository.save(event);
    }

    private void createTestUser() {
        UsersEntity user = usersRepository.findByUsername(SeleniumTestUtil.testusername);
        if (user == null) {
            user = new UsersEntity();
            user.setUsername(SeleniumTestUtil.testusername);
            ContactsEntity contact = new ContactsEntity();
            contact.setName("Max");
            contact.setSurname("Mustermann");
            contact.setCreateDate(new Timestamp(new Date().getTime()));
            contactsRepository.save(contact);
            user.setContactsByContactsId(contactsRepository.findOne(contact.getId()));
            contact.getUsersesById().add(user);
            usersRepository.save(user);
        }
    }

}
