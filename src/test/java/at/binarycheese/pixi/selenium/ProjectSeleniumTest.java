package at.binarycheese.pixi.selenium;

import at.binarycheese.pixi.domain.OrganisationsEntity;
import at.binarycheese.pixi.domain.ProjectEntity;
import at.binarycheese.pixi.repositories.OrganisationsRepository;
import at.binarycheese.pixi.repositories.ProjectsRepository;
import at.binarycheese.pixi.repositories.ProjectstagesRepository;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Created by Tortuga on 22.03.2015.
 */
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProjectSeleniumTest {

    private static WebDriver driver;
    WebElement element;
    private static String projectURL = SeleniumTestUtil.baseURL + "project/index.action";

    @Autowired
    private OrganisationsRepository organisationsRepository;
    @Autowired
    private ProjectsRepository projectsRepository;
    @Autowired
    private ProjectstagesRepository projectstagesRepository;

    @BeforeClass
    public static void openBrowser() {
        driver = SeleniumTestUtil.getWebDriver();
    }

    @Before
    public void Login() {
        SeleniumTestUtil.login(driver);
        driver.get(projectURL);
        cleanDatabase();
    }

    @Test
    public void testT401CreateProjectValid() throws Exception {
        createTestOrg();
        int sizeBefore = projectsRepository.countByProjectnumber("M1-11");
        driver.findElement(By.id("newButton")).click();
        for (int second = 0; ; second++) {
            if (second >= 60) fail("timeout");
            try {
                if (isElementPresent(By.id("projectNr"))) break;
            } catch (Exception e) {
            }
            Thread.sleep(1000);
        }
        driver.findElement(By.id("projectNr")).clear();
        driver.findElement(By.id("projectNr")).sendKeys("M1-11");
        driver.findElement(By.id("projectName")).clear();
        driver.findElement(By.id("projectName")).sendKeys("testproject");
        new Select(driver.findElement(By.id("inputForm_orgId"))).selectByVisibleText("testpartner");
        driver.findElement(By.id("description")).clear();
        driver.findElement(By.id("description")).sendKeys("This is a sample project.");
        driver.findElement(By.id("submitButton")).click();
        SeleniumTestUtil.waitForElement(driver, By.cssSelector(".confirm"));
        driver.findElement(By.cssSelector(".confirm")).click();
        Thread.sleep(5000);
        driver.findElement(By.cssSelector("input[type=\"search\"]")).sendKeys("testproject");
        Thread.sleep(2000);
        Assert.assertEquals("Created Project not visible!", "M1-11", driver.findElement(By.xpath("//table[@id='table']/tbody/tr[1]/td")).getText());
        Assert.assertEquals("Created Project not in Database", sizeBefore + 1, projectsRepository.countByProjectnumber("M1-11"));
    }

    @Test
    public void testT402CreateProjectNoData() throws Exception {
        int sizeBefore = projectsRepository.countByProjectnumber("");
        driver.findElement(By.id("newButton")).click();
        driver.findElement(By.id("submitButton")).click();
        assertTrue(isElementPresent(By.cssSelector("ul.error")));
        Assert.assertEquals("Invalid Project was created but shouldn't", sizeBefore, projectsRepository.countByProjectnumber(""));
    }

    @Test
    public void testT403EditProject() throws Exception {
        createTestProject();
        Thread.sleep(1000);
        driver.get(projectURL);
        driver.findElement(By.cssSelector("input[type=\"search\"]")).clear();
        driver.findElement(By.cssSelector("input[type=\"search\"]")).sendKeys("M1-11");
        driver.findElement(By.xpath("//table[@id='table']/tbody/tr/td[6]/a/i")).click();
        driver.findElement(By.id("description")).clear();
        driver.findElement(By.id("description")).sendKeys("I changed the description");
        driver.findElement(By.id("submitButton")).click();
        driver.findElement(By.cssSelector("button.confirm")).click();
        driver.findElement(By.cssSelector("input[type=\"search\"]")).clear();
        Thread.sleep(5000);
        driver.findElement(By.cssSelector("input[type=\"search\"]")).sendKeys("M1-11");
        Assert.assertEquals("I changed the description", driver.findElement(By.cssSelector("td.descriptionCell")).getText());
        ProjectEntity projectEntity = projectsRepository.findByProjectnumber("M1-11").get(0);
        Assert.assertEquals("Description wasn't changed in the database", "I changed the description", projectEntity.getDescription());
    }

    @Test
    public void testT404DeactivateProject() throws Exception {
        createTestProject();
        Thread.sleep(500);
        driver.get(projectURL);
        driver.findElement(By.cssSelector("input[type=\"search\"]")).clear();
        driver.findElement(By.cssSelector("input[type=\"search\"]")).sendKeys("M1-11");
        Thread.sleep(1000);
        driver.findElement(By.xpath("//table[@id='table']/tbody/tr/td[7]/a/i")).click();
        Thread.sleep(500);
        driver.findElement(By.cssSelector("button.confirm")).click();
        Thread.sleep(500);
        driver.findElement(By.cssSelector("button.confirm")).click();
        Thread.sleep(5000);
        driver.findElement(By.cssSelector("input[type=\"search\"]")).clear();
        driver.findElement(By.cssSelector("input[type=\"search\"]")).sendKeys("M1-11");
        Thread.sleep(1000);
        Assert.assertTrue(isElementPresent(By.cssSelector("td.dataTables_empty")));
        driver.findElement(By.id("deactivatedSwitch")).click();
        Thread.sleep(5000);
        driver.findElement(By.cssSelector("input[type=\"search\"]")).clear();
        driver.findElement(By.cssSelector("input[type=\"search\"]")).sendKeys("M1-11");
        Thread.sleep(1000);
        Assert.assertEquals("This is a sample project.", driver.findElement(By.cssSelector("td.descriptionCell")).getText());
        Assert.assertEquals("Project wasn't deactivated in the database!", 5, projectsRepository.findByProjectnumber("M1-11").get(0).getProjectstagesByProjectStage().getId());
    }

    @Test
    public void testT405ReactivateProject() throws Exception {
        createTestProject();
        ProjectEntity project = projectsRepository.findByProjectnumber("M1-11").get(0);
        project.setProjectstagesByProjectStage(projectstagesRepository.findOne(5));
        projectsRepository.save(project);
        Thread.sleep(500);
        driver.get(projectURL);
        driver.findElement(By.id("deactivatedSwitch")).click();
        Thread.sleep(3000);
        driver.findElement(By.cssSelector("input[type=\"search\"]")).clear();
        driver.findElement(By.cssSelector("input[type=\"search\"]")).sendKeys("M1-11");
        Thread.sleep(500);
        driver.findElement(By.xpath("//table[@id='table']/tbody/tr/td[7]/a/i")).click();
        Thread.sleep(500);
        new Select(driver.findElement(By.id("stageIdSelect"))).selectByVisibleText("Vertrag");
        driver.findElement(By.id("reactivateSubmitButton")).click();
        Thread.sleep(500);
        driver.findElement(By.cssSelector("button.confirm")).click();
        Thread.sleep(5000);
        driver.findElement(By.cssSelector("input[type=\"search\"]")).clear();
        driver.findElement(By.cssSelector("input[type=\"search\"]")).sendKeys("M1-11");
        Thread.sleep(500);
        Assert.assertEquals("This is a sample project.", driver.findElement(By.cssSelector("td.descriptionCell")).getText());
        Assert.assertEquals("Project wasn't reactivated in the database!", 2, projectsRepository.findByProjectnumber("M1-11").get(0).getProjectstagesByProjectStage().getId());
    }

    @Test
    public void testT406UpgradeProjectStage() throws Exception {
        createTestProject();
        driver.get(SeleniumTestUtil.baseURL + "project/index.action");
        driver.findElement(By.cssSelector("input[type=\"search\"]")).clear();
        driver.findElement(By.cssSelector("input[type=\"search\"]")).sendKeys("M1-11");
        driver.findElement(By.cssSelector("td.descriptionCell")).click();
        driver.findElement(By.id("nextStageForm_nextStage")).click();
        driver.findElement(By.linkText("Projekte")).click();
        driver.findElement(By.cssSelector("input[type=\"search\"]")).clear();
        driver.findElement(By.cssSelector("input[type=\"search\"]")).sendKeys("M1-11");
        Assert.assertEquals("Vertrag", driver.findElement(By.xpath("//table[@id='table']/tbody/tr/td[4]")).getText());
        Assert.assertEquals(2, projectsRepository.findByProjectnumber("M1-11").get(0).getProjectstagesByProjectStage().getId());
    }

    @After
    @Transactional
    public void cleanDatabase(){
        List<ProjectEntity> projects = projectsRepository.findByProjectnumber("M1-11");
        for(ProjectEntity projectEntity : projects){
            projectsRepository.delete(projectEntity);
        }
        List<OrganisationsEntity> organisations = organisationsRepository.findByName("testpartner");
        for(OrganisationsEntity organisationsEntity : organisations){
            organisationsRepository.delete(organisationsEntity);
        }
    }

    @AfterClass
    public static void closeBrowser() {
        driver.quit();
    }

    private void createTestOrg(){
        if(organisationsRepository.findByName("testpartner").size() == 0){
            OrganisationsEntity organisationsEntity = new OrganisationsEntity();
            organisationsEntity.setName("testpartner");
            organisationsRepository.save(organisationsEntity);
        }
    }

    private void createTestProject(){
        if(projectsRepository.countByProjectnumber("M1-11") == 0){
            ProjectEntity project = new ProjectEntity();
            project.setProjectnumber("M1-11");
            project.setName("testproject");
            project.setProjectstagesByProjectStage(projectstagesRepository.findOne(1));
            project.setDescription("This is a sample project.");
            projectsRepository.save(project);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }
}
