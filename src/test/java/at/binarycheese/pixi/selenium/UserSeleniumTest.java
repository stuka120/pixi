package at.binarycheese.pixi.selenium;

import at.binarycheese.pixi.domain.*;
import at.binarycheese.pixi.repositories.*;
import at.binarycheese.pixi.service.UsersService;
import com.sun.jna.platform.win32.Netapi32Util;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by Tortuga on 17.03.2015.
 */
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserSeleniumTest {

    private static WebDriver driver;
    private static String baseURL = "http://localhost:17001/";
    private static String userURL = baseURL + "user/index.action";

    @Autowired
    private ContactsRepository contactsRepository;
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private UsersService usersService;
    @Autowired
    private PostleitzahlRepository postleitzahlRepository;
    @Autowired
    private CountriesRepository countriesRepository;
    @Autowired
    private OrganisationsRepository organisationsRepository;
    @Autowired
    private RolesRepository rolesRepository;

    @BeforeClass
    public static void openBrowser() {
        driver = SeleniumTestUtil.getWebDriver();
    }

    @Before
    public void Login() {
        SeleniumTestUtil.login(driver);
        driver.get(userURL);
    }

    @AfterClass
    public static void closeBrowser() {
        driver.quit();
    }

    @Test
    public void testT301CreateUser() throws Exception {
        cleanDB();
        createTestContact();
        driver.findElement(By.id("createUser")).click();
        driver.findElement(By.id("add_userEntity_username")).clear();
        driver.findElement(By.id("add_userEntity_username")).sendKeys(SeleniumTestUtil.testusername);
        driver.findElement(By.cssSelector("#s2id_rolesList input")).click();
        driver.findElement(By.cssSelector("#s2id_rolesList input")).sendKeys("Contactadmin");
        driver.findElement(By.cssSelector(".select2-result-label")).click();
        driver.findElement(By.cssSelector("#s2id_rolesList input")).click();
        driver.findElement(By.cssSelector("#s2id_rolesList input")).sendKeys("Admin");
        driver.findElement(By.cssSelector(".select2-result-label")).click();
        driver.findElement(By.cssSelector("#s2id_rolesList input")).click();
        driver.findElement(By.cssSelector("#s2id_rolesList input")).sendKeys("Projectcontroller");
        driver.findElement(By.cssSelector(".select2-result-label")).click();
        driver.findElement(By.cssSelector("#s2id_selectedContact")).click();
        driver.findElement(By.cssSelector("#s2id_selectedContact a")).click();
        driver.findElement(By.cssSelector("#s2id_autogen2_search")).sendKeys("Mustermann");
        driver.findElement(By.cssSelector(".select2-result-label")).click();
        driver.findElement(By.cssSelector("input[type='submit']")).click();
        Thread.sleep(2000);
        Assert.assertNotNull(usersRepository.findByUsername(SeleniumTestUtil.testusername));
    }

    @Test
    public void testT302CreateUser() throws Exception {
        cleanDB();
        createTestContact();
        driver.findElement(By.id("createUser")).click();
        driver.findElement(By.id("add_userEntity_username")).clear();
        driver.findElement(By.id("add_userEntity_username")).sendKeys(SeleniumTestUtil.testusername);
        driver.findElement(By.cssSelector("#s2id_selectedContact")).click();
        driver.findElement(By.cssSelector("#s2id_selectedContact a")).click();
        driver.findElement(By.cssSelector("#s2id_autogen2_search")).sendKeys("Mustermann");
        driver.findElement(By.cssSelector(".select2-result-label")).click();
        driver.findElement(By.cssSelector("input[type='submit']")).click();
        Thread.sleep(2000);
        Assert.assertNotNull(usersRepository.findByUsername(SeleniumTestUtil.testusername));
    }

    @Test
    public void testT303CreateUserNoUsername() throws Exception{
        cleanDB();
        createTestContact();
        driver.findElement(By.id("createUser")).click();
        driver.findElement(By.cssSelector("#s2id_selectedContact")).click();
        driver.findElement(By.cssSelector("#s2id_selectedContact a")).click();
        driver.findElement(By.cssSelector("#s2id_autogen2_search")).sendKeys("Mustermann");
        driver.findElement(By.cssSelector(".select2-result-label")).click();
        driver.findElement(By.cssSelector("input[type='submit']")).click();
        Thread.sleep(2000);
        Assert.assertTrue(SeleniumTestUtil.isElementPresent(driver, By.id("formerrors")));
        Assert.assertNull(usersRepository.findByUsername(SeleniumTestUtil.testusername));
    }

    //TODO t304

    @Test
    public void testT305CreateUserExistingUsername() throws Exception {
        cleanDB();
        createTestContact();
        createTestUser();
        driver.findElement(By.id("createUser")).click();
        driver.findElement(By.id("add_userEntity_username")).clear();
        driver.findElement(By.id("add_userEntity_username")).sendKeys(SeleniumTestUtil.testusername);
        driver.findElement(By.cssSelector("#s2id_rolesList input")).click();
        driver.findElement(By.cssSelector("#s2id_rolesList input")).sendKeys("Contactadmin");
        driver.findElement(By.cssSelector(".select2-result-label")).click();
        driver.findElement(By.cssSelector("#s2id_rolesList input")).click();
        driver.findElement(By.cssSelector("#s2id_rolesList input")).sendKeys("Admin");
        driver.findElement(By.cssSelector(".select2-result-label")).click();
        driver.findElement(By.cssSelector("#s2id_rolesList input")).click();
        driver.findElement(By.cssSelector("#s2id_rolesList input")).sendKeys("Projectcontroller");
        driver.findElement(By.cssSelector(".select2-result-label")).click();
        driver.findElement(By.cssSelector("input[type='submit']")).click();
        Thread.sleep(2000);
        Assert.assertTrue(SeleniumTestUtil.isElementPresent(driver, By.id("formerrors")));
        Assert.assertNull(usersRepository.findByUsername(SeleniumTestUtil.testusername));
    }

    @Test
    public void testT306CreateUserAlreadyUsedContact() throws Exception{
        cleanDB();
        createTestContact();
        createTestUser();
        driver.findElement(By.id("createUser")).click();
        driver.findElement(By.cssSelector("#s2id_selectedContact")).click();
        driver.findElement(By.cssSelector("#s2id_selectedContact a")).click();
        driver.findElement(By.cssSelector("#s2id_autogen2_search")).sendKeys("Mustermann");
        Assert.assertTrue(SeleniumTestUtil.isElementPresent(driver, By.cssSelector(".select2-no-results")));
    }

    @Test
    public void testT307CreateUserNoData() throws Exception{
        cleanDB();
        createTestContact();
        driver.findElement(By.id("createUser")).click();
        driver.findElement(By.cssSelector("input[type='submit']")).click();
        Thread.sleep(2000);
        Assert.assertTrue(SeleniumTestUtil.isElementPresent(driver, By.id("formerrors")));
        Assert.assertNull(usersRepository.findByUsername(SeleniumTestUtil.testusername));
    }

    @Test
    public void testT309ChangeUserRole() throws Exception{
        cleanDB();
        createTestContact();
        createTestUser();
        UsersEntity user = usersService.findByUserIdWithPermissions(usersService.findByUsername(SeleniumTestUtil.testusername).getId());
        user.removeRole(1);
        user.removeRole(2);
        user.removeRole(4);
        usersRepository.save(user);
        driver.get(userURL);
        driver.findElement(By.cssSelector("input[value='" + SeleniumTestUtil.testusername + "']")).
                findElement(By.xpath("../..")).
                findElement(By.cssSelector(".editUser")).click();
        driver.findElement(By.cssSelector("#s2id_rolesList input")).click();
        driver.findElement(By.cssSelector("#s2id_rolesList input")).sendKeys("Contactadmin");
        driver.findElement(By.cssSelector(".select2-result-label")).click();
        driver.findElement(By.cssSelector("input[type='submit']")).click();
        Thread.sleep(2000);
        Assert.assertTrue(usersService.findByUserIdWithPermissions(user.getId()).hasRole(2));
    }

    @Test
    public void testT310Login() throws Exception{
        Assert.assertNotNull("Login failed: Hi, asdf not visible", driver.findElement(By.linkText("Hi, " + SeleniumTestUtil.username)));
    }

    @Test
    public void testT311LoginIvalidPassword() throws Exception{
        driver.findElement(By.linkText("Hi, "+SeleniumTestUtil.username)).click();
        driver.findElement(By.xpath("//div[@id='drop2']/div[2]/div[2]/a")).click();
        driver.findElement(By.name("j_username")).clear();
        driver.findElement(By.name("j_username")).sendKeys(SeleniumTestUtil.username);
        driver.findElement(By.name("j_password")).clear();
        driver.findElement(By.name("j_password")).sendKeys("password");
        driver.findElement(By.cssSelector("input[type='submit']")).click();
        Assert.assertTrue(driver.getCurrentUrl().contains("login"));
    }

    @Test
    public void testT312LoginIvalidPassword() throws Exception{
        driver.findElement(By.linkText("Hi, "+SeleniumTestUtil.username)).click();
        driver.findElement(By.xpath("//div[@id='drop2']/div[2]/div[2]/a")).click();
        driver.findElement(By.name("j_username")).clear();
        driver.findElement(By.name("j_username")).sendKeys("testuser");
        driver.findElement(By.name("j_password")).clear();
        driver.findElement(By.name("j_password")).sendKeys("password");
        driver.findElement(By.cssSelector("input[type='submit']")).click();
        Assert.assertTrue(driver.getCurrentUrl().contains("login"));
    }

    @Test
    public void testT315EditUserPassword() throws Exception{
        cleanDB();
        createTestContact();
        createTestUser();
        driver.findElement(By.linkText("Hi, " + SeleniumTestUtil.username)).click();
        driver.findElement(By.xpath("//div[@id='drop2']/div[2]/div[2]/a")).click();
        driver.findElement(By.name("j_username")).clear();
        driver.findElement(By.name("j_username")).sendKeys(SeleniumTestUtil.testusername);
        driver.findElement(By.name("j_password")).clear();
        driver.findElement(By.name("j_password")).sendKeys(SeleniumTestUtil.testpassword);
        driver.findElement(By.cssSelector("input[type='submit']")).click();
        driver.findElement(By.linkText("Hi, " + SeleniumTestUtil.testusername)).click();
        driver.findElement(By.xpath("//div[@id='drop2']/div[2]/div[1]/a")).click();
        driver.findElement(By.id("update_oldPW")).clear();
        driver.findElement(By.id("update_oldPW")).sendKeys(SeleniumTestUtil.testpassword);
        driver.findElement(By.id("update_userEntity_password")).clear();
        driver.findElement(By.id("update_userEntity_password")).sendKeys("MyNewPa55word");
        driver.findElement(By.id("update_confirmPW")).clear();
        driver.findElement(By.id("update_confirmPW")).sendKeys("MyNewPa55word");
        driver.findElement(By.cssSelector("#update input[type='submit']")).click();
        Assert.assertEquals(SeleniumTestUtil.testpasswordHash, usersRepository.findByUsername(SeleniumTestUtil.testusername).getPassword());
    }

    @Test
    public void testT316EditUserPasswordInvalidOldPw() throws Exception{
        cleanDB();
        createTestContact();
        createTestUser();
        driver.findElement(By.linkText("Hi, " + SeleniumTestUtil.username)).click();
        driver.findElement(By.xpath("//div[@id='drop2']/div[2]/div[2]/a")).click();
        driver.findElement(By.name("j_username")).clear();
        driver.findElement(By.name("j_username")).sendKeys(SeleniumTestUtil.testusername);
        driver.findElement(By.name("j_password")).clear();
        driver.findElement(By.name("j_password")).sendKeys(SeleniumTestUtil.testpassword);
        driver.findElement(By.cssSelector("input[type='submit']")).click();
        driver.findElement(By.linkText("Hi, " + SeleniumTestUtil.testusername)).click();
        driver.findElement(By.xpath("//div[@id='drop2']/div[2]/div[1]/a")).click();
        driver.findElement(By.id("update_oldPW")).clear();
        driver.findElement(By.id("update_oldPW")).sendKeys("nonsense");
        driver.findElement(By.id("update_userEntity_password")).clear();
        driver.findElement(By.id("update_userEntity_password")).sendKeys("MyNewPa55word");
        driver.findElement(By.id("update_confirmPW")).clear();
        driver.findElement(By.id("update_confirmPW")).sendKeys("MyNewPa55word");
        driver.findElement(By.cssSelector("#update input[type='submit']")).click();
        Assert.assertTrue(SeleniumTestUtil.isElementPresent(driver, By.id("formerrors")));
    }

    @Test
    public void testT317EditUserTooShortPassword() throws Exception{
        cleanDB();
        createTestContact();
        createTestUser();
        driver.findElement(By.linkText("Hi, " + SeleniumTestUtil.username)).click();
        driver.findElement(By.xpath("//div[@id='drop2']/div[2]/div[2]/a")).click();
        driver.findElement(By.name("j_username")).clear();
        driver.findElement(By.name("j_username")).sendKeys(SeleniumTestUtil.testusername);
        driver.findElement(By.name("j_password")).clear();
        driver.findElement(By.name("j_password")).sendKeys(SeleniumTestUtil.testpassword);
        driver.findElement(By.cssSelector("input[type='submit']")).click();
        driver.findElement(By.linkText("Hi, " + SeleniumTestUtil.testusername)).click();
        driver.findElement(By.xpath("//div[@id='drop2']/div[2]/div[1]/a")).click();
        driver.findElement(By.id("update_oldPW")).clear();
        driver.findElement(By.id("update_oldPW")).sendKeys(SeleniumTestUtil.testpassword);
        driver.findElement(By.id("update_userEntity_password")).clear();
        driver.findElement(By.id("update_userEntity_password")).sendKeys("newpw");
        driver.findElement(By.id("update_confirmPW")).clear();
        driver.findElement(By.id("update_confirmPW")).sendKeys("newpw");
        driver.findElement(By.cssSelector("#update input[type='submit']")).click();
        Assert.assertTrue(SeleniumTestUtil.isElementPresent(driver, By.id("formerrors")));
    }

    @Test
    public void testT318EditUserNotMatchingPasswords() throws Exception{
        cleanDB();
        createTestContact();
        createTestUser();
        driver.findElement(By.linkText("Hi, " + SeleniumTestUtil.username)).click();
        driver.findElement(By.xpath("//div[@id='drop2']/div[2]/div[2]/a")).click();
        driver.findElement(By.name("j_username")).clear();
        driver.findElement(By.name("j_username")).sendKeys(SeleniumTestUtil.testusername);
        driver.findElement(By.name("j_password")).clear();
        driver.findElement(By.name("j_password")).sendKeys(SeleniumTestUtil.testpassword);
        driver.findElement(By.cssSelector("input[type='submit']")).click();
        driver.findElement(By.linkText("Hi, " + SeleniumTestUtil.testusername)).click();
        driver.findElement(By.xpath("//div[@id='drop2']/div[2]/div[1]/a")).click();
        driver.findElement(By.id("update_oldPW")).clear();
        driver.findElement(By.id("update_oldPW")).sendKeys(SeleniumTestUtil.testpassword);
        driver.findElement(By.id("update_userEntity_password")).clear();
        driver.findElement(By.id("update_userEntity_password")).sendKeys("newpw");
        driver.findElement(By.id("update_confirmPW")).clear();
        driver.findElement(By.id("update_confirmPW")).sendKeys("newpw");
        driver.findElement(By.cssSelector("#update input[type='submit']")).click();
        Assert.assertTrue(SeleniumTestUtil.isElementPresent(driver, By.id("formerrors")));
    }

    @Test
    public void testT319EditUserEmptyPassword() throws Exception{
        cleanDB();
        createTestContact();
        createTestUser();
        driver.findElement(By.linkText("Hi, " + SeleniumTestUtil.username)).click();
        driver.findElement(By.xpath("//div[@id='drop2']/div[2]/div[2]/a")).click();
        driver.findElement(By.name("j_username")).clear();
        driver.findElement(By.name("j_username")).sendKeys(SeleniumTestUtil.testusername);
        driver.findElement(By.name("j_password")).clear();
        driver.findElement(By.name("j_password")).sendKeys(SeleniumTestUtil.testpassword);
        driver.findElement(By.cssSelector("input[type='submit']")).click();
        driver.findElement(By.linkText("Hi, " + SeleniumTestUtil.testusername)).click();
        driver.findElement(By.xpath("//div[@id='drop2']/div[2]/div[1]/a")).click();
        driver.findElement(By.id("update_oldPW")).clear();
        driver.findElement(By.id("update_oldPW")).sendKeys(SeleniumTestUtil.testpassword);
        driver.findElement(By.id("update_userEntity_password")).clear();
        driver.findElement(By.id("update_confirmPW")).clear();
        driver.findElement(By.cssSelector("#update input[type='submit']")).click();
        Assert.assertTrue(SeleniumTestUtil.isElementPresent(driver, By.id("formerrors")));
    }

    @Test
    public void testT320Logout() throws Exception{
        driver.findElement(By.linkText("Hi, " + SeleniumTestUtil.username)).click();
        driver.findElement(By.xpath("//div[@id='drop2']/div[2]/div[2]/a")).click();
        Assert.assertTrue(driver.getCurrentUrl().contains("login"));
    }

    @Test
    public void testT321DeactivateUser() throws Exception{
        cleanDB();
        createTestContact();
        createTestUser();
        UsersEntity user = usersRepository.findByUsername(SeleniumTestUtil.testusername);
        user.setDeactivated(false);
        usersRepository.save(user);
        driver.get(userURL);
        driver.findElement(By.cssSelector("input[value='" + SeleniumTestUtil.testusername + "']")).
                findElement(By.xpath("../..")).
                findElement(By.cssSelector(".deleteUser")).click();
        Thread.sleep(1000);
        driver.findElement(By.cssSelector("button.confirm")).click();
        Thread.sleep(1000);
        driver.findElement(By.cssSelector("button.confirm")).click();
        Thread.sleep(4000);
        Assert.assertEquals(true, usersRepository.findByUsername(SeleniumTestUtil.testusername).getDeactivated());
    }

    @Test
    public void testT322ActivateUser() throws Exception{
        cleanDB();
        createTestContact();
        createTestUser();
        UsersEntity user = usersRepository.findByUsername(SeleniumTestUtil.testusername);
        user.setDeactivated(true);
        usersRepository.save(user);
        driver.get(userURL);
        driver.findElement(By.cssSelector("input[value='" + SeleniumTestUtil.testusername + "']")).
                findElement(By.xpath("../..")).
                findElement(By.cssSelector(".deleteUser")).click();
        Thread.sleep(500);
        driver.findElement(By.cssSelector("button.confirm")).click();
        Thread.sleep(500);
        driver.findElement(By.cssSelector("button.confirm")).click();
        Thread.sleep(3000);
        Assert.assertEquals(false, usersRepository.findByUsername(SeleniumTestUtil.testusername).getDeactivated());
    }

    public void createTestContact() {
        if(contactsRepository.findByNameLikeOrSurnameLikeAllIgnoreCase("Max", "Mutermann").size() == 0){
            ContactsEntity testContact = new ContactsEntity();
            testContact.setTitle("Mister");
            testContact.setName("Max");
            testContact.setSurname("Mustermann");
            testContact.setAcademicDegreeAfter("PhD");
            testContact.setCountriesByCountriesId(countriesRepository.findOne("AT"));
            testContact.setState("N");
            testContact.setAddress("Musterstraße 1");
            testContact.setPostleitzahlByPlz(postleitzahlRepository.findByPlz(2151));
            testContact.setOrganisationsByOrganisationsId(organisationsRepository.findOne(1));
            testContact.setNewsletter(true);
            testContact.setCreateDate(new Timestamp(new Date().getTime()));
            contactsRepository.save(testContact);

            testContact = contactsRepository.findOne(testContact.getId());
            TelnumbersEntity telnumbersEntity = new TelnumbersEntity();
            telnumbersEntity.setValue("+43 650 12345678");
            telnumbersEntity.setImportant(true);
            ;
            telnumbersEntity.setContact(testContact);
            testContact.getTelnumbers().add(telnumbersEntity);
            EmailsEntity emailsEntity = new EmailsEntity();
            emailsEntity.setImportant(true);
            emailsEntity.setValue("test@test.at");
            emailsEntity.setContact(testContact);
            testContact.getEmails().add(emailsEntity);
            contactsRepository.save(testContact);
        }
    }

    public void createTestUser(){
        UsersEntity usersEntity = new UsersEntity();
        usersEntity.setUsername(SeleniumTestUtil.testusername);
        usersEntity.setPassword(SeleniumTestUtil.testpasswordHash);
        usersEntity.setDeactivated(false);
        for(RolesEntity role : rolesRepository.findAll()){
            if(role.getId() != 6){
                usersEntity.getRoles().add(role);
            }
        }
        usersEntity.setContactsByContactsId(contactsRepository.findByNameLikeOrSurnameLikeAllIgnoreCase("Max", "Mustermann").get(0));
        usersRepository.save(usersEntity);
    }

    @After
    @Transactional
    public void cleanDB() {
        if (usersRepository.findByUsername(SeleniumTestUtil.testusername) != null) {
            UsersEntity user = usersService.findByUsername(SeleniumTestUtil.testusername);
            user = usersService.findByUserIdWithPermissions(user.getId());
            user.getRoles().clear();
            usersRepository.delete(user.getId());
        }
        for (ContactsEntity contactsEntity : contactsRepository.findByNameLikeOrSurnameLikeAllIgnoreCase("Max", "Mustermann")) {
            contactsRepository.delete(contactsEntity.getId());
        }
    }

    @After
    public void logoutWrongUser(){
        if(SeleniumTestUtil.isElementPresent(driver,By.linkText("Hi, " + SeleniumTestUtil.testusername))){
            driver.findElement(By.xpath("//div[@id='drop2']/div[2]/div[2]/a")).click();
            SeleniumTestUtil.login(driver);
        }
    }
}
