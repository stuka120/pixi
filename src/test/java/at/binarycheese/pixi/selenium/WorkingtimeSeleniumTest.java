package at.binarycheese.pixi.selenium;

import at.binarycheese.pixi.domain.WorkingtimesEntity;
import at.binarycheese.pixi.repositories.UsersRepository;
import at.binarycheese.pixi.repositories.WorkingTimesRepository;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

import java.sql.Timestamp;

/**
 * Created by Tortuga on 24.03.2015.
 */
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class WorkingtimeSeleniumTest {
    private static WebDriver driver;
    WebElement element;
    private static String baseURL = "http://localhost:17001/";
    private static String workingtime = baseURL + "workingtime/index.action";

    @Autowired
    private WorkingTimesRepository workingTimesRepository;
    @Autowired
    private UsersRepository usersRepository;

    @BeforeClass
    public static void getWebDriver() {
        driver = SeleniumTestUtil.getWebDriver();
    }

    @Before
    public void Login() {
        SeleniumTestUtil.login(driver);
        cleanDatabase();
        driver.get(workingtime);
    }

    @Test
    public void testT601CreateWorkingTimeRecord() throws Exception {
        driver.findElement(By.id("newButton")).click();
        driver.findElement(By.id("date")).clear();
        driver.findElement(By.id("date")).sendKeys("30.11.2014");
        driver.findElement(By.id("starttime")).clear();
        driver.findElement(By.id("starttime")).sendKeys("08:00");
        driver.findElement(By.id("endtime")).clear();
        driver.findElement(By.id("endtime")).sendKeys("16:00");
        driver.findElement(By.id("duration")).clear();
        driver.findElement(By.id("duration")).sendKeys("7");
        driver.findElement(By.id("myForm_workingtimesEntity_desc")).clear();
        driver.findElement(By.id("myForm_workingtimesEntity_desc")).sendKeys("Did some work");
        driver.findElement(By.id("submitButton")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("//label[contains(text(), '2014')]")).click();
        Thread.sleep(3000);
        driver.findElement(By.cssSelector("input[type=\"search\"]")).sendKeys("30.11.2014");
        Assert.assertEquals("Did some work", driver.findElement(By.cssSelector("div.descriptionCell")).getText());
        Assert.assertEquals("Did some work",
                workingTimesRepository.findByUsersByUserId_UsernameAndStartTime("asdf", new Timestamp(114, 10, 30, 8, 0, 0, 0)).getDesc());
    }

    @Test
    public void testT602CreateWorkingTimeRecordEmptyDate() throws Exception {
        driver.findElement(By.id("newButton")).click();
        driver.findElement(By.id("date")).clear();
        driver.findElement(By.id("starttime")).clear();
        driver.findElement(By.id("starttime")).sendKeys("08:00");
        driver.findElement(By.id("endtime")).clear();
        driver.findElement(By.id("endtime")).sendKeys("16:00");
        driver.findElement(By.id("duration")).clear();
        driver.findElement(By.id("duration")).sendKeys("8");
        driver.findElement(By.id("myForm_workingtimesEntity_desc")).clear();
        driver.findElement(By.id("myForm_workingtimesEntity_desc")).sendKeys("Did some work");
        driver.findElement(By.id("submitButton")).click();
        Assert.assertTrue(SeleniumTestUtil.isElementPresent(driver, By.cssSelector("ul.error")));
    }

    @Test
    public void testT603EditWorkingTimeRecordValid() throws Exception {
        createTestWTREntry();
        driver.get(workingtime);
        driver.findElement(By.xpath("//label[contains(text(), '2014')]")).click();
        Thread.sleep(3000);
        driver.findElement(By.cssSelector("input[type=\"search\"]")).sendKeys("30.11.2014");
        driver.findElement(By.cssSelector(".editButton")).click();
        driver.findElement(By.id("endtime")).clear();
        driver.findElement(By.id("endtime")).sendKeys("17:00");
        driver.findElement(By.id("submitButton")).click();
        Thread.sleep(3000);
        Assert.assertEquals(new Timestamp(0, 0, 0, 17, 0, 0, 0),
                workingTimesRepository.findByUsersByUserId_UsernameAndStartTime("asdf", new Timestamp(114, 10, 30, 8, 0, 0, 0)).getEndTime());
    }

    @Test
    public void testT604EditWorkingTimeNoDuration() throws Exception {
        createTestWTREntry();
        driver.get(workingtime);
        driver.findElement(By.xpath("//label[contains(text(), '2014')]")).click();
        Thread.sleep(3000);
        driver.findElement(By.cssSelector("input[type=\"search\"]")).sendKeys("30.11.2014");
        driver.findElement(By.cssSelector(".editButton")).click();
        driver.findElement(By.id("duration")).clear();
        driver.findElement(By.id("submitButton")).click();
        Assert.assertTrue(SeleniumTestUtil.isElementPresent(driver, By.cssSelector("ul.error")));
    }

    @Test
    public void testT605DeleteWorkingTimeRecord() throws Exception {
        createTestWTREntry();
        driver.get(workingtime);
        driver.findElement(By.xpath("//label[contains(text(), '2014')]")).click();
        Thread.sleep(3000);
        driver.findElement(By.cssSelector("input[type=\"search\"]")).sendKeys("30.11.2014");
        driver.findElement(By.cssSelector(".deleteButton")).click();
        Thread.sleep(500);
        driver.findElement(By.cssSelector("button.confirm")).click();
        Thread.sleep(2000);
        driver.findElement(By.cssSelector("button.confirm")).click();
        Assert.assertNull(workingTimesRepository.findByUsersByUserId_UsernameAndStartTime("asdf",
                new Timestamp(114, 10, 30, 8, 0, 0, 0)));
    }

    @After
    public void cleanDatabase() {
        Timestamp time = new Timestamp(114, 10, 30, 8, 0, 0, 0);
        WorkingtimesEntity entity = workingTimesRepository.findByUsersByUserId_UsernameAndStartTime("asdf", time);
        if (entity != null) {
            workingTimesRepository.delete(entity.getId());
        }
    }

    @AfterClass
    public static void closeBrowser() {
        driver.quit();
    }

    public void createTestWTREntry() {
        WorkingtimesEntity entity = new WorkingtimesEntity();
        entity.setUsersByUserId(usersRepository.findByUsername(SeleniumTestUtil.username));
        entity.setDesc("Did some work");
        entity.setDuration(7.0);
        entity.setStartTime(new Timestamp(114, 10, 30, 8, 0, 0, 0));
        entity.setEndTime(new Timestamp(0, 0, 0, 16, 0, 0, 0));
        workingTimesRepository.save(entity);
    }
}
