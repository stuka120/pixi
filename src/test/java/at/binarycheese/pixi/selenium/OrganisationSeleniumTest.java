package at.binarycheese.pixi.selenium;

import at.binarycheese.pixi.domain.OrganisationsEntity;
import at.binarycheese.pixi.repositories.OrganisationsRepository;
import com.opensymphony.xwork2.interceptor.annotations.After;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

/**
 * Created by Tortuga on 16/04/2015.
 */
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class OrganisationSeleniumTest {
    private static WebDriver driver;
    private static String baseURL = "http://localhost:17001/";
    private static String organisationsURL = baseURL + "organisations/index.action";

    @Autowired
    private OrganisationsRepository organisationsRepository;

    @BeforeClass
    public static void getWebDriver() {
        driver = SeleniumTestUtil.getWebDriver();
    }

    @Before
    public void Login(){
        SeleniumTestUtil.login(driver);
        cleanDatabase();
        driver.get(organisationsURL);
    }

    @Test
    public void testT101CreateOrganisation() throws Exception {
        driver.get(organisationsURL);
        driver.findElement(By.cssSelector(".small-12 a.button")).click();
        driver.findElement(By.id("organisationsEntity_name")).clear();
        driver.findElement(By.id("organisationsEntity_name")).sendKeys("TestOrg");
        driver.findElement(By.id("organisationsEntity_desc")).clear();
        driver.findElement(By.id("organisationsEntity_desc")).sendKeys("This is an organisation for testing purposes.");
        driver.findElement(By.xpath("//input[@value='Organisation erstellen']")).click();
        Thread.sleep(2000);
        driver.get(organisationsURL);
        driver.findElement(By.cssSelector("input[type=\"search\"]")).clear();
        driver.findElement(By.cssSelector("input[type=\"search\"]")).sendKeys("TestOrg");
        Assert.assertEquals("This is an organisation for testing purposes.", driver.findElement(By.xpath("//table[@id='organisationTable']/tbody/tr/td[2]")).getText());
        Assert.assertNotEquals(0, organisationsRepository.findByName("TestOrg").size());
    }

    @Test
    public void testT102CreateOrganisationEmptyName() throws Exception {
        driver.get(organisationsURL);
        driver.findElement(By.cssSelector(".small-12 a.button")).click();
        driver.findElement(By.id("organisationsEntity_desc")).clear();
        driver.findElement(By.id("organisationsEntity_desc")).sendKeys("This is an organisation for testing purposes.");
        driver.findElement(By.xpath("//input[@value='Organisation erstellen']")).click();
        Assert.assertTrue(SeleniumTestUtil.isElementPresent(driver, By.cssSelector(".alert-box")));
    }

    @Test
    public void testT103CreateOrganisationDuplicateName() throws Exception {
        createTestOrg();
        driver.get(organisationsURL);
        driver.findElement(By.cssSelector(".small-12 a.button")).click();
        driver.findElement(By.id("organisationsEntity_name")).clear();
        driver.findElement(By.id("organisationsEntity_name")).sendKeys("TestOrg");
        driver.findElement(By.id("organisationsEntity_desc")).clear();
        driver.findElement(By.id("organisationsEntity_desc")).sendKeys("This is an organisation for testing purposes.");
        driver.findElement(By.xpath("//input[@value='Organisation erstellen']")).click();
        Assert.assertTrue(SeleniumTestUtil.isElementPresent(driver, By.cssSelector(".alert-box")));
    }

    @Test
    public void testT104EditOrganisationName() throws Exception {
        cleanDatabase();
        createTestOrg();
        driver.get(organisationsURL);
        driver.findElement(By.cssSelector("input[type=\"search\"]")).clear();
        driver.findElement(By.cssSelector("input[type=\"search\"]")).sendKeys("TestOrg");
        driver.findElement(By.xpath("//table[@id='organisationTable']/tbody/tr/td[4]/a")).click();
        driver.findElement(By.id("organisationsEntity_name")).clear();
        driver.findElement(By.id("organisationsEntity_name")).sendKeys("TestOrg AG");
        driver.findElement(By.xpath("//input[@value='Organisation Bearbeiten']")).click();
        Assert.assertNotEquals(1, organisationsRepository.findByName("TestOrg AG").size());
    }

    @Test
    public void testT105EditOrganisationNoData() throws Exception {
        cleanDatabase();
        createTestOrg();
        driver.get(organisationsURL);
        driver.findElement(By.cssSelector("input[type=\"search\"]")).clear();
        driver.findElement(By.cssSelector("input[type=\"search\"]")).sendKeys("TestOrg");
        driver.findElement(By.xpath("//table[@id='organisationTable']/tbody/tr/td[4]/a")).click();
        driver.findElement(By.id("organisationsEntity_name")).clear();
        driver.findElement(By.xpath("//input[@value='Organisation Bearbeiten']")).click();
        Assert.assertTrue(SeleniumTestUtil.isElementPresent(driver, By.cssSelector(".alert-box")));
    }

    @Test
    public void testT106DeleteOrganisation() throws Exception {
        cleanDatabase();
        createTestOrg();
        driver.get(organisationsURL);
        driver.findElement(By.cssSelector("input[type=\"search\"]")).clear();
        driver.findElement(By.cssSelector("input[type=\"search\"]")).sendKeys("TestOrg");
        driver.findElement(By.xpath("//table[@id='organisationTable']/tbody/tr/td[5]/a")).click();
        Thread.sleep(1000);
        driver.findElement(By.cssSelector("button.confirm")).click();
        Thread.sleep(1000);
        driver.findElement(By.cssSelector("button.confirm")).click();
        driver.get(organisationsURL);
        driver.findElement(By.cssSelector("input[type=\"search\"]")).clear();
        driver.findElement(By.cssSelector("input[type=\"search\"]")).sendKeys("TestOrg");
        Assert.assertTrue(SeleniumTestUtil.isElementPresent(driver, By.cssSelector(".dataTables_empty")));
        Assert.assertEquals(0,organisationsRepository.findByName("TestOrg").size());
    }

    public void createTestOrg(){
        OrganisationsEntity org = new OrganisationsEntity();
        org.setDesc("This is an organisation for testing purposes.");
        org.setName("TestOrg");
        organisationsRepository.save(org);
    }

    @After
    public void cleanDatabase() {
        organisationsRepository.delete(organisationsRepository.findByName("TestOrg"));
        organisationsRepository.delete(organisationsRepository.findByName("TestOrg AG"));
    }

    @AfterClass
    public static void closeBrowser() {
        driver.quit();
    }
}
