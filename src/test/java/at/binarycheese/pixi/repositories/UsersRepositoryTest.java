package at.binarycheese.pixi.repositories;

import at.binarycheese.pixi.domain.OrganisationsEntity;
import at.binarycheese.pixi.domain.UsersEntity;
import org.junit.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Dominik on 12.02.2015.
 */

@ContextConfiguration(locations={"classpath:test-config.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@Transactional
public class UsersRepositoryTest {

    @Autowired
    UsersRepository usersRepository;
    @Autowired
    OrganisationsRepository organisationsRepository;

    @Before
    public void setup() {
        UsersEntity usersEntity = new UsersEntity();
        usersEntity.setUsername("TestUser");
        usersEntity.setPassword("TestPW");
        usersRepository.save(usersEntity);
    }

    @Test
    public void testFindByUsername() {
        OrganisationsEntity organisationsEntity = organisationsRepository.findOne(4);
        UsersEntity usersEntity = usersRepository.findByUsername("TestUser");
        Assert.assertEquals("TestUser", usersEntity.getUsername());
        Assert.assertEquals("TestPW", usersEntity.getPassword());
    }
}
