package at.binarycheese.pixi.repositories;

import at.binarycheese.pixi.domain.ContactsEntity;
import at.binarycheese.pixi.domain.PostleitzahlEntity;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@ContextConfiguration(locations = {"classpath:test-config.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@Transactional
public class ContactsRepositoryTest {

    @Autowired
    ContactsRepository contactsRepository;

    @Autowired
    PostleitzahlRepository postleitzahlRepository;

    private ContactsEntity contactsEntity;
    private PostleitzahlEntity postleitzahlEntity;


    @Before
    public void setUp() throws Exception {
        contactsEntity = new ContactsEntity();
        contactsEntity.setAcademicDegreeAfter("BSc. MSc.");
        contactsEntity.setAcademicDegreeBefore("PhD.");
        contactsEntity.setAddress("TestGasse 8/11/7");
        contactsEntity.setHompage("TestHomePage");
        contactsEntity.setInfo("TestInfo");
        contactsEntity.setName("TestName");
        contactsEntity.setSurname("TestSurname");

        if (postleitzahlRepository.findByPlz(1100) == null) {
            postleitzahlEntity = new PostleitzahlEntity();
            postleitzahlEntity.setPlz(1100);
            postleitzahlEntity.setName("W");
            postleitzahlRepository.save(postleitzahlEntity);
            contactsEntity.setPostleitzahlByPlz(postleitzahlEntity);
        } else {
            contactsEntity.setPostleitzahlByPlz(postleitzahlRepository.findByPlz(1100));
        }

        contactsEntity.setState("Wien");
        contactsEntity.setNewsletter(true);
        contactsEntity.setTitle("Herr");
        contactsRepository.save(contactsEntity);
    }

    @Test
    public void testFindDistinctById() throws Exception {
        ContactsEntity dbContact = contactsRepository.findDistinctById(contactsEntity.getId());
        Assert.assertEquals("BSc. MSc.", dbContact.getAcademicDegreeAfter());
        Assert.assertEquals("PhD.", dbContact.getAcademicDegreeBefore());
        Assert.assertEquals("TestGasse 8/11/7", dbContact.getAddress());
        Assert.assertEquals("TestHomePage", dbContact.getHompage());
        Assert.assertEquals("TestInfo", dbContact.getInfo());
        Assert.assertEquals("TestName", dbContact.getName());
        Assert.assertEquals("TestSurname", dbContact.getSurname());
        Assert.assertEquals(1100, dbContact.getPostleitzahlByPlz().getPlz());
        Assert.assertEquals("Wien", dbContact.getState());
        Assert.assertEquals(true, dbContact.getNewsletter());
        Assert.assertEquals("Herr", dbContact.getTitle());
        Assert.assertNotEquals(0, dbContact.getId());
    }

    @Test
    public void testFindByNameLikeOrSurnameLike() throws Exception {
        List<ContactsEntity> dbContact = contactsRepository.findByNameLikeOrSurnameLikeAllIgnoreCase("%Name%", "%Name%");
        Assert.assertEquals("TestName", dbContact.get(0).getName());
        Assert.assertEquals("TestSurname", dbContact.get(0).getSurname());
        Assert.assertNotEquals(0, dbContact.get(0).getId());
    }
}