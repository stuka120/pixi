package at.binarycheese.pixi.domain;

import org.junit.Test;

import static org.junit.Assert.*;

public class UsersEntityTest {

    @Test
    public void testGetRoles() throws Exception {
        UsersEntity usersEntity = new UsersEntity();
        usersEntity.setId(1);
        usersEntity.setPlannedHours(Math.PI);
        usersEntity.setPassword("TestPW");
        usersEntity.setUsername("TestUsername");

        RolesEntity admin = new RolesEntity();
        admin.setId(1);
        admin.setName("admin");
        usersEntity.getRoles().add(admin);

        RolesEntity employee = new RolesEntity();
        employee.setId(2);
        employee.setName("employee");
        usersEntity.getRoles().add(employee);

        assertEquals("admin", ((RolesEntity)usersEntity.getRoles().toArray()[0]).getName());
        assertEquals("employee", ((RolesEntity) usersEntity.getRoles().toArray()[1]).getName());
    }
}