package at.binarycheese.pixi.domain;

import org.junit.Before;
import org.junit.Test;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

import static org.junit.Assert.*;

public class ContactsEntityTest {

    ContactsEntity contactsEntity;

    @Before
    public void setup() {
        contactsEntity = new ContactsEntity();
        contactsEntity.setId(1);
        contactsEntity.setName("Max");
        contactsEntity.setSurname("Mustermann");
        contactsEntity.setNewsletter(true);
        contactsEntity.setInfo("TestInfo");
        contactsEntity.setAddress("Trazerberggasse 57b");
        contactsEntity.setCreateDate(new Timestamp(Calendar.getInstance().getTimeInMillis()));
        contactsEntity.setUpdateDate(new Timestamp(Calendar.getInstance().getTimeInMillis() + 604800));
        contactsEntity.setAcademicDegreeAfter("BA.");
        contactsEntity.setAcademicDegreeBefore("Dr.");
        contactsEntity.setHompage("binarycheese.at");

        PostleitzahlEntity postleitzahlEntity = new PostleitzahlEntity();
        postleitzahlEntity.setId(1130);
        postleitzahlEntity.setName("W");

        contactsEntity.setPostleitzahlByPlz(postleitzahlEntity);
        contactsEntity.setState("Wien");
        contactsEntity.setTitle("Herr");
    }

    @Test
    public void testGetLanguages() {

        LanguagesEntity englisch = new LanguagesEntity();
        englisch.setId(1);
        englisch.setEnglishName("English");
        englisch.setGermanName("Englisch");

        LanguagesEntity deutsch = new LanguagesEntity();
        deutsch.setId(2);
        deutsch.setEnglishName("German");
        deutsch.setGermanName("Deutsch");

        SpeaksEntity speaksEnglish = new SpeaksEntity();
        speaksEnglish.getPk().setContactsByContactsPersonId(contactsEntity);
        speaksEnglish.getPk().setLanguagesByLanguagesId(englisch);

        SpeaksEntity speaksDeutsch = new SpeaksEntity();
        speaksDeutsch.getPk().setContactsByContactsPersonId(contactsEntity);
        speaksDeutsch.getPk().setLanguagesByLanguagesId(deutsch);

        contactsEntity.getSpeaksesById().add(speaksEnglish);
        contactsEntity.getSpeaksesById().add(speaksDeutsch);

        List<LanguagesEntity> languages = contactsEntity.getLanguages();
        assertEquals("Englisch", languages.get(0).getGermanName());
        assertEquals("Deutsch", languages.get(1).getGermanName());
    }

}