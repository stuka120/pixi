package at.binarycheese.pixi.actions;

import at.binarycheese.pixi.domain.UsersEntity;
import at.binarycheese.pixi.domain.WorkingtimesEntity;
import at.binarycheese.pixi.service.UsersService;
import at.binarycheese.pixi.service.WorkingTimesService;
import at.binarycheese.pixi.service.impl.UsersServiceImpl;
import at.binarycheese.pixi.service.impl.WorkingTimesServiceImpl;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionProxy;
import org.apache.struts2.StrutsSpringTestCase;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.internal.util.reflection.Whitebox;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

import java.sql.Timestamp;
import java.util.*;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;

@RunWith(PowerMockRunner.class)
@PrepareForTest(WorkingtimeAction.class)
public class WorkingtimeActionTest extends StrutsSpringTestCase {

    @Override
    protected String[] getContextLocations() {
        return new String[] {"test-config.xml"};
    }

    @Before
    public void setupAuthority() {
        // add principal object to SecurityContextHolder
        Set<GrantedAuthority> setAuth = new HashSet<>();
        setAuth.add(new SimpleGrantedAuthority("testRole"));
        User user = new User("Secure", "Secure", true, true, true, true, setAuth);
                /* fill user object */

        Authentication auth = new UsernamePasswordAuthenticationToken(user,null);

        SecurityContextHolder.getContext().setAuthentication(auth);
    }

    @Test
    public void testCreate() throws Exception {
        //First add a new Entry into the DB
//        request.setParameter("workingtimesEntity.id","");
        request.removeAllParameters();
        request.setParameter("date", "21.02.2015");
        request.setParameter("inDate", "22.02.2015");
        request.setParameter("starttime", "09:30");
        request.setParameter("endtime", "12:30");
        request.setParameter("workingtimesEntity.duration", "3");
        request.setParameter("endHolidayDate", "");
        request.setParameter("inEndHolidayDate", "");
        request.setParameter("workingtimesEntity.desc", "testCreateDesc");


        ActionProxy proxy = getActionProxy("/workingtime/save");
        proxy.setExecuteResult(false);
        assertNotNull(proxy);
        WorkingtimeAction workingtimeAction = (WorkingtimeAction) proxy.getAction();
        assertNotNull(workingtimeAction);

        String result = proxy.execute();
//        assertEquals(Action.NONE, result);
        assertEquals("testCreateDesc", workingtimeAction.getWorkingTimesService().findById(workingtimeAction.getWorkingtimesEntity().getId()).getDesc());

    }

    @Test
    public void testBrowse() throws Exception {
        //First add a new Entry into the DB
        request.setParameter("workingtimesEntity.id","");
        request.setParameter("date", "15.02.2015");
        request.setParameter("inDate", "15.02.2015");
        request.setParameter("starttime", "10:30");
        request.setParameter("endtime", "11:30");
        request.setParameter("workingtimesEntity.duration", "1");
        request.setParameter("endHolidayDate", "");
        request.setParameter("inEndHolidayDate", "");
        request.setParameter("workingtimesEntity.desc", "testBrowseDesc");


        ActionProxy proxy = getActionProxy("/workingtime/save");
        assertNotNull(proxy);
        WorkingtimeAction workingtimeAction = (WorkingtimeAction) proxy.getAction();
        assertNotNull(workingtimeAction);

        proxy.execute();

        request.removeAllParameters();
        //Now call the Method
        proxy = getActionProxy("/workingtime/index");
        assertNotNull(proxy);
        workingtimeAction = (WorkingtimeAction) proxy.getAction();
        assertNotNull(workingtimeAction);

        String result = proxy.execute();
        assertEquals(Action.SUCCESS, result);
    }

    @Test
    public void testMenu() throws Exception {
        request.setParameter("selectedUserForExport", "1");

        ActionProxy proxy = getActionProxy("/workingtime/menu");
        assertNotNull(proxy);
        WorkingtimeAction workingtimeAction = (WorkingtimeAction) proxy.getAction();
        assertNotNull(workingtimeAction);

        String result = proxy.execute();
        assertEquals(Action.SUCCESS, result);
    }

    @Test
    public void testTableWithUserIdIsNull() throws Exception {
        request.setParameter("selectedUserForExport", "0");

        ActionProxy proxy = getActionProxy("/workingtime/table");
        assertNotNull(proxy);
        WorkingtimeAction workingtimeAction = (WorkingtimeAction) proxy.getAction();
        assertNotNull(workingtimeAction);

        String result = proxy.execute();
        assertEquals(Action.SUCCESS, result);
    }

    @Test
    public void testTableWithUserIDIsValid() throws Exception {
        request.setParameter("selectedUserForExport", "1");

        ActionProxy proxy = getActionProxy("/workingtime/table");
        assertNotNull(proxy);
        WorkingtimeAction workingtimeAction = (WorkingtimeAction) proxy.getAction();
        assertNotNull(workingtimeAction);

        String result = proxy.execute();
        assertEquals(Action.SUCCESS, result);
    }

    @Test
    public void testTableWithValidSelectedDates() throws Exception {
        request.setParameter("selectedUserForExport", "1");

        ActionProxy proxy = getActionProxy("/workingtime/table");
        assertNotNull(proxy);
        WorkingtimeAction workingtimeAction = (WorkingtimeAction) proxy.getAction();
        assertNotNull(workingtimeAction);

        List<Timestamp> list = new ArrayList<>();
        list.add(new Timestamp(Calendar.getInstance().getTimeInMillis()));
        list.add(new Timestamp(Calendar.getInstance().getTimeInMillis()));
        List<WorkingtimesEntity> workingtimesEntityList = new ArrayList<>();
        WorkingtimesEntity workingtimesEntity = new WorkingtimesEntity();
        workingtimesEntity.setDesc("LOL");
        workingtimesEntity.setDuration(1D);
        workingtimesEntity.setStartTime((Timestamp)list.get(0).clone());
        workingtimesEntityList.add(workingtimesEntity);

        WorkingTimesService workingTimesService = PowerMockito.mock(WorkingTimesServiceImpl.class);
        PowerMockito.when(workingTimesService.findAllByUserId(anyInt())).thenReturn(workingtimesEntityList);
        Whitebox.setInternalState(workingtimeAction, "selectedDates", list);
        Whitebox.setInternalState(workingtimeAction, "workingTimesService", workingTimesService);


        String result = proxy.execute();
        assertEquals(Action.SUCCESS, result);
    }

    @Test
    public void testInputWithIDIsValid() throws Exception {
        //First add a new Entry into the DB
        request.setParameter("workingtimesEntity.id","");
        request.setParameter("date", "15.02.2015");
        request.setParameter("inDate", "15.02.2015");
        request.setParameter("starttime", "10:30");
        request.setParameter("endtime", "11:30");
        request.setParameter("workingtimesEntity.duration", "1");
        request.setParameter("endHolidayDate", "");
        request.setParameter("inEndHolidayDate", "");
        request.setParameter("workingtimesEntity.desc", "testInputDesc");


        ActionProxy proxy = getActionProxy("/workingtime/save");
        assertNotNull(proxy);
        WorkingtimeAction workingtimeAction = (WorkingtimeAction) proxy.getAction();
        assertNotNull(workingtimeAction);

        proxy.execute();

        request.setParameter("id", workingtimeAction.getWorkingtimesEntity().getId() + "");

        proxy = getActionProxy("/workingtime/edit");
        assertNotNull(proxy);
        workingtimeAction = (WorkingtimeAction) proxy.getAction();
        assertNotNull(workingtimeAction);

        String result = proxy.execute();
        assertEquals(Action.INPUT, result);
    }

    @Test
    public void testInputWithNoId() throws Exception {
        ActionProxy proxy = getActionProxy("/workingtime/edit");
        assertNotNull(proxy);
        WorkingtimeAction workingtimeAction = (WorkingtimeAction) proxy.getAction();
        assertNotNull(workingtimeAction);

        String result = proxy.execute();
        assertEquals(Action.INPUT, result);
    }

    @Test
    public void testDelete() throws Exception {
        //First add a new Entry into the DB
        request.removeAllParameters();
        request.setParameter("date", "16.02.2015");
        request.setParameter("inDate", "16.02.2015");
        request.setParameter("starttime", "11:30");
        request.setParameter("endtime", "12:30");
        request.setParameter("workingtimesEntity.duration", "1");
        request.setParameter("endHolidayDate", "");
        request.setParameter("inEndHolidayDate", "");
        request.setParameter("workingtimesEntity.desc", "testDeleteDesc");


        ActionProxy proxy = getActionProxy("/workingtime/save");
        assertNotNull(proxy);
        WorkingtimeAction workingtimeAction = (WorkingtimeAction) proxy.getAction();
        assertNotNull(workingtimeAction);

        proxy.execute();

        request.removeAllParameters();
        request.setParameter("id", workingtimeAction.getWorkingtimesEntity().getId() + "");

        proxy = getActionProxy("/workingtime/delete");
        assertNotNull(proxy);
        workingtimeAction = (WorkingtimeAction) proxy.getAction();
        assertNotNull(workingtimeAction);

        String result = proxy.execute();
        assertEquals(Action.SUCCESS, result);
        assertNull(workingtimeAction.getWorkingTimesService().findById(workingtimeAction.getId()));
    }

    @Test
    public void testExport() throws Exception {
        request.setParameter("selectedUserForExport", "1");

        ActionProxy proxy = getActionProxy("/workingtime/export");
        proxy.setExecuteResult(false);
        assertNotNull(proxy);
        WorkingtimeAction workingtimeAction = (WorkingtimeAction) proxy.getAction();
        assertNotNull(workingtimeAction);

        UsersEntity usersEntity = new UsersEntity();
        usersEntity.setUsername("Username");
        usersEntity.setPassword("Password");
        usersEntity.setPlannedHours(Math.PI);
        List<Timestamp> list = new ArrayList<>();
        for (int i = 0; i < 25; i++) {
            list.add(new Timestamp(Calendar.getInstance().getTimeInMillis()+ 2678400000L*i));
        }
        List<WorkingtimesEntity> workingtimesEntityList = new ArrayList<>();
        for (Timestamp timestamp : list) {
            WorkingtimesEntity workingtimesEntity = new WorkingtimesEntity();
            workingtimesEntity.setDesc("LOL");
            workingtimesEntity.setDuration(1D);
            workingtimesEntity.setStartTime((Timestamp)timestamp.clone());
            workingtimesEntityList.add(workingtimesEntity);
        }

        WorkingTimesService workingTimesService = PowerMockito.mock(WorkingTimesServiceImpl.class);
        UsersService usersService = PowerMockito.mock(UsersServiceImpl.class);
        PowerMockito.when(workingTimesService.findAllByUserId(anyInt())).thenReturn(workingtimesEntityList);
        PowerMockito.when(usersService.findById(anyInt())).thenReturn(usersEntity);
        Whitebox.setInternalState(workingtimeAction, "selectedDates", list);
        Whitebox.setInternalState(workingtimeAction, "workingTimesService", workingTimesService);
        Whitebox.setInternalState(workingtimeAction, "usersService", usersService);

        String result = proxy.execute();
        assertEquals(Action.SUCCESS, result);

    }
}