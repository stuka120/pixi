package at.binarycheese.pixi.actions;

import at.binarycheese.pixi.domain.ProjectEntity;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionProxy;
import org.apache.struts2.StrutsSpringTestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.File;
import java.io.FileInputStream;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ProjectAction.class)
public class ProjectActionTest extends StrutsSpringTestCase {

    @Override
    protected String[] getContextLocations() {
        return new String[]{"test-config.xml"};
    }

    @Test
    public void testBrowse() throws Exception {
        ActionProxy proxy = getActionProxy("/project/index");
        assertNotNull(proxy);
        ProjectAction projectAction = (ProjectAction) proxy.getAction();
        assertNotNull(projectAction);

        String result = proxy.execute();
        assertEquals(Action.SUCCESS, result);
    }

    @Test
    public void testInputWithoutID() throws Exception {
        ActionProxy proxy = getActionProxy("/project/create");
        assertNotNull(proxy);
        ProjectAction projectAction = (ProjectAction) proxy.getAction();
        assertNotNull(projectAction);

        String result = proxy.execute();
        assertEquals(Action.INPUT, result);
    }

    @Test
    public void testInputWithID() throws Exception {
        ProjectEntity projectEntity = saveEntity("testInputWithID");

        request.setParameter("id", "" + projectEntity.getId());

        ActionProxy proxy = getActionProxy("/project/create");
        assertNotNull(proxy);
        ProjectAction projectAction = (ProjectAction) proxy.getAction();
        assertNotNull(projectAction);

        String result = proxy.execute();
        assertEquals(Action.INPUT, result);
    }

    @Test
    public void testCreate() throws Exception {
        request.setParameter("projectEntity.projectnumber", "testCreateProjectnumber");
        request.setParameter("projectEntity.description", "testCreateDescription");
        request.setParameter("projectEntity.name", "testCreateName");
        request.setParameter("orgId", "1");

        ActionProxy proxy = getActionProxy("/project/save");
        assertNotNull(proxy);
        ProjectAction projectAction = (ProjectAction) proxy.getAction();
        assertNotNull(projectAction);

        String result = proxy.execute();
        assertEquals(Action.NONE, result);
        assertEquals("testCreateProjectnumber", projectAction.getProjectsService().findById(projectAction.getProjectEntity().getId()).getProjectnumber());
        assertEquals("testCreateDescription", projectAction.getProjectsService().findById(projectAction.getProjectEntity().getId()).getDescription());

    }

    @Test
    public void testTable() throws Exception {
        ActionProxy proxy = getActionProxy("/project/table");
        assertNotNull(proxy);
        ProjectAction projectAction = (ProjectAction) proxy.getAction();
        assertNotNull(projectAction);

        String result = proxy.execute();
        assertEquals(Action.SUCCESS, result);
    }

    @Test
    public void testDelete() throws Exception {
        ProjectEntity projectEntity = saveEntity("testDelete");

        request.setParameter("id", projectEntity.getId() + "");

        ActionProxy proxy = getActionProxy("/project/delete");
        assertNotNull(proxy);
        ProjectAction projectAction = (ProjectAction) proxy.getAction();
        assertNotNull(projectAction);

        String result = proxy.execute();
        assertEquals(Action.SUCCESS, result);
        assertEquals(5, projectAction.getProjectsService().findById(projectEntity.getId()).getProjectstagesByProjectStage().getId());
    }

    @Test
    public void testDetailsWithIdIsNull() throws Exception {
        request.setParameter("id", "");

        ActionProxy proxy = getActionProxy("/project/details");
        assertNotNull(proxy);
        ProjectAction projectAction = (ProjectAction) proxy.getAction();
        assertNotNull(projectAction);

        String result = proxy.execute();
        assertEquals(Action.ERROR, result);
    }

    @Test
    public void testDetailsWithValidID() throws Exception {
        ProjectEntity projectEntity = saveEntity("testDetailsWithValidID");

        request.setParameter("id", projectEntity.getId() + "");

        File mockfile = PowerMockito.mock(File.class);
        File subFile = PowerMockito.mock(File.class);
        PowerMockito.whenNew(File.class).withAnyArguments().thenReturn(mockfile);
        PowerMockito.when(mockfile.listFiles()).thenReturn(new File[]{subFile});
        PowerMockito.when(subFile.isFile()).thenReturn(true);

        ActionProxy proxy = getActionProxy("/project/details");
        assertNotNull(projectEntity);
        ProjectAction projectAction = (ProjectAction) proxy.getAction();
        assertNotNull(projectAction);

        String result = proxy.execute();
        assertEquals(Action.SUCCESS, result);
    }

    @Test
    public void testFileList() throws Exception {
        ProjectEntity projectEntity = saveEntity("testFileList");

        request.setParameter("id", projectEntity.getId() + "");

        File mockFile = PowerMockito.mock(File.class);
        File subFile = PowerMockito.mock(File.class);
        PowerMockito.whenNew(File.class).withAnyArguments().thenReturn(mockFile);
        PowerMockito.when(mockFile.listFiles()).thenReturn(new File[]{subFile});
        PowerMockito.when(subFile.isDirectory()).thenReturn(false);

        ActionProxy proxy = getActionProxy("/project/fileList");
        assertNotNull(projectEntity);
        ProjectAction projectAction = (ProjectAction) proxy.getAction();
        assertNotNull(projectAction);

        String result = proxy.execute();
        assertEquals(Action.SUCCESS, result);
    }

    @Test
    public void testDownload() throws Exception {
        File mockFile = PowerMockito.mock(File.class);
        FileInputStream fileInputStream = PowerMockito.mock(FileInputStream.class);
        PowerMockito.whenNew(File.class).withAnyArguments().thenReturn(mockFile);
        PowerMockito.whenNew(FileInputStream.class).withAnyArguments().thenReturn(fileInputStream);

        ActionProxy proxy = getActionProxy("/project/download");
        proxy.setExecuteResult(false);
        assertNotNull(proxy);
        ProjectAction projectAction = (ProjectAction) proxy.getAction();
        assertNotNull(projectAction);

        String result = proxy.execute();
        assertEquals(Action.SUCCESS, result);

    }

    @Test
    public void testDeleteFile() throws Exception {
        File mockFile = PowerMockito.mock(File.class);
        PowerMockito.whenNew(File.class).withAnyArguments().thenReturn(mockFile);
        PowerMockito.when(mockFile.delete()).thenReturn(true);

        ActionProxy proxy = getActionProxy("/project/deleteFile");
        assertNotNull(proxy);
        ProjectAction projectAction = (ProjectAction) proxy.getAction();
        assertNotNull(projectAction);

        String result = proxy.execute();
        assertEquals(Action.NONE, result);
    }

    @Test
    public void testDownloadTemplate() throws Exception {
        File mockFile = PowerMockito.mock(File.class);
        FileInputStream fileInputStream = PowerMockito.mock(FileInputStream.class);
        PowerMockito.whenNew(File.class).withAnyArguments().thenReturn(mockFile);
        PowerMockito.whenNew(FileInputStream.class).withAnyArguments().thenReturn(fileInputStream);

        ActionProxy proxy = getActionProxy("/project/downloadTemplate");
        proxy.setExecuteResult(false);
        assertNotNull(proxy);
        ProjectAction projectAction = (ProjectAction) proxy.getAction();
        assertNotNull(projectAction);

        String result = proxy.execute();
        assertEquals(Action.SUCCESS, result);
    }

    @Test
    public void testTemplateList() throws Exception {
        File mockFile = PowerMockito.mock(File.class);
        File subFile = PowerMockito.mock(File.class);
        PowerMockito.whenNew(File.class).withAnyArguments().thenReturn(mockFile);
        PowerMockito.when(mockFile.listFiles()).thenReturn(new File[]{subFile});
        PowerMockito.when(subFile.isDirectory()).thenReturn(false);

        ActionProxy proxy = getActionProxy("/project/templateList");
        assertNotNull(proxy);
        ProjectAction projectAction = (ProjectAction) proxy.getAction();
        assertNotNull(projectAction);

        String result = proxy.execute();
        assertEquals(Action.SUCCESS, result);
    }

    public ProjectEntity saveEntity(String identifier) throws Exception {
        request.setParameter("projectEntity.projectnumber", identifier + "Projectnumber");
        request.setParameter("projectEntity.description", identifier + "Description");
        request.setParameter("projectEntity.name", identifier + "Name");
        request.setParameter("orgId", "1");

        ActionProxy proxy = getActionProxy("/project/save");
        ProjectAction projectAction = (ProjectAction) proxy.getAction();

        proxy.execute();

        return projectAction.getProjectEntity();
    }
}
//
//import at.binarycheese.pixi.domain.OrganisationsEntity;
//import at.binarycheese.pixi.domain.ProjectEntity;
//import at.binarycheese.pixi.domain.ProjectstagesEntity;
//import at.binarycheese.pixi.persistence.OrganisationsDao;
//import at.binarycheese.pixi.persistence.ProjectDao;
//import at.binarycheese.pixi.persistence.ProjectstagesDao;
//import com.opensymphony.xwork2.Action;
//import org.junit.Assert;
//import org.junit.*;
//import org.mockito.internal.util.reflection.Whitebox;
//
//import java.util.ArrayList;
//
//import static org.mockito.Mockito.*;
//
//public class ProjectActionTest {
//
//    ProjectAction projectAction;
//    ProjectDao projectDao;
//    OrganisationsEntity organisationsEntity;
//    OrganisationsDao organisationsDao;
//    ProjectEntity projectEntity;
//    ProjectstagesEntity projectstagesEntity;
//    ProjectstagesDao projectstagesDao;
//
//    @Before
//    public void setUp() throws Exception {
//        projectAction = new ProjectAction();
//
//        projectDao = mock(ProjectDao.class);
//        organisationsEntity = mock(OrganisationsEntity.class);
//        organisationsDao = mock(OrganisationsDao.class);
//        projectEntity = mock(ProjectEntity.class);
//        projectstagesEntity = mock(ProjectstagesEntity.class);
//        projectstagesDao = mock(ProjectstagesDao.class);
//
//        when(projectDao.findAll()).thenReturn(new ArrayList<ProjectEntity>());
//        when(projectDao.findById(anyInt())).thenReturn(projectEntity);
//        when(organisationsDao.findAll()).thenReturn(new ArrayList<OrganisationsEntity>());
//        when(projectEntity.getProjectstagesByProjectStage()).thenReturn(projectstagesEntity);
//        when(projectstagesEntity.getId()).thenReturn(1);
//
//        Whitebox.setInternalState(projectAction, "projectDao", projectDao);
//        Whitebox.setInternalState(projectAction, "organisationsDao", organisationsDao);
//    }
//
//    @Test
//    public void testIndex() {
//        projectAction.browse();
//        Assert.assertEquals(0, projectAction.getProjectEntityList().size());
//    }
//
//    @Test
//    public void testInsert() {
//        projectAction.setId((int)Math.PI);
//        projectAction.input();
//        Assert.assertEquals(projectEntity, projectAction.getProjectEntity());
//        Assert.assertEquals(1, projectAction.getProjectEntity().getProjectstagesByProjectStage().getId());
//    }
//
//    @Test
//    public void testCreateWithIdIs0() {
//        projectAction.setId(0);
//        projectAction.setOrgId("1");
//        try {
//            Assert.assertEquals(Action.NONE, projectAction.create());
//            verify(projectstagesDao, times(1)).findById(1);
//            verify(projectstagesDao, times(1)).save((ProjectstagesEntity)notNull());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    @Test
//    public void testCreateWithIs1() {
//        projectAction.setId(1);
//        projectAction.setOrgId("1");
//        projectAction.setProjectStagesId((int)Math.PI);
//        try{
//            Assert.assertEquals(Action.NONE, projectAction.create());
//            verify(projectstagesDao, times(1)).findById((int)Math.PI);
//            verify(projectDao, times(1)).update((ProjectEntity)notNull());
//        }catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    @Test
//    public void testTableMethod() {
//        projectAction.table();
//        Assert.assertEquals(0, projectAction.getProjectEntityList().size());
//        verify(projectDao, times(1)).findAll();
//    }
//
//    @Test
//    public void testDelete() {
//        projectAction.setId((int)Math.PI);
//        projectAction.delete();
//        verify(projectDao, times(1)).delete((int)Math.PI);
//    }
//}