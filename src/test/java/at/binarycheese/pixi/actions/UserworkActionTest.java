package at.binarycheese.pixi.actions;

import at.binarycheese.pixi.domain.EventsEntity;
import at.binarycheese.pixi.domain.UsersEntity;
import at.binarycheese.pixi.domain.UserworksEntity;
import com.opensymphony.xwork2.ActionProxy;
import org.apache.struts2.StrutsSpringTestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.List;

/**
 * Created by Lukas on 17.02.2015.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(UserworkAction.class)
public class UserworkActionTest extends StrutsSpringTestCase {

    @Override
    protected String[] getContextLocations() {
        return  new String[] {"test-config.xml"};
    }

    @Test
    public void testCreateMultipleUserworks(){

        ActionProxy proxy = getActionProxy("/userwork/createSubmit");
        assertNotNull(proxy);
        UserworkAction userworkAction = (UserworkAction) proxy.getAction();
        assertNotNull(userworkAction);
        EventsEntity testEvent = userworkAction.eventsService.testEntity();
        UsersEntity[] testUsers = new UsersEntity[]{userworkAction.usersService.getTestEntity(), userworkAction.usersService.getTestEntity()};

        request.setParameter("users", testUsers[0].getId()+","+testUsers[1].getId());
        request.setParameter("workstartdate", "11.02.2015, 22:00");
        request.setParameter("workenddate", "11.02.2015, 23:00");
        request.setParameter("event", testEvent.getId()+"");

        proxy = getActionProxy("/userwork/createSubmit");
        assertNotNull(proxy);
        userworkAction = (UserworkAction) proxy.getAction();
        assertNotNull(userworkAction);

        try {
             proxy.execute();
        } catch (Exception e1) {
            e1.printStackTrace();
            fail();
        }
        List<UserworksEntity> userworks = userworkAction.userworkService.findByEvent(testEvent.getId());
        assertSame(2, userworks.size());
        assertNotSame(0, userworks.get(0).getId());
        assertNotSame(0, userworks.get(1).getId());
    }

    @Test
    public void testCreateSingleUserwork(){
        ActionProxy proxy = getActionProxy("/userwork/createSubmit");
        assertNotNull(proxy);
        UserworkAction userworkAction = (UserworkAction) proxy.getAction();
        assertNotNull(userworkAction);
        EventsEntity testEvent = userworkAction.eventsService.testEntity();
        UsersEntity testUser = userworkAction.usersService.getTestEntity();

        request.setParameter("users", Integer.toString(testUser.getId()));
        request.setParameter("workstartdate", "11.02.2015, 22:00");
        request.setParameter("workenddate", "11.02.2015, 23:00");
        request.setParameter("event", testEvent.getId()+"");

        proxy = getActionProxy("/userwork/createSubmit");
        assertNotNull(proxy);
        userworkAction = (UserworkAction) proxy.getAction();
        assertNotNull(userworkAction);

        try {
            proxy.execute();
        } catch (Exception e1) {
            e1.printStackTrace();
            fail();
        }
        List<UserworksEntity> userworks = userworkAction.userworkService.findByEvent(testEvent.getId());
        assertSame(1, userworks.size());
        assertNotSame(0, userworks.get(0).getId());
    }

    @Test
    public void testDeleteUserwork(){
        ActionProxy proxy = getActionProxy("/userwork/delete");
        assertNotNull(proxy);
        UserworkAction userworkAction = (UserworkAction) proxy.getAction();
        assertNotNull(userworkAction);
        EventsEntity testEvent = userworkAction.eventsService.testEntity();
        UsersEntity testUser = userworkAction.usersService.getTestEntity();
        UserworksEntity testUserwork = userworkAction.userworkService.getTestEntity(testEvent,testUser);

        request.setParameter("userwork", Integer.toString(testUserwork.getId()));

        proxy = getActionProxy("/userwork/delete");
        assertNotNull(proxy);
        userworkAction = (UserworkAction) proxy.getAction();
        assertNotNull(userworkAction);
        assertNotSame(0, testUser.getId());
        proxy.setExecuteResult(false);
        try {
            proxy.execute();
        } catch (Exception e1) {
            e1.printStackTrace();
            fail();
        }
        UserworksEntity userworksEntity = userworkAction.userworkService.find(testUserwork.getId());
        assertNull(userworksEntity);
    }
}
