package at.binarycheese.pixi.actions;

import at.binarycheese.pixi.domain.RolesEntity;
import at.binarycheese.pixi.domain.UsersEntity;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionProxy;
import org.apache.struts2.StrutsSpringTestCase;
import org.junit.Test;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.HashSet;
import java.util.Set;

public class UserActionTest extends StrutsSpringTestCase {

    @Override
    protected String[] getContextLocations() {
        return new String[]{"test-config.xml"};
    }


    public void setupAuthority() {
        // add principal object to SecurityContextHolder
        Set<GrantedAuthority> setAuth = new HashSet<>();
        setAuth.add(new SimpleGrantedAuthority("testRole"));
        org.springframework.security.core.userdetails.User user = new org.springframework.security.core.userdetails.User("Secure", "Secure", true, true, true, true, setAuth);
                /* fill user object */

        Authentication auth = new UsernamePasswordAuthenticationToken(user, null);

        SecurityContextHolder.getContext().setAuthentication(auth);
    }

    @Test
    public void testIndex() throws Exception {
        setupAuthority();
        testUrlAction("/user/index", Action.SUCCESS);
    }

    @Test
    public void testCreate() throws Exception {
        setupAuthority();
        testUrlAction("/user/create", Action.SUCCESS);
    }

    @Test
    public void testAdd() throws Exception {
        setupAuthority();
        request.setParameter("userEntity.username", "testAddTestUser");
        request.setParameter("userEntity.plannedHours", "40");
        request.setParameter("confirmPW", "testAddTestPW");
        request.addParameter("selectedRoles", String.valueOf(RolesEntity.AdminId));
        request.addParameter("selectedRoles", String.valueOf(RolesEntity.ContactadminId));
        request.setParameter("selectedContact", "1");

        ActionProxy proxy = getActionProxy("/user/add");
        proxy.setExecuteResult(false);
        assertNotNull(proxy);
        UserAction userAction = (UserAction) proxy.getAction();
        assertNotNull(userAction);

        String result = proxy.execute();
        assertEquals(Action.SUCCESS, result);
        UsersEntity user = userAction.getUsersService().findByUsername("testAddTestUser");
        user = userAction.getUsersService().findByUserIdWithPermissions(user.getId());
        assertNotNull(user);
        assertEquals(userAction.getUserEntity().getId(), user.getId());
        assertEquals("testAddTestUser", user.getUsername());
        assertTrue(user.hasRole(RolesEntity.NewUserId));
        assertEquals(4, user.getRoles().size());
        assertEquals(40, Math.round(Double.parseDouble(user.getPlannedHours().toString())));
    }

    @Test
    public void testEdit() throws Exception {
        setupAuthority();
        UsersEntity usersEntity = saveEntity("testEdit");

        request.removeAllParameters();
        request.setParameter("selectedUser", usersEntity.getId() + "");

        testUrlAction("/user/edit", Action.SUCCESS);
    }

    @Test
    public void testUpdate() throws Exception {
        setupAuthority();
        UsersEntity usersEntity = saveEntity("testUpdate");

        request.removeAllParameters();
        request.setParameter("userEntity.username", "EditedTestUpdateTestUser");
        request.setParameter("userEntity.password", "EditedTestUpdateTestPW");
        request.setParameter("userEntity.plannedHours", "10");
        request.setParameter("confirmPW", "EditedTestUpdate" + "TestPW");
        request.addParameter("selectedRoles", String.valueOf(RolesEntity.AdminId));
        request.addParameter("selectedRoles", String.valueOf(RolesEntity.ContactadminId));
        request.addParameter("selectedRoles", String.valueOf(RolesEntity.ProjectcontrollerId));

        request.setParameter("selectedContact", "11");
        request.setParameter("userEntity.id", "" + usersEntity.getId());

        ActionProxy proxy = getActionProxy("/user/update");
        assertNotNull(proxy);
        proxy.setExecuteResult(false);
        UserAction userAction = (UserAction) proxy.getAction();
        assertNotNull(userAction);

        String result = proxy.execute();
        assertEquals(Action.SUCCESS, result);
        UsersEntity user = userAction.getUsersService().findByUserIdWithPermissions(userAction.getUserEntity().getId());
        assertFalse(user.getUsername().equals("EditedTestUpdateTestUser"));
        assertEquals(4, user.getRoles().size());
        assertEquals(10, Math.round(Double.parseDouble(user.getPlannedHours().toString())));
    }

    @Test
    public void testToggleDeactivatedUser() throws Exception {
        setupAuthority();
        UsersEntity usersEntity = saveEntity("testToggleDeactivatedUser");

        request.removeAllParameters();
        request.setParameter("selectedUser", "" + usersEntity.getId());

        ActionProxy proxy = getActionProxy("/user/toggleDeactivateUser");
        assertNotNull(proxy);
        proxy.setExecuteResult(false);
        UserAction userAction = (UserAction) proxy.getAction();
        assertNotNull(userAction);

        String result = proxy.execute();
        assertEquals(Action.SUCCESS, result);
        assertEquals(!userAction.getUsersService().findById(usersEntity.getId()).getDeactivated(), (boolean) usersEntity.getDeactivated());
    }

    @Test
    public void testResetPW() throws Exception {
        setupAuthority();
        UsersEntity usersEntity = saveEntity("testResetPW");

        request.removeAllParameters();
        request.setParameter("selectedUser", "" + usersEntity.getId());

        ActionProxy proxy = getActionProxy("/user/resetPW");
        assertNotNull(proxy);
        proxy.setExecuteResult(false);
        UserAction userAction = (UserAction) proxy.getAction();
        assertNotNull(userAction);

        String result = proxy.execute();
        assertEquals(Action.NONE, result);
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        assertTrue(bCryptPasswordEncoder.matches(userAction.getNewPW(), userAction.getUsersService().findById(usersEntity.getId()).getPassword()));
    }

    public UserAction testUrlAction(String url, String expectedResult) throws Exception {
        ActionProxy proxy = getActionProxy(url);
        assertNotNull(proxy);
        UserAction userAction = (UserAction) proxy.getAction();
        assertNotNull(userAction);

        String result = proxy.execute();
        assertEquals(expectedResult, result);

        return userAction;
    }

    public UsersEntity saveEntity(String identifier) throws Exception {

        request.setParameter("userEntity.username", identifier + "TestUser");
        request.setParameter("userEntity.password", identifier + "TestPW");
        request.setParameter("userEntity.plannedHours", "10");
        request.setParameter("confirmPW", identifier + "TestPW");
        request.addParameter("selectedRoles", "2");
        request.addParameter("selectedRoles", "4");
        request.setParameter("selectedContact", "1");

        ActionProxy proxy = getActionProxy("/user/add");
        UserAction userAction = (UserAction) proxy.getAction();

        proxy.execute();

        return userAction.getUserEntity();
    }
}