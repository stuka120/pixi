package at.binarycheese.pixi.actions;

import at.binarycheese.pixi.actions.JSON.ContactFilterAction;
import at.binarycheese.pixi.domain.ContactsEntity;
import com.opensymphony.xwork2.ActionProxy;
import org.apache.struts2.StrutsSpringTestCase;
import org.junit.Test;
import org.springframework.core.io.Resource;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;

import javax.sql.DataSource;
import java.text.ParseException;
import java.util.*;

/**
 * Created by Dominik on 15.02.2015.
 */
public class ContactFilterActionTest extends StrutsSpringTestCase {

    @Override
    protected String[] getContextLocations() {
        return new String[] {"test-config.xml"};
    }

    @Test
    public void testNameLike() throws Exception {
        initData();

        ActionProxy proxy = getActionProxy("/contacts/filter");
        assertNotNull(proxy);
        ContactFilterAction contactFilterAction = (ContactFilterAction) proxy.getAction();
        assertNotNull(contactFilterAction);

        List asdf = contactFilterAction.getContactsService().findAll();

        contactFilterAction.setSearchString("con.name like '%ar%'");
        List<HashMap<String, Object>> erg1 = contactFilterAction.getFilteredContacts();
        Collections.sort(erg1, new Comparator<HashMap<String, Object>>() {
            @Override
            public int compare(HashMap<String, Object> o1, HashMap<String, Object> o2) {
                int x1 = (int) o1.get("id");
                int x2 = (int) o2.get("id");
                if((x1 - x2) < 0) {
                    return -1;
                } else if((x1 - x2) > 0) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });

        assertEquals(6, erg1.size());
        assertEquals("Ingomar", erg1.get(0).get("name"));
    }

    @Test
    public void testSurnameLike() throws Exception {
        initData();

        ActionProxy proxy = getActionProxy("/contacts/filter");
        assertNotNull(proxy);
        ContactFilterAction contactFilterAction = (ContactFilterAction) proxy.getAction();
        assertNotNull(contactFilterAction);

        List asdf = contactFilterAction.getContactsService().findAll();

        contactFilterAction.setSearchString("con.surname like '%lana%'");
        List<HashMap<String, Object>> erg1 = contactFilterAction.getFilteredContacts();
        Collections.sort(erg1, new Comparator<HashMap<String, Object>>() {
            @Override
            public int compare(HashMap<String, Object> o1, HashMap<String, Object> o2) {
                int x1 = (int) o1.get("id");
                int x2 = (int) o2.get("id");
                if((x1 - x2) < 0) {
                    return -1;
                } else if((x1 - x2) > 0) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });

        assertEquals(1, erg1.size());
        assertEquals("Katherina", erg1.get(0).get("name"));
        assertEquals("Rilana", erg1.get(0).get("surname"));
    }

    @Test
    public void testStateIs() throws Exception {
        initData();

        ActionProxy proxy = getActionProxy("/contacts/filter");
        assertNotNull(proxy);
        ContactFilterAction contactFilterAction = (ContactFilterAction) proxy.getAction();
        assertNotNull(contactFilterAction);

        List asdf = contactFilterAction.getContactsService().findAll();

        contactFilterAction.setSearchString("con.state = 'Wien'");
        List<HashMap<String, Object>> erg1 = contactFilterAction.getFilteredContacts();
        Collections.sort(erg1, new Comparator<HashMap<String, Object>>() {
            @Override
            public int compare(HashMap<String, Object> o1, HashMap<String, Object> o2) {
                int x1 = (int) o1.get("id");
                int x2 = (int) o2.get("id");
                if((x1 - x2) < 0) {
                    return -1;
                } else if((x1 - x2) > 0) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });

        assertEquals(2, erg1.size());
        assertEquals("Erwin", erg1.get(0).get("name"));
        assertEquals("Rebecka", erg1.get(0).get("surname"));
    }

    @Test
    public void testAddressIs() throws Exception {
        initData();

        ActionProxy proxy = getActionProxy("/contacts/filter");
        assertNotNull(proxy);
        ContactFilterAction contactFilterAction = (ContactFilterAction) proxy.getAction();
        assertNotNull(contactFilterAction);

        List asdf = contactFilterAction.getContactsService().findAll();

        contactFilterAction.setSearchString("con.address = 'Alfred-Kiefer-Weg 80'");
        List<HashMap<String, Object>> erg1 = contactFilterAction.getFilteredContacts();
        Collections.sort(erg1, new Comparator<HashMap<String, Object>>() {
            @Override
            public int compare(HashMap<String, Object> o1, HashMap<String, Object> o2) {
                int x1 = (int) o1.get("id");
                int x2 = (int) o2.get("id");
                if((x1 - x2) < 0) {
                    return -1;
                } else if((x1 - x2) > 0) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });

        assertEquals(1, erg1.size());
        assertEquals("Ornulf", erg1.get(0).get("name"));
        assertEquals("Ayten", erg1.get(0).get("surname"));
    }

    @Test
    public void testHomePageStartsWith() throws Exception {
        initData();

        ActionProxy proxy = getActionProxy("/contacts/filter");
        assertNotNull(proxy);
        ContactFilterAction contactFilterAction = (ContactFilterAction) proxy.getAction();
        assertNotNull(contactFilterAction);

        List asdf = contactFilterAction.getContactsService().findAll();

        contactFilterAction.setSearchString("con.hompage like 'www.infiniti%'");
        List<HashMap<String, Object>> erg1 = contactFilterAction.getFilteredContacts();
        Collections.sort(erg1, new Comparator<HashMap<String, Object>>() {
            @Override
            public int compare(HashMap<String, Object> o1, HashMap<String, Object> o2) {
                int x1 = (int) o1.get("id");
                int x2 = (int) o2.get("id");
                if((x1 - x2) < 0) {
                    return -1;
                } else if((x1 - x2) > 0) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });

        assertEquals(1, erg1.size());
        assertEquals("Erwin", erg1.get(0).get("name"));
        assertEquals("Rebecka", erg1.get(0).get("surname"));
    }

    @Test
    public void testInfoEndsWith() throws Exception {
        initData();

        ActionProxy proxy = getActionProxy("/contacts/filter");
        assertNotNull(proxy);
        ContactFilterAction contactFilterAction = (ContactFilterAction) proxy.getAction();
        assertNotNull(contactFilterAction);

        List asdf = contactFilterAction.getContactsService().findAll();

        contactFilterAction.setSearchString("con.info like '%herp'");
        List<HashMap<String, Object>> erg1 = contactFilterAction.getFilteredContacts();
        Collections.sort(erg1, new Comparator<HashMap<String, Object>>() {
            @Override
            public int compare(HashMap<String, Object> o1, HashMap<String, Object> o2) {
                int x1 = (int) o1.get("id");
                int x2 = (int) o2.get("id");
                if((x1 - x2) < 0) {
                    return -1;
                } else if((x1 - x2) > 0) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });

        assertEquals(8, erg1.size());
        assertEquals("Erwin", erg1.get(0).get("name"));
        assertEquals("Rebecka", erg1.get(0).get("surname"));
    }

    @Test
    public void testNameLikeAndSurnameLike() throws Exception {
        initData();

        ActionProxy proxy = getActionProxy("/contacts/filter");
        assertNotNull(proxy);
        ContactFilterAction contactFilterAction = (ContactFilterAction) proxy.getAction();
        assertNotNull(contactFilterAction);

        List asdf = contactFilterAction.getContactsService().findAll();

        contactFilterAction.setSearchString("con.name like '%a%' and con.surname like '%len%'");
        contactFilterAction.setBooleanAndOr(",and");
        List<HashMap<String, Object>> erg1 = contactFilterAction.getFilteredContacts();
        Collections.sort(erg1, new Comparator<HashMap<String, Object>>() {
            @Override
            public int compare(HashMap<String, Object> o1, HashMap<String, Object> o2) {
                int x1 = (int) o1.get("id");
                int x2 = (int) o2.get("id");
                if ((x1 - x2) < 0) {
                    return -1;
                } else if ((x1 - x2) > 0) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });

        assertEquals(1, erg1.size());
        assertEquals("Siska", erg1.get(0).get("name"));
        assertEquals("Valentina", erg1.get(0).get("surname"));
    }

    @Test
    public void testIdSmallerAs() throws Exception {
        initData();

        ActionProxy proxy = getActionProxy("/contacts/filter");
        assertNotNull(proxy);
        ContactFilterAction contactFilterAction = (ContactFilterAction) proxy.getAction();
        assertNotNull(contactFilterAction);

        List asdf = contactFilterAction.getContactsService().findAll();

        contactFilterAction.setSearchString("con.id < 5");
        List<HashMap<String, Object>> erg1 = contactFilterAction.getFilteredContacts();
        Collections.sort(erg1, new Comparator<HashMap<String, Object>>() {
            @Override
            public int compare(HashMap<String, Object> o1, HashMap<String, Object> o2) {
                int x1 = (int) o1.get("id");
                int x2 = (int) o2.get("id");
                if ((x1 - x2) < 0) {
                    return -1;
                } else if ((x1 - x2) > 0) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });

        assertEquals(4, erg1.size());
        assertEquals("Ingomar", erg1.get(0).get("name"));
        assertEquals("Annemie", erg1.get(0).get("surname"));
    }

    @Test
    public void testIdBiggerAs() throws Exception {
        initData();

        ActionProxy proxy = getActionProxy("/contacts/filter");
        assertNotNull(proxy);
        ContactFilterAction contactFilterAction = (ContactFilterAction) proxy.getAction();
        assertNotNull(contactFilterAction);

        List asdf = contactFilterAction.getContactsService().findAll();

        contactFilterAction.setSearchString("con.id > 15");
        List<HashMap<String, Object>> erg1 = contactFilterAction.getFilteredContacts();
        Collections.sort(erg1, new Comparator<HashMap<String, Object>>() {
            @Override
            public int compare(HashMap<String, Object> o1, HashMap<String, Object> o2) {
                int x1 = (int) o1.get("id");
                int x2 = (int) o2.get("id");
                if ((x1 - x2) < 0) {
                    return -1;
                } else if ((x1 - x2) > 0) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });

        assertEquals(5, erg1.size());
        assertEquals("Ferdinand", erg1.get(0).get("name"));
        assertEquals("Ira", erg1.get(0).get("surname"));
    }

    @Test
    public void testIdEquals() throws Exception {
        initData();

        ActionProxy proxy = getActionProxy("/contacts/filter");
        assertNotNull(proxy);
        ContactFilterAction contactFilterAction = (ContactFilterAction) proxy.getAction();
        assertNotNull(contactFilterAction);

        List asdf = contactFilterAction.getContactsService().findAll();

        contactFilterAction.setSearchString("con.id = 9");
        List<HashMap<String, Object>> erg1 = contactFilterAction.getFilteredContacts();
        Collections.sort(erg1, new Comparator<HashMap<String, Object>>() {
            @Override
            public int compare(HashMap<String, Object> o1, HashMap<String, Object> o2) {
                int x1 = (int) o1.get("id");
                int x2 = (int) o2.get("id");
                if ((x1 - x2) < 0) {
                    return -1;
                } else if ((x1 - x2) > 0) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });

        assertEquals(1, erg1.size());
        assertEquals("Ingowart", erg1.get(0).get("name"));
        assertEquals("Iris", erg1.get(0).get("surname"));
    }

    @Test
    public void testIdEqualsNot() throws Exception {
        initData();

        ActionProxy proxy = getActionProxy("/contacts/filter");
        assertNotNull(proxy);
        ContactFilterAction contactFilterAction = (ContactFilterAction) proxy.getAction();
        assertNotNull(contactFilterAction);

        List asdf = contactFilterAction.getContactsService().findAll();

        contactFilterAction.setSearchString("con.id != 17");
        List<HashMap<String, Object>> erg1 = contactFilterAction.getFilteredContacts();
        Collections.sort(erg1, new Comparator<HashMap<String, Object>>() {
            @Override
            public int compare(HashMap<String, Object> o1, HashMap<String, Object> o2) {
                int x1 = (int) o1.get("id");
                int x2 = (int) o2.get("id");
                if ((x1 - x2) < 0) {
                    return -1;
                } else if ((x1 - x2) > 0) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });

        assertEquals(19, erg1.size());
        assertEquals("Siska", erg1.get(1).get("name"));
        assertEquals("Valentina", erg1.get(1).get("surname"));
    }

    @Test
    public void testCountryLike() throws Exception {
        initData();

        ActionProxy proxy = getActionProxy("/contacts/filter");
        assertNotNull(proxy);
        ContactFilterAction contactFilterAction = (ContactFilterAction) proxy.getAction();
        assertNotNull(contactFilterAction);

        List asdf = contactFilterAction.getContactsService().findAll();

        contactFilterAction.setSearchString("cou.germanName like '%sterr%'");
        List<HashMap<String, Object>> erg1 = contactFilterAction.getFilteredContacts();
        Collections.sort(erg1, new Comparator<HashMap<String, Object>>() {
            @Override
            public int compare(HashMap<String, Object> o1, HashMap<String, Object> o2) {
                int x1 = (int) o1.get("id");
                int x2 = (int) o2.get("id");
                if ((x1 - x2) < 0) {
                    return -1;
                } else if ((x1 - x2) > 0) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });

        assertEquals(10, erg1.size());
        assertEquals("Erwin", erg1.get(0).get("name"));
        assertEquals("Rebecka", erg1.get(0).get("surname"));
    }

    @Test
    public void testOrganisationLike() throws Exception {
        initData();

        ActionProxy proxy = getActionProxy("/contacts/filter");
        assertNotNull(proxy);
        ContactFilterAction contactFilterAction = (ContactFilterAction) proxy.getAction();
        assertNotNull(contactFilterAction);

        List asdf = contactFilterAction.getContactsService().findAll();

        contactFilterAction.setSearchString("org.name like '%erde%'");
        List<HashMap<String, Object>> erg1 = contactFilterAction.getFilteredContacts();
        Collections.sort(erg1, new Comparator<HashMap<String, Object>>() {
            @Override
            public int compare(HashMap<String, Object> o1, HashMap<String, Object> o2) {
                int x1 = (int) o1.get("id");
                int x2 = (int) o2.get("id");
                if ((x1 - x2) < 0) {
                    return -1;
                } else if ((x1 - x2) > 0) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });

        assertEquals(3, erg1.size());
        assertEquals("Ingomar", erg1.get(0).get("name"));
        assertEquals("Annemie", erg1.get(0).get("surname"));
    }

    @Test
    public void testUpdateDateBefore() throws Exception {

        initData();

        ActionProxy proxy = getActionProxy("/contacts/filter");
        assertNotNull(proxy);
        ContactFilterAction contactFilterAction = (ContactFilterAction) proxy.getAction();
        assertNotNull(contactFilterAction);

        List asdf = contactFilterAction.getContactsService().findAll();

        contactFilterAction.setSearchString("con.updateDate > 01.01.2004");
        List<HashMap<String, Object>> erg1 = contactFilterAction.getFilteredContacts();
        Collections.sort(erg1, new Comparator<HashMap<String, Object>>() {
            @Override
            public int compare(HashMap<String, Object> o1, HashMap<String, Object> o2) {
                int x1 = (int) o1.get("id");
                int x2 = (int) o2.get("id");
                if ((x1 - x2) < 0) {
                    return -1;
                } else if ((x1 - x2) > 0) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });

        assertEquals(1, erg1.size());
        assertEquals("Katherina", erg1.get(0).get("name"));
        assertEquals("Rilana", erg1.get(0).get("surname"));
    }

    @Test
    public void testEmailLike() throws Exception {

        initData();

        ActionProxy proxy = getActionProxy("/contacts/filter");
        assertNotNull(proxy);
        ContactFilterAction contactFilterAction = (ContactFilterAction) proxy.getAction();
        assertNotNull(contactFilterAction);

        List asdf = contactFilterAction.getContactsService().findAll();

        contactFilterAction.setSearchString("emails.value like '%@spengergasse.at%'");
        List<HashMap<String, Object>> erg1 = contactFilterAction.getFilteredContacts();
        Collections.sort(erg1, new Comparator<HashMap<String, Object>>() {
            @Override
            public int compare(HashMap<String, Object> o1, HashMap<String, Object> o2) {
                int x1 = (int) o1.get("id");
                int x2 = (int) o2.get("id");
                if ((x1 - x2) < 0) {
                    return -1;
                } else if ((x1 - x2) > 0) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });

        assertEquals(1, erg1.size());
        assertEquals("Ingomar", erg1.get(0).get("name"));
        assertEquals("Annemie", erg1.get(0).get("surname"));
    }

    @Test
    public void testTelnumberIs() throws Exception {

        initData();

        ActionProxy proxy = getActionProxy("/contacts/filter");
        assertNotNull(proxy);
        ContactFilterAction contactFilterAction = (ContactFilterAction) proxy.getAction();
        assertNotNull(contactFilterAction);

        List asdf = contactFilterAction.getContactsService().findAll();

        contactFilterAction.setSearchString("tels.value = '06801234567'");
        List<HashMap<String, Object>> erg1 = contactFilterAction.getFilteredContacts();
        Collections.sort(erg1, new Comparator<HashMap<String, Object>>() {
            @Override
            public int compare(HashMap<String, Object> o1, HashMap<String, Object> o2) {
                int x1 = (int) o1.get("id");
                int x2 = (int) o2.get("id");
                if ((x1 - x2) < 0) {
                    return -1;
                } else if ((x1 - x2) > 0) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });

        assertEquals(1, erg1.size());
        assertEquals("Ingomar", erg1.get(0).get("name"));
        assertEquals("Annemie", erg1.get(0).get("surname"));
    }

    @Test
    public void testCreateDateBefore() throws Exception {

        initData();

        ActionProxy proxy = getActionProxy("/contacts/filter");
        assertNotNull(proxy);
        ContactFilterAction contactFilterAction = (ContactFilterAction) proxy.getAction();
        assertNotNull(contactFilterAction);

        List asdf = contactFilterAction.getContactsService().findAll();

        contactFilterAction.setSearchString("con.createDate < 01.01.1950");
        List<HashMap<String, Object>> erg1 = contactFilterAction.getFilteredContacts();
        Collections.sort(erg1, new Comparator<HashMap<String, Object>>() {
            @Override
            public int compare(HashMap<String, Object> o1, HashMap<String, Object> o2) {
                int x1 = (int) o1.get("id");
                int x2 = (int) o2.get("id");
                if ((x1 - x2) < 0) {
                    return -1;
                } else if ((x1 - x2) > 0) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });

        assertEquals(4, erg1.size());
        assertEquals("Ingomar", erg1.get(0).get("name"));
        assertEquals("Annemie", erg1.get(0).get("surname"));
    }

    @Test
    public void testUpdateDateAfterAndCreateDateBefore() throws Exception {

        initData();

        ActionProxy proxy = getActionProxy("/contacts/filter");
        assertNotNull(proxy);
        ContactFilterAction contactFilterAction = (ContactFilterAction) proxy.getAction();
        assertNotNull(contactFilterAction);

        List asdf = contactFilterAction.getContactsService().findAll();

        contactFilterAction.setSearchString("con.updateDate > 01.01.2003 and con.createDate < 01.01.2000");
        contactFilterAction.setBooleanAndOr(",and");
        List<HashMap<String, Object>> erg1 = contactFilterAction.getFilteredContacts();
        Collections.sort(erg1, new Comparator<HashMap<String, Object>>() {
            @Override
            public int compare(HashMap<String, Object> o1, HashMap<String, Object> o2) {
                int x1 = (int) o1.get("id");
                int x2 = (int) o2.get("id");
                if ((x1 - x2) < 0) {
                    return -1;
                } else if ((x1 - x2) > 0) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });

        assertEquals(1, erg1.size());
        assertEquals("Luzie", erg1.get(0).get("name"));
        assertEquals("Poldi", erg1.get(0).get("surname"));
    }

    @Test
    public void testNewsletterIs() throws Exception {
        initData();

        ActionProxy proxy = getActionProxy("/contacts/filter");
        assertNotNull(proxy);
        ContactFilterAction contactFilterAction = (ContactFilterAction) proxy.getAction();
        assertNotNull(contactFilterAction);

        List asdf = contactFilterAction.getContactsService().findAll();

        contactFilterAction.setSearchString("con.newsletter = true");
        List<HashMap<String, Object>> erg1 = contactFilterAction.getFilteredContacts();
        Collections.sort(erg1, new Comparator<HashMap<String, Object>>() {
            @Override
            public int compare(HashMap<String, Object> o1, HashMap<String, Object> o2) {
                int x1 = (int) o1.get("id");
                int x2 = (int) o2.get("id");
                if ((x1 - x2) < 0) {
                    return -1;
                } else if ((x1 - x2) > 0) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });

        assertEquals(15, erg1.size());
        assertEquals("Erwin", erg1.get(2).get("name"));
        assertEquals("Rebecka", erg1.get(2).get("surname"));

        contactFilterAction.setSearchString("con.newsletter = false");
        erg1 = contactFilterAction.getFilteredContacts();
        Collections.sort(erg1, new Comparator<HashMap<String, Object>>() {
            @Override
            public int compare(HashMap<String, Object> o1, HashMap<String, Object> o2) {
                int x1 = (int) o1.get("id");
                int x2 = (int) o2.get("id");
                if ((x1 - x2) < 0) {
                    return -1;
                } else if ((x1 - x2) > 0) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });

        assertEquals(5, erg1.size());
        assertEquals("Katherina", erg1.get(2).get("name"));
        assertEquals("Rilana", erg1.get(2).get("surname"));
    }

    @Test
    public void testWithoutAnyFilter() throws Exception {
        initData();

        ActionProxy proxy = getActionProxy("/contacts/filter");
        proxy.setExecuteResult(false);
        assertNotNull(proxy);
        ContactFilterAction contactFilterAction = (ContactFilterAction) proxy.getAction();
        assertNotNull(contactFilterAction);

        contactFilterAction.setSearchString("");
        List<HashMap<String, Object>> erg1 = contactFilterAction.getFilteredContacts();
        Collections.sort(erg1, new Comparator<HashMap<String, Object>>() {
            @Override
            public int compare(HashMap<String, Object> o1, HashMap<String, Object> o2) {
                int x1 = (int) o1.get("id");
                int x2 = (int) o2.get("id");
                if ((x1 - x2) < 0) {
                    return -1;
                } else if ((x1 - x2) > 0) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });

        assertEquals(20, erg1.size());
        assertEquals("Ingomar", erg1.get(0).get("name"));
        assertEquals("Annemie", erg1.get(0).get("surname"));
    }

    @Test
    public void testSearchPLZ() throws Exception {
        initData();

        ActionProxy proxy = getActionProxy("/contacts/filter");
        proxy.setExecuteResult(false);
        assertNotNull(proxy);
        ContactFilterAction contactFilterAction = (ContactFilterAction) proxy.getAction();
        assertNotNull(contactFilterAction);

        contactFilterAction.setSearchString("plz.plz = 1100");
        List<HashMap<String, Object>> erg1 = contactFilterAction.getFilteredContacts();
        Collections.sort(erg1, new Comparator<HashMap<String, Object>>() {
            @Override
            public int compare(HashMap<String, Object> o1, HashMap<String, Object> o2) {
                int x1 = (int) o1.get("id");
                int x2 = (int) o2.get("id");
                if ((x1 - x2) < 0) {
                    return -1;
                } else if ((x1 - x2) > 0) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });

        assertEquals(20, erg1.size());
        assertEquals("Ingomar", erg1.get(0).get("name"));
        assertEquals("Annemie", erg1.get(0).get("surname"));
    }

    @Test
    public void testIdOrId() throws Exception {
        initData();

        ActionProxy proxy = getActionProxy("/contacts/filter");
        proxy.setExecuteResult(false);
        assertNotNull(proxy);
        ContactFilterAction contactFilterAction = (ContactFilterAction) proxy.getAction();
        assertNotNull(contactFilterAction);

        contactFilterAction.setSearchString("con.id = 10 or con.id = 11");
        contactFilterAction.setBooleanAndOr(",or");
        List<HashMap<String, Object>> erg1 = contactFilterAction.getFilteredContacts();
        Collections.sort(erg1, new Comparator<HashMap<String, Object>>() {
            @Override
            public int compare(HashMap<String, Object> o1, HashMap<String, Object> o2) {
                int x1 = (int) o1.get("id");
                int x2 = (int) o2.get("id");
                if ((x1 - x2) < 0) {
                    return -1;
                } else if ((x1 - x2) > 0) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });

        assertEquals(2, erg1.size());
        assertEquals("Luzie", erg1.get(0).get("name"));
        assertEquals("Chantal", erg1.get(1).get("surname"));
    }

    @Test
    public void testNameIsOrNameIs() throws Exception {
        initData();

        ActionProxy proxy = getActionProxy("/contacts/filter");
        proxy.setExecuteResult(false);
        assertNotNull(proxy);
        ContactFilterAction contactFilterAction = (ContactFilterAction) proxy.getAction();
        assertNotNull(contactFilterAction);

        contactFilterAction.setSearchString("con.name like '%erwin%' or con.name like '%ornulf%'");
        contactFilterAction.setBooleanAndOr(",or");
        List<HashMap<String, Object>> erg1 = contactFilterAction.getFilteredContacts();
        Collections.sort(erg1, new Comparator<HashMap<String, Object>>() {
            @Override
            public int compare(HashMap<String, Object> o1, HashMap<String, Object> o2) {
                int x1 = (int) o1.get("id");
                int x2 = (int) o2.get("id");
                if ((x1 - x2) < 0) {
                    return -1;
                } else if ((x1 - x2) > 0) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });

        assertEquals(2, erg1.size());
        assertEquals("Erwin", erg1.get(0).get("name"));
        assertEquals("Ayten", erg1.get(1).get("surname"));
    }



//    @Test(expected = NumberFormatException.class)
//    public void testExceptionOnIdIsWord() throws Exception {
//        initData();
//
//        ActionProxy proxy = getActionProxy("/contacts/filter");
//        proxy.setExecuteResult(false);
//        assertNotNull(proxy);
//        ContactFilterAction contactFilterAction = (ContactFilterAction) proxy.getAction();
//        assertNotNull(contactFilterAction);
//
//        contactFilterAction.setSearchString("con.id = asdf");
//        contactFilterAction.getFilteredContacts();
//    }

    @Test
    public void testExceptionTextAsNumberWithoutQuotes() throws Exception {
        initData();

        ActionProxy proxy = getActionProxy("/contacts/filter");
        proxy.setExecuteResult(false);
        assertNotNull(proxy);
        ContactFilterAction contactFilterAction = (ContactFilterAction) proxy.getAction();
        assertNotNull(contactFilterAction);

        contactFilterAction.setSearchString("con.title = 2134");
        contactFilterAction.getFilteredContacts();
    }

//    @Test(expected = ParseException.class)
//    public void testExceptionDateAsNondate() throws ParseException {
//        initData();
//
//        ActionProxy proxy = getActionProxy("/contacts/filter");
//        proxy.setExecuteResult(false);
//        assertNotNull(proxy);
//        ContactFilterAction contactFilterAction = (ContactFilterAction) proxy.getAction();
//        assertNotNull(contactFilterAction);
//
//        contactFilterAction.setSearchString("con.createDate = 2134");
//        try {
//            contactFilterAction.getFilteredContacts();
//        } catch (ParseException e) {
//        }
//    }

    public void initData() {
        Resource resource = applicationContext.getResource("classpath:cleanupContact.sql");
        ResourceDatabasePopulator resourceDatabasePopulator = new ResourceDatabasePopulator(resource);
        resourceDatabasePopulator.execute((DataSource) applicationContext.getBean("dataSource"));

//        JdbcTestUtils.executeSqlScript(new JdbcTemplate((DataSource) applicationContext.getBean("dataSource")), resource, true);

    }
}
