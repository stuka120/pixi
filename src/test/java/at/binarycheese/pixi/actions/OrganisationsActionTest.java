package at.binarycheese.pixi.actions;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionProxy;
import org.apache.struts2.StrutsSpringJUnit4TestCase;
import org.apache.struts2.StrutsSpringTestCase;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(PowerMockRunner.class)
@PrepareForTest(OrganisationsAction.class)
public class OrganisationsActionTest extends StrutsSpringTestCase{


    @Override
    protected String[] getContextLocations() {
        return new String[] {"test-config.xml"};
    }

    @Test
    public void testIndex() throws Exception {
        ActionProxy proxy = getActionProxy("/organisations/index");
        Assert.assertNotNull(proxy);
        OrganisationsAction organisationsAction = (OrganisationsAction) proxy.getAction();
        Assert.assertNotNull(organisationsAction);

        String result = proxy.execute();
        Assert.assertEquals(Action.SUCCESS, result);
    }

    @Test
    public void testCreate() throws Exception {
        ActionProxy proxy = getActionProxy("/organisations/create");
        Assert.assertNotNull(proxy);
        OrganisationsAction organisationsAction = (OrganisationsAction) proxy.getAction();
        Assert.assertNotNull(organisationsAction);

        String result = proxy.execute();
        Assert.assertEquals(Action.INPUT, result);
    }

    @Test
    public void testProcessCreate() throws Exception {
        request.setParameter("organisationsEntity.name", "testCreateName");
        request.setParameter("organisationsEntity.desc", "TestCreateDesc");

        ActionProxy proxy = getActionProxy("/organisations/create");
        Assert.assertNotNull(proxy);
        OrganisationsAction organisationsAction = (OrganisationsAction) proxy.getAction();
        Assert.assertNotNull(organisationsAction);

        String result = proxy.execute();
        Assert.assertEquals(Action.NONE, result);
        Assert.assertNotSame(0, organisationsAction.getOrganisationsEntity().getId());
        Assert.assertEquals("testCreateName", organisationsAction.getOrganisationsService().findById(organisationsAction.getOrganisationsEntity().getId()).getName());
        Assert.assertEquals("TestCreateDesc", organisationsAction.getOrganisationsService().findById(organisationsAction.getOrganisationsEntity().getId()).getDesc());
    }

    @Test
    public void testEdit() throws Exception {
        //First add a new Entity
        request.setParameter("organisationsEntity.name", "testEditName");
        request.setParameter("organisationsEntity.desc", "testEditDesc");

        ActionProxy proxy = getActionProxy("/organisations/create");
        Assert.assertNotNull(proxy);
        OrganisationsAction organisationsAction = (OrganisationsAction) proxy.getAction();
        Assert.assertNotNull(organisationsAction);
        proxy.execute();

        request.removeAllParameters();
        request.setParameter("selectedOrganisation", "" + organisationsAction.getOrganisationsEntity().getId());

        proxy = getActionProxy("/organisations/edit");
        Assert.assertNotNull(proxy);
        organisationsAction = (OrganisationsAction) proxy.getAction();
        Assert.assertNotNull(organisationsAction);

        String result = proxy.execute();
        Assert.assertEquals(Action.INPUT, result);
    }

    @Test
    public void testProceesEdit() throws Exception {
        //First add a new Entity
        request.setParameter("organisationsEntity.name", "testProceesEditName");
        request.setParameter("organisationsEntity.desc", "testProceesEditDesc");

        ActionProxy proxy = getActionProxy("/organisations/create");
        Assert.assertNotNull(proxy);
        OrganisationsAction organisationsAction = (OrganisationsAction) proxy.getAction();
        Assert.assertNotNull(organisationsAction);
        proxy.execute();

        request.removeAllParameters();
        request.setParameter("organisationsEntity.id", "" + organisationsAction.getOrganisationsEntity().getId());
        request.setParameter("organisationsEntity.name", "EditedTestProcessEditName");
        request.setParameter("organisationsEntity.desc", "EditedTestProcessEditDesc");

        proxy = getActionProxy("/organisations/edit");
        Assert.assertNotNull(proxy);
        organisationsAction = (OrganisationsAction) proxy.getAction();
        Assert.assertNotNull(organisationsAction);

        String result = proxy.execute();
        Assert.assertEquals(Action.NONE, result);
        Assert.assertEquals("EditedTestProcessEditName", organisationsAction.getOrganisationsService().findById(organisationsAction.getOrganisationsEntity().getId()).getName());
        Assert.assertEquals("EditedTestProcessEditDesc", organisationsAction.getOrganisationsService().findById(organisationsAction.getOrganisationsEntity().getId()).getDesc());
    }

    @Test
    public void testDelete() throws Exception {
        request.setParameter("organisationsEntity.name", "testDeleteName");
        request.setParameter("organisationsEntity.desc", "testDeleteDesc");

        ActionProxy proxy = getActionProxy("/organisations/create");
        Assert.assertNotNull(proxy);
        OrganisationsAction organisationsAction = (OrganisationsAction) proxy.getAction();
        Assert.assertNotNull(organisationsAction);

        proxy.execute();

        request.removeAllParameters();
        request.setParameter("organisationsEntity.id", "" + organisationsAction.getOrganisationsEntity().getId());


        proxy = getActionProxy("/organisations/delete");
        assertNotNull(proxy);
        proxy.setExecuteResult(false);
        organisationsAction = (OrganisationsAction) proxy.getAction();
        assertNotNull(organisationsAction);

        String result = proxy.execute();
        assertEquals(Action.NONE, result);
    }

    public void testProceesDelete() throws Exception {
        request.setParameter("organisationsEntity.name", "testProceesDeleteName");
        request.setParameter("organisationsEntity.desc", "testProceesDeleteDesc");

        ActionProxy proxy = getActionProxy("/organisations/create");
        Assert.assertNotNull(proxy);
        OrganisationsAction organisationsAction = (OrganisationsAction) proxy.getAction();
        Assert.assertNotNull(organisationsAction);

        proxy.execute();

        request.removeAllParameters();

        request.setParameter("organisationsEntity.id", "" + organisationsAction.getOrganisationsEntity().getId());

        proxy = getActionProxy("/organisations/delete");
        assertNotNull(proxy);
        organisationsAction = (OrganisationsAction) proxy.getAction();
        assertNotNull(organisationsAction);

        String result = proxy.execute();
        assertEquals(Action.NONE, result);
        assertNull(organisationsAction.getContactsService().findById(organisationsAction.getOrganisationsEntity().getId()));
    }

}

//
//import at.binarycheese.pixi.domain.ContactsEntity;
//import at.binarycheese.pixi.domain.OrganisationsEntity;
//import at.binarycheese.pixi.persistence.ContactsDao;
//import at.binarycheese.pixi.persistence.OrganisationsDao;
//import org.junit.Assert.assert;
//import org.junit.Before;
//import org.junit.Test;
//import org.mockito.Mockito;
//import org.mockito.internal.util.reflection.Whitebox;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import static org.mockito.Mockito.*;
//
///**
// * Created by Tortuga on 12.01.2015.
// */
//public class OrganisationsActionTest {
//    private OrganisationsAction organisationsAction;
//    private OrganisationsDao mockOrganisationsDao;
//    private List<OrganisationsEntity> organisationsEntityList;
//    private OrganisationsEntity organisationsEntity1;
//    private OrganisationsEntity organisationsEntity2;
//
//    private List<ContactsEntity> contactsEntityList;
//    private ContactsEntity contactsEntity;
//    private ContactsDao mockContactsDao;
//    private List<String> selectedContacts;
//
//    @Before
//    public void setup() {
//        organisationsAction = new OrganisationsAction();
//        mockOrganisationsDao = mock(OrganisationsDao.class);
//        mockContactsDao = mock(ContactsDao.class);
//        organisationsEntityList = new ArrayList<>();
//
//        organisationsEntity1 = mock(OrganisationsEntity.class);
//        stub(organisationsEntity1.getId()).toReturn(111);
//        organisationsEntity2 = new OrganisationsEntity();
//        organisationsEntity2.setId(222);
//
//        organisationsEntityList.add(organisationsEntity1);
//        organisationsEntityList.add(organisationsEntity2);
//
//        contactsEntityList = new ArrayList<>();
//        contactsEntity = new ContactsEntity();
//        contactsEntity.setId(999);
//        contactsEntityList.add(contactsEntity);
//
//        selectedContacts = new ArrayList<>();
//        selectedContacts.add("999");
//
//        stub(mockOrganisationsDao.findAllUnsafe()).toReturn(organisationsEntityList);
//        stub(mockOrganisationsDao.findAll()).toReturn(organisationsEntityList);
//        stub(mockOrganisationsDao.findUnreferencedContacts()).toReturn(contactsEntityList);
//        stub(mockOrganisationsDao.findByIdUnsafe(111)).toReturn(organisationsEntity1);
//        stub(mockContactsDao.findById(999)).toReturn(contactsEntity);
//        stub(organisationsEntity1.getContactsesById()).toReturn(contactsEntityList);
//
//        Whitebox.setInternalState(organisationsAction, "dao", mockOrganisationsDao);
//        Whitebox.setInternalState(organisationsAction, "contactsDao", mockContactsDao);
//
//        Mockito.doNothing().when(mockOrganisationsDao).save((OrganisationsEntity) notNull());
//        Mockito.doNothing().when(mockOrganisationsDao).commitSession();
//        Mockito.doNothing().when(mockOrganisationsDao).delete((OrganisationsEntity) notNull());
//    }
//
//    @Test
//    public void testIndex() {
//        organisationsAction.index();
//        Assert.assert.Assert.assertEquals(organisationsEntityList, organisationsAction.getOrganisationsEntityList());
//        verify(mockOrganisationsDao, times(1)).findAll();
//    }
//
//    @Test
//    public void testCreateNullEntity() {
//        String result = organisationsAction.create();
//        Assert.assert.Assert.assertEquals("input", result);
//        verify(mockOrganisationsDao, times(1)).findUnreferencedContacts();
//    }
//
//    @Test
//    public void testCreate() {
//        organisationsAction.setOrganisationsEntity(organisationsEntity1);
//        organisationsAction.setSelectedContacts(selectedContacts);
//
//        organisationsAction.create();
//
//        verify(mockOrganisationsDao, times(1)).save(organisationsEntity1);
//    }
//
//    @Test
//    public void testEditNullEntity() {
//        organisationsAction.setSelectedOrganisation(111);
//
//        String result = organisationsAction.edit();
//        Assert.assert.Assert.assertEquals("input", result);
//        Assert.assert.Assert.assertEquals(organisationsEntity1, organisationsAction.getOrganisationsEntity());
//        Assert.assert.Assert.assertEquals(contactsEntityList, organisationsAction.getContactsEntitiesList());
//        verify(mockOrganisationsDao, times(1)).findByIdUnsafe(111);
//        verify(mockOrganisationsDao, times(1)).findUnreferencedContacts();
//    }
//
//    @Test
//    public void testEdit() {
//        organisationsAction.setOrganisationsEntity(organisationsEntity1);
//        organisationsAction.setSelectedContacts(selectedContacts);
//
//        organisationsAction.edit();
//
//        verify(mockOrganisationsDao, times(1)).update(organisationsEntity1);
//    }
//
//    @Test
//    public void testDelete() {
//        organisationsAction.setOrganisationsEntity(organisationsEntity1);
//        organisationsAction.setSelectedContacts(selectedContacts);
//
//        organisationsAction.delete();
//
//        verify(mockOrganisationsDao, times(1)).delete(organisationsEntity1);
//    }
//
//    @Test
//    public void testDeleteNullEntity() {
//        organisationsAction.setSelectedOrganisation(111);
//
//        String result = organisationsAction.delete();
//        Assert.assert.Assert.assertEquals("input", result);
//
//        verify(mockOrganisationsDao, times(1)).findByIdUnsafe(111);
//    }
//}
