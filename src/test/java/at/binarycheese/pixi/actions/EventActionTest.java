package at.binarycheese.pixi.actions;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionProxy;
import org.apache.commons.io.FileUtils;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.StrutsSpringTestCase;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.internal.util.reflection.Whitebox;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import static org.mockito.Matchers.any;

@RunWith(PowerMockRunner.class)
@PrepareForTest(EventAction.class)
public class EventActionTest extends StrutsSpringTestCase {

    @Override
    protected String[] getContextLocations() {
        return new String[] {"test-config.xml"};
    }

    public void setAuthority() throws Exception {
        // add principal object to SecurityContextHolder
        Set<GrantedAuthority> setAuth = new HashSet<>();
        setAuth.add(new SimpleGrantedAuthority("testRole"));
        org.springframework.security.core.userdetails.User user = new org.springframework.security.core.userdetails.User("Secure", "Secure", true, true, true, true, setAuth);
            /* fill user object */

        Authentication auth = new UsernamePasswordAuthenticationToken(user, null);

        SecurityContextHolder.getContext().setAuthentication(auth);
    }

    @Test
    public void testExecute() {
        EventAction eventAction = new EventAction();
        String result = eventAction.execute();
        assertEquals(Action.SUCCESS, result);
    }

    @Test
    public void testCreateEvent() throws Exception {
        ActionProxy proxy = getActionProxy("/event/create");
        assertNotNull(proxy);
        EventAction eventAction = (EventAction) proxy.getAction();
        assertNotNull(eventAction);

        String result = proxy.execute();
        assertEquals(Action.SUCCESS, result);
    }

    @Test
    public void testProcessCreateEvent() throws Exception {
        request.setParameter("event.title", "TestEvent");
        request.setParameter("event.location", "TestLocation");
        request.setParameter("event.startDate", "11.02.2015");
        request.setParameter("starttime", "14:30");
        request.setParameter("event.endDate", "16.02.2015");
        request.setParameter("endtime", "14:00");
        request.setParameter("eventCategory", "1");
        request.setParameter("eventContactPerson", "1");
        request.setParameter("event.description", "TestDecription");
        request.setParameter("event.projectByProjectId.id", "1");
        request.setParameter("event.usersByResponsible.id", "0");

        ActionProxy proxy = getActionProxy("/event/add");
        assertNotNull(proxy);
        EventAction eventAction = (EventAction) proxy.getAction();
        assertNotNull(eventAction);
        String result = proxy.execute();

        assertNotSame(0, eventAction.getEvent().getId());
        assertEquals("TestEvent", eventAction.getEventsService().findById(eventAction.getEvent().getId()).getTitle());
        assertEquals("TestLocation", eventAction.getEventsService().findById(eventAction.getEvent().getId()).getLocation());
    }

    @Test
    public void testEditEvent() throws Exception{

        //First add new Entity
        fillRequestWithParameters();

        ActionProxy proxy = getActionProxy("/event/add");
        assertNotNull(proxy);
        EventAction eventAction = (EventAction) proxy.getAction();
        assertNotNull(eventAction);
        proxy.execute();

        //Now call the Edit Method

        request.removeAllParameters();
        request.setParameter("eventId", "" + eventAction.getEvent().getId());

        proxy = getActionProxy("/event/edit");
        assertNotNull(proxy);
        eventAction = (EventAction) proxy.getAction();
        assertNotNull(eventAction);

        String result = proxy.execute();
        assertEquals(Action.SUCCESS, result);
    }

    @Test
    public void testProcessEditEvent() throws Exception {
        //First add new Entity
        fillRequestWithParameters();


        ActionProxy proxy = getActionProxy("/event/add");
        assertNotNull(proxy);
        EventAction eventAction = (EventAction) proxy.getAction();
        assertNotNull(eventAction);
        proxy.execute();

        //Now call the Edit Method
        request.removeAllParameters();

        fillRequestWithParameters();
        request.setParameter("event.title", "EditedTestProceesEditEventtitle");
        request.setParameter("event.location", "EditedTestProceesEditEventLocation");
        request.setParameter("event.description", "EditedTestProceesEditEventDescription");

        proxy = getActionProxy("/event/update");
        assertNotNull(proxy);
        eventAction = (EventAction) proxy.getAction();
        assertNotNull(eventAction);
        proxy.execute();

        assertEquals("EditedTestProceesEditEventtitle", eventAction.getEventsService().findById(eventAction.getEvent().getId()).getTitle());
        assertEquals("EditedTestProceesEditEventLocation", eventAction.getEventsService().findById(eventAction.getEvent().getId()).getLocation());
        assertEquals("EditedTestProceesEditEventDescription", eventAction.getEventsService().findById(eventAction.getEvent().getId()).getDescription());
    }

    @Test
    public void testDeleteEvent() throws Exception {
        //First add new Entity
        fillRequestWithParameters();
        request.setParameter("event.title", "testDeleteEventTitle");

        ActionProxy proxy = getActionProxy("/event/add");
        assertNotNull(proxy);
        EventAction eventAction = (EventAction) proxy.getAction();
        assertNotNull(eventAction);
        proxy.execute();

        assertNotSame(0, eventAction.getEvent().getId());
        assertEquals("testDeleteEventTitle", eventAction.getEventsService().findById(eventAction.getEvent().getId()).getTitle());

        //Then Delete the newly created event
        request.removeAllParameters();
        request.setParameter("eventId", "" + eventAction.getEvent().getId());

        proxy = getActionProxy("/event/delete");
        assertNotNull(proxy);
        eventAction = (EventAction) proxy.getAction();
        assertNotNull(eventAction);

        String result = proxy.execute();
        assertEquals(Action.SUCCESS, result);
        assertNull(eventAction.getEventsService().findById(eventAction.getEvent().getId()));
    }

    @Test
    public void testEventMediaJsonWithEventIdIsInvalid() throws Exception {
        request.setParameter("eventId", "");

        ActionProxy proxy = getActionProxy("/event/mediajson");
        assertNotNull(proxy);
        EventAction eventAction = (EventAction) proxy.getAction();
        assertNotNull(eventAction);

        String result = proxy.execute();
        assertEquals(Action.ERROR, result);
    }

    @Test
    public void testEventMediaJson() throws Exception {
        //First add new Entity
        fillRequestWithParameters();
        request.setParameter("event.title", "testEventMediaJsonTitle");

        ActionProxy proxy = getActionProxy("/event/add");
        assertNotNull(proxy);
        EventAction eventAction = (EventAction) proxy.getAction();
        assertNotNull(eventAction);
        proxy.execute();

        assertNotSame(0, eventAction.getEvent().getId());
        assertEquals("testEventMediaJsonTitle", eventAction.getEventsService().findById(eventAction.getEvent().getId()).getTitle());

        request.removeAllParameters();


        File fileMock = PowerMockito.mock(File.class);
        File subfile = PowerMockito.mock(File.class);
        PowerMockito.when(fileMock.exists()).thenReturn(true);
        PowerMockito.when(fileMock.listFiles()).thenReturn(new File[] {subfile});
        PowerMockito.when(subfile.isFile()).thenReturn(true);
        PowerMockito.whenNew(File.class).withAnyArguments().thenReturn(fileMock);

        request.setParameter("eventId", "" + eventAction.getEvent().getId());

        proxy = getActionProxy("/event/mediajson");
        assertNotNull(proxy);
        eventAction = (EventAction) proxy.getAction();
        assertNotNull(eventAction);

        String result = proxy.execute();
        assertEquals(Action.SUCCESS, result);

    }

    @Test
    public void testEventMediaWithEventIdIsInvalid() throws Exception {
        request.setParameter("eventId", "");

        ActionProxy proxy = getActionProxy("/event/media");
        assertNotNull(proxy);
        EventAction eventAction = (EventAction) proxy.getAction();
        assertNotNull(eventAction);

        File mockFile = PowerMockito.mock(File.class);
        File subFile = PowerMockito.mock(File.class);
        PowerMockito.whenNew(File.class).withAnyArguments().thenReturn(mockFile);
        PowerMockito.when(mockFile.exists()).thenReturn(false);
        PowerMockito.when(mockFile.mkdir()).thenReturn(true);
        PowerMockito.when(mockFile.listFiles()).thenReturn(new File[] {subFile});
        PowerMockito.when(subFile.getName()).thenReturn("MockedFileName");

        String result = proxy.execute();
        assertEquals(Action.ERROR, result);
    }

    @Test
    public void testEventMedia() throws Exception {

        //First add new Entity
        fillRequestWithParameters();
        request.setParameter("event.title", "testEventMediaTitle");

        ActionProxy proxy = getActionProxy("/event/add");
        assertNotNull(proxy);
        EventAction eventAction = (EventAction) proxy.getAction();
        assertNotNull(eventAction);
        proxy.execute();

        assertNotSame(0, eventAction.getEvent().getId());
        assertEquals("testEventMediaTitle", eventAction.getEventsService().findById(eventAction.getEvent().getId()).getTitle());

        request.removeAllParameters();

        request.setParameter("eventId", "" + eventAction.getEvent().getId());

        proxy = getActionProxy("/event/media");
        assertNotNull(proxy);
        eventAction = (EventAction) proxy.getAction();
        assertNotNull(eventAction);

        File mockFile = PowerMockito.mock(File.class);
        File subFile = PowerMockito.mock(File.class);
        PowerMockito.whenNew(File.class).withAnyArguments().thenReturn(mockFile);
        PowerMockito.when(mockFile.exists()).thenReturn(false);
        PowerMockito.when(mockFile.mkdir()).thenReturn(true);
        PowerMockito.when(mockFile.listFiles()).thenReturn(new File[] {subFile});
        PowerMockito.when(subFile.getName()).thenReturn("MockedFileName");

        String result = proxy.execute();
        assertEquals(Action.SUCCESS, result);
    }

    @Test
    public void testListEvents() throws Exception{
        setAuthority();

        //First add a new Event
        fillRequestWithParameters();

        ActionProxy proxy = getActionProxy("/event/add");
        assertNotNull(proxy);
        EventAction eventAction = (EventAction) proxy.getAction();
        assertNotNull(eventAction);
        proxy.execute();

        proxy = getActionProxy("/event/index");
        assertNotNull(proxy);
        eventAction = (EventAction) proxy.getAction();
        assertNotNull(eventAction);

        String result = proxy.execute();
        assertEquals(Action.SUCCESS, result);
    }

    @Test
    public void testChangeCategorieColor() throws Exception {
        request.setParameter("eventcategorie.id", "2");
        request.setParameter("eventcategorie.color", "#ffffff");

        ActionProxy proxy = getActionProxy("/event/setCategorieColor");
        assertNotNull(proxy);
        EventAction eventAction = (EventAction) proxy.getAction();
        assertNotNull(eventAction);

        String result = proxy.execute();
        assertEquals(Action.SUCCESS, result);
        assertEquals("#ffffff" ,eventAction.getEventcategoriesService().findById(eventAction.getEventcategorie().getId()).getColor());
    }

    @Test
    public void testExport() throws Exception {
        ActionProxy proxy = getActionProxy("/event/export");
        assertNotNull(proxy);
        EventAction eventAction = (EventAction) proxy.getAction();
        assertNotNull(eventAction);

        String result = proxy.execute();
        assertEquals(Action.SUCCESS, result);
    }

    public void fillRequestWithParameters(){
        request.setParameter("event.title", "TestEvent");
        request.setParameter("event.location", "TestLocation");
        request.setParameter("event.startDate", "11.02.2015");
        request.setParameter("starttime", "14:30");
        request.setParameter("event.endDate", "16.02.2015");
        request.setParameter("endtime", "14:00");
        request.setParameter("eventCategory", "1");
        request.setParameter("eventContactPerson", "1");
        request.setParameter("event.description", "TestDecription");
        request.setParameter("event.projectByProjectId.id", "1");
        request.setParameter("event.usersByResponsible.id", "0");
    }

}

//import at.binarycheese.pixi.domain.ContactsEntity;
//import at.binarycheese.pixi.domain.EventcategoriesEntity;
//import at.binarycheese.pixi.domain.EventsEntity;
//import at.binarycheese.pixi.persistence.ContactsDao;
//import at.binarycheese.pixi.persistence.EventCategoriesDao;
//import at.binarycheese.pixi.persistence.EventDao;
//import com.google.common.base.Verify;
//import com.opensymphony.xwork2.Action;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//
//import java.sql.Timestamp;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.Date;
//
//import static org.mockito.Mockito.*;
//
///**
// * Created by Thomas on 16.11.2014.
// */
//public class EventActionTest {
//    private EventAction eventAction;
//    private EventDao mockEventDao;
//    private EventsEntity testEvent;
//    private ArrayList<EventsEntity> testList;
//    private ContactsDao mockContactsDao;
//    private ContactsEntity mockContactsEntity;
//    private EventCategoriesDao mockCategoriesDao;
//    private EventcategoriesEntity eventcategoriesEntity;
//    private ArrayList<EventcategoriesEntity> categoriesList;
//
//    @Before
//    public void setup() {
//        eventAction = new EventAction();
//        mockEventDao = mock(EventDao.class);
//        mockContactsDao = mock(ContactsDao.class);
//        mockCategoriesDao = mock(EventCategoriesDao.class);
//        mockContactsEntity = mock(ContactsEntity.class);
//        categoriesList = new ArrayList();
//        eventcategoriesEntity = mock(EventcategoriesEntity.class);
//        categoriesList.add(new EventcategoriesEntity());
//
//        testEvent = new EventsEntity();
//        testEvent.setId(111);
//        testEvent.setStartDate(new Timestamp(new Date().getTime()));
//        testList = new ArrayList<EventsEntity>();
//        testList.add(testEvent);
//
//        stub(mockEventDao.findAll()).toReturn(testList);
//        stub(mockEventDao.findById(anyInt())).toReturn(testEvent);
//        stub(mockCategoriesDao.findAll()).toReturn(categoriesList);
//        when(mockCategoriesDao.findById(anyInt())).thenReturn(eventcategoriesEntity);
//        when(mockContactsDao.findAll()).thenReturn(new ArrayList<ContactsEntity>());
//        when(mockContactsDao.findById(anyInt())).thenReturn(mockContactsEntity);
//
//
//        eventAction.dao = mockEventDao;
//        eventAction.event = testEvent;
//        eventAction.catDao = mockCategoriesDao;
//        eventAction.contactsDao = mockContactsDao;
//    }
//
//    @Test
//    public void testAddEvent() {
//        String s = eventAction.addEvent();
//
//        verify(mockEventDao, times(1)).save(testEvent);
//        Assert.assertEquals(Action.SUCCESS, s);
//    }
//
//    @Test
//    public void testEditEvent() {
//        String s = eventAction.editEvent();
//
//        Assert.assertEquals(Action.SUCCESS, s);
//
//    }
//
//    @Test
//    public void testUpdateEvent() {
//        eventAction.addEvent();
//        eventAction.event.setDescription("Changed Description");
//        eventAction.event.setLocation("ChangedLocation");
//        String s = eventAction.updateEvent();
//        Assert.assertEquals(Action.SUCCESS, s);
//    }
//
//    @Test
//    public void testCreateEvent() {
//        String s = eventAction.createEvent();
//
//        verify(mockContactsDao, times(1)).findAll();
//        verify(mockCategoriesDao, times(1)).findAll();
//        Assert.assertEquals(categoriesList, eventAction.categoriesList);
//        Assert.assertEquals(Action.SUCCESS, s);
//    }
//
//    @Test
//    public void testDeleteEvent() {
//        eventAction.setEventId(111);
//
//        eventAction.deleteEvent();
//
//        verify(mockEventDao, times(1)).delete(testEvent);
//    }
//
//    @Test
//    public void testListEvents() {
//        eventAction.listEvents();
//
//        verify(mockCategoriesDao, times(1)).findAll();
//        Assert.assertNotNull(eventAction.eventList);
//    }
//}
