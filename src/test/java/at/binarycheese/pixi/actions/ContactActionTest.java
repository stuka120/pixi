package at.binarycheese.pixi.actions;

import at.binarycheese.pixi.domain.ContactsEntity;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionProxy;
import org.apache.struts2.StrutsSpringTestCase;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;

public class ContactActionTest extends StrutsSpringTestCase {

    @Override
    public String[] getContextLocations() {

        return new String[]{"classpath:test-config.xml"};
    }

    @Test
    public void testIndex() {
        ActionProxy proxy = getActionProxy("/contacts/index");

        ContactAction contactAction = (ContactAction) proxy.getAction();
        assertNotNull(contactAction);
        String result = "";
        try {
            result = contactAction.index();
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertEquals(Action.SUCCESS, result);
    }

    @Test
    public void testCreate() throws Exception {
        ActionProxy proxy = getActionProxy("/contacts/create");
        ContactAction contactAction = (ContactAction) proxy.getAction();
        assertNotNull(contactAction);
        String result = proxy.execute();

        assertEquals(Action.INPUT, result);
    }

    @Test
    public void testProcessCreate() throws Exception {

        request.setParameter("contactsEntity.title", "Frau");
        request.setParameter("contactsEntity.academicDegreeBefore", "Drs.");
        request.setParameter("contactsEntity.academicDegreeAfter", "MSc.");
        request.setParameter("contactsEntity.name", "testProceedCreateName");
        request.setParameter("contactsEntity.surname", "testProceedCreateSurname");
        request.setParameter("selectedLanguages", "3");
        request.setParameter("selectedPhones", "06802066384");
        request.setParameter("selectedEmails", "dominik.schiener@gmx.at");
        request.setParameter("selectedEmailsPrivate", "test@gmx.at");
        request.setParameter("selectedPhonesPrivate", "phonep");
        request.setParameter("contactsEntity.hompage", "binarycheese.at");
        request.setParameter("contactsEntity.info", "info");
        request.setParameter("contactsEntity.address", "Spengergasse 20");
        request.setParameter("contactsEntity.postleitzahlByPlz.plz", "1130");
        request.setParameter("contactsEntity.postleitzahlByPlz.name", "Hitzing");
        request.setParameter("contactsEntity.state", "W");
        request.setParameter("selectedCountry", "AT");
        request.setParameter("selectedOrganisation", "4");
        request.setParameter("addName", "");
        request.setParameter("addValue", "");

        ActionProxy proxy = getActionProxy("/contacts/create");
        ContactAction contactAction = (ContactAction) proxy.getAction();
        assertNotNull(contactAction);

        String result = proxy.execute();
        assertEquals(Action.SUCCESS, result);
        assertNotSame(0, contactAction.getContactsEntity().getId());
        assertEquals("testProceedCreateName", contactAction.getContactsService().findById(contactAction.getContactsEntity().getId()).getName());
        assertEquals("testProceedCreateSurname", contactAction.getContactsService().findById(contactAction.getContactsEntity().getId()).getSurname());

    }

    @Test
    public void testDelete() throws Exception {
        request.setParameter("contactsEntity.title", "Herr");
        request.setParameter("contactsEntity.academicDegreeBefore", "Dr.");
        request.setParameter("contactsEntity.academicDegreeAfter", "MSc. BSc.");
        request.setParameter("contactsEntity.name", "TestDeleteName");
        request.setParameter("contactsEntity.surname", "TestDeleteSurname");
        request.setParameter("selectedLanguages", "1");
        request.setParameter("selectedPhones", "06802066384");
        request.setParameter("selectedEmails", "dominik.schiener@gmx.at");
        request.setParameter("contactsEntity.hompage", "binarycheese.at");
        request.setParameter("selectedEmailsPrivate", "test@gmx.at");
        request.setParameter("selectedPhonesPrivate", "phonep");
        request.setParameter("contactsEntity.info", "info");
        request.setParameter("contactsEntity.address", "Spengergasse 20");
        request.setParameter("contactsEntity.postleitzahlByPlz.plz", "1130");
        request.setParameter("contactsEntity.postleitzahlByPlz.name", "Hitzing");
        request.setParameter("contactsEntity.state", "W");
        request.setParameter("selectedCountry", "AT");
        request.setParameter("selectedOrganisation", "1");
        request.setParameter("addName", "");
        request.setParameter("addValue", "");

        ActionProxy proxy = getActionProxy("/contacts/create");
        ContactAction contactAction = (ContactAction)proxy.getAction();
        assertNotNull(contactAction);
        String result = proxy.execute();
        assertEquals(Action.SUCCESS, result);

        request.removeAllParameters();
        request.setParameter("selectedContact", "" + contactAction.getContactsEntity().getId());
        proxy = getActionProxy("/contacts/delete");
        contactAction = (ContactAction)proxy.getAction();
        assertNotNull(contactAction);
        result = proxy.execute();
        assertEquals(Action.INPUT, result);
    }

    @Test
    public void testProcessDelete() throws Exception {
        request.setParameter("contactsEntity.title", "Herr");
        request.setParameter("contactsEntity.academicDegreeBefore", "Dr.");
        request.setParameter("contactsEntity.academicDegreeAfter", "MSc. BSc.");
        request.setParameter("contactsEntity.name", "testProcessDeleteName");
        request.setParameter("contactsEntity.surname", "testProcessDeleteSurname");
        request.setParameter("selectedLanguages", "1");
        request.setParameter("selectedPhones", "06802066384");
        request.setParameter("selectedEmails", "dominik.schiener@gmx.at");
        request.setParameter("selectedEmailsPrivate", "test@gmx.at");
        request.setParameter("selectedPhonesPrivate", "phonep");
        request.setParameter("contactsEntity.hompage", "binarycheese.at");
        request.setParameter("contactsEntity.info", "info");
        request.setParameter("contactsEntity.address", "Spengergasse 20");
        request.setParameter("contactsEntity.postleitzahlByPlz.plz", "1130");
        request.setParameter("contactsEntity.postleitzahlByPlz.name", "Hitzing");
        request.setParameter("contactsEntity.state", "W");
        request.setParameter("selectedCountry", "AT");
        request.setParameter("selectedOrganisation", "1");
        request.setParameter("addName", "");
        request.setParameter("addValue", "");

        ActionProxy proxy = getActionProxy("/contacts/create");
        ContactAction contactAction = (ContactAction) proxy.getAction();
        assertNotNull(contactAction);
        String result = proxy.execute();

        request.removeAllParameters();

        request.setParameter("contactsEntity.id", "" + contactAction.getContactsEntity().getId());

        proxy = getActionProxy("/contacts/delete");
        contactAction = (ContactAction) proxy.getAction();
        assertNotNull(contactAction);
        result = proxy.execute();
        assertEquals(Action.SUCCESS, result);
        assertNull(contactAction.getContactsService().findById(contactAction.getContactsEntity().getId()));

    }

    @Test
    public void testEdit() throws Exception {
        //First create new Entity
        request.setParameter("contactsEntity.title", "Frau");
        request.setParameter("contactsEntity.academicDegreeBefore", "Drs.");
        request.setParameter("contactsEntity.academicDegreeAfter", "MSc.");
        request.setParameter("contactsEntity.name", "testEditName");
        request.setParameter("contactsEntity.surname", "testEditSurname");
        request.setParameter("selectedLanguages", "3");
        request.setParameter("selectedPhones", "06802066384");
        request.setParameter("selectedEmails", "dominik.schiener@gmx.at");
        request.setParameter("selectedEmailsPrivate", "test@gmx.at");
        request.setParameter("selectedPhonesPrivate", "phonep");
        request.setParameter("contactsEntity.hompage", "binarycheese.at");
        request.setParameter("contactsEntity.info", "info");
        request.setParameter("contactsEntity.address", "Spengergasse 20");
        request.setParameter("contactsEntity.postleitzahlByPlz.plz", "1130");
        request.setParameter("contactsEntity.postleitzahlByPlz.name", "Hitzing");
        request.setParameter("contactsEntity.state", "W");
        request.setParameter("selectedCountry", "AT");
        request.setParameter("selectedOrganisation", "4");
        request.setParameter("addName", "");
        request.setParameter("addValue", "");

        ActionProxy proxy = getActionProxy("/contacts/create");
        ContactAction contactAction = (ContactAction) proxy.getAction();
        assertNotNull(contactAction);
        proxy.execute();
        assertEquals("testEditName", contactAction.getContactsService().findById(contactAction.getContactsEntity().getId()).getName());
        assertEquals("testEditSurname", contactAction.getContactsService().findById(contactAction.getContactsEntity().getId()).getSurname());

        //The try to open the Edit Screen
        request.removeAllParameters();
        request.setParameter("selectedContact", "" + contactAction.getContactsEntity().getId());

        proxy = getActionProxy("/contacts/edit");
        contactAction = (ContactAction) proxy.getAction();
        assertNotNull(contactAction);
        String result = proxy.execute();

        assertEquals(Action.INPUT, result);
    }

    @Test
    public void testProcessEdit() throws Exception {
        //first Create new Contact
        request.setParameter("contactsEntity.title", "Frau");
        request.setParameter("contactsEntity.academicDegreeBefore", "Drs.");
        request.setParameter("contactsEntity.academicDegreeAfter", "MSc.");
        request.setParameter("contactsEntity.name", "testProcessEditName");
        request.setParameter("contactsEntity.surname", "testProcessEditSurname");
        request.setParameter("selectedLanguages", "3");
        request.setParameter("selectedPhones", "06802066384");
        request.setParameter("selectedEmails", "dominik.schiener@gmx.at");
        request.setParameter("selectedEmailsPrivate", "test@gmx.at");
        request.setParameter("selectedPhonesPrivate", "phonep");
        request.setParameter("contactsEntity.hompage", "binarycheese.at");
        request.setParameter("contactsEntity.info", "info");
        request.setParameter("contactsEntity.address", "Spengergasse 20");
        request.setParameter("contactsEntity.postleitzahlByPlz.plz", "1130");
        request.setParameter("contactsEntity.postleitzahlByPlz.name", "Hitzing");
        request.setParameter("contactsEntity.state", "W");
        request.setParameter("selectedCountry", "AT");
        request.setParameter("selectedOrganisation", "4");
        request.setParameter("addName", "");
        request.setParameter("addValue", "");

        ActionProxy proxy = getActionProxy("/contacts/create");
        ContactAction contactAction = (ContactAction) proxy.getAction();
        assertNotNull(contactAction);
        proxy.execute();
        assertNotSame(0, contactAction.getContactsEntity().getId());
        assertEquals("testProcessEditName", contactAction.getContactsService().findById(contactAction.getContactsEntity().getId()).getName());
        assertEquals("testProcessEditSurname", contactAction.getContactsService().findById(contactAction.getContactsEntity().getId()).getSurname());

        //The Edit the newly Created Contact
        request.removeAllParameters();
        request.setParameter("contactsEntity.id", "" + contactAction.getContactsEntity().getId());
        request.setParameter("contactsEntity.title", "Herr");
        request.setParameter("contactsEntity.academicDegreeBefore", "EditedDrs.");
        request.setParameter("contactsEntity.academicDegreeAfter", "EditedMSc.");
        request.setParameter("contactsEntity.name", "EditedtestProcessEditName");
        request.setParameter("contactsEntity.surname", "EditedtestProcessEditSurname");
        request.setParameter("selectedLanguages", "1");
        request.setParameter("selectedLanguages", "2");
        request.setParameter("selectedLanguages", "11");
        request.setParameter("selectedPhones", "EditedNumber");
        request.setParameter("selectedEmails", "test@gmx.at");
        request.setParameter("selectedEmailsPrivate", "test@gmx.at");
        request.setParameter("selectedPhonesPrivate", "phonep");
        request.setParameter("contactsEntity.hompage", "EditedMail");
        request.setParameter("contactsEntity.info", "EditedInfo");
        request.setParameter("contactsEntity.address", "EditedAddress");
        request.setParameter("contactsEntity.postleitzahlByPlz.plz", "1130");
        request.setParameter("contactsEntity.postleitzahlByPlz.name", "Hitzing");
        request.setParameter("contactsEntity.state", "St");
        request.setParameter("selectedCountry", "HU");
        request.setParameter("selectedOrganisation", "2");
        request.setParameter("addName", "");
        request.setParameter("addValue", "");

        proxy = getActionProxy("/contacts/edit");
        contactAction = (ContactAction) proxy.getAction();
        assertNotNull(contactAction);
        String result = proxy.execute();
        assertEquals(Action.SUCCESS, result);
        assertEquals("EditedtestProcessEditName", contactAction.getContactsService().findById(contactAction.getContactsEntity().getId()).getName());
        assertEquals("EditedtestProcessEditSurname", contactAction.getContactsService().findById(contactAction.getContactsEntity().getId()).getSurname());
    }

    @Test
    public void testDetails() throws Exception {

        //Create Contact
        request.setParameter("contactsEntity.title", "Frau");
        request.setParameter("contactsEntity.academicDegreeBefore", "Drs.");
        request.setParameter("contactsEntity.academicDegreeAfter", "MSc.");
        request.setParameter("contactsEntity.name", "testDetailsName");
        request.setParameter("contactsEntity.surname", "testDetailsSurname");
        request.setParameter("selectedLanguages", "3");
        request.setParameter("selectedPhones", "06802066384");
        request.setParameter("selectedEmails", "dominik.schiener@gmx.at");
        request.setParameter("selectedEmailsPrivate", "test@gmx.at");
        request.setParameter("selectedPhonesPrivate", "phonep");
        request.setParameter("contactsEntity.hompage", "binarycheese.at");
        request.setParameter("contactsEntity.info", "info");
        request.setParameter("contactsEntity.address", "Spengergasse 20");
        request.setParameter("contactsEntity.postleitzahlByPlz.plz", "1130");
        request.setParameter("contactsEntity.postleitzahlByPlz.name", "Hitzing");
        request.setParameter("contactsEntity.state", "W");
        request.setParameter("selectedCountry", "AT");
        request.setParameter("selectedOrganisation", "4");
        request.setParameter("addName", "");
        request.setParameter("addValue", "");

        ActionProxy proxy = getActionProxy("/contacts/create");
        ContactAction contactAction = (ContactAction) proxy.getAction();
        assertNotNull(contactAction);
        proxy.execute();

        //Show Details Page
        request.setParameter("selectedContact", "" + contactAction.getContactsEntity().getId());

        proxy = getActionProxy("/contacts/details");
        assertNotNull(proxy);
        contactAction = (ContactAction) proxy.getAction();
        assertNotNull(contactAction);

        String result = proxy.execute();
        assertEquals(Action.SUCCESS, result);
    }

    @Test
    public void testLiveSearch() throws Exception {

        ActionProxy proxy = getActionProxy("/contacts/search");
        assertNotNull(proxy);
        ContactJsonAction contactJsonAction = (ContactJsonAction) proxy.getAction();
        assertNotNull(contactJsonAction);

        ContactsEntity contactsEntity = new ContactsEntity();
        contactsEntity.setName("Dominik");
        contactsEntity.setSurname("Schiener");
        contactsEntity.setInfo("Info");

        contactJsonAction.getContactsService().saveContact(contactsEntity);

        contactJsonAction.setSearchString("Dom");
        List<HashMap<String, String>> erg = contactJsonAction.getContacts();
        assertEquals("Dominik", erg.get(0).get("name"));
        assertEquals("Schiener", erg.get(0).get("surname"));

    }
}
