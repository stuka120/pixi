package at.binarycheese.pixi.actions.components;

import at.binarycheese.pixi.actions.ContactAction;
import at.binarycheese.pixi.domain.ContactsEntity;
import at.binarycheese.pixi.domain.CountriesEntity;
import at.binarycheese.pixi.service.ContactsService;
import at.binarycheese.pixi.service.CountriesService;
import at.binarycheese.pixi.service.LanguagesService;
import at.binarycheese.pixi.service.OrganisationsService;
import at.binarycheese.pixi.service.impl.ContactsServiceImpl;
import at.binarycheese.pixi.service.interfaces.ContactActionInterface;
import com.opensymphony.xwork2.Action;
import org.apache.struts2.StrutsSpringTestCase;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.internal.util.reflection.Whitebox;

import java.util.ArrayList;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.powermock.api.mockito.PowerMockito.doThrow;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 * Created by Lukas on 19.02.2015.
 */
public class ContactComponentTest extends StrutsSpringTestCase {

    ContactAction contactAction;
    ContactsEntity contactsEntity;
    ContactsEntity mockContactsEntity;
    ContactsService mockContactsService;
    CountriesService mockCountriesService;
    OrganisationsService mockOrganisationsService;
    LanguagesService mockLanguagesService;

    @Override
    protected String[] getContextLocations() {
        return new String[] {"test-config.xml"};
    }

    @Override
    public void setUp(){
        contactAction = new ContactAction();
        contactsEntity = new ContactsEntity();
        contactsEntity.setName("TestName");

        mockContactsEntity = mock(ContactsEntity.class);
        mockContactsService = mock(ContactsServiceImpl.class);
        contactAction.setContactsService(mockContactsService);
        mockCountriesService = mock(CountriesService.class);
        contactAction.setCountriesService(mockCountriesService);
        mockOrganisationsService = mock(OrganisationsService.class);
        contactAction.setOrganisationsService(mockOrganisationsService);
        mockLanguagesService = mock(LanguagesService.class);
        contactAction.setLanguagesService(mockLanguagesService);

    }

    @Test
    public void testIndex() throws Exception {
        Assert.assertEquals(Action.SUCCESS, contactAction.index());
    }

    @Test
    public void testDetails() throws Exception {
        when(mockContactsService.findById(anyInt())).thenReturn(contactsEntity);
        Assert.assertEquals(Action.SUCCESS, contactAction.details());
    }

    @Test
    public void testDetailsDoesNotFind() throws Exception {
        when(mockContactsService.findById(anyInt())).thenReturn(null);
        Assert.assertEquals(Action.ERROR, contactAction.details());
    }

    @Test
    public void testCreateContactisNull(){
        Assert.assertEquals(Action.INPUT, contactAction.create());
    }

    @Test
    public void testCreateWithContact(){
        contactAction.setContactsEntity(contactsEntity);
        when(mockCountriesService.findAll()).thenReturn(mock((ArrayList.class)));
        Assert.assertEquals(Action.SUCCESS, contactAction.create());
    }

    @Test
    public void testDeleteWithNull(){
        Assert.assertEquals(Action.INPUT, contactAction.delete());
    }

    @Test
    public void testEdit(){
        contactAction.setContactsEntity(mockContactsEntity);
        Assert.assertEquals(Action.SUCCESS, contactAction.edit());
    }

    @Test
    public void testEditNull(){
        Assert.assertEquals(Action.INPUT, contactAction.edit());
    }

    @Test(expected = NullPointerException.class)
    public void testEditException(){
        contactAction.setContactsEntity(mockContactsEntity);
        doThrow(new NullPointerException()).when(mockContactsService).update(any(ContactActionInterface.class));
        Assert.assertEquals(Action.ERROR, contactAction.edit());
    }
}
