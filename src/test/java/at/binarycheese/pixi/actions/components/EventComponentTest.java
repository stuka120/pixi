package at.binarycheese.pixi.actions.components;

import at.binarycheese.pixi.actions.EventAction;
import at.binarycheese.pixi.domain.EventsEntity;
import at.binarycheese.pixi.service.ContactsService;
import at.binarycheese.pixi.service.EventcategoriesService;
import at.binarycheese.pixi.service.EventsService;
import at.binarycheese.pixi.service.ProjectsService;
import com.opensymphony.xwork2.Action;
import org.apache.commons.lang.ObjectUtils;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.StrutsSpringTestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import javax.servlet.ServletContext;
import java.io.File;
import java.sql.Timestamp;
import java.util.ArrayList;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.whenNew;

/**
 * Created by Lukas on 20.02.2015.
 */

@RunWith(PowerMockRunner.class)
@PrepareForTest({EventAction.class, ServletActionContext.class, File.class})
public class EventComponentTest extends StrutsSpringTestCase {
    EventAction eventAction;
    ContactsService mockContactsService;
    EventcategoriesService mockEventcategoriesService;
    EventsService mockEventsService;
    ProjectsService mockProjectService;

    @Override
    public void setUp(){
        eventAction = new EventAction();
        mockContactsService = mock(ContactsService.class);
        mockEventcategoriesService = mock(EventcategoriesService.class);
        mockEventsService = mock(EventsService.class);
        mockProjectService = mock(ProjectsService.class);

        eventAction.setContactsService(mockContactsService);
        eventAction.setEventcategoriesService(mockEventcategoriesService);
        eventAction.setEventsService(mockEventsService);
        eventAction.setProjectsService(mockProjectService);
    }

    public void testFileUrlToFileName() throws Exception {
        eventAction.setFileUrl("localhost:807\\files\\122\\test.txt");
        assertEquals("test.txt", eventAction.getFileName());
    }

    @Test
    public void testExecute(){
        assertEquals(Action.SUCCESS, eventAction.execute());
    }

    @Test
    public void testCreate(){
        when(mockEventcategoriesService.findAll()).thenReturn(mock(ArrayList.class));
        when(mockContactsService.findAll()).thenReturn(mock(ArrayList.class));
        when(mockProjectService.findAllActive()).thenReturn(mock(ArrayList.class));
        assertEquals(Action.SUCCESS, eventAction.createEvent());
    }

    @Test
    public void testEditNull(){
        when(mockEventsService.findById(anyInt())).thenReturn(null);
        when(mockEventcategoriesService.findAll()).thenReturn(mock(ArrayList.class));
        when(mockContactsService.findAll()).thenReturn(mock(ArrayList.class));
        when(mockProjectService.findAllActive()).thenReturn(mock(ArrayList.class));
        assertEquals(Action.ERROR, eventAction.editEvent());
    }

    @Test
    public void testEdit(){
        when(mockEventsService.findById(anyInt())).thenReturn(mock(EventsEntity.class));
        when(mockEventcategoriesService.findAll()).thenReturn(mock(ArrayList.class));
        when(mockContactsService.findAll()).thenReturn(mock(ArrayList.class));
        when(mockProjectService.findAllActive()).thenReturn(mock(ArrayList.class));
        assertEquals(Action.SUCCESS, eventAction.editEvent());
    }

    @Test
    public void testDates(){
        Timestamp startdate = new Timestamp(115,2,26,22,0,0,0);
        Timestamp enddate = new Timestamp(115,2,26,23,59,0,0);
        eventAction.setStartDate("26.03.2015,22:00");
        assertEquals(startdate, eventAction.getEvent().getStartDate());
        assertEquals(enddate, eventAction.getEvent().getEndDate());
        enddate = new Timestamp(115,2,27,22,0,0,0);
        eventAction.setEndDate("27.03.2015,22:00");
        assertEquals(enddate, eventAction.getEvent().getEndDate());
    }

    @Test
    public void testMediaJSONEventIsNull(){
        when(mockEventsService.findById(anyInt())).thenReturn(null);
        assertEquals(Action.ERROR, eventAction.eventMediaJson());
    }

    @Test
    public void testMediaJSONFolderDoesntExist(){
        when(mockEventsService.findById(anyInt())).thenReturn(mock(EventsEntity.class));
        File mockFile = mock(File.class);
        mockStatic(ServletActionContext.class);
        ServletContext mockServletContext = mock(ServletContext.class);
        when(ServletActionContext.getServletContext()).thenReturn(mockServletContext);
        when(mockServletContext.getRealPath(anyString())).thenReturn("");
        try {
            PowerMockito.whenNew(File.class).withAnyArguments().thenThrow(NullPointerException.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        when(mockFile.exists()).thenReturn(false);
        assertEquals(Action.ERROR, eventAction.eventMediaJson());
    }
    @Test
    public void testMediaJSON(){
        when(mockEventsService.findById(anyInt())).thenReturn(mock(EventsEntity.class));
        File mockFile = mock(File.class);
        mockStatic(ServletActionContext.class);
        ServletContext mockServletContext = mock(ServletContext.class);
        when(ServletActionContext.getServletContext()).thenReturn(mockServletContext);
        when(mockServletContext.getRealPath(anyString())).thenReturn("");
        try {
            PowerMockito.whenNew(File.class).withAnyArguments().thenReturn(mockFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
        when(mockFile.exists()).thenReturn(true);
        when(mockFile.listFiles()).thenReturn(new File[]{});
        assertEquals(Action.SUCCESS, eventAction.eventMediaJson());
    }

}
