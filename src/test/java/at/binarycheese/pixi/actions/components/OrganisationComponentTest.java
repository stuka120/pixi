package at.binarycheese.pixi.actions.components;

import at.binarycheese.pixi.actions.EventAction;
import at.binarycheese.pixi.actions.OrganisationsAction;
import at.binarycheese.pixi.domain.OrganisationsEntity;
import at.binarycheese.pixi.service.ContactsService;
import at.binarycheese.pixi.service.OrganisationsService;
import com.opensymphony.xwork2.Action;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.StrutsSpringTestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.Response;
import java.io.File;
import java.io.PrintWriter;
import java.util.List;

import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 * Created by Lukas on 26.02.2015.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ServletActionContext.class, PrintWriter.class, HttpServletResponse.class})
public class OrganisationComponentTest extends StrutsSpringTestCase {
    OrganisationsAction organisationsAction;
    ContactsService mockContactsService;
    OrganisationsService mockOrganisationsService;


    @Override
    public void setUp() throws Exception {
        organisationsAction = new OrganisationsAction();
        mockContactsService = mock(ContactsService.class);
        organisationsAction.setContactsService(mockContactsService);
        mockOrganisationsService = mock(OrganisationsService.class);
        organisationsAction.setOrganisationsService(mockOrganisationsService);
        mockStatic(ServletActionContext.class);
        HttpServletResponse httpServletResponseMock = mock(HttpServletResponse.class);
        when(httpServletResponseMock.getWriter()).thenReturn(mock(PrintWriter.class));
        when(ServletActionContext.getResponse()).thenReturn(httpServletResponseMock);
    }

    @Test
    public void testIndex() throws Exception {
        assertEquals(Action.SUCCESS, organisationsAction.index());
    }

    @Test
    public void testCreateOrganisationEntitityIsNull() throws Exception {
        when(mockOrganisationsService.findUnreferencedContacts()).thenReturn(mock(List.class));
        assertEquals(Action.INPUT, organisationsAction.create());
    }

    @Test
    public void testCreate() throws Exception {
        organisationsAction.setOrganisationsEntity(mock(OrganisationsEntity.class));
        assertEquals(Action.NONE, organisationsAction.create());
    }

    @Test(expected = NullPointerException.class)
    public void testEditWithEntityNull() throws Exception {
        assertEquals(Action.INPUT, organisationsAction.edit());

    }

    @Test
    public void testEdit() throws Exception {
        organisationsAction.setOrganisationsEntity(mock(OrganisationsEntity.class));
        assertEquals(Action.NONE, organisationsAction.edit());
    }

    @Test(expected = NullPointerException.class)
    public void testDeleteWithEntityIsNull() throws Exception{
        assertEquals(Action.NONE, organisationsAction.delete());
    }

    @Test
    public void testDelete() throws Exception {
        organisationsAction.setOrganisationsEntity(mock(OrganisationsEntity.class));
        assertEquals(Action.NONE, organisationsAction.delete());
    }
}
