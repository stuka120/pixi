package at.binarycheese.pixi.actions.components;

import at.binarycheese.pixi.actions.EventAction;
import org.apache.struts2.StrutsSpringTestCase;

import java.sql.Timestamp;

/**
 * Created by Lukas on 07.03.2015.
 */
public class EventActionComponentTest extends StrutsSpringTestCase {
    EventAction eventAction;
    @Override
    public void setUp() throws Exception {
        eventAction = new EventAction();
    }

    public void testTimeAndDateStringToTimestamp() throws Exception {
        String test1 = "01.03.2015,22:00";
        String test2 = "01.03.2015, 22:00";
        String test3 = "22:00,01.03.2015";
        String test4 = "01.03.2015";
        Timestamp timestamp = new Timestamp(2015-1900,2,1,22,0,0,0);
        Timestamp timestamp1 = new Timestamp(2015-1900,2,1,0,0,0,0);
        assertEquals(timestamp, eventAction.timeAndDateStringToTimestamp(test1));
        assertEquals(timestamp, eventAction.timeAndDateStringToTimestamp(test2));
        assertEquals(timestamp, eventAction.timeAndDateStringToTimestamp(test3));
        assertEquals(timestamp1, eventAction.timeAndDateStringToTimestamp(test4));
    }
}
