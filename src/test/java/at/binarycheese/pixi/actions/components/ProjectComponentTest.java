package at.binarycheese.pixi.actions.components;

import at.binarycheese.pixi.actions.ProjectAction;
import at.binarycheese.pixi.domain.ProjectEntity;
import at.binarycheese.pixi.domain.ProjectstagesEntity;
import at.binarycheese.pixi.service.OrganisationsService;
import at.binarycheese.pixi.service.ProjectStagesService;
import at.binarycheese.pixi.service.ProjectsService;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.TextProvider;
import org.apache.struts2.StrutsSpringTestCase;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.List;
import java.util.Map;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.powermock.api.mockito.PowerMockito.*;
import static org.powermock.api.support.membermodification.MemberMatcher.method;

/**
 * Created by Lukas on 01.03.2015.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(TextProvider.class)
public class ProjectComponentTest extends StrutsSpringTestCase{
    ProjectAction projectAction;
    ProjectsService mockProjectsService;
    OrganisationsService mockOrganisationsService;
    ProjectStagesService mockProjectStagesService;
    @Override
    public void setUp() throws Exception {
        projectAction = spy(new ProjectAction());
        mockProjectsService = mock(ProjectsService.class);
        projectAction.setProjectsService(mockProjectsService);
        mockOrganisationsService = mock(OrganisationsService.class);
        projectAction.setOrganisationsService(mockOrganisationsService);
        mockProjectStagesService = mock(ProjectStagesService.class);
        projectAction.setProjectStagesService(mockProjectStagesService);


    }

    public void testBrowse() throws Exception {
        assertEquals(Action.SUCCESS, projectAction.browse());
        assertNotNull(projectAction.getProjectEntityList());
    }

    public void testValidateWithErrors(){
        projectAction.setProjectEntity(new ProjectEntity());
        doReturn("test").when(projectAction).getText(anyString());
        projectAction.validate();
        assertNotNull(projectAction.getOrganisationsEntityList());
        Map errors = projectAction.getFieldErrors();
        assertTrue(errors.containsKey("nameErr"));
        assertTrue(errors.containsKey("projNrErr"));
    }

    public void testValidateNoErrors() throws Exception {
        ProjectEntity projectEntity = new ProjectEntity();
        projectEntity.setId(5);
        projectEntity.setName("test");
        projectEntity.setProjectnumber("lel");
        projectAction.setProjectEntity(projectEntity);
        projectAction.validate();
        assertNotNull(projectAction.getOrganisationsEntityList());
        Map errors = projectAction.getFieldErrors();
        assertEquals(0, errors.size());
    }

    public void testInput() throws Exception {
        ProjectEntity projectEntity = spy(new ProjectEntity());
        projectEntity.setId(5);
        projectEntity.setName("test");
        projectEntity.setProjectnumber("lel");
        doReturn(mock(ProjectstagesEntity.class)).when(projectEntity).getProjectstagesByProjectStage();
        doReturn(projectEntity).when(mockProjectsService).findById(anyInt());
        projectAction.setId(22);
        assertEquals(Action.INPUT, projectAction.input());
        assertNotNull(projectAction.getOrganisationsEntityList());


    }
}
